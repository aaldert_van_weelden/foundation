package nl.telecats.web.gwt.player.flowplayer.event;

public class FlowPlayerEvent {

    public enum Action {
        PLAY, PAUSE, STOP, CUEPOINT, LOADED, FINISH, RESUME
    };

    private Action action;
    private Object payload;

    public FlowPlayerEvent(Action action) {
        this.action = action;
    }

    public void setPayload(Object payload) {
        this.payload = payload;
    }

    public Object getPayload() {
        return this.payload;
    }

    public void handleEvent(FlowPlayerListener fpl) {
        if (this.action == Action.PLAY) {
            fpl.onPlay(this);
        } else if (this.action == Action.PAUSE) {
            fpl.onPause(this);
        } else if (this.action == Action.STOP) {
            fpl.onStop(this);
        } else if (this.action == Action.CUEPOINT) {
            fpl.onCuepoint(this);
        } else if (this.action == Action.LOADED) {
            fpl.onLoad(this);
        } else if (this.action == Action.FINISH) {
            fpl.onFinish(this);
        } else if (this.action == Action.RESUME) {
            fpl.onResume(this);
        }

    }
}
