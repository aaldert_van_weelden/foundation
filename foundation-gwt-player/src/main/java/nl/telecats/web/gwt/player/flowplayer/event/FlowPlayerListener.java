package nl.telecats.web.gwt.player.flowplayer.event;

public interface FlowPlayerListener {
    public void onPlay(FlowPlayerEvent fpe);

    public void onStop(FlowPlayerEvent fpe);

    public void onPause(FlowPlayerEvent fpe);

    public void onCuepoint(FlowPlayerEvent fpe);

    public void onLoad(FlowPlayerEvent fpe);

    public void onFinish(FlowPlayerEvent fpe);

    public void onResume(FlowPlayerEvent fpe);
}
