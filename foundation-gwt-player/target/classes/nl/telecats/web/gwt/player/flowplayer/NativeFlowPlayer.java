package nl.telecats.web.gwt.player.flowplayer;

import nl.telecats.web.gwt.player.flowplayer.event.FlowPlayerEvent;

public class NativeFlowPlayer {

    private final static String CUEPOINT = "CUEPOINT_EVENT";
    private final static String FINISH = "FINISH_EVENT";
    private final static String PLAY = "PLAY_EVENT";
    private final static String PAUSE = "PAUSE_EVENT";
    private final static String STOP = "STOP_EVENT";
    private final static String RESUME = "RESUME_EVENT";

    private FlowPlayer fp;
    private String fpid;

    public NativeFlowPlayer(FlowPlayer fp) {
        this.fp = fp;
        this.fpid = fp.getFlowPlayerId();

        initRtmpPlayer();

        // register onPause,onStop,onFinish,onStart handlers in common clip, use
        // only once
        this.registerClipEvents();

        registerCallbackFunction();
    }

    public void addCuePoint(int posMs, String content) {
        String js;
        js = "$f(\"" + this.fpid + "\").onCuepoint( [ 2, {time: " + String.valueOf(posMs);
        js += " , title: '" + content + "'}], ";
        js += "function(clip, cuepoint) { if(cuepoint.title){ callback('";
        js += CUEPOINT + "',cuepoint.title);} });";
        this.executeScript(js);
    }

    private void registerClipEvents() {
        String js;
        js = "$f(\"" + fpid + "\").getCommonClip().onFinish( ";
        js += "function() { callback('";
        js += FINISH + "',''); });";
        js += "$f(\"" + fpid + "\").getCommonClip().onStart( ";
        js += "function() { callback('";
        js += PLAY + "',''); });";
        js += "$f(\"" + fpid + "\").getCommonClip().onStop( ";
        js += "function() { callback('";
        js += STOP + "',''); });";
        js += "$f(\"" + fpid + "\").getCommonClip().onPause( ";
        js += "function() { callback('";
        js += PAUSE + "',''); });";
        js += "$f(\"" + fpid + "\").getCommonClip().onResume( ";
        js += "function() { callback('";
        js += RESUME + "',''); });";
        this.executeScript(js);
    }

    private native void executeScript(String jsfunc) /*-{
                                                     $wnd.callFunction( jsfunc );
                                                     }-*/;

    private native void registerCallbackFunction() /*-{
                                                   var _this = this;
                                                   $wnd.callback = function(type,content) {
                                                   _this.@nl.telecats.web.gwt.player.flowplayer.NativeFlowPlayer::callback(Ljava/lang/String;Ljava/lang/String;)(type,content);
                                                   };
                                                   }-*/;

    public void callback(String type, String content) {
        if (type.matches(CUEPOINT)) {
            // content = "Callback: " + type + " Content: [" + content + "]";
            FlowPlayerEvent fpe = new FlowPlayerEvent(FlowPlayerEvent.Action.CUEPOINT);
            fpe.setPayload(content);
            fp.fireEvent(fpe);
        } else if (type.matches(FINISH)) {
            FlowPlayerEvent fpe = new FlowPlayerEvent(FlowPlayerEvent.Action.FINISH);
            fp.fireEvent(fpe);

        } else if (type.matches(PLAY)) {
            FlowPlayerEvent fpe = new FlowPlayerEvent(FlowPlayerEvent.Action.PLAY);
            fp.fireEvent(fpe);
        } else if (type.matches(PAUSE)) {
            FlowPlayerEvent fpe = new FlowPlayerEvent(FlowPlayerEvent.Action.PAUSE);
            fp.fireEvent(fpe);
        } else if (type.matches(STOP)) {
            FlowPlayerEvent fpe = new FlowPlayerEvent(FlowPlayerEvent.Action.STOP);
            fp.fireEvent(fpe);
        } else if (type.matches(RESUME)) {
            FlowPlayerEvent fpe = new FlowPlayerEvent(FlowPlayerEvent.Action.RESUME);
            fp.fireEvent(fpe);
        }

    }

    private native void alertMessage(String s) /*-{
                                               $wnd.alert(s);
                                               }-*/;

    public void stop() {
        this.stop(fpid);
    }

    native private void stop(String fpid) /*-{
                                          $wnd.$f(fpid).stop();
                                          }-*/;

    native private void play(String fpid) /*-{
                                          $wnd.$f(fpid).play();
                                          }-*/;

    public void play() {
        this.play(this.fpid);
    }

    native private void seek(String fpid, int seconds) /*-{
                                                       $wnd.$f(fpid).seek(seconds);
                                                       }-*/;

    public void seek(int seconds) {
        this.seek(this.fpid, seconds);
    }

    native private double getPosition(String fpid) /*-{
                                                   var x = $wnd.$f(fpid).getTime();
                                                   if(isNaN(x))
                                                   return 0.0;
                                                   return x;
                                                   }-*/;

    public double getPosition() {
        return this.getPosition(this.fpid);
    }

    native private boolean toggle(String fpid) /*-{
                                               return $wnd.$f(fpid).toggle();
                                               }-*/;

    native private void unloadPlayer(String fpid) /*-{
                                                  $wnd.$f(fpid).unload();
                                                  }-*/;

    public void unload() {
        this.unloadPlayer(this.fpid);
    }

    public void load() {
        this.loadPlayer(fpid);
    }

    public boolean isLoaded() {
        return this.isLoaded(fpid);
    }

    private native void loadPlayer(String fpid)/*-{
                                               $wnd.$f(fpid).load();
                                               }-*/;

    public boolean toggle() {
        return this.toggle(this.fpid);
    }

    native private double getDuration(String fpid) /*-{
                                                   var dur;
                                                   var clip = $wnd.$f(fpid).getClip();
                                                   dur = clip.duration;
                                                   if(isNaN(dur))
                                                   return 0.0;
                                                   return dur;
                                                   }-*/;

    native public int countPlayers()/*-{
                                    // loop through all players on the page
                                    var i = 0;
                                    $wnd.$f("*").each(
                                    function(){
                                    i++;
                                    }
                                    );
                                    return i;
                                    }-*/;

    public double getDuration() {
        return this.getDuration(this.fpid);
    }

    private native boolean isLoaded(String fpid)/*-{
                                                return $wnd.$f(fpid).isLoaded();
                                                }-*/;

    private void initRtmpPlayer() {
        String cnf = "clip: { provider: 'rtmpStream', autoPlay: false }, ";
        cnf += "plugins: { rtmpStream: { url: '" + fp.getRtmpSwf() + "' }, ";
        if (fp.getToken() != null)
            cnf += "secure: { url: '" + fp.getSecureSwf() + "', token: '" + fp.getToken() + "' },";
        cnf += "controls: { fullscreen: false, play: false, height: 24 }" + " }";
        this.initPlayer(cnf);
    };

    public void setClip(String clipUrl, String netConUrl) {
        if (fp.getToken() == null)
            this.setClip(fpid, clipUrl, netConUrl);
        else
            this.setClipSecure(fpid, clipUrl, netConUrl);
    }

    public void addPlugin(String name, String url) {
        this.addPlugin(fpid, name, url);
    }

    private native void addPlugin(String fpid, String name, String url)/*-{
                                                                       $wnd.$f(fpid).loadPlugin(name, url);
                                                                       }-*/;

    private native void setClip(String fpid, String clipUrl, String netConUrl)/*-{
                                                                              var p = $wnd.$f(fpid);
                                                                              var c = {
                                                                                  url: clipUrl,
                                                                                  netConnectionUrl: netConUrl,
                                                                                  provider: 'rtmpStream'
                                                                              };
                                                                              if (p.getState() > 1) { 
                                                                                  p.stop(); 
                                                                              } 
                                                                              p.getClip(0).update(c);
                                                                              }-*/;

    public void setToken(String token) {
        setToken(fpid, token);
    }

    private native void setToken(String fpid, String token)/*-{
                                                            $wnd.alert('updating token');
                                                           var player = $wnd.$f(fpid);
                                                           var plugin = player.getPlugin("secure");
                                                                            
                                                           
                                                           plugin.config.token = token;
                                                           
                                                           }-*/;

    private native void setClipSecure(String fpid, String clipUrl, String netConUrl)/*-{
                                                                                    var p = $wnd.$f(fpid);
                                                                                    var c = {
                                                                                        url: clipUrl,
                                                                                        netConnectionUrl: netConUrl,
                                                                                        provider: 'rtmpStream',
                                                                                        connectionProvider: 'secure'
                                                                                    };
                                                                                    if (p.getState() > 1) { 
                                                                                        p.stop(); 
                                                                                    } 
                                                                                    p.getClip(0).update(c);
                                                                                    }-*/;

    public String getClipInfo() {
        return this.getClipInfo(fpid);
    }

    private native String getClipInfo(String fpid)/*-{
                                                  try{
                                                  var c = $wnd.$f(fpid).getClip();
                                                  return 'URL='+c.url+' netConnectionUrl='+c.netConnectionUrl+' provider='+c.provider;
                                                  }
                                                  catch(e){
                                                  return "error";
                                                  }
                                                  }-*/;

    private void initPlayer(String configuration) {
        String js = "var randomizer =  \" \";";
        js = js + " randomizer = \"?rdm=\" + Math.random() * 99999;";
        if (configuration.length() > 0) {
            js = js + "$f(\"" + fpid + "\",\"" + fp.getPlayerSwf() + "\"+ randomizer" + " , { " + configuration
                    + " });";
        } else {
            js = "$f(\"" + fpid + "\", \"" + fp.getPlayerSwf() + "\")";
        }
        this.executeScript(js);
    };

}
