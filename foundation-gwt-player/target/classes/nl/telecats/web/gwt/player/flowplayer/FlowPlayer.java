package nl.telecats.web.gwt.player.flowplayer;

import java.util.ArrayList;

import nl.telecats.web.gwt.player.flowplayer.event.FlowPlayerEvent;
import nl.telecats.web.gwt.player.flowplayer.event.FlowPlayerListener;

import com.extjs.gxt.ui.client.core.El;
import com.extjs.gxt.ui.client.widget.BoxComponent;
import com.extjs.gxt.ui.client.widget.MessageBox;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Timer;

/**
 * @author martijnvg
 * @see http://flowplayer.org/documentation/api/player.html
 */
public final class FlowPlayer extends BoxComponent {

    // default height of the Flash component for FlowPlayer
    // shows only the control-bar used for audio content
    private final int DEFAULT_HEIGHT = 24;

    // default name/location for the flash files
    private String playerSwf = "flowplayer-3.1.5.swf";
    private String rtmpSwf = "flowplayer.rtmp-3.1.3.swf";
    private String secureSwf = "flowplayer.securestreaming-3.1.1.swf";
    // default location of RTMP server
    private String netConnectionUrl = "rtmp://nohost";
    // default location of first clip
    private String defaultClip = "mp3:noclip";

    private String token = null;

    // array of event listeners
    private ArrayList<FlowPlayerListener> listeners;

    // native flowplayer
    private NativeFlowPlayer nfp;

    // id of the flowplayer
    private String flowPlayerId;

    private boolean ready = false;

    // container element of the flowplayer
    private El container;

    public FlowPlayer() {
        super();
        super.setHeight(DEFAULT_HEIGHT);
        this.listeners = new ArrayList<FlowPlayerListener>();
        this.ready = true;
    }

    public FlowPlayer(boolean autoStart) {
        this();
        this.ready = autoStart;
    }

    public void configurationFinished() {
        ready = true;
    }

    public void finalize() {
        this.unload();
    }

    public String getFlowPlayerId() {
        return this.flowPlayerId;
    }

    @Override
    public void onRender(com.google.gwt.user.client.Element target, int index) {
        super.onRender(target, index);

        com.google.gwt.user.client.Element fpDiv = DOM.createDiv();
        setElement(fpDiv, target, index);

        container = el();
        // container.setId("bla");
        flowPlayerId = container.getId();
        // System.out.println(flowPlayerId);
        container.setElementAttribute("style", "width:100%;height:100%");

        // container moet 'html content' hebben anders werkt het load/unload
        // mechanisme niet
        StringBuffer divContent = new StringBuffer();
        divContent.append("GwtFlowPlayer");
        container.setInnerHtml(divContent.toString());

    }

    public void methodCountPlayers() {
        MessageBox.alert("Debug", "Number of active FlowPlayer instances: " + String.valueOf(this.nfp.countPlayers())
                + "", null);
    }

    public void showClipInfo() {
        MessageBox.alert("Debug", "Clip Info: " + String.valueOf(this.nfp.getClipInfo()), null);
    }

    @Override
    protected void onAttach() {
        super.onAttach();
        // MessageBox.alert("Debug", "On attach called on FlowPlayer\\n", null);
        this.load();
    }

    @Override
    protected void onDetach() {
        super.onDetach();
        // MessageBox.alert("Debug", "On detach called on FlowPlayer\\n", null);
        this.unload();
    }

    @Override
    protected void onResize(int width, int height) {
        super.onResize(width, height);
        container.setSize(width, height);
    }

    private synchronized void load() {

        if (ready) {
            // create new instance
            nfp = new NativeFlowPlayer(this);

            // actually loads the player
            nfp.load();

            // default settings toepassen nadat het flash object volledig is geladen
            Timer t = new Timer() {
                public void run() {
                    if (nfp.isLoaded()) {
                        this.cancel();
                        nfp.setClip(defaultClip, netConnectionUrl);
                        // fire loaded event
                        fireEvent(new FlowPlayerEvent(FlowPlayerEvent.Action.LOADED));
                    }
                }
            };
            t.scheduleRepeating(100);
        }
    }

    public void addFlowPlayerListener(FlowPlayerListener fpl) {
        this.listeners.add(fpl);
    }

    public void fireEvent(FlowPlayerEvent fpe) {
        for (FlowPlayerListener fpl : listeners) {
            fpe.handleEvent(fpl);
        }
    }

    public void stop() {
        nfp.stop();
    }

    public void play() {
        nfp.play();
    }

    private synchronized void unload() {
        nfp.unload();
    }

    public long getPositionMs() {
        double pos = nfp.getPosition();
        return Math.round(1000 * pos);
    }

    public void relativeSeek(int seconds) {
        double pos = getPosition();
        int seek = (int) Math.round(pos) + seconds;
        nfp.seek(seek);
    }

    public double getPosition() {
        return nfp.getPosition();
    }

    /**
     * Toggles the state between play and pause. Returns the new state of the player, with true as playing and false as
     * paused. (return value determined by FlowPlayer JS API)
     * 
     * @return boolean ifPlaying
     */
    public boolean toggle() {
        return nfp.toggle();
    }

    public double getDuration() {
        double dur = (double) nfp.getDuration();
        return dur;
    }

    public long getDurationMs() {
        double dur = (double) nfp.getDuration();
        return Math.round(dur * 1000);
    }

    public String getPlayerSwf() {
        return playerSwf;
    }

    public void setPlayerSwf(String swf) {
        this.playerSwf = swf;
    }

    public void setRtmpSwf(String rtmpSwf) {
        this.rtmpSwf = rtmpSwf;
    }

    public String getRtmpSwf() {
        return rtmpSwf;
    }

    public String getSecureSwf() {
        return secureSwf;
    }

    public void setNetConnectionUrl(String netConnectionUrl) {
        this.netConnectionUrl = netConnectionUrl;
    }

    public String getNetConnectionUrl() {
        return netConnectionUrl;
    }

    public void setDefaultClip(String defaultClip) {
        this.defaultClip = defaultClip;
    }

    public void setClip(String clipUrl) {
        this.setDefaultClip(clipUrl);
        if (nfp != null) {
            if (this.nfp.isLoaded()) {
                this.nfp.setClip(clipUrl, netConnectionUrl);
            }
        }
    }

    public String getDefaultClip() {
        return defaultClip;
    }

    public void unloadPlayer() {
        nfp.unload();
    }

    public String getToken() {
        return token;
    }

    /**
     * Set the player security token, can only be set before the player is loaded
     * 
     * @param token
     */
    public void setToken(String token) {
        this.token = token;
    }

}
