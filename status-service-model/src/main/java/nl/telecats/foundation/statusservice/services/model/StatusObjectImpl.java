package nl.telecats.foundation.statusservice.services.model;

import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

 /**
  * Represents the status of a service by providing a short description (resume
  * of the statusdetail) and the status of the service defined by the Status enum
  * @author martijnvg
  * 
  */
@JacksonXmlRootElement(localName="getStatusResult")
public class StatusObjectImpl implements StatusObject {

    private Status status;
    private String shortDescription;
    private List<StatusDetail> statusDetails;

    public StatusObjectImpl() {
        statusDetails = new ArrayList<StatusDetail>();
    }

    /**
     * @return the status
     */
    public Status getStatus() {
        return status;
    }

    /**
     * @param status
     *            the status to set
     */
    public void setStatus(Status status) {
        this.status = status;
    }

    /**
     * @return the shortDescription
     */
    public String getShortDescription() {
        return shortDescription;
    }

    /**
     * @param shortDescription
     *            the shortDescription to set
     */
    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    /**
     * @return the statusDetails
     */
    public List<StatusDetail> getStatusDetails() {
        return statusDetails;
    }

    /**
     * @param statusDetails
     *            the statusDetails to set
     */
    public void setStatusDetails(List<StatusDetail> statusDetails) {
        this.statusDetails = statusDetails;
    }

}
