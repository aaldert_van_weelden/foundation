package nl.telecats.foundation.statusservice.services.model;

public interface StatusDetail extends Comparable<StatusDetail> {

    /**
     * @return the status
     */
    public Status getStatus();

    /**
     * @return the shortDescription
     */
    public String getShortDescription();

    /**
     * @return the payload
     */
    public String getData();
}
