package nl.telecats.web.security.entities;

import org.springframework.util.AntPathMatcher;

/**
 * An authentication description for a web service.<br />
 * <br />
 * Consists of
 * <dl>
 * <dt>Path</dt>
 * <dd>The path to the service, from the perspective of the client, minus host. An endpoint for a CXF-created SOAP
 * webservice of a project called myproject with endpoint myservice will have a full link of
 * http(s)://host:port/myproject/myservice. The path is /myproject/myservice paths are matched as ANT path patterns. @see
 * {@link AntPathMatcher} and http://ant.apache.org/manual/dirtasks.html for more</dd>
 * <dt>username</dt>
 * <dd>username for the path</dd>
 * <dt>password</dt>
 * <dd>password for the path</dd>
 * </dl>
 * 
 * @author Danny Dam Wichers
 */
public class AuthenticationMapping {

    /**
     * Path to the service. Uses ANT-style paths. @see {@link AntPathMatcher} and
     * http://ant.apache.org/manual/dirtasks.html for more
     */
    private String path;

    /**
     * username for authentication
     */
    private String username;

    /**
     * password for authentication
     */
    private String password;

    /**
     * Constructor
     */
    public AuthenticationMapping() {
    }

    /**
     * Constructor
     * 
     * @param path
     *            service path; uses ANT-style paths
     * @param username
     *            username for authentication
     * @param password
     *            password for authentication
     */
    public AuthenticationMapping(String path, String username, String password) {

    }

    /**
     * @return the path
     */
    public String getPath() {
        return path;
    }

    /**
     * @param path
     *            the path to set
     */
    public void setPath(String path) {
        this.path = path;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username
     *            the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password
     *            the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((password == null) ? 0 : password.hashCode());
        result = prime * result + ((path == null) ? 0 : path.hashCode());
        result = prime * result + ((username == null) ? 0 : username.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof AuthenticationMapping)) {
            return false;
        }
        AuthenticationMapping other = (AuthenticationMapping) obj;
        if (password == null) {
            if (other.password != null) {
                return false;
            }
        } else if (!password.equals(other.password)) {
            return false;
        }
        if (path == null) {
            if (other.path != null) {
                return false;
            }
        } else if (!path.equals(other.path)) {
            return false;
        }
        if (username == null) {
            if (other.username != null) {
                return false;
            }
        } else if (!username.equals(other.username)) {
            return false;
        }
        return true;
    }
}