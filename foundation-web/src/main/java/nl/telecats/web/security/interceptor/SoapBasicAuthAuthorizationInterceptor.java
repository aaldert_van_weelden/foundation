package nl.telecats.web.security.interceptor;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import nl.telecats.web.security.entities.AuthenticationMapping;

import org.apache.commons.lang.StringUtils;
import org.apache.cxf.binding.soap.interceptor.SoapHeaderInterceptor;
import org.apache.cxf.configuration.security.AuthorizationPolicy;
import org.apache.cxf.endpoint.Endpoint;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.message.Exchange;
import org.apache.cxf.message.Message;
import org.apache.cxf.transport.Conduit;
import org.apache.cxf.ws.addressing.EndpointReferenceType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.util.AntPathMatcher;

/**
 * This interceptor can be added to a (CXF) webservice to add Basic Authenication. It requires a set of configuration
 * lines that set a username and password for each path that has to be protected. This is done by calling
 * {@link #setConfigResource(Resource)} with a resource file, which should hold 3 lines for each service that needs to
 * be protected by Basic Authentication:
 * <dl>
 * <dt>servicename.path ({@link #PATH_KEY})</dt>
 * <dd>path to the service; this is an ANT-style path starting from the hostname.</dd>
 * <dt>servicename.username ({@link #USERNAME_KEY})</dt>
 * <dd>username for authentication</dd>
 * <dt>servicename.password ({@link #PASSWORD_KEY})</dt>
 * <dd>password for authentication</dd>
 * </dl>
 * <em>servicename</em> must be the same for each of the lines. Note: never use a dot (.) as part of the servicename;
 * use only alphanumeric characters preferably.
 * 
 * @see {@link AuthenticationMapping} for more information on these variables.<strong>If any of the values is missing or
 *      empty, the entry will not be set and no authentication will be enforced on that particular path.</strong><br/>
 * <br />
 *      Each request will be matched against the set of paths. Paths will be matched in the order in which they were
 * 
 *      and if a match is found, the headers will be checked for the basic authentication header. If no such header is
 *      found, a 401 Authentication Required will be returned to the client.<br />
 *      If the header is present, it's contents will be matched against the supplied username and password and if it
 *      matches, the service call will proceed as normal. However, if either the username or password fails to match, a
 *      403 Forbidden will be returned.<br />
 * <br />
 *      <strong>Notice</strong>: This interceptor acts as a blacklist: only entries in the configuration file that were
 *      fully configured will be checked for authentication, <em>all others will succeed by default</em>. This effect
 *      can be changed by adding a default service at the end of a configuration file that acts as a catch all.
 * 
 * @author Danny Dam Wichers
 */
public class SoapBasicAuthAuthorizationInterceptor extends SoapHeaderInterceptor {

    /**
     * Path identifier of the key of a configuration pair.
     */
    public static final String PATH_KEY = "path";

    /**
     * Username identifier of the key of a configuration pair.
     */
    public static final String USERNAME_KEY = "username";

    /**
     * Password identifier of the key of a configuration pair.
     */
    public static final String PASSWORD_KEY = "password";

    /**
     * Logger
     */
    private Logger log = LoggerFactory.getLogger(SoapBasicAuthAuthorizationInterceptor.class);

    /**
     * Collection of authentication mappings
     */
    private List<AuthenticationMapping> authenticationMappings = new ArrayList<AuthenticationMapping>();

    /**
     * Configuration file
     */
    private Resource configResource;

    /**
     * Matcher for ANT paths.
     */
    private static final AntPathMatcher antPathMatcher = new AntPathMatcher();

    /**
     * Takes the configuration resource {@link #configResource} and creates authentication mappings.
     */
    protected void processConfig() {
        if (configResource == null) {
            log.error("No configuration resource supplied; unable to set configration. ALL REQUESTS WILL BE HONORED");
            return;
        }

        // load configuration as a propeties file
        Properties properties = new Properties();

        try {
            properties.load(getConfigResource().getInputStream());
        } catch (IOException ioe) {
            log.error(
                    "Unable to load configuration from resource; unable to set configration. ALL REQUESTS WILL BE HONORED",
                    ioe);
            return;
        }

        // aggregate data from resource
        Map<String, AuthenticationMapping> mappings = new HashMap<String, AuthenticationMapping>();
        for (String configKey : properties.stringPropertyNames()) {
            String[] configSplit = configKey.split("\\.");
            String configValue = properties.getProperty(configKey);

            if (configSplit.length == 2) {
                String serviceName = configSplit[0];

                AuthenticationMapping am = mappings.containsKey(serviceName) ? mappings.get(serviceName)
                        : new AuthenticationMapping();

                if (configSplit[1].equals(PATH_KEY)) {
                    am.setPath(configValue);
                } else if (configSplit[1].equals(USERNAME_KEY)) {
                    am.setUsername(configValue);
                } else if (configSplit[1].equals(PASSWORD_KEY)) {
                    am.setPassword(configValue);
                } else {
                    log.warn("Wrong entry with key (no valid configuration directive found) : " + configKey
                            + "; skipping line");
                }

                mappings.put(serviceName, am);
            } else {
                log.warn("Wrong entry with key (too many dots or no configuration directive) : " + configKey
                        + "; skipping line");
            }
        }

        // build final mapping
        for (Map.Entry<String, AuthenticationMapping> am : mappings.entrySet()) {
            if (StringUtils.isEmpty(am.getValue().getPath())) {
                log.warn("No path found for service " + am.getKey() + "; mapping not set");
            } else if (StringUtils.isEmpty(am.getValue().getUsername())
                    && StringUtils.isEmpty(am.getValue().getPassword())) {
                log.debug("No username and password supplied for service " + am.getKey()
                        + "; all requests to this service will be honored regardless of authentication");
                authenticationMappings.add(am.getValue());
            } else if (StringUtils.isEmpty(am.getValue().getUsername())) {
                log.warn("No username supplied for service " + am.getKey()
                        + "; configuration not complete, rule not set");
            } else if (StringUtils.isEmpty(am.getValue().getPassword())) {
                log.warn("No password supplied for service " + am.getKey()
                        + "; configuration not complete, rule not set");
            } else {
                log.debug("Protecting service " + am.getKey() + " with basic authentication");
                authenticationMappings.add(am.getValue());
            }
        }

        if (authenticationMappings.size() < 1) {
            log.warn("No authentication mappings found; ALL REQUESTS WILL BE HONORED");
        }
    }

    /**
     * Main method which gets called by the webservice handler with the message that was received.
     * 
     * @param message
     *            the message that was received.
     * @throws {@link Fault} if an error occurred during processing.
     */
    @Override
    public void handleMessage(Message message) throws Fault {
        // check if authentication is required for the service that is called.
        String path = (String) message.get(Message.PATH_INFO);

        if (path == null) {
            log.info("handleMessage: path is null; passing through");
            return;
        }

        log.debug("Processing request to " + path);

        if (requiresAuthentication(path)) {
            AuthorizationPolicy policy = message.get(AuthorizationPolicy.class);

            // If the policy is not set, the user did not specify credentials.
            // 401 is sent to the client to indicate that authentication is required.
            if (policy == null) {
                log.warn("Authorization needed for " + path + ", but no authorization header found");
                sendErrorResponse(message, HttpURLConnection.HTTP_UNAUTHORIZED);
                return;
            }

            String username = policy.getUserName();
            String password = policy.getPassword();

            // CHECK USERNAME AND PASSWORD
            if (!checkLogin(username, password, path)) {
                log.warn("handleMessage: Invalid username or password for path: " + path);
                sendErrorResponse(message, HttpURLConnection.HTTP_FORBIDDEN);
                return;
            }

            log.debug("Request passed through authentication");
        } else {
            log.debug("No authetication required for " + path);
        }
    }

    /**
     * Returns <code>true</code> if the path requires (basic) authentication.
     * 
     * @param path
     *            the path to check; this is the part of the URI after the host/port pair and without the query string.
     * @return <code>true</code> if the path requires (basic) authentication.
     */
    private boolean requiresAuthentication(String path) {
        for (AuthenticationMapping am : authenticationMappings) {
            if (antPathMatcher.match(am.getPath(), path)) {
                if (StringUtils.isEmpty(am.getUsername()) && StringUtils.isEmpty(am.getPassword())) {
                    log.debug("Match found for " + path + " in " + am.getPath()
                            + "; no authentication required (username and password are empty)");
                    return false;
                } else {
                    log.debug("Match found for " + path + " in " + am.getPath() + "; authentication required");
                    return true;
                }
            }
        }

        log.debug("No authetication set for " + path);
        return false;
    }

    /**
     * Checks the supplied username and password for
     * 
     * @param username
     *            username for authentication.
     * @param password
     *            password for authentication.
     * @param path
     *            path to check authentication for.
     * @return <code>true</code> if the authentication succeeded.
     */
    private boolean checkLogin(String username, String password, String path) {
        for (AuthenticationMapping am : authenticationMappings) {
            if (antPathMatcher.match(am.getPath(), path)) {
                log.debug("Checking login for " + path + " matched by " + am.getPath());
                return am.getUsername().equals(username) && am.getPassword().equals(password);
            }
        }

        log.warn("Login check called against a path that has no mapping: " + path);
        return true;
    }

    /**
     * Send an error to the client
     * 
     * @param message
     *            message that was received.
     * @param responseCode
     *            HTTP response code
     */
    private void sendErrorResponse(Message message, int responseCode) {
        Message outMessage = getOutMessage(message);
        outMessage.put(Message.RESPONSE_CODE, responseCode);

        // uncomment the following code to get the request headers in case more information is needed;
        // @SuppressWarnings("unchecked")
        // Map<String, List<String>> headers = (Map<String, List<String>>) =
        // message.get(Message.PROTOCOL_HEADERS);

        // Set the response headers; create a new set
        Map<String, List<String>> responseHeaders = new HashMap<String, List<String>>();//

        if (responseHeaders != null) {
            responseHeaders.put("WWW-Authenticate", Arrays.asList(new String[] { "Basic realm=realm" }));
            responseHeaders.put("Content-Length", Arrays.asList(new String[] { "0" }));
        }
        message.getInterceptorChain().abort();

        try {
            getConduit(message).prepare(outMessage);

            outMessage.put(Message.PROTOCOL_HEADERS, responseHeaders);

            close(outMessage);
        } catch (IOException ioe) {
            log.error("Unable to send error message", ioe);
        }
    }

    /**
     * Retrieve the outgoing message.
     * 
     * @param inMessage
     *            the incoming message.
     * @return the outgoing message.
     */
    private Message getOutMessage(Message inMessage) {
        Exchange exchange = inMessage.getExchange();
        Message outMessage = exchange.getOutMessage();
        if (outMessage == null) {
            log.debug("Creating new out message");
            Endpoint endpoint = exchange.get(Endpoint.class);
            outMessage = endpoint.getBinding().createMessage();
            exchange.setOutMessage(outMessage);
        } else {
            log.debug("Using existing out message");
        }
        outMessage.putAll(inMessage);
        return outMessage;
    }

    /**
     * Retrieve the conduit of the message.
     * 
     * @param inMessage
     *            the message.
     * @return the conduit of the message.
     * @throws IOException
     *             if the back channel could not be retrieved from the message destination.
     */
    private Conduit getConduit(Message inMessage) throws IOException {
        Exchange exchange = inMessage.getExchange();
        EndpointReferenceType target = exchange.get(EndpointReferenceType.class);
        Conduit conduit = exchange.getDestination().getBackChannel(inMessage, null, target);
        exchange.setConduit(conduit);
        return conduit;
    }

    /**
     * Close the stream.
     * 
     * @param outMessage
     *            output message to close stream of.
     * @throws IOException
     *             if the stream could not be closed.
     */
    private void close(Message outMessage) throws IOException {
        OutputStream os = outMessage.getContent(OutputStream.class);
        os.flush();
        os.close();
    }

    /**
     * Set a new configuration resource, which will be applied immediately
     * 
     * @param configResource
     *            the configResource to set
     */
    public void setConfigResource(Resource configResource) {
        this.configResource = configResource;
        processConfig();
    }

    /**
     * Retrieve the configuration resource.
     * 
     * @return the configResource
     */
    public Resource getConfigResource() {
        return configResource;
    }

    /**
     * Retrieve the authentication mappings after parsing them from the configuration file.
     * 
     * @return the authenticationMappings
     */
    public List<AuthenticationMapping> getAuthenticationMappings() {
        return authenticationMappings;
    }

    /**
     * Set the authentication mappings directly.
     * 
     * @param authenticationMappings
     *            the authenticationMappings to set
     */
    public void setAuthenticationMappings(List<AuthenticationMapping> authenticationMappings) {
        this.authenticationMappings = authenticationMappings;
    }
}