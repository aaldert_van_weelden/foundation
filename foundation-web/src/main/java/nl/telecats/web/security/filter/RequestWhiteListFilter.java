package nl.telecats.web.security.filter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.cxf.common.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.filter.GenericFilterBean;

/**
 * A filter that blocks HTTP requests that do not match a white list. This filter requires a file that contains the path
 * patterns: {@value #WHITE_LIST_FILE}<br />
 * The filter will block any HTTP-request, until the {@link #initList()} function is called.<br />
 * <br />
 * <strong>All non-HTTP-requests are approved automatically.</strong><br />
 * <br />
 * Best practice is to set this filter as the first filter through which requests pass; this will ensure filters further
 * along the chain or in other chains don't get called, saving time and resources. This is done best by adding a new
 * filter to the top of web.xml, pointing to a filter chain in the Spring context that only contains this filter.<br />
 * <br />
 * Patterns are ANT path patterns. @see {@link AntPathMatcher} and http://ant.apache.org/manual/dirtasks.html for more
 * information.
 * 
 * @author danny
 */
@Component
public class RequestWhiteListFilter extends GenericFilterBean {

    /**
     * Application context. Used to locate white list file.
     */
    @Autowired
    private ApplicationContext applicationContext;

    /**
     * Local cache of matching patterns
     */
    private List<String> patterns = new ArrayList<String>();

    /**
     * The matcher.
     */
    private static final AntPathMatcher apm = new AntPathMatcher();

    /**
     * Logger
     */
    private Logger log = LoggerFactory.getLogger(RequestWhiteListFilter.class);

    /**
     * The location of the white list file. Defaults to classpath:request_whitelist.txt
     */
    private static final String WHITE_LIST_FILE = "classpath:request_whitelist.txt";

    /*
     * (non-Javadoc)
     * 
     * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest, javax.servlet.ServletResponse,
     * javax.servlet.FilterChain)
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException,
            ServletException {

        // make this filter more robust by checking for HTTP requests. All other requests are approved automatically
        if (request instanceof HttpServletRequest && response instanceof HttpServletResponse) {
            // create a HTTP request out of the servlet request, to get access to the request line
            HttpServletRequest httpRequest = (HttpServletRequest) request;

            // remove the project from the URI
            String localRequest = httpRequest.getRequestURI().substring(httpRequest.getContextPath().length());

            log.trace("Checking " + localRequest);

            // check for validity
            if (hasMatch(localRequest)) {
                // move along
                chain.doFilter(request, response);
            } else {
                // exit immediately
                if (patterns.size() == 0) {
                    log.warn("Blocking request to " + localRequest + "; filter was not configured! Setup patterns in "
                            + WHITE_LIST_FILE);
                } else {
                    log.debug("Blocking request to " + localRequest);
                }

                HttpServletResponse httpResponse = (HttpServletResponse) response;
                httpResponse.sendError(HttpServletResponse.SC_FORBIDDEN, "Not allowed");
            }
        } else {
            log.warn("Request was not a HTTP request; defaulting to approve");
            chain.doFilter(request, response);
        }
    }

    /**
     * Initializes the list. <strong>If this function is not called, every request will be approved.</strong>
     */
    @PostConstruct
    public void initList() {
        log.debug("Setting up " + this.getClass().getName() + " with match file " + WHITE_LIST_FILE);

        Resource whitelistResource = applicationContext.getResource(WHITE_LIST_FILE);

        patterns.clear();

        try {
            InputStream is = whitelistResource.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));

            String line;
            while ((line = br.readLine()) != null) {
                if (!StringUtils.isEmpty(line) && !line.startsWith("#")) {
                    log.debug("Added pattern " + line);

                    patterns.add(line);
                }
            }
            br.close();
        } catch (IOException ioe) {
            String message = "Unable to open resource " + WHITE_LIST_FILE;
            log.error(message, ioe);

            throw new IllegalArgumentException(message, ioe);
        }
    }

    /**
     * Checks whether the supplied request has a match on the white list.
     * 
     * @param path
     *            request to check. The URL of the request, relative to the webapp.
     * @return true if the request has a match on the white list.
     */
    protected boolean hasMatch(String path) {
        for (String pattern : patterns) {
            if (apm.match(pattern, path)) {
                return true;
            }
        }

        return false;
    }
}