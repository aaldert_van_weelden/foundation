package nl.telecats.web.security.interceptor;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URL;
import java.util.Properties;

import javax.jws.WebMethod;
import javax.jws.WebService;

import junit.framework.Assert;

import org.apache.commons.lang.StringUtils;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.apache.cxf.jaxws.JaxWsServerFactoryBean;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @author Danny Dam Wichers
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:foundation-web-test-context.xml" })
public class SoapBasicAuthAuthorizationInterceptorFilterTest {

    /**
     * A trivial port on the system that has to bee free.
     */
    @Value("${soap.freeport}")
    private int FREE_SOAP_PORT;

    /**
     * Default endpoint for testing
     */
    private static final String DEFAULT_ENDPOINT = "/wstest/baaitest";

    /**
     * Alternative endpoint for testing
     */
    private static final String ALTERNATIVE_ENDPOINT = "/wstest/freetest";

    /**
     * The interceptor
     */
    private static final SoapBasicAuthAuthorizationInterceptor baai = new SoapBasicAuthAuthorizationInterceptor();

    @Test
    public void testDefaultConfiguration() throws IOException {
        SoapBasicAuthAuthorizationInterceptor baai = new SoapBasicAuthAuthorizationInterceptor();

        Properties p = new Properties();
        p.put("myservice.username", "myserviceUsername");
        p.put("myservice.password", "myservicePassword");
        p.put("myservice.path", "myservicePath");

        Resource bar = getResourceFromProperties(p);
        baai.setConfigResource(bar);

        Assert.assertEquals(1, baai.getAuthenticationMappings().size());
    }

    @Test
    public void testPublicConfiguration() throws IOException {
        SoapBasicAuthAuthorizationInterceptor baai = new SoapBasicAuthAuthorizationInterceptor();

        Properties p = new Properties();
        p.put("myservice.username", "");
        p.put("myservice.password", "");
        p.put("myservice.path", "myservicePath");

        Resource bar = getResourceFromProperties(p);
        baai.setConfigResource(bar);

        Assert.assertEquals(1, baai.getAuthenticationMappings().size());
    }

    @Test
    public void testIncompleteConfiguration() throws IOException {
        SoapBasicAuthAuthorizationInterceptor baai = new SoapBasicAuthAuthorizationInterceptor();

        Properties p = new Properties();
        p.put("myservice.username", "myserviceUsername");
        p.put("myservice.password", "myservicePassword");

        Resource bar = getResourceFromProperties(p);
        baai.setConfigResource(bar);

        Assert.assertEquals(0, baai.getAuthenticationMappings().size());

        p = new Properties();
        p.put("myservice.username", "myserviceUsername");
        p.put("myservice.path", "myservicePath");

        bar = getResourceFromProperties(p);
        baai.setConfigResource(bar);

        Assert.assertEquals(0, baai.getAuthenticationMappings().size());

        p = new Properties();
        p.put("myservice.path", "myservicePath");
        p.put("myservice.password", "myservicePassword");

        bar = getResourceFromProperties(p);
        baai.setConfigResource(bar);

        Assert.assertEquals(0, baai.getAuthenticationMappings().size());
    }

    // TODO Danny gaat dit onder FOUNDATION-686 oplossen; deze unittest mislukt op de buildserver.
    @Test
    @Ignore
    public void testReconfigure() throws IOException {
        Properties p = new Properties();

        configureAndSetClient(p, DEFAULT_ENDPOINT, null, null).doIt();
        configureAndSetClient(p, DEFAULT_ENDPOINT, "a", "b").doIt();

        p.put("myservice.username", "a");
        p.put("myservice.password", "b");
        p.put("myservice.path", DEFAULT_ENDPOINT);

        try {
            configureAndSetClient(p, DEFAULT_ENDPOINT, null, null).doIt();

            Assert.fail("Should not be reached due to HTTP 401");
        } catch (javax.xml.ws.soap.SOAPFaultException sfe) {
            // expected behavior in this case
            Assert.assertEquals(java.net.HttpRetryException.class, sfe.getCause().getCause().getClass());
        }

        configureAndSetClient(p, DEFAULT_ENDPOINT, "a", "b").doIt();
    }

    @Test
    public void test401() throws IOException {
        Properties p = new Properties();
        p.put("myservice.username", "a");
        p.put("myservice.password", "b");
        p.put("myservice.path", DEFAULT_ENDPOINT);

        try {
            configureAndSetClient(p, DEFAULT_ENDPOINT, null, null).doIt();

            Assert.fail("Should not be reached due to HTTP 401");
        } catch (javax.xml.ws.soap.SOAPFaultException sfe) {
            // expected behavior in this case
            Assert.assertEquals(java.net.HttpRetryException.class, sfe.getCause().getCause().getClass());
        }

        configureAndSetClient(p, DEFAULT_ENDPOINT, "a", "b").doIt();

        // test an alternative endpoint that is not protected
        configureAndSetClient(p, ALTERNATIVE_ENDPOINT, null, null).doIt();
    }

    @Test
    public void test403() throws IOException {
        Properties p = new Properties();
        p.put("myservice.username", "a");
        p.put("myservice.password", "b");
        p.put("myservice.path", DEFAULT_ENDPOINT);

        try {
            configureAndSetClient(p, DEFAULT_ENDPOINT, "a", "a").doIt();

            Assert.fail("Should not be reached due to HTTP 401");
        } catch (javax.xml.ws.soap.SOAPFaultException sfe) {
            Assert.assertTrue(sfe.getCause().getCause().getMessage()
                    .startsWith("Server returned HTTP response code: 403"));
        }

        // do it right
        configureAndSetClient(p, DEFAULT_ENDPOINT, "a", "b").doIt();

        // test an alternative endpoint that is not protected
        configureAndSetClient(p, ALTERNATIVE_ENDPOINT, "a", "b").doIt();
    }

    private WSTest configureAndSetClient(Properties interceporConfig, String serviceEndpoint, String username,
            String password) throws IOException {

        WSTestImpl endpoint = new WSTestImpl();
        JaxWsServerFactoryBean svrFactory = new JaxWsServerFactoryBean();
        svrFactory.setServiceClass(WSTest.class);
        svrFactory.setAddress("http://localhost:" + FREE_SOAP_PORT + serviceEndpoint);
        svrFactory.setServiceBean(endpoint);
        svrFactory.getBus().getInInterceptors().add(baai);

        try {
            svrFactory.create();
        } catch (RuntimeException rte) {
            // service already registered.
        }

        JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
        factory.setServiceClass(WSTest.class);
        factory.setAddress("http://localhost:" + FREE_SOAP_PORT + serviceEndpoint);

        if (StringUtils.isNotEmpty(username)) {
            factory.setUsername(username);
        }

        if (StringUtils.isNotEmpty(password)) {
            factory.setPassword(password);
        }

        WSTest client = (WSTest) factory.create();

        Resource bar = getResourceFromProperties(interceporConfig);
        baai.setConfigResource(bar);

        return client;
    }

    private Resource getResourceFromProperties(Properties input) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        input.store(baos, "");

        ByteArrayResource bar = new ByteArrayResource(new ByteArrayInputStream(baos.toByteArray()));

        return bar;
    }

    private class ByteArrayResource implements Resource {

        ByteArrayInputStream bais = null;

        public ByteArrayResource(ByteArrayInputStream bais) {
            this.bais = bais;
        }

        @Override
        public InputStream getInputStream() throws IOException {
            return bais;
        }

        @Override
        public boolean exists() {
            return bais != null;
        }

        @Override
        public boolean isReadable() {
            return bais != null;
        }

        @Override
        public boolean isOpen() {
            return bais != null;
        }

        @Override
        public URL getURL() throws IOException {
            return null;
        }

        @Override
        public URI getURI() throws IOException {
            return null;
        }

        @Override
        public File getFile() throws IOException {
            return null;
        }

        @Override
        public long lastModified() throws IOException {
            return 0;
        }

        @Override
        public Resource createRelative(String relativePath) throws IOException {
            return null;
        }

        @Override
        public String getFilename() {
            return null;
        }

        @Override
        public String getDescription() {
            return "Byte Array Resource";
        }
    }

    @WebService(name = "WSTest", portName = "wstestport", serviceName = "wstestservice", targetNamespace = "nl.telecats.foundation.wstest")
    public interface WSTest {
        @WebMethod(action = "doIt", operationName = "doIt")
        public String doIt();
    }

    public class WSTestImpl implements WSTest {
        @Override
        public String doIt() {
            return "RETURN";
        }
    }
}