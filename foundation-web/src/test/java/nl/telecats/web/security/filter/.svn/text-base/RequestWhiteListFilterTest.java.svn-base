package nl.telecats.web.security.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @author Danny Dam Wichers
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:foundation-web-test-context.xml" })
public class RequestWhiteListFilterTest {

    @Autowired
    private RequestWhiteListFilter filter;

    private MockHttpServletResponse response;

    private int DEFAULT_STATUS = 200;

    private int ERROR_STATUS = 403;

    private FilterChain mockFilterChain = new FilterChain() {
        @Override
        public void doFilter(ServletRequest arg0, ServletResponse arg1) throws IOException, ServletException {
            // do nothing
        }
    };

    @Test
    public void testAutowired() {
        Assert.assertNotNull(filter);
    }

    @Test
    public void testFullPath() throws IOException, ServletException {
        MockHttpServletRequest request = new MockHttpServletRequest("GET", "/public/test.jpg");
        filter.doFilter(request, response, mockFilterChain);
        Assert.assertEquals(DEFAULT_STATUS, response.getStatus());

        request = new MockHttpServletRequest("GET", "/public/test.jpeg");
        filter.doFilter(request, response, mockFilterChain);
        Assert.assertEquals(ERROR_STATUS, response.getStatus());
    }

    @Test
    public void testStartWith() throws IOException, ServletException {
        MockHttpServletRequest request = new MockHttpServletRequest("GET", "/secure/super1");
        filter.doFilter(request, response, mockFilterChain);
        Assert.assertEquals(DEFAULT_STATUS, response.getStatus());

        request = new MockHttpServletRequest("GET", "/secure/superabcd/ssdf33.jsp");
        filter.doFilter(request, response, mockFilterChain);
        Assert.assertEquals(DEFAULT_STATUS, response.getStatus());

        request = new MockHttpServletRequest("GET", "/secur/superabcd/ssdf33.jsp");
        filter.doFilter(request, response, mockFilterChain);
        Assert.assertEquals(ERROR_STATUS, response.getStatus());
    }

    @Test
    public void testEndWith() throws IOException, ServletException {
        MockHttpServletRequest request = new MockHttpServletRequest("GET", "/my/js/function.js");
        filter.doFilter(request, response, mockFilterChain);
        Assert.assertEquals(DEFAULT_STATUS, response.getStatus());

        request = new MockHttpServletRequest("GET", "/testjs");
        filter.doFilter(request, response, mockFilterChain);
        Assert.assertEquals(DEFAULT_STATUS, response.getStatus());

        request = new MockHttpServletRequest("GET", "/testjss");
        filter.doFilter(request, response, mockFilterChain);
        Assert.assertEquals(ERROR_STATUS, response.getStatus());
    }

    @Test
    public void testInBetween() throws IOException, ServletException {
        MockHttpServletRequest request = new MockHttpServletRequest("GET", "/secure/my/gwt/service/here.gwt");
        filter.doFilter(request, response, mockFilterChain);
        Assert.assertEquals(DEFAULT_STATUS, response.getStatus());

        request = new MockHttpServletRequest("GET", "/secure/.gwt");
        filter.doFilter(request, response, mockFilterChain);
        Assert.assertEquals(DEFAULT_STATUS, response.getStatus());

        request = new MockHttpServletRequest("GET", "/secu/.gwt");
        filter.doFilter(request, response, mockFilterChain);
        Assert.assertEquals(ERROR_STATUS, response.getStatus());
    }

    @Test
    public void testBlocked() throws IOException, ServletException {
        MockHttpServletRequest request = new MockHttpServletRequest("GET", "/not-matched");
        filter.doFilter(request, response, mockFilterChain);
        Assert.assertEquals(ERROR_STATUS, response.getStatus());
    }

    @Before
    public void setup() {
        response = new MockHttpServletResponse();
        response.setStatus(DEFAULT_STATUS);
    }
}