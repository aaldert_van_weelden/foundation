package nl.telecats.foundation.config;

/**
 * Classes implementing this interface can register itself as a listener to
 * property changes on a {@link PropertiesConfiguration} object. See
 * {@link PropertiesConfiguration#registerForPropertyChange(PropertyConfigurationChangedListener, String)}
 * 
 * @since Foundation 2.2
 * @author Eelco Brolman
 * 
 */
public interface PropertyConfigurationChangedListener {

    /**
     * This function is called upon a property change. The property is given,
     * including the configuration in which the new value of the property can be
     * retrieved.
     * 
     * @param property
     *            The name of the changed property
     * @param pccn
     *            The PropertyConfigurationChangedNotifier, on which the
     *            configurationApplied function can be called
     */
    public void propertyUpdated(String property, PropertyConfigurationChangedNotifier pccn);

}