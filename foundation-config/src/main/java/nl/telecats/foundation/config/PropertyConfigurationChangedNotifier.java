package nl.telecats.foundation.config;

/**
 * Exposes the {@link #configurationApplied()} function, so that classes can
 * notify that a configuration change has been applied.
 * 
 * @since Foundation 2.2
 * @author Eelco Brolman
 * 
 */
public interface PropertyConfigurationChangedNotifier {

    /**
     * A class implementing
     * {@link PropertyConfigurationChangedListener#propertyUpdated(String, PropertyConfigurationChangedNotifier)}
     * must call this function to notify that a configuration change is applied.
     */
    public void configurationApplied();
}
