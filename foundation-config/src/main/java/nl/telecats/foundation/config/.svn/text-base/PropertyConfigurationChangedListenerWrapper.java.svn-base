package nl.telecats.foundation.config;

/**
 * A wrapper around a PropertyConfigurationChangedListener, which holds beside
 * the listener also the status. On change, the status is set to PENDING, which
 * can be reset by the listener by calling
 * {@link PropertyConfigurationChangedNotifier#configurationApplied()} in the
 * {@link PropertyConfigurationChangedListener#propertyUpdated(String, PropertyConfigurationChangedNotifier)}
 * function call.
 * 
 * @author eelco
 * 
 */
public class PropertyConfigurationChangedListenerWrapper implements PropertyConfigurationChangedNotifier {

    private PropertyConfigurationChangedListener propertyConfigurationChangedListener;
    private Status status;

    /**
     * Default constructor
     * 
     * @param propertyConfigurationChangedListener
     * @param status
     */
    protected PropertyConfigurationChangedListenerWrapper(
            PropertyConfigurationChangedListener propertyConfigurationChangedListener, Status status) {
        super();
        this.propertyConfigurationChangedListener = propertyConfigurationChangedListener;
        this.status = status;
    }

    /**
     * Default constructor with Status.OK as Status
     * 
     * @param pccl
     */
    protected PropertyConfigurationChangedListenerWrapper(PropertyConfigurationChangedListener pccl) {
        this(pccl, Status.OK);
    }

    /**
     * Get the listener of this wrapper
     * 
     * @return a {@link PropertyConfigurationChangedListener}
     */
    public PropertyConfigurationChangedListener getPropertyConfigurationChangedListener() {
        return propertyConfigurationChangedListener;
    }

    protected void setPropertyConfigurationChangedListener(
            PropertyConfigurationChangedListener propertyConfigurationChangedListener) {
        this.propertyConfigurationChangedListener = propertyConfigurationChangedListener;
    }

    /**
     * Get the status of this wrapper
     * 
     * @return a {@link Status}
     */
    public Status getStatus() {
        return status;
    }

    protected void setStatus(Status status) {
        this.status = status;
    }

    /*
     * (non-Javadoc)
     * 
     * @seenl.telecats.foundation.config.PropertyConfigurationChangedNotifier#
     * configurationApplied()
     */
    public void configurationApplied() {
        this.status = Status.OK;
    }

    public enum Status {
        /**
         * Status pending, the configuration parameter is changed, but the
         * change is not yet applied by the listener
         */
        PENDING, /**
         * The listener has applied the value
         */
        OK
    }
}
