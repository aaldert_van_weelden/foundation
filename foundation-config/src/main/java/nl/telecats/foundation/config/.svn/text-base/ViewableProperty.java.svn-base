package nl.telecats.foundation.config;

import java.util.List;

/**
 * View on a property, which holds beside the property a list of
 * {@link PropertyConfigurationChangedListenerWrapper}s, where the listeners and
 * their corresponding status is stored.<br/>
 * <br/>
 * Use only for presentation purposes.
 * 
 * @author Eelco Brolman
 * 
 */
public class ViewableProperty {

    private Object property;
    private List<PropertyConfigurationChangedListenerWrapper> listeners;
    private String key;

    /**
     * Default constructor
     * 
     * @param key
     *            The key of the property
     * @param property
     *            The property (as Object)
     * @param list
     *            A list of {@link PropertyConfigurationChangedListenerWrapper}
     *            s.
     */
    public ViewableProperty(String key, Object property, List<PropertyConfigurationChangedListenerWrapper> list) {
        this.property = property;
        this.listeners = list;
        this.key = key;
    }

    /**
     * @return The key
     */
    public String getKey() {
        return key;
    }

    /**
     * @return The property itself (as Object)
     */
    public Object getProperty() {
        return property;
    }

    /**
     * @return The list of wrappers
     */
    public List<PropertyConfigurationChangedListenerWrapper> getListeners() {
        return listeners;
    }

}
