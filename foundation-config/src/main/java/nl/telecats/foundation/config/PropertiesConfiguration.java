package nl.telecats.foundation.config;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.TreeMap;

import nl.telecats.foundation.config.PropertyConfigurationChangedListenerWrapper.Status;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.event.ConfigurationEvent;
import org.apache.commons.configuration.event.ConfigurationListener;
import org.apache.commons.configuration.reloading.ManagedReloadingStrategy;
import org.apache.commons.configuration.reloading.ManagedReloadingStrategyMBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class that extends the {@link org.apache.commons.configuration.PropertiesConfiguration}. It overrides the
 * Constructor(String), so it also accepts arguments in the form of <code>classpath:/</code>.<br/>
 * <br/>
 * This class can be used to insert configuration as beans into your Spring enabled application. Usage inside the
 * applicationContext.xml:
 * 
 * <pre>
 *  &lt;bean id="config1" class="nl.telecats.foundation.config.PropertiesConfiguration"&gt;
 *         &lt;constructor-arg value="classpath:/test1.properties" /&gt;
 *  &lt;/bean&gt;
 * </pre>
 * 
 * This bean can be (auto)wired into another service bean.<br/>
 * <br/>
 * If the service bean want to get notified about updates on the configuration values, the service bean should implement
 * a {@link PropertyConfigurationChangedListener}, and add itself as a listener by calling
 * <code>config1.registerForPropertyChange(this, "test1.property1");</code> in the init method of the bean.<br/>
 * <br/>
 * 
 * @since Foundation 2.2
 * @author Eelco Brolman
 * 
 */
public class PropertiesConfiguration extends org.apache.commons.configuration.PropertiesConfiguration implements
        ConfigurationListener {

    private static Logger log = LoggerFactory.getLogger(PropertiesConfiguration.class);

    private Map<String, List<PropertyConfigurationChangedListenerWrapper>> listeners = new HashMap<String, List<PropertyConfigurationChangedListenerWrapper>>();

    private ManagedReloadingStrategyMBean managedReloadingStrategy;

    public PropertiesConfiguration() {
        super();
        init();
    }

    /**
     * Returns a sorted <code>Map&lt;String, {@link ViewableProperty}&gt;</code> (sorted on property key) of all the
     * properties in this configuration.
     * 
     * @return The sorted Map
     */
    @SuppressWarnings("unchecked")
    public Map<String, ViewableProperty> getAllProps() {
        TreeMap<String, ViewableProperty> theMap = new TreeMap<String, ViewableProperty>();
        getKeys();
        for (Iterator iterator = getKeys(); iterator.hasNext();) {
            String prop = (String) iterator.next();
            theMap.put(prop, new ViewableProperty(prop, getProperty(prop), listeners.get(prop)));
        }
        return theMap;
    }

    /**
     * Creates and loads the extended properties from the specified file. The specified file can contain "include = "
     * properties which then are loaded and merged into the properties.<br/>
     * <br/>
     * String can start with "classpath:", in that case the <code>getClass().getResource()</code> is called to get the
     * file.
     * 
     * @param fileName
     *            The name of the properties file to load.
     * @throws ConfigurationException
     *             Error while loading the properties file
     */
    public PropertiesConfiguration(String fileName) throws ConfigurationException {
        if (fileName.startsWith("classpath:")) {
            URL url = getClass().getResource(fileName.substring("classpath:".length()));
            if (url == null) {
                throw new ConfigurationException("Cannot locate classpath configuration source " + fileName);
            } else {
                // store the file name
                setURL(url);

                // load the file
                load();
            }
        } else {
            // store the file name
            setFileName(fileName);

            // load the file
            load();

        }
        init();

    }

    /**
     * Creates and loads the extended properties from the specified file. The specified file can contain "include = "
     * properties which then are loaded and merged into the properties. If the file does not exist, an empty
     * configuration will be created. Later the <code>save()</code> method can be called to save the properties to the
     * specified file.
     * 
     * @param file
     *            The properties file to load.
     * @throws ConfigurationException
     *             Error while loading the properties file
     */
    public PropertiesConfiguration(File file) throws ConfigurationException {
        super(file);
        init();
    }

    /**
     * Creates and loads the extended properties from the specified URL. The specified file can contain "include = "
     * properties which then are loaded and merged into the properties.
     * 
     * @param url
     *            The location of the properties file to load.
     * @throws ConfigurationException
     *             Error while loading the properties file
     */
    public PropertiesConfiguration(URL url) throws ConfigurationException {
        super(url);
        init();
    }

    private void init() {
        ManagedReloadingStrategy managedReloadingStrategy = new ManagedReloadingStrategy();
        this.managedReloadingStrategy = managedReloadingStrategy;
        this.setReloadingStrategy(managedReloadingStrategy);
        registerAsListener();
    }

    /**
     * Register itself as listener
     */
    private void registerAsListener() {
        super.addConfigurationListener(this);
    }

    /**
     * Returns a set of properties for which listeners are registered
     * 
     * @return The set with property keys
     */
    public Set<String> getListenedForProperties() {
        return listeners.keySet();
    }

    @Override
    public final void configurationChanged(ConfigurationEvent event) {
        if (event.getType() == EVENT_SET_PROPERTY) {
            // ignore before updates
            if (!event.isBeforeUpdate()) {
                List<PropertyConfigurationChangedListenerWrapper> l = listeners.get(event.getPropertyName());
                notifyListeners(l, event.getPropertyName(), event.getPropertyValue());
            }
        } else if (event.getType() == EVENT_RELOAD) {
            //

        }

    }

    private void notifyListeners(List<PropertyConfigurationChangedListenerWrapper> l, String propertyName,
            Object propertyValue) {
        if (l == null) {
            log.warn("Property '" + propertyName + "' got updated with no listeners active!");
        } else {
            log.info("Notifying " + l.size() + " listeners for property change of '" + propertyName + "' (new value: '"
                    + propertyValue + "')");
            for (PropertyConfigurationChangedListenerWrapper pcclw : l) {
                log.debug("Notifying '" + pcclw.getPropertyConfigurationChangedListener().getClass().getName()
                        + "' for property change of '" + propertyName + '"');
                pcclw.setStatus(Status.PENDING);
                pcclw.getPropertyConfigurationChangedListener().propertyUpdated(propertyName, pcclw);
            }
        }
    }

    /**
     * Register a class which implements {@link PropertyConfigurationChangedListener} as listeners. The class will get
     * notified (by {@link PropertyConfigurationChangedNotifier#configurationApplied()}) of changes to properties.
     * 
     * @param pccl
     *            The {@link PropertyConfigurationChangedListener}
     * @param prop
     *            The key of the property to listen for
     */
    public void registerForPropertyChange(PropertyConfigurationChangedListener pccl, String prop) {
        if (!containsKey(prop)) {
            // TODO is possible
            throw new NoSuchElementException("Property '" + prop + "' not set in '" + getFileName()
                    + "', cannot registrer for changes on it.");
        }
        List<PropertyConfigurationChangedListenerWrapper> l = listeners.get(prop);
        if (l == null) {
            l = new ArrayList<PropertyConfigurationChangedListenerWrapper>();
            listeners.put(prop, l);
        }
        log.info("Adding '" + pccl.getClass().getName() + "' as listener for property change of '" + prop + '"');
        l.add(new PropertyConfigurationChangedListenerWrapper(pccl));
    }

    /**
     * Refresh the config from its source
     * 
     * @since 2.4
     */
    public void refresh() {
        // get all old properties
        Map<String, ViewableProperty> oldProps = getAllProps();

        // refresh
        managedReloadingStrategy.refresh();

        // notify changes
        for (Map.Entry<String, ViewableProperty> entry : oldProps.entrySet()) {
            if (!entry.getValue().getProperty().equals(getProperty(entry.getKey()))) {
                notifyListeners(entry.getValue().getListeners(), entry.getKey(), getProperty(entry.getKey()));
            }
        }
    }
    // TODO add property in configuration.jsp
}
