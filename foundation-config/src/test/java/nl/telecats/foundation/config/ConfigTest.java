package nl.telecats.foundation.config;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.apache.commons.configuration.ConfigurationException;
import org.junit.Test;

public class ConfigTest implements PropertyConfigurationChangedListener {

    private List<String> updatedProperty = new ArrayList<String>();

    @Test
    public void doTest() throws ConfigurationException {
        URL url = getClass().getResource("/test.properties");
        PropertiesConfiguration pc = new PropertiesConfiguration(url);

        // basic checks
        Assert.assertNotNull(pc);
        Assert.assertTrue(pc.containsKey("prop1"));

        // listen for updates on prop1
        pc.registerForPropertyChange(this, "prop1");
        Assert.assertTrue(pc.getListenedForProperties().contains("prop1"));

        // set a prop
        pc.setProperty("prop1", "newval");

        // is the callback and value ok?
        Assert.assertEquals(1, updatedProperty.size());
        Assert.assertTrue("updatedProperty does not contain prop1", updatedProperty.contains("prop1"));
        Assert.assertEquals("newval", pc.getString("prop1"));

        Assert.assertEquals(PropertyConfigurationChangedListenerWrapper.Status.OK, pc.getAllProps().get("prop1")
                .getListeners().get(0).getStatus());
    }

    @Override
    public void propertyUpdated(String property, PropertyConfigurationChangedNotifier pw) {
        updatedProperty.add(property);
        pw.configurationApplied();

    }

    @Test
    public void arrayListTest() throws ConfigurationException {
        URL url = getClass().getResource("/test.properties");
        PropertiesConfiguration pc = new PropertiesConfiguration(url);

        Object bla = pc.getProperty("arraylist");
        Assert.assertTrue(bla instanceof List);
    }

    @Test
    public void testInclude() throws ConfigurationException {
        URL url = getClass().getResource("/include.properties");
        PropertiesConfiguration pc = new PropertiesConfiguration(url);

        Assert.assertEquals("valid", pc.getString("includedProp"));
    }

    @Test
    public void testDollar() throws ConfigurationException {
        URL url = getClass().getResource("/dollar.properties");
        PropertiesConfiguration pc = new PropertiesConfiguration(url);

        Assert.assertEquals("valid", pc.getString("dollarProp"));

    }

    @Test
    public void refreshTest() throws ConfigurationException {
        URL url = getClass().getResource("/test.properties");
        PropertiesConfiguration pc = new PropertiesConfiguration(url);

        // listen for updates on prop1
        pc.registerForPropertyChange(this, "prop1");
        pc.registerForPropertyChange(this, "prop2");
        Assert.assertTrue(pc.getListenedForProperties().contains("prop1"));
        Assert.assertTrue(pc.getListenedForProperties().contains("prop2"));

        // set a prop
        pc.setProperty("prop1", "newval");

        // is the callback and value ok?
        Assert.assertEquals(1, updatedProperty.size());
        Assert.assertTrue("updatedProperty does not contain prop1", updatedProperty.contains("prop1"));
        Assert.assertEquals("newval", pc.getString("prop1"));
        Assert.assertEquals(PropertyConfigurationChangedListenerWrapper.Status.OK, pc.getAllProps().get("prop1")
                .getListeners().get(0).getStatus());

        // reset
        updatedProperty.clear();
        pc.refresh();

        // is the callback and value ok?
        Assert.assertEquals(1, updatedProperty.size());
        Assert.assertTrue("updatedProperty does not contain prop1", updatedProperty.contains("prop1"));
        Assert.assertEquals("een", pc.getString("prop1"));
        Assert.assertEquals(PropertyConfigurationChangedListenerWrapper.Status.OK, pc.getAllProps().get("prop1")
                .getListeners().get(0).getStatus());

        // reset
        updatedProperty = null;
        pc.refresh();

    }

}
