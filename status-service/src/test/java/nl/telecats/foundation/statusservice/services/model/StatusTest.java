package nl.telecats.foundation.statusservice.services.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import junit.framework.Assert;

import org.junit.Test;

public class StatusTest {

    @Test
    public void testAndBaseOK() {
        Assert.assertEquals(Status.OK, Status.OK.and(Status.OK));
        Assert.assertEquals(Status.UNKNOWN, Status.OK.and(Status.UNKNOWN));
        Assert.assertEquals(Status.WARNING, Status.OK.and(Status.WARNING));
        Assert.assertEquals(Status.CRITICAL, Status.OK.and(Status.CRITICAL));

    }

    @Test
    public void testAndBaseUnknown() {

        Assert.assertEquals(Status.UNKNOWN, Status.UNKNOWN.and(Status.OK));
        Assert.assertEquals(Status.UNKNOWN, Status.UNKNOWN.and(Status.UNKNOWN));
        Assert.assertEquals(Status.WARNING, Status.UNKNOWN.and(Status.WARNING));
        Assert.assertEquals(Status.CRITICAL, Status.UNKNOWN.and(Status.CRITICAL));
    }

    @Test
    public void testAndBaseWarning() {

        Assert.assertEquals(Status.WARNING, Status.WARNING.and(Status.OK));
        Assert.assertEquals(Status.WARNING, Status.WARNING.and(Status.UNKNOWN));
        Assert.assertEquals(Status.WARNING, Status.WARNING.and(Status.WARNING));
        Assert.assertEquals(Status.CRITICAL, Status.WARNING.and(Status.CRITICAL));
    }

    @Test
    public void testAndBaseCritical() {

        Assert.assertEquals(Status.CRITICAL, Status.CRITICAL.and(Status.OK));
        Assert.assertEquals(Status.CRITICAL, Status.CRITICAL.and(Status.UNKNOWN));
        Assert.assertEquals(Status.CRITICAL, Status.CRITICAL.and(Status.WARNING));
        Assert.assertEquals(Status.CRITICAL, Status.CRITICAL.and(Status.CRITICAL));

    }

    @Test
    public void compareTestBaseCritical() {

        Assert.assertTrue(Status.CRITICAL.compareTo(Status.CRITICAL) == 0);
        Assert.assertTrue(Status.CRITICAL.compareTo(Status.WARNING) < 0);
        Assert.assertTrue(Status.CRITICAL.compareTo(Status.UNKNOWN) < 0);
        Assert.assertTrue(Status.CRITICAL.compareTo(Status.OK) < 0);
    }

    @Test
    public void compareTestBaseWarning() {

        Assert.assertTrue(Status.WARNING.compareTo(Status.CRITICAL) > 0);
        Assert.assertTrue(Status.WARNING.compareTo(Status.WARNING) == 0);
        Assert.assertTrue(Status.WARNING.compareTo(Status.UNKNOWN) < 0);
        Assert.assertTrue(Status.WARNING.compareTo(Status.OK) < 0);
    }

    @Test
    public void compareTestBaseUnknown() {

        Assert.assertTrue(Status.UNKNOWN.compareTo(Status.CRITICAL) > 0);
        Assert.assertTrue(Status.UNKNOWN.compareTo(Status.WARNING) > 0);
        Assert.assertTrue(Status.UNKNOWN.compareTo(Status.UNKNOWN) == 0);
        Assert.assertTrue(Status.UNKNOWN.compareTo(Status.OK) < 0);
    }

    @Test
    public void compareTestBaseOK() {

        Assert.assertTrue(Status.OK.compareTo(Status.CRITICAL) > 0);
        Assert.assertTrue(Status.OK.compareTo(Status.WARNING) > 0);
        Assert.assertTrue(Status.OK.compareTo(Status.UNKNOWN) > 0);
        Assert.assertTrue(Status.OK.compareTo(Status.OK) == 0);
    }

    @Test
    public void sort() {
        List<Status> l = new ArrayList<Status>();
        l.add(Status.WARNING);
        l.add(Status.OK);
        l.add(Status.CRITICAL);
        l.add(Status.WARNING);
        Collections.sort(l);
        Assert.assertEquals(Status.CRITICAL, l.get(0));
        Assert.assertEquals(Status.WARNING, l.get(1));
        Assert.assertEquals(Status.WARNING, l.get(2));
        Assert.assertEquals(Status.OK, l.get(3));
    }
}
