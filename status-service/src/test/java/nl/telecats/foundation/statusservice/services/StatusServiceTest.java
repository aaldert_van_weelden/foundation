package nl.telecats.foundation.statusservice.services;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;
import nl.telecats.foundation.statusservice.services.mock.MockServableService;
import nl.telecats.foundation.statusservice.services.model.Status;
import nl.telecats.foundation.statusservice.services.model.StatusDetail;
import nl.telecats.foundation.statusservice.services.model.StatusDetailImpl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:foundation-status-service-context-test.xml" })
public class StatusServiceTest {

    @Autowired
    private StatusService statusServiceImpl;

    @Autowired
    private MockServableService details;

    @Test
    public void testInit() {
        Assert.assertNotNull(statusServiceImpl);
        Assert.assertNotNull(details);
    }

    @Test
    public void testOKStatus() {
        List<StatusDetail> sdl = new ArrayList<StatusDetail>();
        sdl.add(new StatusDetailImpl("", "", Status.OK));
        sdl.add(new StatusDetailImpl("", "", Status.OK));
        sdl.add(new StatusDetailImpl("", "", Status.OK));

        testStatus(sdl, Status.OK);
    }

    @Test
    public void testUNKNOWNStatus() {
        List<StatusDetail> sdl = new ArrayList<StatusDetail>();
        sdl.add(new StatusDetailImpl("", "", Status.UNKNOWN));
        sdl.add(new StatusDetailImpl("", "", Status.OK));
        sdl.add(new StatusDetailImpl("", "", Status.OK));

        testStatus(sdl, Status.UNKNOWN);

    }

    @Test
    public void testWARNINGStatus() {
        List<StatusDetail> sdl = new ArrayList<StatusDetail>();
        sdl.add(new StatusDetailImpl("", "", Status.OK));
        sdl.add(new StatusDetailImpl("", "", Status.WARNING));
        sdl.add(new StatusDetailImpl("", "", Status.OK));

        testStatus(sdl, Status.WARNING);

        sdl.add(new StatusDetailImpl("", "", Status.UNKNOWN));

        testStatus(sdl, Status.WARNING);

    }

    @Test
    public void testCRITICALStatus() {
        List<StatusDetail> sdl = new ArrayList<StatusDetail>();
        sdl.add(new StatusDetailImpl("", "", Status.OK));
        sdl.add(new StatusDetailImpl("", "", Status.CRITICAL));
        sdl.add(new StatusDetailImpl("", "", Status.OK));

        testStatus(sdl, Status.CRITICAL);

        sdl.add(new StatusDetailImpl("", "", Status.WARNING));

        testStatus(sdl, Status.CRITICAL);

        sdl.add(new StatusDetailImpl("", "", Status.UNKNOWN));

        testStatus(sdl, Status.CRITICAL);

    }

    private void testStatus(List<StatusDetail> sdl, Status st) {
        details.setStatusDetails(sdl);
        Assert.assertEquals(statusServiceImpl.getStatus().getStatus(), st);

    }

}
