package nl.telecats.foundation.statusservice.services.mock;

import java.util.ArrayList;
import java.util.List;

import nl.telecats.foundation.statusservice.services.StatusDetailServable;
import nl.telecats.foundation.statusservice.services.model.StatusDetail;

/**
 * Mock implementation of StatusDetailServable service; returns an empty list
 * upon inquiry.
 * 
 * @author martijnvg
 * 
 */
public class MockServableService implements StatusDetailServable {

    private List<StatusDetail> sds = new ArrayList<StatusDetail>();

    @Override
    public List<StatusDetail> getStatusDetails() {
        return sds;
    }

    public void setStatusDetails(List<StatusDetail> sds) {
        this.sds = sds;
    }
}
