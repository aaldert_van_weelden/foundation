package nl.telecats.foundation.statusservice.services;

import java.util.List;

import nl.telecats.foundation.statusservice.services.model.StatusDetail;

/**
 * This interface should be implemented by beans, services and other entities that offer their status to a StatusService
 * bean/service. It provides the possibility to offer a list of StatusDetails.
 * 
 * @author Martijn van Ginkel
 * @see StatusService, StatusServiceImpl
 * 
 * @deprecated use StatusDetailServableV2
 */
public interface StatusDetailServable {

    /**
     * Retrieve the list of StatusDetails
     * 
     * @return The list of StatusDetails. Can be empty but should not be null.
     */
    public List<StatusDetail> getStatusDetails();

}
