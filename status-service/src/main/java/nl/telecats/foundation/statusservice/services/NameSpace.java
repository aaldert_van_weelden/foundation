package nl.telecats.foundation.statusservice.services;

public class NameSpace {

    public static final String nameSpace = "http://www.telecats.nl/nl.telecats.foundation.status-service";

    private NameSpace() {
    }
}
