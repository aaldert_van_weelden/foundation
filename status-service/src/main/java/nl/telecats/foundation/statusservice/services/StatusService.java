package nl.telecats.foundation.statusservice.services;

import javax.jws.WebMethod;
import javax.jws.WebResult;
import javax.jws.WebService;

import nl.telecats.foundation.statusservice.services.model.StatusObject;

/**
 * Interface definition of the StatusService.
 * 
 * @author Martijn van Ginkel
 * 
 */
@WebService(serviceName = StatusService.serviceName, targetNamespace = NameSpace.nameSpace)
public interface StatusService {

    public static final String serviceName = "StatusService";

    /**
     * Retrieve the status of the service as a StatusObject
     * 
     * @return The StatusObject that represents the status of the implementing
     *         service
     */
    @WebMethod(operationName = "getStatus", action = "getStatus")
    @WebResult(name = "getStatusResult")
    public StatusObject getStatus();

}
