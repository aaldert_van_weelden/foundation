package nl.telecats.foundation.statusservice.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jws.WebService;

import nl.telecats.foundation.statusservice.services.model.Status;
import nl.telecats.foundation.statusservice.services.model.StatusDetail;
import nl.telecats.foundation.statusservice.services.model.StatusDetailServableV2;
import nl.telecats.foundation.statusservice.services.model.StatusObject;
import nl.telecats.foundation.statusservice.services.model.StatusObjectImpl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

/**
 * This class provides a partial implementation of the StatusService. It's init
 * method queries the ApplicationContext for beans that implement
 * {@link StatusDetailsServable} and stores it in an internal map. The
 * getStatusDetails() method queries all the stored services for their
 * StatusDetails, and adds them to the result.
 * 
 * @author Martijn van Ginkel
 * 
 */
@WebService(serviceName = StatusService.serviceName, targetNamespace = NameSpace.nameSpace)
public abstract class AbstractStatusService implements StatusService {

    private final Log logger = LogFactory.getLog(AbstractStatusService.class);

    @Autowired
    private ApplicationContext applicationContext;

    /**
     * The status of this method is the <code>and</code> of the {@link Status}
     * of all statusdetails of all the StatusServable beans; the description
     * contains the shortDescription of all statusDetails.
     * 
     * @see nl.telecats.foundation.statusservice.services.StatusService#getStatus()
     */
    @Override
    final public StatusObject getStatus() {

        StatusObjectImpl serverStatus = getSpecificStatus();

        String description = "";

        // add status of services in the WAR to server status details
        serverStatus.getStatusDetails().addAll(getServiceDetails());

        // gather all the short descriptions from all details, and determine
        // overal status
        // The order is:
        // CRITICAL
        // WARNING
        // UNKNOWN
        // OK
        Collections.sort(serverStatus.getStatusDetails());
        Status overallStatus = Status.OK;
        Map<Status, Integer> count = new HashMap<Status, Integer>();
        for (StatusDetail d : serverStatus.getStatusDetails()) {
            countAdd(count, d.getStatus());
            overallStatus = overallStatus.and(d.getStatus());
        }
        description = "[" + overallStatus + "]: " + countGet(count, Status.OK) + " OK, "
                + countGet(count, Status.WARNING) + " WARN, " + countGet(count, Status.CRITICAL) + " CRIT, "
                + countGet(count, Status.UNKNOWN) + " UNKNOWN";

        // add short description to server status
        serverStatus.setShortDescription(description);

        // set the overall status
        serverStatus.setStatus(overallStatus);

        return serverStatus;
    }

    private int countGet(Map<Status, Integer> count, Status status) {
        if (count.get(status) == null) {
            return 0;
        } else {
            return count.get(status);
        }
    }

    private void countAdd(Map<Status, Integer> count, Status status) {
        if (count.get(status) == null) {
            count.put(status, 1);
        } else {
            count.put(status, count.get(status) + 1);
        }
    }

    /**
     * Retrieve the overall list of StatusDetails of all StatusServable beans
     * 
     * Retrieve the {@link StatusDetailServable} beans from the
     * applicationContext.
     * 
     * @return The complete list of StatusDetails (can be empty but is never
     *         null)
     */
    private List<StatusDetail> getServiceDetails() {

        List<StatusDetail> details = new ArrayList<StatusDetail>();

        String[] statusBeans = applicationContext.getBeanNamesForType(StatusDetailServable.class);

        for (String beanName : statusBeans) {
            StatusDetailServable sds = (StatusDetailServable) applicationContext.getBean(beanName);
            details.addAll(sds.getStatusDetails());
        }

        // also support StatusDetailServableV2
        String[] statusBeansV2 = applicationContext.getBeanNamesForType(StatusDetailServableV2.class);

        for (String beanName : statusBeansV2) {
            StatusDetailServableV2 sds = (StatusDetailServableV2) applicationContext.getBean(beanName);
            details.addAll(sds.getStatusDetails());
        }

        return details;
    }

    /**
     * Retrieve the specific status of this serivce
     * 
     * @return The specific status
     */
    abstract protected StatusObjectImpl getSpecificStatus();

}
