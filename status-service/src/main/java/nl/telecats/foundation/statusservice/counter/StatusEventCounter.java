package nl.telecats.foundation.statusservice.counter;

/**
 * Class for counting events in a given period. The period to count for is set
 * at construction time, and can be modified ??
 * 
 * @author eelco
 * 
 */
public class StatusEventCounter {

    /**
     * Create a event counter, with the given interval in seconds
     * 
     * @param interval
     *            for how many seconds events are considered in the count
     */
    public StatusEventCounter(int interval) {
    }

    /**
     * Count an event
     */
    public void count() {

    }

    /**
     * Return how many event are counted in the interval
     * 
     * @return
     */
    public int getCount() {
        return 1;
    }
}
