package nl.telecats.foundation.statusservice.services;

import javax.jws.WebService;

import nl.telecats.foundation.statusservice.services.model.Status;
import nl.telecats.foundation.statusservice.services.model.StatusDetailImpl;
import nl.telecats.foundation.statusservice.services.model.StatusObjectImpl;

/**
 * This class serves as a standalone webservice that extends the
 * {@link AbstractStatusService}, thus querying all available StatusServable
 * beans and adds some platform information such as memory and threads.
 * 
 * <br/>
 * This class can be directly exposed by a server project
 * 
 * @author Martijn van Ginkel
 * 
 */
public class StatusServiceImpl extends AbstractStatusService implements StatusService {

    private static final int THREADS_WARN_LEVEL = 150;
    private static final int THREADS_CRITICAL_LEVEL = 200;
    private static final int RAM_CRITICAL_PERCENTAGE = 5;
    private static final int RAM_WARN_PERCENTAGE = 10;

    public StatusServiceImpl() {
    }

    /**
     * The specific status consists of the available memory and the number of
     * threads.
     * 
     * @see nl.telecats.foundation.statusservice.services.AbstractStatusService#getSpecificStatus()
     */
    @Override
    protected StatusObjectImpl getSpecificStatus() {

        StatusObjectImpl status = new StatusObjectImpl();

        long freeRam = Runtime.getRuntime().freeMemory() / (1024 * 1024);
        long maxRam = Runtime.getRuntime().maxMemory() / (1024 * 1024);
        long totalRam = Runtime.getRuntime().totalMemory() / (1024 * 1024);

        int threads = Thread.activeCount();

        // create detailed status for the service
        StatusDetailImpl ramDetail = new StatusDetailImpl();
        long freeRamPercentage = ((freeRam + (maxRam - totalRam)) * 100) / maxRam;
        if (freeRamPercentage < RAM_WARN_PERCENTAGE) {
            ramDetail.setStatus(Status.WARNING);
        } else if (freeRamPercentage < RAM_CRITICAL_PERCENTAGE) {
            ramDetail.setStatus(Status.CRITICAL);
        } else {
            ramDetail.setStatus(Status.OK);
        }
        ramDetail.setShortDescription("Mem free/total/max: " + freeRam + "M/" + totalRam + "M/" + maxRam + "M (free: "
                + freeRamPercentage + "%)");
        status.getStatusDetails().add(ramDetail);

        StatusDetailImpl threadDetail = new StatusDetailImpl();
        threadDetail.setShortDescription("Threads: " + threads);
        if (threads > THREADS_CRITICAL_LEVEL) {
            threadDetail.setStatus(Status.CRITICAL);
        } else if (threads > THREADS_WARN_LEVEL) {
            threadDetail.setStatus(Status.WARNING);
        } else {
            threadDetail.setStatus(Status.OK);
        }
        status.getStatusDetails().add(threadDetail);

        // will be set in AbstractStatusService, based on all the gathered
        // details
        status.setStatus(Status.UNKNOWN);

        return status;
    }
}
