<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <groupId>nl.telecats.foundation.status-service</groupId>
    <artifactId>status-service</artifactId>
    <version>2.7.8-SNAPSHOT</version>
    <packaging>jar</packaging>
    <url>http://www.telecats.nl</url>
    <name>status-service</name>
    <description>Description placeholder for status-service</description>

	<parent>
        <groupId>nl.telecats.foundation</groupId>
        <artifactId>foundation-core-apis</artifactId>
        <version>2.7.8-SNAPSHOT</version>
	</parent>
    
    <properties>
        <!-- 3rd party -->
        <cxf.version>2.1.5</cxf.version>
        <junit.version>4.8.1</junit.version>
        <log4j.version>1.2.15</log4j.version>
        <maven-antrun-plugin.version>1.3</maven-antrun-plugin.version>
        <maven-eclipse-plugin.version>2.7</maven-eclipse-plugin.version>
        <slf4j.version>1.6.3</slf4j.version>
        <spring.version>3.0.2.RELEASE</spring.version>

        <!-- telecats -->
        <releaseInfoPlugin.version>1.0</releaseInfoPlugin.version>
    </properties>

    <build>
        <plugins>
            <plugin>
                <groupId>nl.telecats.maven.plugins</groupId>
                <artifactId>releaseInfoPlugin</artifactId>
                <version>${releaseInfoPlugin.version}</version>
                <executions>
                    <execution>
                        <phase>prepare-package</phase>
                        <goals>
                            <goal>generaterelease</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-eclipse-plugin</artifactId>
                <version>${maven-eclipse-plugin.version}</version>
                <configuration>
                    <wtpversion>2.0</wtpversion>
                </configuration>
            </plugin>
            <plugin>
                <artifactId>maven-antrun-plugin</artifactId>
                <version>${maven-antrun-plugin.version}</version>
                <executions>
                    <execution>
                        <phase>compile</phase>
                        <goals>
                            <goal>run</goal>
                        </goals>
                        <configuration>
                            <tasks>
                                <copy todir="${basedir}/target/classes">
                                    <fileset dir="${basedir}/src/main/java">
                                        <include name="**/model/*.java" />
                                        <include name="**/*.properties" />
                                        <include name="**/*.gwt.xml" />
                                        <include name="**/*.hbm.xml" />
                                    </fileset>
                                </copy>
                            </tasks>
                        </configuration>
                    </execution>
                    <execution>
                        <id>fix_classpath</id>
                        <phase>compile</phase>
                        <goals>
                            <goal>run</goal>
                        </goals>
                        <configuration>
                            <tasks>
                                <echo message="fix_classpath" />
                                <replace dir=".">
                                    <include name=".classpath" />
                                    <replacetoken><![CDATA[ <classpathentry excluding="**" kind="src" output="target/classes" path="src/main/resources" />]]></replacetoken>
                                    <replacevalue><![CDATA[ <classpathentry kind="src" output="target/classes" path="src/main/resources" />]]></replacevalue>
                                </replace>
                                <replace dir=".">
                                    <include name=".classpath" />
                                    <replacetoken><![CDATA[ <classpathentry excluding="**" kind="src" output="target/test-classes" path="src/test/resources" />]]></replacetoken>
                                    <replacevalue><![CDATA[ <classpathentry kind="src" output="target/test-classes" path="src/test/resources" />]]></replacevalue>
                                </replace>
                            </tasks>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <artifactId>maven-war-plugin
                </artifactId>
                <configuration>
                    <warSourceDirectory>${basedir}/src/main/webapp</warSourceDirectory>
                </configuration>
            </plugin>
        </plugins>
    </build>

    <dependencies>
        <dependency>
            <groupId>nl.telecats.foundation.status-service</groupId>
            <artifactId>status-service-model</artifactId>
            <version>2.7.8-SNAPSHOT</version>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-api</artifactId>
            <version>${slf4j.version}</version>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-log4j12</artifactId>
            <version>${slf4j.version}</version>
        </dependency>
        <dependency>
            <groupId>log4j</groupId>
            <artifactId>log4j</artifactId>
            <version>${log4j.version}</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-core</artifactId>
            <version>${spring.version}</version>
            <type>jar</type>
            <optional>false</optional>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-aop</artifactId>
            <version>${spring.version}</version>
            <type>jar</type>
            <optional>false</optional>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-test</artifactId>
            <version>${spring.version}</version>
            <type>jar</type>
            <optional>false</optional>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-orm</artifactId>
            <version>${spring.version}</version>
            <type>jar</type>
            <optional>false</optional>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-web</artifactId>
            <version>${spring.version}</version>
            <type>jar</type>
            <optional>false</optional>
        </dependency>
        <dependency>
            <groupId>org.apache.cxf</groupId>
            <artifactId>cxf-rt-frontend-jaxws
            </artifactId>
            <version>${cxf.version}</version>
        </dependency>
        <dependency>
            <groupId>org.apache.cxf</groupId>
            <artifactId>cxf-rt-transports-http
            </artifactId>
            <version>${cxf.version}</version>
        </dependency>
        <dependency>
            <groupId>org.apache.cxf</groupId>
            <artifactId>cxf-rt-transports-http-jetty
            </artifactId>
            <version>${cxf.version}</version>
        </dependency>
        <dependency>
            <groupId>org.apache.cxf</groupId>
            <artifactId>cxf-rt-databinding-aegis
            </artifactId>
            <version>${cxf.version}</version>
        </dependency>
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>${junit.version}</version>
            <scope>test</scope>
        </dependency>
        <!-- <dependency> -->
        <!-- <groupId>your.model.groupId</groupId> -->
        <!-- <artifactId>yourModelArtifactId</artifactId> -->
        <!-- <version>yourModelVersion</version> -->
        <!-- </dependency> -->
    </dependencies>

    <profiles>
        <profile>
            <activation>
                <property>
                    <name>hudson</name>
                </property>
            </activation>
            <build>
                <plugins>
                    <plugin>
                        <artifactId>maven-antrun-plugin</artifactId>
                        <version>${maven-antrun-plugin.version}</version>
                        <executions>
                            <execution>
                                <phase>compile</phase>
                                <goals>
                                    <goal>run</goal>
                                </goals>
                                <configuration>
                                    <tasks>
                                        <copy todir="${basedir}/target/classes">
                                            <fileset dir="${basedir}/src/main/java">
                                                <include name="**/client/**/*.java" />
                                                <include name="**/model/*.java" />
                                                <include name="**/*.properties" />
                                                <include name="**/*.gwt.xml" />
                                                <include name="**/*.hbm.xml" />
                                            </fileset>
                                        </copy>
                                    </tasks>
                                </configuration>
                            </execution>
                        </executions>
                    </plugin>

                    <plugin>
                        <groupId>nl.telecats.maven.plugins</groupId>
                        <artifactId>releaseInfoPlugin</artifactId>
                        <version>${releaseInfoPlugin.version}</version>
                        <executions>
                            <execution>
                                <phase>compile</phase>
                                <goals>
                                    <goal>generaterelease</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
        </profile>
    </profiles>
</project>
