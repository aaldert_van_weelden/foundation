package nl.telecats.foundation.statusservice.services.model;

/**
 * Status represents the service status based on Nagios status
 * 
 * @author martijnvg
 * 
 */
public enum Status {

    // status based on nagios, in natural ordering of importance
    CRITICAL, WARNING, UNKNOWN, OK;

    /**
     * Logically AND 2 statusses, based on priority the most "critical" status
     * is returned. The order is:
     * <ul>
     * <li>{@link #CRITICAL}
     * <li>{@link #WARNING}
     * <li>{@link #UNKNOWN}
     * <li>{@link #OK}
     * </ul>
     * 
     * @param status
     * @return
     */
    public Status and(Status status) {
        if (this.compareTo(status) <= 0) {
            return this;
        } else {
            return status;
        }
    }

}
