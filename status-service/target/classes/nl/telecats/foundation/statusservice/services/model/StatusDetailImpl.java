package nl.telecats.foundation.statusservice.services.model;

import java.io.Serializable;

/**
 * Describes one of the aspects of a service status.
 * 
 * @author martijnvg
 * 
 */
public class StatusDetailImpl implements StatusDetail, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 8899051010714568509L;
    private String data;
    private String shortDescription;
    private Status status = Status.UNKNOWN;

    public StatusDetailImpl() {

    }

    public StatusDetailImpl(String data, String shortDescription, Status status) {
        super();
        this.data = data;
        this.shortDescription = shortDescription;
        this.status = status;
    }

    /**
     * @return the shortDescription
     */
    @Override
    public String getShortDescription() {
        return shortDescription;
    }

    /**
     * @param shortDescription
     *            the shortDescription to set
     */
    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    /**
     * @return the status
     */
    @Override
    public Status getStatus() {
        return status;
    }

    /**
     * @param status
     *            the status to set
     */
    public void setStatus(Status status) {
        this.status = status;
    }

    /**
     * @param data
     *            the data to set
     */
    public void setData(String data) {
        this.data = data;
    }

    /**
     * @return the data
     */
    @Override
    public String getData() {
        return data;
    }

    @Override
    public int compareTo(StatusDetail other) {
        return this.status.compareTo(other.getStatus());
    }

}
