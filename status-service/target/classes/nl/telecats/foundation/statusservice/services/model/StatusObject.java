package nl.telecats.foundation.statusservice.services.model;

import java.util.List;

public interface StatusObject {

    /**
     * @return the status
     */
    public Status getStatus();

    /**
     * @return the shortDescription
     */
    public String getShortDescription();

    /**
     * @return the statusDetails
     */
    public List<StatusDetail> getStatusDetails();

}
