package nl.telecats.foundation.model.audit.hibernate;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import nl.telecats.foundation.commons.util.DateTimeUtil;
import nl.telecats.foundation.model.audit.AuditActionEnum;
import nl.telecats.foundation.model.audit.AuditLogEntry;
import nl.telecats.foundation.model.audit.AuditLogWriter;
import nl.telecats.foundation.model.audit.DefaultAuditLogWriter;
import nl.telecats.foundation.model.audit.EnhancedAuditablePersistentObject;
import nl.telecats.foundation.model.audit.SimpleAuditablePersistentObject;

import org.apache.commons.lang.StringUtils;
import org.hibernate.EmptyInterceptor;
import org.hibernate.type.Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * Interceptor writing audit log for modifications to instances of EnhancedAuditablePersistentObject.
 * <p/>
 * This implementation will use the DefaultAuditLogWriter by default.
 * 
 * @author raymond
 * 
 * @see EnhancedAuditablePersistentObject, AuditLogWriter, DefaultAuditLogWriter
 * 
 */
public class AuditInterceptorHibernateImpl extends EmptyInterceptor {
    private static final long serialVersionUID = -4485161172679364147L;

    private static final String ANONYMOUS = "anonymous";

    private static final Logger log = LoggerFactory.getLogger(AuditInterceptorHibernateImpl.class);

    private List<AuditLogEntry> pendingAuditLogEntries = new ArrayList<AuditLogEntry>();

    private AuditLogWriter auditLogWriter = new DefaultAuditLogWriter();

    public void setAuditLogWriter(AuditLogWriter auditLogWriter) {
        this.auditLogWriter = auditLogWriter;
    }

    public AuditLogWriter getAuditLogWriter() {
        return auditLogWriter;
    }

    @Override
    public boolean onFlushDirty(Object entity, Serializable id, Object[] currentState, Object[] previousState,
            String[] propertyNames, Type[] types) {

        // in this method we can detect modifications
        log.debug("onFlushDirty:entity:" + entity + ", id:" + id + ",curr:" + currentState + ", prev:" + previousState);
        if (entity instanceof EnhancedAuditablePersistentObject || entity instanceof SimpleAuditablePersistentObject) {
            prepareAuditLogEntry(AuditActionEnum.MODIFICATION, entity, id, currentState, previousState, propertyNames,
                    types);
        } else {
            auditLogWriter.logUnauditedModification("Modifying entity, but entity not an instanceof '"
                    + EnhancedAuditablePersistentObject.class.getSimpleName() + "' or '"
                    + SimpleAuditablePersistentObject.class.getSimpleName()
                    + "', modification wont be logged to auditLog:" + entity.getClass().getSimpleName() + "," + id);
        }
        return false;
    }

    @Override
    public void onDelete(Object entity, Serializable id, Object[] state, String[] propertyNames, Type[] types) {
        log.debug("onDelete:entity:" + entity + ", id:" + id + ",state:" + state + ", prop:" + propertyNames
                + ", type:" + types);
        prepareAuditLogEntry(AuditActionEnum.DELETE, entity, id, null, state, propertyNames, types);
        super.onDelete(entity, id, state, propertyNames, types);
    }

    @Override
    public boolean onSave(Object entity, Serializable id, Object[] state, String[] propertyNames, Type[] types) {
        log.debug("onSave:entity:" + entity + ", id:" + id + ",state:" + state + ", prop:" + propertyNames + ", type:"
                + types);
        if (entity instanceof EnhancedAuditablePersistentObject) {
            EnhancedAuditablePersistentObject mm = (EnhancedAuditablePersistentObject) entity;

            // set creation metadata for newObjects AND set modification
            // metadata for modified objects
            String activeUsername = deterimeUsernameOfActiveUser();
            String now = DateTimeUtil.getInstance().getNowIso8601();
            boolean newObject = mm != null && StringUtils.isEmpty(mm.getCreatedBy());// TODO check this, may not be
                                                                                     // fullproof
            boolean modifiedObject = newObject || pendingAuditLogEntries.size() > 0;
            log.debug("newObject      : " + newObject);
            log.debug("modifiedObject : " + modifiedObject);
            log.debug("activeUsername : " + activeUsername);
            log.debug("now            : " + now);
            if (newObject) {
                prepareAuditLogEntry(AuditActionEnum.CREATE, entity, id, state, null, propertyNames, types);
            }
            for (int n = 0; n < propertyNames.length; n++) {
                String propName = propertyNames[n];
                if (newObject && propName.equals(EnhancedAuditablePersistentObject.CREATED_BY_FIELD)) {
                    state[n] = activeUsername;
                } else if (newObject && propName.equals(EnhancedAuditablePersistentObject.CREATED_ON_FIELD)) {
                    state[n] = now;
                } else if (modifiedObject && propName.equals(EnhancedAuditablePersistentObject.LAST_MODIFIED_BY_FIELD)) {
                    state[n] = activeUsername;
                } else if (modifiedObject && propName.equals(EnhancedAuditablePersistentObject.LAST_MODIFIED_ON_FIELD)) {
                    state[n] = now;
                }
            }
            return true;
        }

        if (entity instanceof SimpleAuditablePersistentObject) {
            SimpleAuditablePersistentObject mm = (SimpleAuditablePersistentObject) entity;

            // set creation metadata for newObjects AND set modification
            // metadata for modified objects
            String activeUsername = deterimeUsernameOfActiveUser();
            String now = DateTimeUtil.getInstance().getNowIso8601();
            boolean newObject = (mm != null && mm.getInternalVersion() == null);// TODO check this, may not be fullproof
            boolean modifiedObject = newObject || pendingAuditLogEntries.size() > 0;
            log.debug("newObject      : " + newObject);
            log.debug("modifiedObject : " + modifiedObject);
            log.debug("activeUsername : " + activeUsername);
            log.debug("now            : " + now);
            if (newObject) {
                prepareAuditLogEntry(AuditActionEnum.CREATE, entity, id, state, null, propertyNames, types);
            }

            return true;
        }
        auditLogWriter.logUnauditedModification("Modifying entity, but entity not an instanceof '"
                + EnhancedAuditablePersistentObject.class.getSimpleName()
                + "', modification wont be logged to auditLog:" + entity.getClass().getSimpleName() + "," + id);
        return false;

    }

    @Override
    public void postFlush(@SuppressWarnings("rawtypes") Iterator entities) {
        log.debug("postFlush");
        flusPendingAuditLogs();
        super.postFlush(entities);
    }

    /**
     * Schedule modifications, should be saved after they are in the database
     * 
     * 
     * @param entity
     * @param id
     * @param currentState
     * @param previousState
     * @param propertyNames
     * @param types
     */
    private void prepareAuditLogEntry(AuditActionEnum action, Object entity, Serializable id, Object[] currentState,
            Object[] previousState, String[] propertyNames, Type[] types) {
        AuditLogEntry entry = new AuditLogEntry(action, entity, id, currentState, previousState, propertyNames, types,
                deterimeUsernameOfActiveUser());
        log.debug("prepareAuditLogEntry:" + entry.toString() + ", entry count:" + pendingAuditLogEntries.size());
        if (entry.isModification() && !pendingAuditLogEntries.contains(entry)) {
            // don't log when nothing is changed
            // don't log same change twice
            pendingAuditLogEntries.add(entry);
        } else {
            log.trace("duplicate or no changes, skipping:" + entity.toString());
        }
    }

    private void flusPendingAuditLogs() {
        // determine size at this time, so 'new' entries won't be flushed
        int count = pendingAuditLogEntries.size();
        log.debug("flusPendingAuditLogs, count:" + count);
        for (int index = 0; index < count; index++) {
            AuditLogEntry auditLogEntry = pendingAuditLogEntries.get(0);
            AuditActionEnum action = auditLogEntry.getAction();
            if (action == AuditActionEnum.CREATE) {
                auditLogWriter.logCreation(auditLogEntry);
            } else if (action == AuditActionEnum.MODIFICATION) {
                auditLogWriter.logModification(auditLogEntry);
            } else if (action == AuditActionEnum.DELETE) {
                auditLogWriter.logDeletion(auditLogEntry);
            } else {
                log.warn("Unkown auditAction:" + action);
            }
            pendingAuditLogEntries.remove(0);
        }
    }

    private String deterimeUsernameOfActiveUser() {
        try {
            String usernameOfActiveUser = SecurityContextHolder.getContext().getAuthentication().getName();
            return usernameOfActiveUser;
        } catch (Throwable t) {
            log.debug("modification by:" + ANONYMOUS);
            return ANONYMOUS;
        }
    }

}
