package nl.telecats.foundation.model.audit;

import java.io.Serializable;

import nl.telecats.foundation.commons.util.DateTimeUtil;

import org.apache.commons.lang.StringUtils;
import org.hibernate.type.Type;

/**
 * Pojo containing audit modification data.
 * 
 * @author raymond
 * 
 */
public class AuditLogEntry {
    String entityName;
    String stringId;
    String modifier;
    String now;
    String changeLog;
    AuditActionEnum action;

    public AuditLogEntry(AuditActionEnum action, Object entity, Serializable id, Object[] currentState,
            Object[] previousState, String[] propertyNames, Type[] types, String modifier) {
        this.action = action;
        entityName = entity != null ? entity.getClass().getSimpleName() : "null";
        stringId = "" + id;
        this.modifier = modifier;
        now = DateTimeUtil.getInstance().getNowIso8601();
        StringBuffer changeLogSB = new StringBuffer();
        for (int index = 0; index < propertyNames.length; index++) {
            if ((!ignoredField(propertyNames[index]))
                    && ((readValue(currentState, index) == null && readValue(previousState, index) != null)
                            || (readValue(currentState, index) != null && readValue(previousState, index) == null) || (readValue(
                            currentState, index) != null && readValue(previousState, index) != null && !readValue(
                            currentState, index).equals(readValue(previousState, index))))) {

                changeLogSB.append(propertyNames[index] + "(old)=" + readValue(previousState, index) + ","
                        + propertyNames[index] + "(new)=" + readValue(currentState, index) + ",");

            }
        }
        changeLog = changeLogSB.toString();
    }

    public String getEntityName() {
        return entityName;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    public String getStringId() {
        return stringId;
    }

    public void setStringId(String stringId) {
        this.stringId = stringId;
    }

    public String getModifier() {
        return modifier;
    }

    public void setModifier(String modifier) {
        this.modifier = modifier;
    }

    public String getNow() {
        return now;
    }

    public void setNow(String now) {
        this.now = now;
    }

    public String getChangeLog() {
        return changeLog;
    }

    public void setChangeLog(String changeLog) {
        this.changeLog = changeLog;
    }

    public AuditActionEnum getAction() {
        return action;
    }

    public void setAction(AuditActionEnum action) {
        this.action = action;
    }

    private boolean ignoredField(String field) {
        return (field.equals(EnhancedAuditablePersistentObject.CREATED_BY_FIELD)
                || field.equals(EnhancedAuditablePersistentObject.CREATED_ON_FIELD)
                || field.equals(EnhancedAuditablePersistentObject.LAST_MODIFIED_BY_FIELD)
                || field.equals(EnhancedAuditablePersistentObject.LAST_MODIFIED_ON_FIELD)
                || field.equals(EnhancedAuditablePersistentObject.CREATED_ON_FIELD) || field.equals("internalVersion"));
    }

    private Object readValue(Object[] data, int index) {
        if (data != null && data.length > index) {
            return data[index];
        } else {
            return null;
        }

    }

    public boolean isModification() {
        return !StringUtils.isEmpty(changeLog);
    }

    @Override
    public boolean equals(Object obj) {
        return (obj == this || (obj != null && obj instanceof AuditLogEntry && obj.toString().equals(toString())));
    }

    @Override
    public int hashCode() {
        return toString().hashCode();
    }

    @Override
    public String toString() {
        return "[AuditLogEntry] TIMESTAMP=" + now + ", USER=" + modifier + ", ACTION=" + action + ", ENTITY="
                + entityName + ", ID=" + stringId + ", MODIFIED=" + changeLog;
    }
}