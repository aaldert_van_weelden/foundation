package nl.telecats.foundation.model.audit;

import nl.telecats.foundation.model.audit.hibernate.AuditInterceptorHibernateImpl;

public interface EnhancedAuditablePersistentObject {

    public static final String CREATED_BY_FIELD = "createdBy";
    public static final String CREATED_ON_FIELD = "createdOn";
    public static final String LAST_MODIFIED_BY_FIELD = "lastModifiedBy";
    public static final String LAST_MODIFIED_ON_FIELD = "lastModifiedOn";

    /**
     * @see EnhancedAbstractAuditablePersistentObject#setCreatedBy(String)
     * @return creator of this record
     */
    public String getCreatedBy();

    /**
     * set creator of this record <b>Don't call this setter, this setter should only be invoked by the
     * MutationInterceptor</b>
     * 
     * @see AuditInterceptorHibernateImpl
     * 
     * @param createdBy
     */
    public void setCreatedBy(String createdBy);

    /**
     * @see EnhancedAbstractAuditablePersistentObject#setCreatedOn(String)
     * @return
     */
    public String getCreatedOn();

    /**
     * <b>Don't call this setter, this setter should only be invoked by the MutationInterceptor</b>
     * 
     * @see AuditInterceptorHibernateImpl
     * 
     * @param createdOn
     *            ISO 8601 date
     */
    public void setCreatedOn(String createdOn);

    /**
     * @see EnhancedAbstractAuditablePersistentObject#setLastMofifiedBy(String)
     * @return username of most recent modifier
     */
    public String getLastModifiedBy();

    /**
     * Set the username of the user last modified this record
     * 
     * <b>Don't call this setter, this setter should only be invoked by the MutationInterceptor</b>
     * 
     * @see AuditInterceptorHibernateImpl
     * @param lastModifiedBy
     */
    public void setLastModifiedBy(String lastModifiedBy);

    /**
     * @see EnhancedAbstractAuditablePersistentObject#setLastModifiedOn(String)
     * @return most recent modification date
     */
    public String getLastModifiedOn();

    /**
     * Set the date of the modification of this record.
     * <p/>
     * <b>Don't call this setter, this setter should only be invoked by the MutationInterceptor</b>
     * 
     * @see AuditInterceptorHibernateImpl
     * 
     * @param lastModifiedOn
     */
    public void setLastModifiedOn(String lastModifiedOn);
}
