package nl.telecats.foundation.model.audit;

import nl.telecats.foundation.model.audit.hibernate.AuditInterceptorHibernateImpl;
import nl.telecats.foundation.model.entities.PersistentObject;

/**
 * Interface used for plugging in the audit logging with minimal impact. The entity should implement this interface if
 * you only need the logger, not the database tables
 * 
 * @author Aaldert van Weelden
 * @see AuditInterceptorHibernateImpl
 */
public interface SimpleAuditablePersistentObject {

    /**
     * @see PersistentObject
     * @return the internalVersion
     */
    public Integer getInternalVersion();
}
