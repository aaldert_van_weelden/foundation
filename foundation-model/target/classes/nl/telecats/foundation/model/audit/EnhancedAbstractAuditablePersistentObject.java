package nl.telecats.foundation.model.audit;

import nl.telecats.foundation.model.audit.hibernate.AuditInterceptorHibernateImpl;
import nl.telecats.foundation.model.entities.PersistentObject;

/**
 * This class is abstract because it shouldn't be instantiated directy. It should be extended by PersistableObject(s)
 * you like to be audited
 * <p/>
 * Following fields will be updated on modification:
 * <ul>
 * <li/>createdBy
 * <li/>createdOn
 * <li/>lastModifiedBy
 * <li/>lastModifiedOn
 * </ul>
 * 
 * @author raymond
 * @see AuditInterceptorHibernateImpl
 */
public abstract class EnhancedAbstractAuditablePersistentObject extends PersistentObject implements
        EnhancedAuditablePersistentObject {

    private static final long serialVersionUID = 2022656406149938221L;

    private String createdBy;
    private String createdOn;
    private String lastModifiedBy;
    private String lastModifiedOn;

    /*
     * (non-Javadoc)
     * 
     * @see nl.telecats.foundation.model.audit.EnhancedAuditablePersistentObject#getCreatedBy()
     */
    @Override
    public String getCreatedBy() {
        return createdBy;
    }

    /*
     * (non-Javadoc)
     * 
     * @see nl.telecats.foundation.model.audit.EnhancedAuditablePersistentObject#setCreatedBy(java.lang.String)
     */
    @Override
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    /*
     * (non-Javadoc)
     * 
     * @see nl.telecats.foundation.model.audit.EnhancedAuditablePersistentObject#getCreatedOn()
     */
    @Override
    public String getCreatedOn() {
        return createdOn;
    }

    /*
     * (non-Javadoc)
     * 
     * @see nl.telecats.foundation.model.audit.EnhancedAuditablePersistentObject#setCreatedOn(java.lang.String)
     */
    @Override
    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    /*
     * (non-Javadoc)
     * 
     * @see nl.telecats.foundation.model.audit.EnhancedAuditablePersistentObject#getLastModifiedBy()
     */
    @Override
    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    /*
     * (non-Javadoc)
     * 
     * @see nl.telecats.foundation.model.audit.EnhancedAuditablePersistentObject#setLastModifiedBy(java.lang.String)
     */
    @Override
    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    /*
     * (non-Javadoc)
     * 
     * @see nl.telecats.foundation.model.audit.EnhancedAuditablePersistentObject#getLastModifiedOn()
     */
    @Override
    public String getLastModifiedOn() {
        return lastModifiedOn;
    }

    /*
     * (non-Javadoc)
     * 
     * @see nl.telecats.foundation.model.audit.EnhancedAuditablePersistentObject#setLastModifiedOn(java.lang.String)
     */
    @Override
    public void setLastModifiedOn(String lastModifiedOn) {
        this.lastModifiedOn = lastModifiedOn;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        String fqClassName = getClass().toString();
        return fqClassName.substring(fqClassName.lastIndexOf(".")) + "@" + getId();
    }
}
