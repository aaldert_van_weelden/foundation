package nl.telecats.foundation.model.audit;

/**
 * AuditLogWriter's should implement this interface.
 * 
 * @author raymond
 * @see DefaultAuditLogWriter
 */
public interface AuditLogWriter {
    void logCreation(AuditLogEntry auditLogEntry);

    void logModification(AuditLogEntry auditLogEntry);

    void logDeletion(AuditLogEntry auditLogEntry);

    void logUnauditedModification(String details);
}
