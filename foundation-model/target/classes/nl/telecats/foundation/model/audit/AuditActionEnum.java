package nl.telecats.foundation.model.audit;

/**
 * Enum describing modification actions.
 * 
 * @author raymond
 * 
 */
public enum AuditActionEnum {
    CREATE("Create"), MODIFICATION("Modify"), DELETE("Delete");

    private final String name;

    AuditActionEnum(String name) {
        this.name = name;
    }

    /**
     * @return unlocalized name of ths day
     */
    public String getName() {
        return name;
    }

    public String toString() {
        return name;
    }

}
