package nl.telecats.foundation.model.exceptions;

import java.io.Serializable;

/**
 * This exception should be thrown whenever an entity could not be found, but
 * was expected
 * 
 * @author reinier
 * 
 */
public class ObjectNotFoundException extends DatabaseException implements Serializable {

    private String entityClassName; // The classname of the entity
    private Serializable id; // the ID

    /**
     * Hide the empty constructor; use
     * <code>ObjectNotFoundException(String entityClassName, Serializable id)</code>
     * instead.
     */
    @SuppressWarnings("unused")
    private ObjectNotFoundException() {
    }

    /**
     * Create a new ObjectNotFoundException based on the classname and the id of
     * the entity that could not be found.
     * 
     * @param entityClassName
     *            The classname of the entity
     * @param id
     *            The ID that identifies the entity
     */
    public ObjectNotFoundException(String entityClassName, Serializable id) {
        super("Object not found for class '" + entityClassName + "', with id '" + id + "'");
        this.entityClassName = entityClassName;
        this.id = id;
    }

    private static final long serialVersionUID = -5301168909173168071L;

    /**
     * Retrieve the classname of the entity
     * 
     * @return The classname of the entity
     */
    public String getEntityClassName() {
        return entityClassName;
    }

    /**
     * Retrieve the ID of the entity that could not be found
     * 
     * @return The ID of the entity
     */
    public Serializable getId() {
        return id;
    }

    /**
     * An ObjectNotFoundException is equal to another if and only if:
     * <ul>
     * <li>The other ObjectNotFoundException is not null</li>
     * <li>The classnames are not null and the same</li>
     * <li>The IDs are not null and the same</li>
     * </ul>
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (!other.getClass().equals(getClass())) {
            return false;
        }
        ObjectNotFoundException onfe = (ObjectNotFoundException) other;
        if (entityClassName == null && onfe.entityClassName != null || !entityClassName.equals(onfe.entityClassName)) {
            return false;
        }
        if (id == null && onfe.id != null || !id.equals(onfe.id)) {
            return false;
        }
        return true;
    }
}
