package nl.telecats.foundation.model.exceptions;

import java.io.Serializable;

/**
 * Base exception for all database (hibernate) related exceptions
 * 
 * @author reinier
 * 
 */
public abstract class DatabaseException extends Exception implements Serializable {
    private static final long serialVersionUID = 1752275637597102061L;

    public DatabaseException() {
    }

    /**
     * Create a new DatabaseException and set the message
     * 
     * @param msg
     *            The message
     */
    public DatabaseException(String msg) {
        super(msg);
    }

    /**
     * Create a new DatabaseException based on a <code>Throwable</code> instance
     * 
     * @param e
     *            The throwable instance
     */
    public DatabaseException(Throwable e) {
        super(e);
    }

    /**
     * Create a new DatabaseException based on a message and a throwable.
     * 
     * @param msg
     *            The message
     * @param e
     *            The throwable
     */
    public DatabaseException(String msg, Throwable e) {
        super(msg, e);
    }
}
