package nl.telecats.foundation.model.exceptions;

import java.io.Serializable;

/**
 * Exception thrown when a records is updates (or inserted) and a unique constraint is violated.
 * 
 * @author reinier
 * 
 */
public class NotUniqueException extends DatabaseException implements Serializable {
    private static final long serialVersionUID = 3237680911291990404L;

    private String entityClassName;
    private Serializable id;
    private String field;

    /**
     * Hide the empty constructor; use
     * <code>NotUniqueException(String entityClassName, Serializable id, String field, Serializable value)</code>
     * instead.
     */
    @SuppressWarnings("unused")
    public NotUniqueException() {
    }

    /**
     * Construct a {@link NotUniqueException} for the given values
     * 
     * @param entityClassName
     *            The Model class we have dealt with
     * @param id
     *            The id of the existing object of type entityClassName (only on update)
     * @param field
     *            The field which is violated
     * @param value
     *            The value that was tried to update or insert
     */
    public NotUniqueException(String entityClassName, Serializable id, String field, Serializable value) {
        super(constructMessage(entityClassName, id, field, value));
        this.entityClassName = entityClassName;
        this.id = id;
        this.field = field;
    }

    /**
     * Construct the message for this exception based on:
     * 
     * @param entityClassName
     *            The classname of the entity
     * @param id
     *            The ID of the entity
     * @param field
     *            The field that violated the unique constraint
     * @param value
     *            The value of the field
     * @return The message containing the required information
     */
    private static String constructMessage(String entityClassName, Serializable id, String field, Serializable value) {
        String msg = "Uniqueness constraint violated on field '" + field + "' for class '" + entityClassName
                + "', with value '" + value + "'";
        if (id != null) {
            msg += ", object id [" + id + "]";
        }
        return msg;
    }

    /**
     * Create a new NotUniqueException based on an instance of <code>Throwable</code>
     * 
     * @param e
     *            The throwable
     */
    public NotUniqueException(Throwable e) {
        super(e);
    }

    /**
     * Retrieve the classname of the object that was violated
     * 
     * @return The classname of the object that was violated
     */
    public String getEntityClassName() {
        return entityClassName;
    }

    /**
     * Retrieve the ID of the violated entity
     * 
     * @return The id of the existing object for which the field ( {@link #getField()}) was violated
     */
    public Serializable getId() {
        return id;
    }

    /**
     * Retrieve the field that violated the unique constraint
     * 
     * @return The database field that was violated
     */
    public String getField() {
        return field;
    }

    /**
     * A NotUniqueException is equal to another if and only if:
     * <ul>
     * <li>The other NotUniqueException is not null</li>
     * <li>The classnames are not null and the same
     * <li>
     * <li>The IDs are not null and the same</li>
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (!other.getClass().equals(getClass())) {
            return false;
        }
        NotUniqueException onfe = (NotUniqueException) other;
        if (entityClassName == null && onfe.entityClassName != null || !entityClassName.equals(onfe.entityClassName)) {
            return false;
        }
        if (id == null && onfe.id != null || !id.equals(onfe.id)) {
            return false;
        }
        if (field == null && onfe.field != null || !field.equals(onfe.field)) {
            return false;
        }
        return true;
    }
}
