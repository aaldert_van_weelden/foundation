package nl.telecats.foundation.model.exceptions;

public class FileCreationException extends Exception {

    private static final long serialVersionUID = 7628659036505924102L;

    @SuppressWarnings("unused")
    private FileCreationException() {
    }

    public FileCreationException(String message) {
        super(message);
    }
}
