package nl.telecats.foundation.model.util;

import java.util.Enumeration;
import java.util.Properties;

import org.apache.commons.dbcp.BasicDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This util helps you to log your db settings during startup of your applicaiton.
 * 
 * Usage (in your context):
 * 
 * <pre>
 *     &lt;bean class="nl.telecats.foundation.model.util.DBSettingsLogUtil"
 *         init-method="logSettings"&gt;
 *         &lt;property name="hibernateProperties" value="#{hibernateProperties}" /&gt;
 *         &lt;property name="basicDataSource" value="#{dataSource}" /&gt;
 *     &lt;/bean&gt;
 * </pre>
 * 
 * @author raymond
 * 
 */
public class DBSettingsLogUtil {
    private static final Logger logger = LoggerFactory.getLogger(DBSettingsLogUtil.class);

    Properties hibernateProperties;
    BasicDataSource basicDataSource;

    public Properties getHibernateProperties() {
        return hibernateProperties;
    }

    public void setHibernateProperties(Properties hibernateProperties) {
        this.hibernateProperties = hibernateProperties;
    }

    public BasicDataSource getBasicDataSource() {
        return basicDataSource;
    }

    public void setBasicDataSource(BasicDataSource basicDataSource) {
        this.basicDataSource = basicDataSource;
    }

    public void logSettings() {
        // note we use stringbuffer so logging will be one block (not
        // interrupted by other threads)
        StringBuffer sb = new StringBuffer();
        sb.append("\n================================================");
        sb.append("\n[ DATABASE SETTINGS ]");
        if (hibernateProperties != null) {
            sb.append("\nUsing HibernateProperties");
            Enumeration<Object> keys = hibernateProperties.keys();
            while (keys.hasMoreElements()) {
                String key = (String) keys.nextElement();
                sb.append("\n" + key + " = " + hibernateProperties.get(key));
            }
        } else {
            sb.append("\nNO hibernateProperties");
        }

        sb.append("\n------------------------------------------------");

        if (basicDataSource != null) {
            sb.append("\nUsing BasicDataSource");
            sb.append("\nDriverClassName    :" + basicDataSource.getDriverClassName());
            sb.append("\nUrl                :" + basicDataSource.getUrl());
            sb.append("\nUsername           :" + basicDataSource.getUsername());
            sb.append("\nValidationQuery    :" + basicDataSource.getValidationQuery());

            sb.append("\n\nDefaultAutoCommit  :" + basicDataSource.getDefaultAutoCommit());
            sb.append("\nDefaultCatalog     :" + basicDataSource.getDefaultCatalog());
            sb.append("\nDefaultReadOnly    :" + basicDataSource.getDefaultReadOnly());
            sb.append("\nInitialSize        :" + basicDataSource.getInitialSize());
            sb.append("\nLogAbandoned       :" + basicDataSource.getLogAbandoned());
            sb.append("\nMaxActive          :" + basicDataSource.getMaxActive());
            sb.append("\nMaxIdle            :" + basicDataSource.getMaxIdle());
            sb.append("\nMaxOpenPrepStmts   :" + basicDataSource.getMaxOpenPreparedStatements());
            sb.append("\nMaxWait            :" + basicDataSource.getMaxWait());
            sb.append("\nMinEvictable(ms)   :" + basicDataSource.getMinEvictableIdleTimeMillis());
            sb.append("\nMinIdle            :" + basicDataSource.getMinIdle());
            sb.append("\nNumActive          :" + basicDataSource.getNumActive());
            sb.append("\nNumIdle            :" + basicDataSource.getNumIdle());
            sb.append("\nNumTestPerEvictRun :" + basicDataSource.getNumTestsPerEvictionRun());
            sb.append("\nRemoveAbandoned    :" + basicDataSource.getRemoveAbandoned());
            sb.append("\nRemoveAbandonedTO  :" + basicDataSource.getRemoveAbandonedTimeout());
            sb.append("\nTestOnBorrow       :" + basicDataSource.getTestOnBorrow());
            sb.append("\nTestOnReturn       :" + basicDataSource.getTestOnReturn());
            sb.append("\nTestWhileIdle      :" + basicDataSource.getTestWhileIdle());
            sb.append("\nTimeBetweenEvictRun:" + basicDataSource.getTimeBetweenEvictionRunsMillis());
        } else {
            sb.append("\nNOT Using BasicDataSource");
        }

        sb.append("\n================================================");
        logger.info(sb.toString());
    }
}
