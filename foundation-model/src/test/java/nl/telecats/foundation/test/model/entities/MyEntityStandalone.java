package nl.telecats.foundation.test.model.entities;

import nl.telecats.foundation.model.entities.IdentifiableObject;

/**
 * Persistent Entity to use in unit tests
 * 
 */
public class MyEntityStandalone implements IdentifiableObject {
    String content;
    String id;
    Integer version = 0;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

}