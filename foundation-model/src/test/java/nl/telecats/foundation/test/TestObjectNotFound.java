package nl.telecats.foundation.test;

import java.util.List;

import nl.telecats.foundation.model.exceptions.DatabaseException;
import nl.telecats.foundation.model.exceptions.ObjectNotFoundException;
import nl.telecats.foundation.test.model.dao.MyEntityChildDao;
import nl.telecats.foundation.test.model.dao.MyEntityParentDao;
import nl.telecats.foundation.test.model.dao.MyEntityStandaloneDao;
import nl.telecats.foundation.test.model.entities.MyEntityChild;
import nl.telecats.foundation.test.model.entities.MyEntityParent;
import nl.telecats.foundation.test.model.entities.MyEntityStandalone;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

/**
 * There was a bug in foundation. After an object is deleted, it was still found by dao findBy(id). <br/>
 * This test verifies the bugfix in AbstractGenericDaoImpl is working.
 * 
 * @author raymond
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:foundation-model-test-context.xml" })
@TransactionConfiguration(transactionManager = "foundation-model-transactionManager", defaultRollback = false)
public class TestObjectNotFound {
    @Autowired
    private MyEntityStandaloneDao mesDao;

    @Autowired
    private MyEntityParentDao mepDao;

    @Autowired
    private MyEntityChildDao mecDao;

    /**
     * Test after entity is deleted it isn't found by dao.
     */
    @Transactional
    @Test
    public void testObjectNotFoundAfterDeletionStandalone() {
        Assert.assertNotNull("Dao autowired", mesDao);
        MyEntityStandalone savedMe = null;
        // save my entity
        try {
            MyEntityStandalone me = new MyEntityStandalone();
            me.setContent("foo");
            savedMe = mesDao.save(me);
        } catch (DatabaseException e) {
            e.printStackTrace();
            Assert.fail("MyEntity saved");
        }

        // check it is there
        try {
            Assert.assertNotNull("MyEntity found", mesDao.findObject(savedMe.getId()));
        } catch (ObjectNotFoundException e) {
            e.printStackTrace();
            Assert.fail("MyEntity found");
        }

        // delete it
        mesDao.delete(savedMe);

        // check it is there
        boolean myEntityFound = true;
        try {
            mesDao.findObject(savedMe.getId());
        } catch (ObjectNotFoundException e) {
            myEntityFound = false;
        }
        Assert.assertTrue("ObjectNotFoundException", !myEntityFound);
    }

    /**
     * Test after delete of entity (using inheritance relation) it isn't found by dao.
     * 
     * @throws DatabaseException
     */
    @Transactional
    @Test
    public void testObjectNotFoundAfterDeletionParentChild() throws DatabaseException {
        MyEntityChild mec = new MyEntityChild();
        mec = mecDao.save(mec);

        List<MyEntityParent> entities1 = mepDao.findAll();
        for (MyEntityParent mep : entities1) {
            mepDao.delete(mep);
        }

        mepDao.findAll();

        // Item's existence is checked
        boolean menuItemFound = true;
        try {
            mecDao.findObject(mec.getId());
        } catch (ObjectNotFoundException e) {
            menuItemFound = false;
        }
        Assert.assertTrue("MyEntityChild not found", !menuItemFound);
    }
}