package nl.telecats.foundation.test.model.entities;


/**
 * Persistent Entity to use in unit tests
 * 
 */
public class MyEntityChild extends MyEntityParent {
    String content;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

}