package nl.telecats.foundation.test.model.dao;

import nl.telecats.foundation.model.dao.GenericDao;
import nl.telecats.foundation.test.model.entities.MyEntityStandalone;

/**
 * Dao for accessing MyEntity
 */
public interface MyEntityStandaloneDao extends GenericDao<MyEntityStandalone> {

}
