package nl.telecats.foundation.test.model.dao;

import nl.telecats.foundation.model.dao.hibernate.AbstractGenericDaoImpl;
import nl.telecats.foundation.test.model.entities.MyEntityChild;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * @see MyEntityChildDao
 */
@Repository(value = "foundationModel-hibernate-MyEntityChildDaoImpl")
public class MyEntityChildDaoImpl extends AbstractGenericDaoImpl<MyEntityChild> implements MyEntityChildDao {

    Log log = LogFactory.getLog(MyEntityChildDaoImpl.class);

    public MyEntityChildDaoImpl() {
        super(MyEntityChild.class);
    }

    @Autowired
    public final void setFoundationModelSessionFactory(SessionFactory foundationModelSessionFactory) {
        if (foundationModelSessionFactory == null) {
            log.warn("Trying to set a null foundationModelSessionFactory");
        } else {
            log.info("Setting foundationModelSessionFactory to " + foundationModelSessionFactory);
        }
        super.setSessionFactory(foundationModelSessionFactory);
    }
}
