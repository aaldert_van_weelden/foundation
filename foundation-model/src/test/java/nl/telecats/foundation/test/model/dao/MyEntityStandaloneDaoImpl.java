package nl.telecats.foundation.test.model.dao;

import nl.telecats.foundation.model.dao.hibernate.AbstractGenericDaoImpl;
import nl.telecats.foundation.test.model.entities.MyEntityStandalone;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * @see MyEntityStandaloneDao
 */
@Repository(value = "foundationModel-hibernate-MyEntityStandaloneDaoImpl")
public class MyEntityStandaloneDaoImpl extends AbstractGenericDaoImpl<MyEntityStandalone> implements
        MyEntityStandaloneDao {

    Log log = LogFactory.getLog(MyEntityStandaloneDaoImpl.class);

    public MyEntityStandaloneDaoImpl() {
        super(MyEntityStandalone.class);
    }

    @Autowired
    public final void setFoundationModelSessionFactory(SessionFactory foundationModelSessionFactory) {
        if (foundationModelSessionFactory == null) {
            log.warn("Trying to set a null foundationModelSessionFactory");
        } else {
            log.info("Setting foundationModelSessionFactory to " + foundationModelSessionFactory);
        }
        super.setSessionFactory(foundationModelSessionFactory);
    }
}
