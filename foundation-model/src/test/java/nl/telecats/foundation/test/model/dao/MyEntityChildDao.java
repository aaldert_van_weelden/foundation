package nl.telecats.foundation.test.model.dao;

import nl.telecats.foundation.model.dao.GenericDao;
import nl.telecats.foundation.test.model.entities.MyEntityChild;

/**
 * Dao for accessing MyEntity
 */
public interface MyEntityChildDao extends GenericDao<MyEntityChild> {

}
