package nl.telecats.foundation.test.model.dao;

import nl.telecats.foundation.model.dao.hibernate.AbstractGenericDaoImpl;
import nl.telecats.foundation.test.model.entities.MyEntityParent;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * @see MyEntityParentDao
 */
@Repository(value = "foundationModel-hibernate-MyEntityParentDaoImpl")
public class MyEntityParentDaoImpl extends AbstractGenericDaoImpl<MyEntityParent> implements MyEntityParentDao {

    Log log = LogFactory.getLog(MyEntityParentDaoImpl.class);

    public MyEntityParentDaoImpl() {
        super(MyEntityParent.class);
    }

    @Autowired
    public final void setFoundationModelSessionFactory(SessionFactory foundationModelSessionFactory) {
        if (foundationModelSessionFactory == null) {
            log.warn("Trying to set a null foundationModelSessionFactory");
        } else {
            log.info("Setting foundationModelSessionFactory to " + foundationModelSessionFactory);
        }
        super.setSessionFactory(foundationModelSessionFactory);
    }
}
