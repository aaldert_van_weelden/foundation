package nl.telecats.foundation.test.model.dao;

import nl.telecats.foundation.model.dao.GenericDao;
import nl.telecats.foundation.test.model.entities.MyEntityParent;

/**
 * Dao for accessing MyEntityParent
 */
public interface MyEntityParentDao extends GenericDao<MyEntityParent> {

}
