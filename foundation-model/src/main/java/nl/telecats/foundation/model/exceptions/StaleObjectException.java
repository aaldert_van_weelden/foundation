package nl.telecats.foundation.model.exceptions;

import java.io.Serializable;

/**
 * This exception should be thrown whenever there is a version conflict of two
 * instances of a given entity. For example, when saving an entity, this
 * exception should be thrown if the version of the to be saved entity does not
 * match the version of the entity in / retrieved using hibernate.
 * 
 * @author Reinier
 * 
 */
public class StaleObjectException extends DatabaseException implements Serializable {

    private static final long serialVersionUID = -5605469278337939474L;

    private String entityClassName;
    private Serializable id;
    private int version;

    /**
     * Hide the empty constructor; use
     * <code>StaleObjectException(String entityClassName, Serializable id, int version)</code>
     * instead.
     */
    @SuppressWarnings("unused")
    private StaleObjectException() {
    }

    /**
     * Create a new StaleObjectException based on the classname, ID and version
     * of the entity that became stale
     * 
     * @param entityClassName
     *            The classname of the stale entity
     * @param id
     *            The id of the stale entity
     * @param version
     *            The version of the stale entity
     */
    public StaleObjectException(String entityClassName, Serializable id, int version) {
        super("Trying to update a stale object for class '" + entityClassName + "', with id '" + id + "', version '"
                + version + "'");
        this.entityClassName = entityClassName;
        this.id = id;
        this.version = version;
    }

    /**
     * Retrieve the classname of the stale entity
     * 
     * @return The classname of the stale entity
     */
    public String getEntityClassName() {
        return entityClassName;
    }

    /**
     * Retrieve the ID of the stale entity
     * 
     * @return The ID of the stale entity
     */
    public Serializable getId() {
        return id;
    }

    /**
     * Retrieve the version of the stale entity
     * 
     * @return The version of the stale entity
     */
    public int getVersion() {
        return version;
    }

    /**
     * An ObjectNotFoundException is equals to another if and only if:
     * <ul>
     * <li>The other ObjectNotFoundException is not null
     * <li>
     * <li>The classnames are not null and the same</li>
     * <li>The IDs are not null and the same</li>
     * </ul>
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (!other.getClass().equals(getClass())) {
            return false;
        }
        StaleObjectException soe = (StaleObjectException) other;
        if (entityClassName == null && soe.entityClassName != null || !entityClassName.equals(soe.entityClassName)) {
            return false;
        }
        if (id == null && soe.id != null || !id.equals(soe.id)) {
            return false;
        }
        return true;
    }

}
