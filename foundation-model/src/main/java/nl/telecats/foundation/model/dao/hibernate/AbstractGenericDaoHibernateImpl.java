package nl.telecats.foundation.model.dao.hibernate;

import nl.telecats.foundation.model.entities.IdentifiableObject;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * This class is used for creating an autowired enabled
 * {@link AbstractGenericDaoImpl}.
 * 
 * 
 * @deprecated For reasons of supporting multiple model / database connections
 *             in one project, DOA's overriding should extend
 *             {@link AbstractGenericDaoImpl} directly or via a locally extend
 *             GenericDao, and create an function for wiring the session factory
 *             <b>byName</b> or via <b>@Autowired</b> </ul>
 * @author Eelco Brolman
 * 
 * 
 * @param <T>
 */
@Deprecated
@Transactional(propagation = Propagation.MANDATORY)
public abstract class AbstractGenericDaoHibernateImpl<T extends IdentifiableObject> extends AbstractGenericDaoImpl<T> {

    public AbstractGenericDaoHibernateImpl(Class<T> type) {
        super(type);
    }

    /*
     * (non-Javadoc)
     * 
     * @seenl.telecats.foundation.model.dao.hibernate.AbstractGenericDaoImpl#
     * setSessionFactory(org.hibernate.SessionFactory)
     */
    @Autowired
    @Override
    protected void setSessionFactory(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
    }

}
