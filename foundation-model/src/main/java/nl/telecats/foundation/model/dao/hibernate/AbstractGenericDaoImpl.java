package nl.telecats.foundation.model.dao.hibernate;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.SQLException;
import java.util.List;

import nl.telecats.foundation.model.dao.GenericDao;
import nl.telecats.foundation.model.entities.IdentifiableObject;
import nl.telecats.foundation.model.exceptions.DatabaseException;
import nl.telecats.foundation.model.exceptions.NotNullableException;
import nl.telecats.foundation.model.exceptions.NotUniqueException;
import nl.telecats.foundation.model.exceptions.ObjectNotFoundException;
import nl.telecats.foundation.model.exceptions.StaleObjectException;
import nl.telecats.foundation.model.util.HibernateUtils;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.StaleObjectStateException;
import org.hibernate.criterion.Restrictions;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.HibernateTemplate;

/**
 * Abstract implementation of the GenericDao in Hibernate.<br/>
 * <br/>
 * This class was previously (< 2.2) AbstractGenericDaoHibernateImpl. It is a copy with the
 * {@link #setSessionFactory(SessionFactory)} function non-autowired, to support multiple model projects within one
 * service project.</br> <br/>
 * Extend this class (preferably via a project specific GenericDao, in which a project specific SessionFactory is wired
 * (either <b>byName</b> or <b>@Autowired</b>)<br/>
 * <br/>
 * Previously (< 2.2), Transaction was MANDATORY on all functions of this class. This behavior has been discarded, every
 * class using DAO's is on it's own for Transaction management.
 * 
 * SessionFactory initializatio nexample:
 * 
 * <pre>
 * &#064;Autowired
 * public final void setFoundationModelSessionFactory(SessionFactory foundationModelSessionFactory) {
 *     if (foundationModelSessionFactory == null) {
 *         log.warn(&quot;Trying to set a null foundationModelSessionFactory&quot;);
 *     } else {
 *         log.info(&quot;Setting foundationModelSessionFactory to &quot; + foundationModelSessionFactory);
 *     }
 *     super.setSessionFactory(foundationModelSessionFactory);
 * }
 * </pre>
 * 
 * 
 * @author Eelco Brolman
 * @author Reinier Boon
 * @since 2.2
 * @param <T>
 *            The {@link IdentifiableObject} for this DAO.
 */
public abstract class AbstractGenericDaoImpl<T extends IdentifiableObject> implements GenericDao<T> {

    final private Class<T> type;
    private SessionFactory sessionFactory;
    private HibernateTemplate hibernateTemplate;

    public AbstractGenericDaoImpl(Class<T> type) {
        this.type = type;
    }

    /**
     * Set the hibernate session factory and create a hibernate template based on it. This function can be used directly
     * to spring wire a sessionFactory in it. Better still to override/call this function in a project specific
     * AbstractDao, and use some form of (auto) wiring there.
     * 
     * @param sessionFactory
     *            The hibernate session factory
     */
    protected void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
        this.hibernateTemplate = new HibernateTemplate(sessionFactory);
    }

    /*
     * (non-Javadoc)
     * 
     * @see nl.telecats.foundation.model.dao.GenericDao#getSessionFactory()
     */
    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    /**
     * Retrieve the hibernate template
     * 
     * @return The hibernate template
     */
    protected HibernateTemplate getHibernateTemplate() {
        return hibernateTemplate;
    }

    /*
     * (non-Javadoc)
     * 
     * @see nl.telecats.foundation.model.dao.GenericDao#getHibernateUtils()
     */
    public HibernateUtils getHibernateUtils() {
        return new HibernateUtils(getSessionFactory());
    }

    /*
     * (non-Javadoc)
     * 
     * @see nl.telecats.speechanalysis.dao.GenericDao#findObject(java.lang.Class, java.io.Serializable)
     */
    @SuppressWarnings("unchecked")
    public T findObject(final Serializable id) throws ObjectNotFoundException {
        try {
            T result = (T) getHibernateTemplate().execute(new HibernateCallback() {
                public T doInHibernate(Session session) throws HibernateException, SQLException {
                    return (T) session.get(type, id);
                    // Don't use load, this will find already deleted objects.
                    // Details:
                    // http://docs.jboss.org/hibernate/envers/3.5/javadocs/org/hibernate/Session.html#load(java.lang.Class,
                    // java.io.Serializable)
                    // return (T) session.load(type, id);
                }
            });
            if (result != null) {
                return result;
            } else {
                throw new ObjectNotFoundException(type.getClass().getName(), id);
            }
        } catch (org.springframework.orm.ObjectRetrievalFailureException e) {
            throw new ObjectNotFoundException(e.getPersistentClassName(), e.getIdentifier().toString());
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see nl.telecats.speechanalysis.dao.GenericDao#findObject(java.lang.Class, java.io.Serializable)
     */
    @SuppressWarnings("unchecked")
    public T getObject(final Serializable id) {
        return (T) getHibernateTemplate().execute(new HibernateCallback() {
            public T doInHibernate(Session session) throws HibernateException, SQLException {
                return (T) session.get(type, id);
            }
        });
    }

    /*
     * (non-Javadoc)
     * 
     * @see nl.telecats.speechanalysis.dao.GenericDao#findAll(java.lang.Class)
     */
    public final List<T> findAll() {
        return getHibernateTemplate().loadAll(type);
    }

    /*
     * (non-Javadoc)
     * 
     * @see nl.telecats.foundation.model.dao.GenericDao#refresh(nl.telecats.foundation
     * .model.entities.IdentifiableObject)
     */
    @SuppressWarnings("unchecked")
    public final <C extends IdentifiableObject> C refresh(final C entity) throws ObjectNotFoundException {
        try {
            return (C) getHibernateTemplate().execute(new HibernateCallback() {
                public C doInHibernate(Session session) throws HibernateException, SQLException {
                    return (C) session.load(entity.getClass(), entity.getId());
                }
            });
        } catch (org.springframework.orm.ObjectRetrievalFailureException e) {
            throw new ObjectNotFoundException(e.getPersistentClassName(), e.getIdentifier().toString());
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see nl.telecats.speechanalysis.dao.GenericDao#delete(java.lang.Object)
     */
    public <C> void delete(C entity) throws DataAccessException {
        getHibernateTemplate().delete(entity);
    }

    /*
     * (non-Javadoc)
     * 
     * @see nl.telecats.speechanalysis.dao.GenericDao#save(java.lang.Object)
     */
    @SuppressWarnings("unchecked")
    public <C extends IdentifiableObject> C save(final C entity) throws DatabaseException {

        class ErrorWrapper {
            private DatabaseException databaseException;

            public DatabaseException getDatabaseException() {
                return databaseException;
            }

            public void setDatabaseException(DatabaseException databaseException) {
                this.databaseException = databaseException;
            }
        }

        final ErrorWrapper errorWrapper = new ErrorWrapper();

        try {
            return (C) getHibernateTemplate().execute(new HibernateCallback() {
                public C doInHibernate(Session session) throws HibernateException, SQLException {
                    C hibEntity = entity;
                    if (type.isAssignableFrom(entity.getClass())) {
                        try {
                            testProperties(session, (T) hibEntity);
                        } catch (DatabaseException dbe) {
                            errorWrapper.setDatabaseException(dbe);
                            throw new RuntimeException();
                        }
                    }
                    if (entity.getVersion() != null) {
                        // existing object!
                        try {
                            hibEntity = (C) session.merge(entity);
                        } catch (StaleObjectStateException soe) {
                            // catch the StaleObjectStateException, and wrap
                            // it up
                            errorWrapper.setDatabaseException(new StaleObjectException(entity.getClass()
                                    .getCanonicalName(), entity.getId(), entity.getVersion()));
                            throw new RuntimeException();
                        }
                    }
                    session.saveOrUpdate(hibEntity);
                    return hibEntity;
                }
            });
        } catch (RuntimeException e) {
            if (errorWrapper.getDatabaseException() != null) {
                throw errorWrapper.getDatabaseException();
            } else if (e.getCause() instanceof SQLException && ((SQLException) e.getCause()).getErrorCode() == 2601) {
                throw new NotUniqueException(e);
            } else {
                throw e;
            }
        }
    }

    public void testProperties(Session session, T entity) throws NotUniqueException, NotNullableException {
        // default: all is ok
    }

    /**
     * Utility function for overriden testProperties methods, to test the non-null-ness and uniqueness of properties for
     * a given entity.
     * 
     * @param session
     * @param entity
     * @param property
     * @throws NotNullableException
     * @throws NotUniqueException
     */
    @SuppressWarnings("unchecked")
    protected void testProperty(Session session, Object entity, String property) throws NotNullableException,
            NotUniqueException {
        String getterName = "get" + StringUtils.capitalize(property);
        Serializable value = null;
        for (Method m : entity.getClass().getMethods()) {
            if (m.getName().equals(getterName) && m.getParameterTypes().length == 0) {
                try {
                    value = (Serializable) m.invoke(entity, new Object[] {});
                    break;
                } catch (IllegalAccessException e) {
                } catch (InvocationTargetException e) {
                }
            }
        }
        if (value == null) {
            throw new NotNullableException(entity.getClass().getCanonicalName(), property);
        }
        Criteria crit = session.createCriteria(entity.getClass());
        crit.add(Restrictions.eq(property, value));

        List<Object> l = crit.list();
        if (!l.isEmpty()) {
            // check if list contains only ourself
            if (l.size() == 1 && l.get(0).equals(entity)) {
                // all ok
            } else {
                throw new NotUniqueException(entity.getClass().getCanonicalName(), null, property, value);
            }
        }
    }

    /**
     * Recursive reflexive method to fully save an entity. First, save the enitity itself, then recursively invoke the
     * collection typed getters to retrieve the collections, and save each member of the collection after refreshing it.
     * 
     * 
     * @see nl.telecats.foundation.model.dao.GenericDao#saveFull(nl.telecats.foundation.model.entities.IdentifiableObject,
     *      int)
     */
    @SuppressWarnings("unchecked")
    public final <C extends IdentifiableObject> C saveFull(final C entity, final int level) throws DatabaseException {

        /**
         * Wrapper for the typed exceptions that can be thrown. For now, only DatabaseExceptions are supported.
         */
        class ErrorWrapper {
            private DatabaseException databaseException;

            /**
             * Retrieve the wrapped DatabaseException
             * 
             * @return The wrapped DatabaseException
             */
            public DatabaseException getDatabaseException() {
                return databaseException;
            }

            /**
             * Set the DatabaseException to be wrapped
             * 
             * @param databaseException
             *            The DatabaseException to be wrapped
             */
            @SuppressWarnings("unused")
            public void setDatabaseException(DatabaseException databaseException) {
                this.databaseException = databaseException;
            }
        }

        // the instance of the error wrapper
        final ErrorWrapper errorWrapper = new ErrorWrapper();

        try {
            return (C) getHibernateTemplate().execute(new HibernateCallback() {

                /**
                 * Save/Update the provided entity. Afterwards, iterate through all the methods to check for methods
                 * that return a collection, indicating there is a set of child objects that need to be saved as well.
                 * For each to be saved child, the parent backlink should be updated to the newly saved object.
                 * <p/>
                 * The depth to which the children should be saved can be specified in level. With level=-1, this method
                 * will continue until no more child collections can be found.
                 * 
                 * @param session
                 *            The hibernate session in which the save/update should take place
                 * @param entity
                 *            The current entity to save and examine
                 * @param level
                 *            The number of levels remaining
                 * @param parent
                 *            For the current entity, it's potential parent is stored here.
                 */
                private void iterateObjectCollectionsAndSave(Session session, Object entity, int level, Object parent) {
                    try {
                        session.saveOrUpdate(entity);
                        if (level != 0) {
                            for (Method m : entity.getClass().getMethods()) {
                                if (isParentSetterMethod(m, parent)) {
                                    m.invoke(entity, parent);
                                }

                                if (getHibernateUtils().isCollectionTypedGetter(m)) {
                                    for (Object entry : ((java.util.Collection) m.invoke(entity, new Object[] {}))) {

                                        iterateObjectCollectionsAndSave(session, entry, level - 1, entity);
                                    }
                                }
                            }
                        }
                        // TODO some checking a la save()

                    } catch (SecurityException e) {
                        // TODO
                        e.printStackTrace();
                    } catch (IllegalArgumentException e) {
                        // TODO
                        e.printStackTrace();
                    } catch (IllegalAccessException e) {
                        // TODO
                        e.printStackTrace();
                    } catch (InvocationTargetException e) {
                        // TODO
                        e.printStackTrace();
                    }
                }

                /**
                 * Determine if a given method is a setter for the parent. This is determined by:
                 * <ul>
                 * <li>The return type of the method must be void</li>
                 * <li>The method should have only 1 parameter</li>
                 * <li>The type of that parameter is of parent</li>
                 * </ul>
                 * 
                 * @param method
                 *            The method to examine
                 * @param parent
                 *            The parent object to check for
                 * @return If the provided method is the parent setter.
                 */
                private boolean isParentSetterMethod(Method method, Object parent) {
                    if (parent == null) {
                        return false;
                    }
                    boolean voidReturnType = method.getReturnType().equals(void.class);

                    Class[] params = method.getParameterTypes();
                    return (voidReturnType && (params.length == 1) && (params[0].equals(parent.getClass())));
                }

                /**
                 * Startpoint of the iteration.
                 * 
                 * @see org.springframework.orm.hibernate3.HibernateCallback#doInHibernate(org.hibernate.Session)
                 */
                public C doInHibernate(Session session) throws HibernateException, SQLException {
                    C hibEntity = entity;
                    iterateObjectCollectionsAndSave(session, hibEntity, level, null);
                    return hibEntity;
                }
            });
        } catch (RuntimeException e) {
            if (errorWrapper.getDatabaseException() != null) {
                throw errorWrapper.getDatabaseException();
            } else if (e.getCause() instanceof SQLException && ((SQLException) e.getCause()).getErrorCode() == 2601) {
                throw new NotUniqueException(e);
            } else {
                throw e;
            }
        }
    }

}
