package nl.telecats.foundation.model.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import net.sf.hibernate4gwt.core.HibernateBeanManager;
import net.sf.hibernate4gwt.core.hibernate.HibernateUtil;
import net.sf.hibernate4gwt.core.store.stateless.StatelessPojoStore;
import nl.telecats.foundation.model.entities.IdentifiableObject;

import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;

/**
 * A collection of utility methods for working with persistent objects outside
 * the Hibernate session scope.
 * 
 * @author reinier
 * 
 */
public class HibernateUtils {
    private SessionFactory sessionFactory;

    public HibernateUtils(SessionFactory sessionFactory) {
        super();
        this.sessionFactory = sessionFactory;
    }

    private SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public <C extends Object> C initAndstrip(C enhancedHibernateObject) {
        if (enhancedHibernateObject instanceof IdentifiableObject) {
            initializeCollectionTypedProperties(enhancedHibernateObject);
        }
        return stripHibernateEnhancements(enhancedHibernateObject);
    }

    /**
     * <p>
     * Fill all public accessible collection typed properties of <i>entity</i>
     * with data fetched from the database (i.e. eager load collection
     * properties).
     * </p>
     * <p>
     * Normally, when an object is retrieved by hibernate, its collection typed
     * properties will not be fetched (lazy loading). Instead, a persistent set
     * is created filled with references that can be converted to real database
     * objects on request. Once the session is closed however, these objects are
     * not accessible anymore. When stripHibernateEnhancements() is then called
     * on the collection owner, this results in simply empty collections. When
     * stripHibernateEnhancements() is not called, but objects are retrieved
     * from the persistent set <b>outside</b> when the hibernate session is
     * closed, hibernate will throw not-initialized-object exceptions. To avoid
     * all this malheur, call initializeCollectionTypedProperties() before
     * closing the session.
     * </p>
     * 
     * @param <C>
     * @param entity
     *            the entity to be initialized
     */
    @SuppressWarnings("unchecked")
    protected <C> void initializeCollectionTypedProperties(C entity) {
        for (Method m : entity.getClass().getMethods()) {
            try {
                if (isCollectionTypedGetter(m)) {
                    java.util.Collection c = (java.util.Collection) m.invoke(entity, new Object[] {});
                    Hibernate.initialize(c);
                }
            } catch (IllegalAccessException e) {
            } catch (InvocationTargetException e) {
            }
        }
    }

    public boolean isCollectionTypedGetter(Method m) {
        if (!m.getName().startsWith("get")) {
            return false;
        }
        if (m.getParameterTypes().length != 0) {
            return false;
        }
        if (!java.util.Collection.class.isAssignableFrom(m.getReturnType())) {
            return false;
        }
        return true;
    }

    /**
     * Hibernate enhances objects with hibernate specific data. When gwt
     * receives this enhanced object it doesn't know how to interpret the
     * enhanced hibernate data.
     * <p/>
     * This is why this enhanced data should be stripped from the object before
     * returning it to the gwt client.
     * 
     * @param <C>
     * 
     * @param enhancedHibernateObject
     * @return stripped object without hibernate enhanced data
     */
    @SuppressWarnings("unchecked")
    protected <C extends Object> C stripHibernateEnhancements(C enhancedHibernateObject) {
        getSessionFactory().getCurrentSession().flush();
        // trigger all kinds of malheur
        HibernateUtil persistenceUtil = new HibernateUtil();
        persistenceUtil.setSessionFactory(getSessionFactory());

        HibernateBeanManager beanManager = new HibernateBeanManager();
        beanManager.setPersistenceUtil(persistenceUtil);
        beanManager.setPojoStore(new StatelessPojoStore());
        return (C) beanManager.clone(enhancedHibernateObject);
    }
}
