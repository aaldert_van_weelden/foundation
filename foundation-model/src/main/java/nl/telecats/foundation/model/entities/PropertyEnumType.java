package nl.telecats.foundation.model.entities;

//$Id: EnumType.java 10537 2006-09-29 19:18:26Z epbernard $

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.lang.reflect.Method;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.AssertionFailure;
import org.hibernate.HibernateException;
import org.hibernate.MappingException;
import org.hibernate.property.Getter;
import org.hibernate.usertype.EnhancedUserType;
import org.hibernate.usertype.ParameterizedType;
import org.hibernate.util.ReflectHelper;

/**
 * Custom enum type hibernate mapper, that does not use name or ordinal value of
 * the enum, but instead uses a property on the enum to define the values that
 * are stored in the database. The values show be unique and non-null. </p>
 * <p>
 * Use with integer values, string property and column values. The param
 * <i>sqlType</i> is optional, and can have the values
 * <i>string</i>,<i>integer</i>, or one of the values of java.sql.Types (use the
 * integer ordinal value).
 * </p>
 * <p>
 * Usage:
 * 
 * <pre>
 * &lt;property name="my_enum" not-null="false" column="my_enum"&gt;
 *   &lt;type name="nl.telecats.foundation.model.entities.PropertyEnumType"&gt;
 *     &lt;param name="enumClass"&gt;nl.telecats.x.MyEnumType&lt;/param&gt; 
 *     &lt;param name="enumPropery"&gt;value&lt;/param&gt;
 *     &lt;param name="sqlType"&gt;integer&lt;/param&gt;
 *   &lt;/type>
 * &lt;/property>
 * </pre>
 * 
 * @author Reinier Boon, based on org.hibernate.type.EnumType from Emmanuel
 *         Bernard
 * 
 */
// TODO implements readobject/writeobject to recalculate the enumclasses
public class PropertyEnumType implements EnhancedUserType, ParameterizedType, Serializable {

    private static final long serialVersionUID = 2114089359082389398L;
    private static Log log = LogFactory.getLog(PropertyEnumType.class);
    private static final boolean IS_TRACE_ENABLED = true;

    public static final String ENUM = "enumClass";
    public static final String SQLTYPE = "sqlType";
    public static final String PROPERTY = "enumPropery";

    @SuppressWarnings("rawtypes")
    private static Map<Class<? extends Enum>, Object[]> enumValues = new HashMap<Class<? extends Enum>, Object[]>();

    @SuppressWarnings("rawtypes")
    private Class<? extends Enum> enumClass;
    private int sqlType = Types.INTEGER; // before any guessing
    private Getter enumProperty;
    private String enumPropertyName;

    public int[] sqlTypes() {
        return new int[] { sqlType };
    }

    @SuppressWarnings("rawtypes")
    public Class returnedClass() {
        return enumClass;
    }

    public boolean equals(Object x, Object y) throws HibernateException {
        return x == y;
    }

    public int hashCode(Object x) throws HibernateException {
        return x == null ? 0 : x.hashCode();
    }

    public Object nullSafeGet(ResultSet rs, String[] names, Object owner) throws HibernateException, SQLException {
        Object object = rs.getObject(names[0]);
        if (rs.wasNull()) {
            if (IS_TRACE_ENABLED) {
                log.debug("Returning null as column " + names[0]);
            }
            return null;
        }
        String value = object.toString();

        Object[] values = enumValues.get(enumClass);
        if (values == null)
            throw new AssertionFailure("enumValues not preprocessed: " + enumClass);

        for (Object val : values) {
            if (value.equals(enumProperty.get(val).toString())) {
                if (IS_TRACE_ENABLED) {
                    log.debug("Returning '" + val.toString() + "' as column " + names[0]);
                }
                return val;
            }
        }
        throw new IllegalArgumentException("Unknown value for enum " + enumClass + ": " + object);
    }

    public void nullSafeSet(PreparedStatement st, Object value, int index) throws HibernateException, SQLException {
        if (value == null) {
            if (IS_TRACE_ENABLED)
                log.debug("Binding null to parameter: " + index);
            st.setNull(index, sqlType);
        } else {
            st.setObject(index, enumProperty.get(value), sqlType);
        }
    }

    private boolean isOrdinal(int paramType) {
        switch (paramType) {
        case Types.INTEGER:
        case Types.NUMERIC:
        case Types.SMALLINT:
        case Types.TINYINT:
        case Types.BIGINT:
        case Types.DECIMAL: // for Oracle Driver
        case Types.DOUBLE: // for Oracle Driver
        case Types.FLOAT: // for Oracle Driver
            return true;
        case Types.CHAR:
        case Types.LONGVARCHAR:
        case Types.VARCHAR:
            return false;
        default:
            throw new HibernateException("Unable to persist an Enum in a column of SQL Type: " + paramType);
        }
    }

    public Object deepCopy(Object value) throws HibernateException {
        return value;
    }

    public boolean isMutable() {
        return false;
    }

    public Serializable disassemble(Object value) throws HibernateException {
        return (Serializable) value;
    }

    public Object assemble(Serializable cached, Object owner) throws HibernateException {
        return cached;
    }

    public Object replace(Object original, Object target, Object owner) throws HibernateException {
        return original;
    }

    @SuppressWarnings("unchecked")
    public void setParameterValues(Properties parameters) {
        String enumClassName = parameters.getProperty(ENUM);
        try {
            enumClass = ReflectHelper.classForName(enumClassName, this.getClass()).asSubclass(Enum.class);
        } catch (ClassNotFoundException exception) {
            throw new HibernateException("Enum class not found", exception);
        }
        enumPropertyName = parameters.getProperty(PROPERTY);
        if (enumPropertyName == null) {
            throw new HibernateException("No property specified for hibernate mapping on enum " + enumClassName);
        }
        try {
            enumProperty = ReflectHelper.getGetter(enumClass, enumPropertyName);
        } catch (MappingException exception) {
            throw new HibernateException("No getter found for property " + enumPropertyName + " on enum "
                    + enumClassName, exception);
        }
        // this is threadsafe to do it here, setParameterValues() is called
        // sequencially
        initEnumValue();
        // nullify unnullified properties yuck!

        String type = parameters.getProperty(SQLTYPE).toLowerCase();
        if (type != null) {
            if (type.equals("string")) {
                sqlType = Types.VARCHAR;
            } else if (type.equals("integer")) {
                sqlType = Types.INTEGER;
            } else {
                try {
                    sqlType = Integer.decode(type).intValue();
                } catch (NumberFormatException nfe) {
                    throw new HibernateException("Invalid sqlType specified for hibernate mapping on enum "
                            + enumClassName);
                }
            }
        }
    }

    private void initEnumValue() {
        Object[] values = enumValues.get(enumClass);
        if (values == null) {
            try {
                Method method = null;
                method = enumClass.getDeclaredMethod("values", new Class[0]);
                values = (Object[]) method.invoke(null, new Object[0]);
            } catch (Exception e) {
                throw new HibernateException("Error while accessing enum.values(): " + enumClass, e);
            }
            // now check validity and non-duplicity of the property values.
            Map<Object, Object> propertyValues = new HashMap<Object, Object>();
            for (Object val : values) {
                Object propertyValue = enumProperty.get(val);
                if (propertyValue == null) {
                    throw new HibernateException("Null property value for enum " + enumClass + "." + val.toString()
                            + "." + enumPropertyName);
                } else if (propertyValues.containsKey(propertyValue)) {
                    throw new HibernateException("Duplicate property value for enum " + enumClass + "."
                            + val.toString() + "." + enumPropertyName);
                }
                propertyValues.put(propertyValue, null);
            }
            enumValues.put(enumClass, values);
        }
    }

    private void readObject(ObjectInputStream ois) throws IOException, ClassNotFoundException {
        // FIXME Hum, I think I break the thread safety here
        ois.defaultReadObject();
        initEnumValue();
    }

    public String objectToSQLString(Object value) {
        boolean isOrdinal = isOrdinal(sqlType);
        String val = enumProperty.get(value).toString();
        if (isOrdinal) {
            return val;
        } else {
            return '\'' + val + '\'';
        }
    }

    public String toXMLString(Object value) {
        return enumProperty.get(value).toString();
    }

    public Object fromXMLString(String xmlValue) {
        Object[] values = enumValues.get(enumClass);
        if (values == null)
            throw new AssertionFailure("enumValues not preprocessed: " + enumClass);

        for (Object val : values) {
            if (toXMLString(val).equals(xmlValue)) {
                return val;
            }
        }
        throw new IllegalArgumentException("Unknown value for enum " + enumClass + ": " + xmlValue);
    }
}
