package nl.telecats.foundation.model.entities;

import java.io.Serializable;

/**
 * Model entities need to be identified, and facilate versioning (in order to
 * detect collisions). This interface defines the required methods that all
 * model entities should impelement.
 * 
 * @author Raymond
 * 
 */
public interface IdentifiableObject extends Serializable {

    /**
     * Retrieve the unique ID of an entity (typically an UUID)
     * 
     * @return The ID
     */
    public String getId();

    /**
     * Set thte unique ID of an entitty
     * 
     * @param id
     *            The ID to set
     */
    public void setId(String id);

    /**
     * Retrieve the version of an entity
     * 
     * @return The version
     */
    public Integer getVersion();
}
