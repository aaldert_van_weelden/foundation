package nl.telecats.foundation.model.exceptions;

import java.io.Serializable;

/**
 * This exception should be thrown whenever a not-null constraint is (about to
 * be) violated.
 * 
 * @author reinier
 * 
 */
public class NotNullableException extends DatabaseException implements Serializable {
    private static final long serialVersionUID = -7668111429014062328L;

    private String entityClassName;
    private Serializable field;

    /**
     * Hide the empty constructor; the constructor
     * <code>NotNullableException(String entityClassName, String field)</code>
     * should be used instead.
     */
    @SuppressWarnings("unused")
    private NotNullableException() {
    }

    /**
     * Create a new instance of the exception based on the classname of the
     * entity, and the field that violates a notnull constraint.
     * 
     * @param entityClassName
     *            The classname of the entity
     * @param field
     *            The field of the entity that violates the constraint
     */
    public NotNullableException(String entityClassName, String field) {
        super("Field '" + field + "' of class '" + entityClassName + " may not be null");
        this.entityClassName = entityClassName;
        this.field = field;
    }

    /**
     * Create a new instance based on a <code>Throwable<code> instance
     * 
     * @param e
     *            The exception to base this one on.
     */
    public NotNullableException(Throwable e) {
        super(e);
    }

    /**
     * Retrieve the class name
     * 
     * @return The class name
     */
    public String getEntityClassName() {
        return entityClassName;
    }

    /**
     * Retrieve the fieldname that violated the notnull constraint
     * 
     * @return The name of the field.
     */
    public Serializable getField() {
        return field;
    }

    /**
     * A NotNullableExceptions is equal to another if and only if:
     * <ul>
     * <li>The other is not null</li>
     * <li>The classnames are not null and the same</li>
     * <li>The fieldnames are not null and the same</li>
     * </ul>
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (!other.getClass().equals(getClass())) {
            return false;
        }
        NotNullableException onfe = (NotNullableException) other;
        if (entityClassName == null && onfe.entityClassName != null || !entityClassName.equals(onfe.entityClassName)) {
            return false;
        }
        if (field == null && onfe.field != null || !field.equals(onfe.field)) {
            return false;
        }
        return true;
    }
}
