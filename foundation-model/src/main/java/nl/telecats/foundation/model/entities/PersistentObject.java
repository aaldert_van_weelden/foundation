package nl.telecats.foundation.model.entities;

/**
 * Describes the root entity of persistent objects. Contains an ID and version.
 * 
 */
public class PersistentObject implements IdentifiableObject {

    private static final long serialVersionUID = 901684905808495181L;

    private String internalId;
    private Integer internalVersion;

    @Override
    public String getId() {
        return getInternalId();
    }

    @Override
    public Integer getVersion() {
        return getInternalVersion();
    }

    @Override
    public void setId(String id) {
        setInternalId(id);
    }

    public void setVersion(Integer version) {
        setInternalVersion(version);
    }

    /**
     * @param internalId
     *            the internalId to set
     */
    public void setInternalId(String internalId) {
        this.internalId = internalId;
    }

    /**
     * @return the internalId
     */
    public String getInternalId() {
        return internalId;
    }

    /**
     * @param internalVersion
     *            the internalVersion to set
     */
    public void setInternalVersion(Integer internalVersion) {
        this.internalVersion = internalVersion;
    }

    /**
     * @return the internalVersion
     */
    public Integer getInternalVersion() {
        return internalVersion;
    }

    /**
     * Two persistent objects if, and only if, the internalId and
     * internalVersion are the same.
     * 
     * @param Object
     *            o The (persistant) object to compare to
     * 
     */
    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || !(o.getClass().equals(this.getClass()))) {
            return false;
        }

        PersistentObject other = (PersistentObject) o;

        // if the uuid is missing, return false
        if (internalId == null)
            return false;

        // if internalversion is set, include it in equals; otherwise equal on
        // UUID.
        if (internalVersion != null) {
            return (internalId.equals(other.getInternalId()) && (internalVersion.equals(other.getInternalVersion())));
        } else {
            return internalId.equals(other.getInternalId());
        }
    }

    /**
     * Calculate the hashcode on <code>internalId.internalVersion</code> when
     * both are set; otherwise just on <code>internalId</code>. In case the
     * internalId is null as well; return the hashcode from the parent.
     * 
     * @return the hashcode for a persistentobject instance; based on internalId
     *         and internalVersion
     */
    @Override
    public int hashCode() {
        if (internalId != null) {
            if (internalVersion != null) {
                return (internalId + "." + internalVersion).hashCode();
            } else {
                return internalId.hashCode();
            }
        } else {
            return super.hashCode();
        }
    }

}
