package nl.telecats.foundation.model.dao;

import java.io.Serializable;
import java.util.List;

import nl.telecats.foundation.model.entities.IdentifiableObject;
import nl.telecats.foundation.model.exceptions.DatabaseException;
import nl.telecats.foundation.model.exceptions.NotNullableException;
import nl.telecats.foundation.model.exceptions.NotUniqueException;
import nl.telecats.foundation.model.exceptions.ObjectNotFoundException;
import nl.telecats.foundation.model.util.HibernateUtils;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.dao.DataAccessException;

/**
 * @author eelco
 * 
 * @param <T>
 */
public interface GenericDao<T extends IdentifiableObject> {

    /**
     * Retrieve the hibernate session factory
     * 
     * @return The hibernate session factory
     */
    public SessionFactory getSessionFactory();

    /**
     * Retrieve the instance of the hibernateUtils
     * 
     * @return The hibernateUtils instance
     */
    public HibernateUtils getHibernateUtils();

    /**
     * Find the object for the given Id. Throws an exception (HibernateObjectRetrievalFailureException) when not found.
     * 
     * @param id
     * @return The object of type <T> with the given Id. Throws an exception (HibernateObjectRetrievalFailureException)
     *         when not found.
     * @throws ObjectNotFoundException
     */
    public abstract T findObject(Serializable id) throws ObjectNotFoundException;

    /**
     * Get an object with the given Id. Does NOT throw an exception when not found, but returns null.
     * 
     * @param id
     * @return The object of type <T> or null if not found.
     */
    public abstract T getObject(Serializable id);

    /**
     * Find all objects of the type T (the type this DAO is instantiated for).
     * 
     * @return
     */
    public abstract List<T> findAll();

    /**
     * Save the passed object, returns the updated entity (e.g. modified version property)
     * 
     * @param entity
     * @return
     * @throws NotUniqueException
     */
    public abstract <C extends IdentifiableObject> C save(final C entity) throws DatabaseException;

    /**
     * Refresh an entity by (re-)retrieving it using hibernate
     * 
     * @param <C>
     *            class of the passed entity
     * @param entity
     *            the object to refresh
     * @return a new object with the same hibernate identity (i.e. class and id) as <i>entity</i>, with all its
     *         collection properties filled.
     * @throws ObjectNotFoundException
     *             when entity is not found in the database.
     */
    public abstract <C extends IdentifiableObject> C refresh(C entity) throws ObjectNotFoundException;

    /**
     * Delete an entity
     * 
     * @param <C>
     *            The class of the entity to delete
     * @param entity
     *            The object to delete
     * 
     * @throws DataAccessException
     *             when delete fails
     */
    public abstract <C> void delete(C entity) throws DataAccessException;

    public void testProperties(Session session, T entity) throws NotUniqueException, NotNullableException;

    /**
     * Save the passed object, and traverse level deep into all collection typed properties. This emulates the cascading
     * save.<Br>
     * <br>
     * TODO: error handling and some basic uniques checking identical to the {@link #save(IdentifiableObject)} function.
     * 
     * @param <C>
     * @param entity
     *            the {@link IdentifiableObject} to save
     * @param level
     *            how deep to traverse through collections, -1 for infinite
     * @return the saved object (id and version filled now)
     * @throws DatabaseException
     *             TODO
     */
    public abstract <C extends IdentifiableObject> C saveFull(C entity, int level) throws DatabaseException;

}