package nl.telecats.foundation.model.audit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Log modifications using slf4j.Logger
 * <p/>
 * Below an example of lines to add to your log4j.properties configuration:
 * 
 * <pre>
 * log4j.logger.nl.telecats.foundation.model.audit.AuditLogWriter=NONE
 * log4j.logger.nl.telecats.foundation.model.audit.AuditLogWriter=INFO, AUDITLOG
 * 
 * # F2 is a FileAppender
 * log4j.appender.AUDITLOG=org.apache.log4j.RollingFileAppender
 * log4j.appender.AUDITLOG.file=${telecats.log_directory}/scm-audit.log
 * log4j.appender.AUDITLOG.layout=org.apache.log4j.PatternLayout
 * log4j.appender.AUDITLOG.layout.ConversionPattern=%d [%-5p] %C{1} - %m%n
 * log4j.appender.AUDITLOG.maxBackupIndex=25
 * log4j.appender.AUDITLOG.maxFileSize=10MB
 * log4j.appender.AUDITLOG.threshold=INFO
 * </pre>
 * <p/>
 * <b>Note:</b> AuditLogWriter is used for log class, not DefaultAuditLogWriter
 * 
 * @author raymond
 * @see AuditLogWriter
 */
public class DefaultAuditLogWriter implements AuditLogWriter {

    private static final Logger log = LoggerFactory.getLogger(AuditLogWriter.class);

    @Override
    public void logCreation(AuditLogEntry auditLogEntry) {
        log.info(auditLogEntry.toString());

    }

    @Override
    public void logModification(AuditLogEntry auditLogEntry) {
        log.info(auditLogEntry.toString());
    }

    @Override
    public void logDeletion(AuditLogEntry auditLogEntry) {
        log.info(auditLogEntry.toString());
    }

    @Override
    public void logUnauditedModification(String details) {
        log.debug(details);
    }
}
