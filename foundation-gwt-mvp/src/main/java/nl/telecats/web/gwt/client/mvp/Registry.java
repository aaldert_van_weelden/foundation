package nl.telecats.web.gwt.client.mvp;

import java.util.HashMap;
import java.util.Map;

/**
 * This registry is used for keeping track of your presenters. The
 * foundation-gwt-mvp library needs this registry to obtain references to you
 * presenters when needed.
 * <p/>
 * <b>Usage:</b>
 * <p/>
 * Registry.register(ApplicationPresenter.ID, new ApplicationPresenter(eventBus,
 * userInfoGwtService, userGwtService));
 * 
 * @author Raymond Domingo
 * 
 */
public class Registry {
    /**
     * Map containing all registered presenters
     */
    protected static Map<String, Object> map = new HashMap<String, Object>();

    /**
     * Register your presenters in this registry.
     * 
     * @param id
     * @param value
     */
    public static void register(String id, Object value) {
        map.put(id, value);
    }

    /**
     * Remove an object registered in the store
     * 
     * @param id
     *            Remove the object identified by this ID.
     */
    public static void unregister(String id) {
        map.remove(id);
    }

    @SuppressWarnings("unchecked")
    public static <X> X get(String id) {
        return (X) map.get(id);
    }
}
