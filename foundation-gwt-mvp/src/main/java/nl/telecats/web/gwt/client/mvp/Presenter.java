package nl.telecats.web.gwt.client.mvp;

import java.util.ArrayList;

import nl.telecats.web.gwt.client.state.Bookmarkable;
import nl.telecats.web.gwt.client.state.BookmarkableManager;
import nl.telecats.web.gwt.client.state.StateAware;
import nl.telecats.web.gwt.client.state.StateManager;

import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.google.gwt.event.shared.HandlerManager;

/**
 * Base class for your Presenter implementations.
 * 
 * @author Raymond Domingo
 * 
 * @see DatePresenter, StateManager, BookmarkableManager, PresenterListener
 */
public abstract class Presenter  implements
        StateAware, Bookmarkable {

    private ArrayList<PresenterListener> listeners = new ArrayList<PresenterListener>();
    private boolean initialized = false;
    private String title;
    private String subtitle;
    private HandlerManager eventBus;

    /**
     * Don't forget to call this contstructor from your implementation. NOTE:
     * This will register your view in the registry.
     * 
     * @param controller
     *            of your view
     */
//    public MVCView(C controller) {
//        super(controller);
//        MVCRegistry.register(getViewId(), this);
//    }

    
    public Presenter(HandlerManager eventBus) {
    	this.eventBus = eventBus;
    }

    /**
     * In this method you should initialize the stateless parts of your view .
     */
    protected abstract void initView();
    

    /**
     * Registers this instance of the view with StateManager and
     * BookmarkableManager
     */
    protected void initialize() {
        initialized = true;
        StateManager.getInstance().registerStateAwareObject(this);
        BookmarkableManager.getInstance().registerBookmarkableObject(this);
        initView();
    }

    /**
     * This method should return an unique id for your view.
     * 
     * @return
     */
    protected abstract String getId();

    /**
     * This method should return the container, containing your view.
     * 
     * @return
     */
    public abstract LayoutContainer getView();

    /**
     * Set title of this view, note this will fire an update notification
     * 
     * @param title
     * @see fireMVCViewUpdateNotification
     */
    public void setTitle(String title) {
        this.title = title;
        fireMVCViewUpdateNotification();
    }

    public String getTitle() {
        return title;
    }

    /**
     * Set subTitle of this view, note this will fire an update notification
     * 
     * @param title
     * @see fireMVCViewUpdateNotification
     */
    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
        fireMVCViewUpdateNotification();
    }

    public String getSubtitle() {
        return subtitle;
    }

    public boolean isInitialized() {
        return initialized;
    }

    public void setInitialized(boolean initialized) {
        this.initialized = initialized;
    }

    public String getRegistryId() {
        return getId();
    }

    public void addListener(PresenterListener listener) {
        if (!listeners.contains(listener)) {
            listeners.add(listener);
        }
    }

    public void removeListener(PresenterListener listener) {
        listeners.remove(listener);
    }

    protected void fireMVCViewUpdateNotification() {
        for (PresenterListener l : listeners) {
            l.viewUpdateNotification(this);
        }
    }

	public HandlerManager getEventBus() {
		return eventBus;
	}

	public void setEventBus(HandlerManager eventBus) {
		this.eventBus = eventBus;
	}
}