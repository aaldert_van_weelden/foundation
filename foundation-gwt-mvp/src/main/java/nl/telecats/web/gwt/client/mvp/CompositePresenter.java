package nl.telecats.web.gwt.client.mvp;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.http.client.URL;

/**
 * A Composite Presenter is a presenter that explicitly contains one or more
 * presenters. Regular presenters are capable of containing (sub) presenters,
 * but this CompositePresenter also handles bookmarkable and session states
 * between composite and sub presenters.
 * <p/>
 * Sub presenters are regular Presenters (or perhaps CompositePresenters)
 * <em>that are NOT registered in the Application Registry</em>; the
 * CompositePresenter is responsible for their life cycle and state.
 * <p/>
 * Best practice: in {@link #initView()}, create your sub presenters and
 * register them using {@link #addSubPresenter(Presenter)}. Then create your
 * view which has parameters for the views of the sub presenters, which can be
 * retrieved by using the {@link Presenter#getView()}-method of a sub presenter.
 * 
 * @author danny
 */
public abstract class CompositePresenter extends Presenter {

    /**
     * Event bus for events
     */
    private HandlerManager localEventBus = new HandlerManager(this);

    /**
     * Logging prefix
     */
    private String logPrefix = this.getId() + " (composite): ";

    /**
     * Constructor
     * 
     * @param eventBus
     */
    public CompositePresenter(HandlerManager eventBus) {
        super(eventBus);
    }

    /**
     * All subpresenters, indexed by order of adding.
     */
    private Map<String, Presenter> subPresenters = new HashMap<String, Presenter>();

    /**
     * Adds a sub presenter to the local registry and initializes it's view (
     * {@link Presenter#initView()}).
     * 
     * @param subPresenter
     *            the presenter to add
     * @return the presenter
     */
    protected Presenter addSubPresenter(Presenter subPresenter) {
        subPresenter.initView();
        subPresenter.setInitialized(true);
        String regId = subPresenter.getRegistryId();
        if (subPresenters.containsKey(regId)) {
            int count = 0;
            while (subPresenters.containsKey(regId + count)) {
                count++;
            }
            regId = regId + count;
        }
        subPresenters.put(regId, subPresenter);
        return subPresenter;
    }

    /**
     * Retrieve the presenter that is attached to the provided view.
     * 
     * @param view
     *            view to find presenter for.
     * @return the presenter.
     */
    protected Presenter getPresenterForView(LayoutContainer view) {
        if (view != null) {
            for (Presenter presenter : subPresenters.values()) {
                if (view.equals(presenter.getView())) {
                    return presenter;
                }
            }
        }

        return null;
    }

    /**
     * Return the event bus for this presenter and it's subpresenter.
     * 
     * @return the local event bus.
     */
    public HandlerManager getLocalEventBus() {
        return localEventBus;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * nl.telecats.web.gwt.client.state.StateAware#loadSessionState(java.util
     * .Map)
     */
    @SuppressWarnings("unchecked")
    @Override
    public final void loadSessionState(Map<String, ? extends Serializable> state) {
        for (String index : state.keySet()) {
            if (index.equals("0")) {
                loadLocalSessionState((Map<String, ? extends Serializable>) state.get(index));
            } else {
                subPresenters.get(index).loadSessionState((Map<String, String>) state.get(index));
            }
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see nl.telecats.web.gwt.client.state.StateAware#saveSessionState()
     */
    @Override
    public final HashMap<String, ? extends Serializable> saveSessionState() {
        HashMap<String, HashMap<String, ? extends Serializable>> sessionState = new HashMap<String, HashMap<String, ? extends Serializable>>();
        // save state of main presenter
        sessionState.put(Integer.toString(0), saveLocalSessionState());

        // save state of subpresenters
        for (String index : subPresenters.keySet()) {
            sessionState.put(index, subPresenters.get(index).saveSessionState());
        }

        return sessionState;
    }

    /*
     * (non-Javadoc)
     * 
     * @see nl.telecats.web.gwt.client.state.StateAware#resetState()
     */
    @Override
    public final void resetState() {
        resetLocalState();

        for (String index : subPresenters.keySet()) {
            subPresenters.get(index).resetState();
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * nl.telecats.web.gwt.client.state.Bookmarkable#loadBookmarkableState(java
     * .util.Map)
     */
    @Override
    public final void loadBookmarkableState(Map<String, String> bookmarkableState) {
        for (String index : bookmarkableState.keySet()) {
            Map<String, String> subState = unserialize((String) bookmarkableState.get(index));

            if (index.equals("0")) {
                loadLocalBookmarkableState(subState);
            } else if (subPresenters.containsKey(index)) {
                subPresenters.get(index).loadBookmarkableState(subState);
            }
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * nl.telecats.web.gwt.client.state.Bookmarkable#saveBookmarkableState()
     */
    @Override
    public final Map<String, String> saveBookmarkableState() {
        HashMap<String, String> bookmarkableState = new HashMap<String, String>();

        // serialize the local data
        Map<String, String> subState = saveLocalBookmarkableState();
        if (subState != null) {
            // serialize data
            String serialized = serialize(subState);
            // make URL safe
            serialized = URL.encode(serialized);

            bookmarkableState.put(Integer.toString(0), serialized);
        }

        // serialize for each sub presenter

        for (String index : subPresenters.keySet()) {

            subState = subPresenters.get(index).saveBookmarkableState();
            if (subState != null) {
                String serialized = serialize(subState);
                serialized = URL.encode(serialized);

                bookmarkableState.put(index, serialized);
            }
        }

        return bookmarkableState;
    }

    /**
     * Turns a Map<String, String> into a String
     * 
     * @param data
     *            the Map to serialize
     * @return the Map as serialized String
     */
    private String serialize(Map<String, String> data) {
        String serialized = "";

        for (String key : data.keySet()) {
            if (data.get(key) != null) {
                String value = data.get(key);
                serialized += key.length() + "," + value.length() + "," + key + value;
            }
        }

        return serialized;
    }

    /**
     * Recreates a Map<String, String> from a properly formatted String
     * 
     * @param serialized
     *            the String to make into a Map
     * @return the Map recreated from the String
     */
    private Map<String, String> unserialize(String serialized) {
        Map<String, String> unserialized = new HashMap<String, String>();

        while (serialized.matches("\\d*,\\d*,.*")) {
            String[] parts = serialized.split(",", 3);
            int keyLength = Integer.parseInt(parts[0]);
            int valueLength = Integer.parseInt(parts[1]);

            String key = parts[2].substring(0, keyLength);
            String value = parts[2].substring(keyLength, keyLength + valueLength);

            unserialized.put(key, value);

            serialized = parts[2].substring(keyLength + valueLength);
        }

        return unserialized;
    }

    /**
     * Save (retrieve) the values of the session of this presenter, <em>not</em>
     * the sub presenters.
     * 
     * @return session variables of the local presenter.
     */
    public abstract HashMap<String, ? extends Serializable> saveLocalSessionState();

    /**
     * Load/restore the session of this presenter, <em>not</em> the sub
     * presenters.
     * 
     * @param state
     */
    public abstract void loadLocalSessionState(Map<String, ? extends Serializable> state);

    /**
     * Reset the state of this presenter, <em>not</em> the sub presenters.
     * 
     * @param state
     */
    public abstract void resetLocalState();

    /**
     * Load/restore the bookmarkable state of this presenter, <em>not</em> the
     * sub presenters.
     * 
     * @param state
     */
    public abstract void loadLocalBookmarkableState(Map<String, String> bookmarkableState);

    /**
     * Save the bookmarkable state of this presenter, <em>not</em> the sub
     * presenters.
     * 
     * @param state
     */
    public abstract Map<String, String> saveLocalBookmarkableState();
}
