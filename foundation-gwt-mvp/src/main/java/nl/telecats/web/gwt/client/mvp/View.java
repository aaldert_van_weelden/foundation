package nl.telecats.web.gwt.client.mvp;

import nl.telecats.web.gwt.client.callback.SimpleCallBack;

import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.widget.LayoutContainer;

/**
 * Interface for all views, providing convenience and utility functions.<br />
 * <br />
 * Presenters should create a linking interface based on {@link View} as follows:<br />
 * <code>
 * public class MyPresenter extends Presenter {
 *      public interface Display extends View {
 *      }
 * }
 * </code> <br />
 * Best practice is to create an abstract implementation of this class for the environment it is deployed in, for
 * instance {@link DefaultViewImpl} for GXT environments.<br />
 * <br />
 * The actual view can then combine all these functions:<br />
 * <code>
 * public class MyView extends DefaultViewImpl implements MyPresenter.Display {
 * }
 * </code>
 * 
 * @author Danny Dam Wichers
 */
public interface View {

    /**
     * Retrieve the view.
     * 
     * @return the view.
     */
    public LayoutContainer getView();

    /**
     * Show a dialog in YES/NO style.
     * 
     * @param title
     *            title for the dialog.
     * @param question
     *            question to ask.
     * @param answer
     *            answer on the question: <code>true</code> for yes, <code>false</code> for no.
     */
    public void showConfirm(String title, String question, SimpleCallBack<Boolean> answer);

    /**
     * Show an alert dialog.
     * 
     * @param title
     *            title for the dialog.
     * @param message
     *            alert message.
     * @param callback
     *            optional callback.
     */
    public void showAlert(String title, String message, SimpleCallBack<BaseEvent> callback);

    /**
     * Shows an information panel.
     * 
     * @param title
     *            title for the information panel.
     * @param message
     *            message to show.
     */
    public void showInfo(String title, String message);
}