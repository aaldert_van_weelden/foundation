package nl.telecats.web.gwt.client.mvp;

/**
 * Initializer for your Presenter implementations.
 * 
 * @author Raymond Domingo
 * 
 * @see MVPContainer#openView(String, PresenterInitializer)
 */
public interface PresenterInitializer<P> {
    /**
     * Your initialization code can be placed in here.
     * 
     * @param presenter
     *            to initialize
     */
    public void initialize(P presenter);
}