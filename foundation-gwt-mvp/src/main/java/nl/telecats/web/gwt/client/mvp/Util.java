package nl.telecats.web.gwt.client.mvp;

import com.google.gwt.user.client.History;

/**
 * Utility methods for use in mvp.
 * 
 * @author Raymond Domingo
 * 
 */
public class Util {
    // private static final Dispatcher dispatcher = Dispatcher.get();

    private Util() {
        // This is a Util, don't instantiate it
    }

    /**
     * Obtain a reference to the view registered using specified viewId.
     * 
     * @param presenterId
     * @return
     */
    public static Presenter getPresenter(final String presenterId) {
        Presenter view = Registry.get(presenterId);
        // Make sure the presenter is initialized
        initializeIfNeeded(view);
        return view;
    }

    private static void initializeIfNeeded(Presenter presenter) {
        if (!presenter.isInitialized()) {
            presenter.initialize();
        }
    }

    public static String getUrlParameters() {
        return History.getToken();
    }
}