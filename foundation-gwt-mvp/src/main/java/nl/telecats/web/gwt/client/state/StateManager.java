package nl.telecats.web.gwt.client.state;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Stack;

import nl.telecats.web.gwt.client.mvp.Presenter;

import com.allen_sauer.gwt.log.client.Log;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.History;

/**
 * This manager manages the state of your StateAware objects and updates your url when a new state is created.
 * <p/>
 * Browser back/forward buttons triggers this manager to update the state of your Stateaware objects.
 * <p/>
 * When creating a new state your BookmarkableManager will be consulted for your bookmarkable state, your bookmarkable
 * state will be added to your url.
 * <p/>
 * instanceSpecificStateId consists of: <i>classname</i>:<i>stateId</i>
 * 
 * @author Raymond Domingo
 * 
 * @see StateAware, BookmarkableManager
 */
public class StateManager implements ValueChangeHandler<String> {

    public static final String STATE_ID_TAG = "StateId";
    private static StateManager singleton;
    boolean processing = true;
    private String currentStateId;

    private Stack<String> stateIdentifiers = new Stack<String>();
    private ArrayList<StateAware> stateAwareObjects = new ArrayList<StateAware>();
    private HashMap<String, HashMap<String, ? extends Serializable>> stateStore = new HashMap<String, HashMap<String, ? extends Serializable>>();
    private boolean manualStateCreationTriggered;

    /**
     * Singleton: Private constructor.
     */
    private StateManager() {
        // Add history listener
        History.addValueChangeHandler(this);
    }

    public HashMap<String, HashMap<String, ? extends Serializable>> getStateStore() {
        return stateStore;
    }

    public void registerStateAwareObject(StateAware obj) {
        stateAwareObjects.add(obj);
    }

    public void deregisterStateAwareObject(StateAware obj) {
        stateAwareObjects.remove(obj);
    }

    public static StateManager getInstance() {
        if (singleton == null) {
            singleton = new StateManager();
        }
        return singleton;
    }

    public String getInstanceSpecificStateId(StateAware stateAware) {
        return getInstanceSpecificStateId(stateAware, currentStateId);
    }

    public String getInstanceSpecificStateId(StateAware stateAware, String stateId) {
        String instanceSpecificStateId = stateAware.getClass().getName() + ":" + stateId;
        if (stateAware instanceof Presenter) {
            instanceSpecificStateId += ((Presenter) stateAware).getRegistryId();
        }
        return instanceSpecificStateId;
    }

    /**
     * Update state of a single stateAware object
     * 
     * @param stateAware
     */
    private void updateCurrentState(StateAware stateAware) {
        if (currentStateId != null) {
            String instanceSpecificStateId = getInstanceSpecificStateId(stateAware);
            HashMap<String, ? extends Serializable> state = stateAware.saveSessionState();
            saveStateToStateStore(instanceSpecificStateId, state);
        }
    }

    /**
     * Call this method when your current state information should be updated, because your StateAware objects contain
     * newer state information about your current state.
     */
    public void updateCurrentState() {
        for (StateAware sa : stateAwareObjects) {
            updateCurrentState(sa);
        }
    }

    /**
     * Trigger the loading of state from your stateStore into your StateAware objects.
     * 
     * @param stateId
     */
    private void triggerLoadState(String stateId) {
        for (StateAware sa : stateAwareObjects) {
            sa.resetState();
        }

        for (StateAware sa : stateAwareObjects) {
            String instanceSpecificStateId = getInstanceSpecificStateId(sa, stateId);
            HashMap<String, ? extends Serializable> state = loadStateFromStateStore(instanceSpecificStateId);
            sa.loadSessionState(state != null ? state : new HashMap<String, Serializable>());
        }
    }

    /**
     * Trigger the saving of state from your StateAware objects into your stateStore
     * 
     * @param stateId
     */
    private void triggerSaveState(String stateId) {
        for (StateAware sa : stateAwareObjects) {
            String instanceSpecificStateId = getInstanceSpecificStateId(sa, stateId);
            HashMap<String, ? extends Serializable> state = sa.saveSessionState();
            saveStateToStateStore(instanceSpecificStateId, state);
        }
    }

    /**
     * Save state to your stateStore.
     * 
     * @param instanceSpecificStateId
     * @param state
     */
    public void saveStateToStateStore(String instanceSpecificStateId, HashMap<String, ? extends Serializable> state) {
        if (state == null) {
            return;
        } else {
            stateStore.put(instanceSpecificStateId, state);
        }
    }

    /**
     * Load state from your stateStore
     * 
     * @param instanceSpecificStateId
     * @return
     */
    public HashMap<String, ? extends Serializable> loadStateFromStateStore(String instanceSpecificStateId) {
        HashMap<String, ? extends Serializable> result = stateStore.get(instanceSpecificStateId);
        return result != null ? result : new HashMap<String, Serializable>();
    }

    /**
     * Create a new state.
     */
    public void createNewState() {
        manualStateCreationTriggered = true;
        // cleanup
        String currentToken = readStateId(History.getToken());
        while (stateIdentifiers.size() > 0 && !stateIdentifiers.peek().equals(currentToken)) {
            // cleanup old state identifiers
            String oldToken = stateIdentifiers.pop();
            // cleanup old state information
            String[] stateIdArray = (String[]) stateStore.keySet().toArray(new String[stateStore.size()]);
            for (String oldStateId : stateIdArray) {
                if (oldStateId.endsWith(":" + oldToken)) {
                    stateStore.remove(oldStateId);
                }
            }
        }

        BookmarkableManager.getInstance().pushBookmarkableState(
                BookmarkableManager.getInstance().saveBookmarkableState());

    }

    /**
     * cleans the initialBookmarkState and the state. Used when logging out.
     */
    public void cleanState() {
        BookmarkableManager.getInstance().cleanBookmarkState();
        stateStore.clear();
        for (StateAware sa : stateAwareObjects) {
            sa.resetState();
        }
        // History.newItem("");
    }

    /**
     * Read staetId
     * 
     * @param urlParameters
     * @return
     */
    private String readStateId(String urlParameters) {
        // initial state has no token, this is why we then return the first
        // stateId on the stack
        if (urlParameters.equals("") && stateIdentifiers.size() > 0) {
            return stateIdentifiers.firstElement();
        }

        if (!urlParameters.contains(STATE_ID_TAG + "=")) {
            Log.debug("can't parse token, assuming there is no state:" + urlParameters);
            return null;
        }

        String stateId = urlParameters.substring(urlParameters.indexOf(STATE_ID_TAG + "="));
        stateId = stateId.substring(0, stateId.indexOf(";") + 1);
        return stateId;
    }

    /**
     * Browser back or forward is pressed, or a new state is created
     */
    public void onValueChange(ValueChangeEvent<String> event) {
        String token = event.getValue();
        String stateId = readStateId(token);
        // determine what to do
        if (stateIdentifiers.contains(stateId)) {
            // back / forward is pressed
            updateCurrentState();
            triggerLoadState(stateId);
        } else if (manualStateCreationTriggered) {
            manualStateCreationTriggered = false;
            // a new state is created
            triggerSaveState(stateId);
            stateIdentifiers.add(stateId);
        } else {
            // load bookmark
            triggerPageRefresh();
        }
        currentStateId = stateId;
    }

    public native void triggerPageRefresh() /*-{
		$wnd.location.reload();
    }-*/;

}