package nl.telecats.web.gwt.client.mvp;

import com.google.gwt.event.shared.HandlerManager;


/**
 * Base class for your Data Presenters. It helps to standardize the way data is:
 * <ul>
 * <li/>pushed to the view
 * <li/>reloaded
 * </ul>  
 * 
 * @author Raymond Domingo
 * 
 * @see Presenter
 */
public abstract class DataPresenter <D> extends
        Presenter {
    D data;
    
    public DataPresenter(HandlerManager eventBus) {
    	super(eventBus);
    }

    public void setData(D p) {
        setData(p, true);
    }

    public void setData(D p, boolean needsReload) {
        this.data = p;
        if (data != null) {
            if (needsReload) {
                reloadData();
            } else {
                pushDataToView();
            }
        }
    }

    public D getData() {
        return data;
    }

    public abstract void reloadData();

    protected abstract void pushDataToView();

    protected abstract void applyValues();
}