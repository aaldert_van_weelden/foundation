package nl.telecats.web.gwt.client.mvp;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import nl.telecats.web.gwt.client.mvp.helper.PresenterInstanceHolder;
import nl.telecats.web.gwt.client.state.Bookmarkable;
import nl.telecats.web.gwt.client.state.StateAware;
import nl.telecats.web.gwt.client.state.StateManager;

import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.layout.CardLayout;

/**
 * This container is able of managing a presenter stack.
 * <p/>
 * Additional, when all patterns are correctly implemented, it can provide:
 * <ul>
 * <li/><b>state awareness</b> so your browser back and forward buttons can be
 * used to navigate the view stack
 * <li/><b>bookmarking support</b> so when using your browser refresh button
 * current view is refreshed and bookmarks can be created
 * </ul>
 * <p/>
 * <b>Notes</b>
 * <ul>
 * <li/>To enable <b>state awareness</b> register your instance of the
 * MVPContainer at the StateManager
 * <li/>To enable <b>bookmarking support</b>, register your instance of the
 * MVPContainer at the StateManager and the BookmarkManager.
 * <li/><b>Each instance of a MVPContainer</b> should have an unique registryid.
 * </ul>
 * 
 * @author Raymond Domingo
 * 
 * @see StateManager, BookmarkableManager, MVPContainerListener
 * 
 */
public class MVPContainer extends LayoutContainer implements Bookmarkable, StateAware, PresenterListener {

    public static final String DEFAULT_REGISTRY_ID = "MVPContainer";

    private String registryId = DEFAULT_REGISTRY_ID;

    /**
     * The stack of presenters currently managed by this MVPContainer instance
     */
    private Stack<PresenterInstanceHolder> presenterStack = new Stack<PresenterInstanceHolder>();

    /**
     * Layout used for switching to the active presenter's view
     */
    private CardLayout mvpContainerLayout = new CardLayout();

    /**
     * Listeners of this instance of the MVPContainer
     */
    private List<MVPContainerListener> containerListeners = new ArrayList<MVPContainerListener>();

    /**
     * Create an instance of the MVPContainer using the DEFAULT_REGISTRY_ID
     */
    public MVPContainer() {
        this(null);
    }

    /**
     * This constructor allows you to specify a custom registryId, use this
     * constructor when you use multiple instances of the MVPContainer
     * 
     * @param registryId
     *            to use for this instance.
     */
    public MVPContainer(String registryId) {
        if (registryId != null) {
            setRegistryId(registryId);
        }
        initView();
    }

    /**
     * initialize
     */
    private void initView() {
        setLayout(mvpContainerLayout);
        Registry.register(registryId, this);
    }

    /**
     * This methods opens a view using the presenterID of the to this view
     * related presenter.
     * <p/>
     * <b>Details</b>
     * <ol>
     * <li/>Updates the state before opening the specified view.
     * <li/>Remember the last known state of the view
     * <li/>Resets the state of view to open and initializes is if not
     * initialized before.
     * <li/>Opens the new view and sets it active
     * <li/>Initializes the view when an MVPViewinitializer is specified
     * <li/>Fires openViewNotification is fired before a new state is created
     * <li/>Creates a new state using the StateManager
     * </ol>
     * 
     * @param presenterId
     *            of presenter related to the view to open
     * @param viewInitializer
     *            code to initialize the view (optional, may be null)
     * 
     * @see PresenterInitializer, StateManager, Presenter, Bookmarkable,
     *      PresenterListener
     */
    @SuppressWarnings("unchecked")
    public void openView(final String presenterId, PresenterInitializer viewInitializer) {

        // update the current state
        StateManager.getInstance().updateCurrentState();

        // stop listening to active view and remember it's last known state
        if (getPresenterOfActiveView() != null) {
            getPresenterOfActiveView().removeListener(this);
            getActivePresenterHolder().setLastKnownState(
                    StateManager.getInstance().getInstanceSpecificStateId(getPresenterOfActiveView()));
        }

        // reset state of next view
        Presenter nextView = Util.getPresenter(presenterId);
        nextView.resetState();

        // init next view
        if (viewInitializer != null) {
            viewInitializer.initialize(nextView);
        }

        // open next view
        PresenterInstanceHolder ovh = new PresenterInstanceHolder(nextView.getTitle(), nextView.getSubtitle(),
                presenterId);
        internalOpenView(ovh, false);

        // notifications
        fireOpenViewNotification(ovh);

        // create a new state for the opened view
        StateManager.getInstance().createNewState();
    }

    /**
     * Doesn't trigger events or inform state manager... This method is
     * internally used to restore a state.
     * 
     * @param instanceHolder
     *            OpenViewHolder
     * @paramp reloadData, true will invoke reloadData if view is a MVPDataView
     */
    private void internalOpenView(PresenterInstanceHolder instanceHolder, boolean reloadData) {
        // obtain reference to instance of the view
        Presenter presenter = getPresenter(instanceHolder);

        // update viewstack, should be done before adding 'this' as a
        // mvpviewlistener
        if (!(presenterStack.size() > 0 && getActivePresenterHolder() == instanceHolder)) {
            // when re-opening a view, don't add it to the stack (again)
            presenterStack.push(instanceHolder);
        }

        // (re)load data if DataPresenter
        if (reloadData && presenter instanceof DataPresenter<?>) {
            DataPresenter<?> dataView = (DataPresenter<?>) presenter;
            dataView.reloadData();
        }

        // add listener before initialization (so (sub)title changes will be
        // detected)
        presenter.addListener(this);

        // add view to container
        add(presenter.getView());

        // activate view
        mvpContainerLayout.setActiveItem(presenter.getView());
        layout();
    }

    /**
     * Closes the current active view, restores the previous view or close the
     * last view.
     * 
     * @see closeOpenView(final boolean restorePreviousOpenView, final boolean
     *      allowCloseLastView)
     */
    public void closeOpenView() {
        closeOpenView(true, true);
    }

    /**
     * <b>Details</b>
     * <ol>
     * <li>Updates current state before closing the view.
     * <li/>Before a new state is created, the closeViewNotification is called
     * <li/>After closing the view, the creation of a new state is triggered
     * </ol>
     * <li/>if restorePreviousOpenView is set to true, the previous open window
     * will be restored. <li/>if previousView is a MVPData view, reloadData will
     * be invoked
     * 
     * <b>Note:</b>When there is no active view, this method doesn't do
     * anything.
     * 
     * @param restorePreviousOpenView
     * @param allowCloseLastView
     * @see StateManager, MVPDataView
     */
    @SuppressWarnings("unchecked")
    public void closeOpenView(final boolean restorePreviousOpenView, final boolean allowCloseLastView) {
        if (hasOpenViewCandidates() && (allowCloseLastView || hasMultipleOpenViewCandidates())) {
            StateManager.getInstance().updateCurrentState();
            // close current open view
            PresenterInstanceHolder instanceHolder = presenterStack.pop();
            getPresenter(instanceHolder).removeListener(this);
            removeAll();

            if (restorePreviousOpenView && presenterStack.size() > 0) {
                PresenterInstanceHolder ovh1 = getActivePresenterHolder();

                // restore state of previous open view
                HashMap<String, ? extends Serializable> lastKnownState = StateManager.getInstance()
                        .loadStateFromStateStore(ovh1.getLastKnownStateId());
                getPresenterOfActiveView().resetState();
                getPresenterOfActiveView().loadSessionState(lastKnownState);
                if (getPresenterOfActiveView() instanceof DataPresenter) {
                    ((DataPresenter) getPresenterOfActiveView()).pushDataToView();
                }

                // open previous open view
                internalOpenView(ovh1, true);
            }

            layout();
            fireCloseViewNotification(instanceHolder);
            StateManager.getInstance().createNewState();
        }
    }

    private boolean hasOpenViewCandidates() {
        return presenterStack.size() > 0;
    }

    private boolean hasMultipleOpenViewCandidates() {
        return presenterStack.size() > 1;
    }

    /**
     * @return the current active view in this instance of the MVPContainer
     */
    public Presenter getPresenterOfActiveView() {
        if (presenterStack.size() > 0) {
            return Util.getPresenter(getActivePresenterHolder().getPresenterId());
        } else {
            return null;
        }
    }

    private PresenterInstanceHolder getActivePresenterHolder() {
        return presenterStack.peek();
    }

    /**
     * @param instanceHolder
     * @return view
     */
    private Presenter getPresenter(PresenterInstanceHolder instanceHolder) {
        Presenter view = Util.getPresenter(instanceHolder.getPresenterId());
        return view;
    }

    private void fireOpenViewNotification(PresenterInstanceHolder ovh) {
        for (MVPContainerListener l : containerListeners) {
            l.openViewNotification(getPresenter(ovh), this);
        }
    }

    private void fireCloseViewNotification(PresenterInstanceHolder ovh) {
        for (MVPContainerListener l : containerListeners) {
            l.closeViewNotification(getPresenter(ovh), this);
        }
    }

    private void fireUpdateViewNotification(PresenterInstanceHolder ovh) {
        for (MVPContainerListener l : containerListeners) {
            l.updateViewNotification(getPresenter(ovh), this);
        }
    }

    public void addContainerListener(MVPContainerListener listener) {
        containerListeners.add(listener);
    }

    public void removeContainerListener(MVPContainerListener listener) {
        containerListeners.remove(listener);
    }

    /**
     * @return clone of the viewStack
     */
    @SuppressWarnings("unchecked")
    public Stack<PresenterInstanceHolder> getViewStack() {
        return (Stack<PresenterInstanceHolder>) presenterStack.clone();
    }

    private String nullPatch(String string) {
        return string != null ? string : "";
    }

    /**
     * When the 'active' (last opened) view fires a MVPViewUpdateNotification, a
     * fireUpdateViewNotification is triggered.
     */
    public void viewUpdateNotification(Presenter view) {
        PresenterInstanceHolder ovh = getActivePresenterHolder();
        ovh.setTitle(view.getTitle());
        ovh.setSubtitle(view.getSubtitle());
        fireUpdateViewNotification(ovh);
    }

    // ###############################################################
    // Implementation of StateAware
    // ###############################################################
    @SuppressWarnings("unchecked")
    public void loadSessionState(Map<String, ? extends Serializable> state) {

        String tempRegId = (String) state.get("registryId");
        if (tempRegId != null) {
            registryId = tempRegId;
        }
        removeAll();
        if (state.get("viewStack") != null) {
            presenterStack = (Stack<PresenterInstanceHolder>) ((Stack<PresenterInstanceHolder>) state.get("viewStack"))
                    .clone();
            if (presenterStack.size() > 0) {
                internalOpenView(getActivePresenterHolder(), false);
            }
        }

    }

    @SuppressWarnings("unchecked")
    public HashMap<String, ? extends Serializable> saveSessionState() {
        HashMap<String, Serializable> stateMap = new HashMap<String, Serializable>();
        stateMap.put("registryId", registryId);
        stateMap.put("viewStack", (Stack<PresenterInstanceHolder>) presenterStack.clone());
        return stateMap;
    }

    public void resetState() {
        if (presenterStack.size() > 0) {
            getPresenter(getActivePresenterHolder()).removeListener(this);
        }
        presenterStack.clear();
    }

    // ###############################################################
    // Implementation of Bookmarkable
    // ###############################################################
    public void loadBookmarkableState(Map<String, String> bookmarkableState) {
        resetState();
        for (int cursor = 0; cursor < bookmarkableState.size(); cursor++) {
            String openViewDef = (String) bookmarkableState.get("" + cursor);
            String viewId = openViewDef.substring(0, openViewDef.indexOf(","));
            openViewDef = openViewDef.substring(viewId.length() + 1);
            String title = openViewDef.substring(0, openViewDef.indexOf(","));
            openViewDef = openViewDef.substring(title.length() + 1);
            String subtitle = openViewDef.substring(0);

            // open next view
            Presenter nextView = Util.getPresenter(viewId);
            PresenterInstanceHolder ovh = new PresenterInstanceHolder(title, subtitle, viewId);
            ovh.setLastKnownState(StateManager.getInstance().getInstanceSpecificStateId(nextView));
            internalOpenView(ovh, false);
        }
    }

    public Map<String, String> saveBookmarkableState() {
        HashMap<String, String> bmState = new HashMap<String, String>();
        Iterator<PresenterInstanceHolder> iter = presenterStack.iterator();
        int indexer = 0;
        while (iter.hasNext()) {
            PresenterInstanceHolder view = iter.next();
            String openViewDef = view.getPresenterId() + "," + nullPatch(view.getTitle()) + ","
                    + nullPatch(view.getSubtitle());
            bmState.put("" + indexer++, openViewDef);
        }
        return bmState;
    }

    /**
     * The registryId used to register this instance in the registry
     */
    public String getRegistryId() {
        return registryId;
    }

    /**
     * This method should be private because once this instance is registered
     * the registryId shouldn't be changed anymore.
     * 
     * @see MVPContainer(String registryId)
     */
    private void setRegistryId(String registryId) {
        this.registryId = registryId;
    }

}