package nl.telecats.web.gwt.client.mvp;

/**
 * Interface for listening to changes to an instance of MVPContainer
 * 
 * @author Raymond Domingo
 * 
 * @see MVPContainer
 */
public interface MVPContainerListener {
    void openViewNotification(Presenter openViewHolder, MVPContainer container);

    void closeViewNotification(Presenter openViewHolder, MVPContainer container);

    void updateViewNotification(Presenter openViewHolder,
            MVPContainer container);

}