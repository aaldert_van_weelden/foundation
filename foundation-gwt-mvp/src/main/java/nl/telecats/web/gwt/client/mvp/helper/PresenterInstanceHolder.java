package nl.telecats.web.gwt.client.mvp.helper;

import java.io.Serializable;

/**
 * This is a datacontainer. It is used to store information about presenter in
 * your mvp Stack.
 * 
 * @author Raymond Domingo
 */
public class PresenterInstanceHolder implements Serializable, Comparable<PresenterInstanceHolder> {

    private static final long serialVersionUID = -5354816411218134911L;

    private String subtitle;
    private String title;
    private String viewId;
    private String lastKnownState;

    /**
     * 
     * @param title
     *            of your MVCView instance
     * @param subtitle
     *            of your MVCView instance
     * @param viewId
     *            of your MVCView instance
     */
    public PresenterInstanceHolder(final String title, final String subtitle, final String viewId) {
        setTitle(title);
        setSubtitle(subtitle);
        setViewId(viewId);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPresenterId() {
        return viewId;
    }

    public void setViewId(String viewId) {
        this.viewId = viewId;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getLastKnownStateId() {
        return lastKnownState;
    }

    public void setLastKnownState(String lastKnownState) {
        this.lastKnownState = lastKnownState;
    }

    public int compareTo(PresenterInstanceHolder o) {
        return (title + subtitle + viewId + lastKnownState).compareTo(o.getTitle() + o.getPresenterId());
    }

    @Override
    public int hashCode() {
        return (title + subtitle + viewId + lastKnownState).hashCode();
    }
}