Usage

* EntryPoint
  
  When your applications starts the first thing that will get executed is your
  entryPoints onModuleLoad methode. This is where you should initialize the resources
  needed by your presenters, for example your gwtservices.
  
  After all resources are initialized you should create your Presenters and register these.
  
  See the example.

* BookmarkableManager

  First thing to do in your EntryPoint's onModuleLoad (before initializing any resources) is reading your state from the url.
  This way you obtain your state before the url is changed for whatever reason.
  
  See the example.
  
* Events

  By default we also pass an instance of an HandlerManager (eventBus) to our presenters. This enables
  our presenters to fire or receive events.
  
  For example you could fire an event when the initialization of your application is completed so your ApplicationPresenter knows
  the first view can be opened.
  
  See the example..

* StateManager

  The stateManager is responsible for managing and keeping track of the state of your application.
  After all presenters are initialized and your application is started we update the initial state to reflect
  the 'started' state. This way when your applications user is pressing the back button it will get back to
  this state.
  
  See the example.

*  Example

-----

...
public class Application implements EntryPoint {
...
  public void onModuleLoad() {
    // save state info before url get's changed
    BookmarkableManager.getInstance().saveInitialBookmarkedState();
    
    // create event bus
    HandlerManager eventBus = new HandlerManager(null);

    // create GWTServices
    UserInfoGWTServiceAsync userInfoGwtService = GWT.create(UserInfoGWTService.class);
    ...
  
    // create and register your presenters
    Registry.register(ApplicationPresenter.ID, new ApplicationPresenter(eventBus, userInfoGwtService,
       userGwtService));
    ...
    
    // fire initialized event
    eventBus.fireEvent(new LifeCycleEvent(LifeCycleEvent.Status.INITIALIZED));
    ...
    // update the initial state
    StateManager.getInstance().updateCurrentState();
  }     
}
-----