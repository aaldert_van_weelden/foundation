package nl.telecats.web.gwt.client.state;

import java.util.Map;

/**
 * If the state of your object should be bookmarkable you should implement this
 * interface and register your object on the BookmarkableManager.
 * 
 * @see BookmarkableManager
 * 
 * @author Raymond Domingo
 */
public interface Bookmarkable {

    /**
     * Restore the state of your object using data from the bookmarkableState
     * map
     * 
     * @param bookmarkableState
     */
    void loadBookmarkableState(Map<String, String> bookmarkableState);

    /**
     * Create a map containing the state of your object, so you can restore your
     * state using the data from this map at a later time.
     * 
     * @return your map
     */
    Map<String, String> saveBookmarkableState();

    /**
     * Bookmarkable objects should be available in registry.
     * 
     * @return registryId of your object
     */
    String getRegistryId();
}