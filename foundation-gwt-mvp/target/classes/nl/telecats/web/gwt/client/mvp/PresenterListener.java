package nl.telecats.web.gwt.client.mvp;

/**
 * Listen for changes in your MVCView instance
 * 
 * @author Raymond Domingo
 */
public interface PresenterListener {
    /**
     * This method will be invoked if the data of your presenter instance
     * changes. Like title or subtitle, you can also trigger this yourself.
     * 
     * @param presenter
     * 
     * @see Presenter#fireMVCViewUpdateNotification()
     */
    void viewUpdateNotification(Presenter presenter);
}