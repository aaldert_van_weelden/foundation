package nl.telecats.web.gwt.client.state;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Stack;

import nl.telecats.web.gwt.client.mvp.Presenter;
import nl.telecats.web.gwt.client.mvp.Registry;
import nl.telecats.web.gwt.client.mvp.Util;

import com.allen_sauer.gwt.log.client.Log;
import com.google.gwt.core.client.GWT;
import com.google.gwt.http.client.URL;
import com.google.gwt.user.client.History;

/**
 * This manager manages bookmarkable objects.
 * 
 * NOTE: Your BOOKMARK url will look like:
 * 
 * <pre>
 * ...Application.html#StateId=1244539587554;,BOOKMARK=[MVPContainer],0:MENU_VIEW,null,Hoofdmenu;,1:MENU_VIEW,null,Open menu 1;
 * </pre>
 * 
 * NOTE: Your url (bookmarkable state) is updated after calling createState on the StateManager
 * 
 * @author Raymond Domingo
 * 
 * @see StateManager, Bookmarkable
 */
public class BookmarkableManager {

    private final static String REGISTRY_ID_START_TOKEN = "[";
    private final static String REGISTRY_ID_END_TOKEN = "]";
    private final static String VALUE_SEPARATOR_TOKEN = ",";
    private final static String KEY_VALUE_SEPARATOR_TOKEN = ":";
    private final static String KEY_VALUE_ENTRY_SEPARATOR_TOKEN = ";";

    private final static String BOOKMARK_PARAMETER = "BOOKMARK";
    private static BookmarkableManager singleton;
    private ArrayList<Bookmarkable> bookmarkableObjects = new ArrayList<Bookmarkable>();
    private Stack<String> bookmarkStack = new Stack<String>();
    private String initialBookmarkedState = null;
    private boolean restoringInitialState;

    // some browsers have limited url length.
    // For example IExpl8 urlMaxlENGTH = 2083.
    // we use littel margin because
    // 'Application4HostedBrowser.html?gwt.codesvr=127.0.0.1:9997#'
    // is ignored in calculation and escaping depends on browser implementation
    private static final int MAX_URL_LENGTH = 1900;

    /**
     * singleton, don't allow others to instantiate this class
     */
    private BookmarkableManager() {
    }

    /**
     * Register a bookmarkable object to be managed by this manager
     * 
     * @param BookmarkableObject
     */
    public void registerBookmarkableObject(Bookmarkable bookmarkableObject) {
        bookmarkableObjects.add(bookmarkableObject);
    }

    /**
     * Deregister a bookmarkable object from this manager
     * 
     * @param BookmarkableObject
     */
    public void deregisterSBookmarkableObject(Bookmarkable bookmarkableObject) {
        bookmarkableObjects.remove(bookmarkableObject);
    }

    /**
     * @return the single instance of this manager
     */
    public static BookmarkableManager getInstance() {
        if (singleton == null) {
            singleton = new BookmarkableManager();
        }
        return singleton;
    }

    /**
     * Save's the state of all your Bookmarkable objects and returns a string containing this state.
     * 
     * @return
     */
    public String saveBookmarkableState() {
        String stateString = "";
        for (Bookmarkable b : bookmarkableObjects) {
            Map<String, String> bookmarkableState = b.saveBookmarkableState();

            if (bookmarkableState != null && !bookmarkableState.isEmpty()) {
                Iterator<String> paramIter = bookmarkableState.keySet().iterator();
                stateString += REGISTRY_ID_START_TOKEN + b.getRegistryId() + REGISTRY_ID_END_TOKEN;
                while (paramIter.hasNext()) {
                    String paramKey = paramIter.next();
                    stateString += VALUE_SEPARATOR_TOKEN + paramKey + KEY_VALUE_SEPARATOR_TOKEN
                            + bookmarkableState.get(paramKey) + KEY_VALUE_ENTRY_SEPARATOR_TOKEN;
                }
            }
        }
        return BOOKMARK_PARAMETER + "=" + stateString;
    }

    /**
     * Add new bookmarkable state to the bookmarkable state stack
     */
    public void pushBookmarkableState(String urlParameters) {
        Log.info("pushBookmarkableState:" + urlParameters);
        bookmarkStack.push(urlParameters);
        Iterator<String> bmIter = bookmarkStack.iterator();
        StringBuffer urlStringBuffer = new StringBuffer();
        while (bmIter.hasNext()) {
            urlStringBuffer.append(bmIter.next());
        }

        String baseUrl = GWT.getModuleBaseURL();
        String urlString = StateManager.STATE_ID_TAG + "=" + System.currentTimeMillis() + ";,"
                + urlStringBuffer.toString();
        int l1 = urlString.length();
        int l2 = (MAX_URL_LENGTH - baseUrl.length());
        try {
            // browser are encoding the url, so we need the length of encoded
            // url
            String encodedUrl = URL.encode(urlString);
            l1 = encodedUrl.length();
        } catch (Exception e) {
            Log.debug("Couldn't encode url to determine it's length to detect if maxUrlLength is exceeded.");
        }
        if (l1 > l2) {
            // remove oldest bookmarkable entry, so maxUrlLength isn't exceeded
            Log.info("Removing 'oldest' entry from bookmarkable state. Current size:" + (baseUrl + urlString).length()
                    + " exeeds MAX_URL_LENGTH:" + MAX_URL_LENGTH);
            bookmarkStack.remove(0);
            pushBookmarkableState(null);
        } else {
            History.newItem(urlString, true);
        }
    }

    /**
     * if Url parameters contain a bookmark, the bookmark will be used to load the state of your bookmarkable objects.
     * <p/>
     * When no bookmark is found in your urlParameters, this method will do nothing.
     */
    public void loadBookmarkableState(String urlParameters) {
        String params = urlParameters;
        while (containsBookmark(params)) {
            params = params.substring(params.indexOf(BOOKMARK_PARAMETER + "=") + (BOOKMARK_PARAMETER + "=").length());
            while (params.startsWith("[")) {
                HashMap<String, String> bookmarkableParams = new HashMap<String, String>();
                String regId = readRegistrationId(params);
                params = params.substring(regId.length() + 2);
                while (params.startsWith(VALUE_SEPARATOR_TOKEN)) {
                    String paramKey = readParamKey(params);
                    params = params.substring(paramKey.length() + 1);
                    String paramValue = readParamValue(params);
                    params = params.substring(paramValue.length() + 2);
                    bookmarkableParams.put(paramKey, paramValue);
                }
                Bookmarkable b = Registry.get(regId);
                if (b == null) {
                    Log.warn("No registry entry found for:" + regId + ", can't restore bookmark");
                } else {
                    if (b instanceof Presenter) {
                        // this will initialize the presenter if needed
                        b = Util.getPresenter(regId);
                    }
                    b.loadBookmarkableState(bookmarkableParams);
                }
            }

            if (!restoringInitialState) {
                StateManager.getInstance().createNewState();
            } else {
                Log.debug("restoringInitialState, skipping StateManager.getInstance().createNewState();");
            }
        }
    }

    /**
     * Remember current url parameters as the 'initial bookmarked state'. This enables you to restore this state at any
     * later time. <br/>
     * This might be helpful to login before returning to a bookmarked page.
     */
    public void saveInitialBookmarkedState() {
        initialBookmarkedState = Util.getUrlParameters();
    }

    public boolean hasInitialBookmarkedState() {
        return containsBookmark(initialBookmarkedState);
    }

    /**
     * If there is initialBookmarkedState information available, it will be used to restore the state of the
     * application. Afther restoring the initialBookmarkedState will be reset/cleaned so it won't be used again.
     */
    public void restoreInitialBookmarkedState() {
        String stateToRestore = initialBookmarkedState;
        restoringInitialState = true;
        loadBookmarkableState(stateToRestore);
        restoringInitialState = false;
    }

    /**
     * 
     * @param urlParameters
     * @return true if urlParameters contain a BOOKMARK_PARAMETER (see the final BOOKMARK_PARAMETER in this class)
     */
    public boolean containsBookmark(String urlParameters) {
        return urlParameters != null && urlParameters.contains(BOOKMARK_PARAMETER + "=");
    }

    private String readRegistrationId(String params) {
        return params.substring(1, params.indexOf(REGISTRY_ID_END_TOKEN));
    }

    private String readParamKey(String params) {
        return params.substring(1, params.indexOf(KEY_VALUE_SEPARATOR_TOKEN));
    }

    private String readParamValue(String params) {
        return params.substring(1, params.indexOf(KEY_VALUE_ENTRY_SEPARATOR_TOKEN));
    }

    public void cleanBookmarkState() {
        bookmarkStack.clear();
        initialBookmarkedState = null;

    }

}