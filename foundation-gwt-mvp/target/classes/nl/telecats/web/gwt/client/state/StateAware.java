package nl.telecats.web.gwt.client.state;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Your statefull objects which state should be correctly managed during browser
 * back/forward should be implementing this interface and be registered at the
 * StateManager.
 * 
 * @author Raymond Domingo
 * 
 * @see StateManager
 */
public interface StateAware {

    /**
     * In this method you should restore the state of your object, using the
     * state information from the state hashMap
     * 
     * @param state
     *            to restore
     */
    void loadSessionState(Map<String, ? extends Serializable> state);

    /**
     * In this method you should store sufficient information about your current
     * state, so you can restore your state at a later point in time using this
     * information
     * 
     * @return state information
     */
    HashMap<String, ? extends Serializable> saveSessionState();

    void resetState();

}