package nl.telecats.web.gwt.client.mvp;

import nl.telecats.web.gwt.client.callback.SimpleCallBack;

import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.MessageBoxEvent;
import com.extjs.gxt.ui.client.widget.Dialog;
import com.extjs.gxt.ui.client.widget.Info;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.MessageBox;
import com.extjs.gxt.ui.client.widget.button.Button;

/**
 * Default implementation for {@link View} in GXT environments. This implementation uses {@link LayoutContainer} as it's
 * base, {@link MessageBox} for default dialogs and {@link Info} for information panels.
 * 
 * @author Danny Dam Wichers
 */
public abstract class DefaultViewImpl extends LayoutContainer implements View {

    /**
     * Translation for YES
     */
    private String yes = Dialog.YES;

    /**
     * Translation for NO
     */
    private String no = Dialog.NO;

    /**
     * Constructor
     * 
     * @param yes
     *            translation for YES
     * @param no
     *            translation for NO
     */
    public DefaultViewImpl(String yes, String no) {
        this.yes = yes;
        this.no = no;
    }

    /*
     * (non-Javadoc)
     * 
     * @see nl.telecats.web.gwt.client.mvp.View#showConfirm(java.lang.String, java.lang.String,
     * nl.telecats.web.gwt.client.callback.SimpleCallBack)
     */
    @Override
    public void showConfirm(String title, String question, final SimpleCallBack<Boolean> answer) {
        MessageBox mb = new MessageBox();
        mb.setTitle(title);
        mb.setMessage(question);
        mb.setButtons(Dialog.YESNO);
        mb.addCallback(new Listener<MessageBoxEvent>() {
            @Override
            public void handleEvent(MessageBoxEvent mbe) {
                if (answer != null) {
                    if (Dialog.YES.equals(mbe.getButtonClicked().getItemId())) {
                        answer.doCallBack(true);
                    } else {
                        answer.doCallBack(false);
                    }
                }
            }
        });

        ((Button) mb.getDialog().getButtonBar().getItemByItemId(Dialog.YES)).setText(yes);
        ((Button) mb.getDialog().getButtonBar().getItemByItemId(Dialog.NO)).setText(no);

        mb.show();
    }

    /*
     * (non-Javadoc)
     * 
     * @see nl.telecats.web.gwt.client.mvp.View#showInfo(java.lang.String, java.lang.String)
     */
    @Override
    public void showInfo(String title, String message) {
        Info.display(title, message);
    }

    /*
     * (non-Javadoc)
     * 
     * @see nl.telecats.web.gwt.client.mvp.View#showAlert(java.lang.String, java.lang.String,
     * nl.telecats.web.gwt.client.callback.SimpleCallBack)
     */
    @Override
    public void showAlert(String title, String message, final SimpleCallBack<BaseEvent> callback) {
        MessageBox.alert(title, message, new Listener<MessageBoxEvent>() {
            public void handleEvent(MessageBoxEvent mbe) {
                if (callback != null) {
                    callback.doCallBack(mbe);
                }
            }
        });
    }

    /*
     * (non-Javadoc)
     * 
     * @see nl.telecats.web.gwt.client.mvp.View#getView()
     */
    @Override
    public LayoutContainer getView() {
        return this;
    }
}