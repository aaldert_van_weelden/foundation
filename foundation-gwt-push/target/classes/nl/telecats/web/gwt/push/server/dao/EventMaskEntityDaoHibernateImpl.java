package nl.telecats.web.gwt.push.server.dao;

import java.util.ArrayList;
import java.util.List;

import nl.telecats.web.gwt.push.common.entities.EventMaskEntity;

import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Extend from the AbstractModelDao so we explicitly get the correct transaction
 * manager
 * 
 * @author Raymond Domingo
 * @see EventMaskEntityDao
 * 
 */
@Repository
@Transactional(propagation = Propagation.REQUIRED, value = "foundationGwtPush-transactionManager")
public class EventMaskEntityDaoHibernateImpl extends FoundationGwtPushAbstractModelDao<EventMaskEntity> implements EventMaskEntityDao {

    public EventMaskEntityDaoHibernateImpl() {
        super(EventMaskEntity.class);
    }

    /**
     * @see EventMaskEntityDao#findByListener(String)
     */
    public List<EventMaskEntity> findByListener(String listenerId) {
        return this.findByCiteria(listenerId, null, null, (String) null);
    }

    /**
     * @see EventMaskEntityDao#findByCiteria(String, String, String, String)
     */
    @SuppressWarnings("unchecked")
    public List<EventMaskEntity> findByCiteria(String listenerId, String type, String subtype, String properties) {
        boolean hasCriteria = false;
        DetachedCriteria crit = DetachedCriteria.forClass(EventMaskEntity.class);
        if (!StringUtils.isEmpty(type)) {
            hasCriteria = true;
            crit.add(Restrictions.eq("type", type));
        }
        if (!StringUtils.isEmpty(subtype)) {
            hasCriteria = true;
            crit.add(Restrictions.eq("subtype", subtype));
        }
        if (!StringUtils.isEmpty(properties)) {
            hasCriteria = true;
            crit.add(Restrictions.eq("properties", properties));
        }
        if (!StringUtils.isEmpty(listenerId)) {
            hasCriteria = true;
            crit.add(Restrictions.eq("listenerId", listenerId));
        }

        List<EventMaskEntity> contents = null;
        if (hasCriteria) {
            contents = getHibernateTemplate().findByCriteria(crit);
        } else {
            contents = new ArrayList<EventMaskEntity>();
        }
        return contents;
    }

    /**
     * @see EventMaskEntityDao#deleteAll(List)
     */
    public void deleteAll(List<EventMaskEntity> eventMasks) {
        getHibernateTemplate().deleteAll(eventMasks);
    }
}
