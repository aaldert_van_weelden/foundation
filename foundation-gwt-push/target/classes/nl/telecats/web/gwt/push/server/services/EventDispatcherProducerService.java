package nl.telecats.web.gwt.push.server.services;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import nl.telecats.web.gwt.push.client.event.EventDispatcherConnector;
import nl.telecats.web.gwt.push.common.entities.EventDispatcherConfigEntity;
import nl.telecats.web.gwt.push.common.entities.EventEntity;

/**
 * Distributed event dispatcher, used for pushing events to your gwt clients.
 * 
 * @author Raymond Domingo
 * 
 * @see EventDispatcherConfigEntity
 * @see EventDispatcherConnector
 * @see EventDispatcherServiceImpl
 * 
 */
@WebService(targetNamespace = NameSpace.nameSpace)
public interface EventDispatcherProducerService {

    public static final String serviceName = "EventDispatcherClientService";
    public static final String portName = "EventDispatcherClientServicePort";

    /**
     * Fire an event which will be send to all listeners having registered (using eventMasks) for this kind of event.
     * 
     * @param se
     */
    @WebMethod
    public void fireEvent(@WebParam(name = "event") final EventEntity event);

}
