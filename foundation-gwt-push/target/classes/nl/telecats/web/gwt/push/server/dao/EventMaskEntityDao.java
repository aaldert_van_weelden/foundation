package nl.telecats.web.gwt.push.server.dao;

import java.util.List;

import nl.telecats.foundation.model.dao.GenericDao;
import nl.telecats.web.gwt.push.common.entities.EventMaskEntity;

/**
 * Dao for accessing your EventMask entities
 * 
 * @author Raymond Domingo
 * @see GenericDao
 * 
 */
public interface EventMaskEntityDao extends GenericDao<EventMaskEntity> {

    /**
     * @param listenerId
     * @return All masks of specified listener
     */
    List<EventMaskEntity> findByListener(String listenerId);

    /**
     * Find by some criteria (all parameters are optional) or empty list when no
     * criteria (all params are null) are specified.
     * 
     * @param listenerId
     * @param type
     * @param subtype
     * @param properties
     * @return eventMasks matching specified criteria
     */
    List<EventMaskEntity> findByCiteria(String listenerId, String type, String subtype, String properties);

    /**
     * Delete multiple masks in one call
     * 
     * @param masks
     */
    void deleteAll(List<EventMaskEntity> eventMasks);

}
