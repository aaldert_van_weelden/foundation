package nl.telecats.web.gwt.push.server.dao;

import nl.telecats.web.gwt.push.common.entities.EventDispatcherConfigEntity;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 
 * Extend from the AbstractModelDao so we explicitly get the correct transaction
 * manager
 * 
 * @author Raymond Domingo
 * @see EventDispatcherConfigEntityDao
 * 
 */
@Repository
@Transactional(propagation = Propagation.REQUIRED, value = "foundationGwtPush-transactionManager")
public class EventDispatcherConfigEntityDaoHibernateImpl extends FoundationGwtPushAbstractModelDao<EventDispatcherConfigEntity>
        implements EventDispatcherConfigEntityDao {

    public EventDispatcherConfigEntityDaoHibernateImpl() {
        super(EventDispatcherConfigEntity.class);
    }
}
