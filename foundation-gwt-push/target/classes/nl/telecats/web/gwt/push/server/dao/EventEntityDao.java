package nl.telecats.web.gwt.push.server.dao;

import java.util.List;

import nl.telecats.foundation.model.dao.GenericDao;
import nl.telecats.web.gwt.push.common.entities.EventEntity;

/**
 * Doa for accessing your event entities
 * 
 * @author Raymond Domingo
 * 
 * @see GenericDao
 * 
 */
public interface EventEntityDao extends GenericDao<EventEntity> {

    /**
     * This will return all 'new' (timestamp after listeners lastActivity
     * timestamp) event entities matching the event masks of specified listener.
     * 
     * @param listenerId
     * @return
     */
    List<EventEntity> findByListenerMasksAndTimestamp(String listenerId);

    /**
     * This will return all event entities matching the type, subType and
     * date-range
     * 
     * @param type
     *            The type of the events to retrieve
     * @param subType
     *            The subtype of the events to retrieve
     * @param startDateTime
     *            The time of the events should be past this time (Long)
     * @param endDateTime
     *            The events have to occur before this date and time (Long)
     * @return
     */
    List<EventEntity> findBy(String type, String subType, Long startDateTime, Long endDateTime);

    /**
     * Delete a list of event entities
     * 
     * @param list
     *            of event entities to delete
     */
    void deleteAll(List<EventEntity> list);

    /**
     * This will delete all 'expired' events. These are events received by all
     * listeners currently listening. (event timestamp before 'oldest' listener
     * lastActivity timestamp)
     * 
     * @param purgerId
     *            Id of the entity that requested the purge
     * 
     * @return List containing deleted events
     */
    List<EventEntity> purgeExpiredEvents(String purgerId);

}
