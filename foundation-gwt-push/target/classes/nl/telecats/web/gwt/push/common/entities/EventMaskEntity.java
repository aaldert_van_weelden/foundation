package nl.telecats.web.gwt.push.common.entities;

import java.util.Map;

import nl.telecats.foundation.model.entities.IdentifiableObject;
import nl.telecats.web.gwt.push.common.util.ConversionUtil;

/**
 * By adding an EventMask you can inform the dispatcher about the events a
 * specific listener is interested in. You will only receiving notifications of
 * events matching your eventMasks.
 * <p/>
 * <b>You should always specify your listenerId, otherwise the service won't be
 * able to link eventMasks to your listener.</b>
 * <p/>
 * <b>NOTE:</b> Just setting subtype won't do anything!!!
 * <p/>
 * 
 * <b>NOTE: When an field (type,subtype or properties) is empty, it will be
 * interpreted as wildcard</b>
 * <p/>
 * 
 * <b> First type will be inspected, when type is set the subtype will be
 * inspected and when both (type and subtype) are set the properties will be
 * inspected. </b>
 * <p/>
 * <b>Note</b>: empty string or NULL for type, subtype, or properties will be
 * interpreted as a wildcard.
 * <p/>
 * 
 * Example:
 * 
 * <pre>
 * myEventDispatcherConnector.addEventMask(new EventMaskEntity(eventDispatcherConnector.getListenerId(), MY_TYPE,
 *         MY_SUBTYPE, MY_PROPERTIES));
 * </pre>
 * 
 * @see EventEntity
 * @author Raymond Domingo
 * 
 */
public class EventMaskEntity implements IdentifiableObject {

    private static final long serialVersionUID = -7998633598262593844L;

    private int version = 0;
    private String id;
    private String type;
    private String subtype;
    private String properties;
    private String listenerId;

    /**
     * Default constructor needed for serialization
     */
    public EventMaskEntity() {
        super();
    }

    /**
     * 
     * @param listenerId
     *            (required field)
     * @param type
     *            (optional)
     * @param subtype
     *            (optional)
     * @param properties
     *            (optional)
     */
    public EventMaskEntity(String listenerId, String type, String subtype, String properties) {
        this.listenerId = listenerId;
        this.type = type;
        this.subtype = subtype;
        this.properties = properties;
    }

    /**
     * @see setId
     */
    public String getId() {
        return id;
    }

    /**
     * Technical database id, don't use this
     */
    public void setId(String id) {
        this.id = (String) id;
    }

    /**
     * @see setVersion
     */
    public Integer getVersion() {
        return version;
    }

    /**
     * Technical database version, don't use this
     */
    public void setVersion(Integer v) {
        version = v;
    }

    /**
     * @see setType
     * @return
     */
    public String getType() {
        return type;
    }

    /**
     * When setting the type, you will only receive events matching the
     * specified type.
     * 
     * @param type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @see setSubtype
     * @return
     */
    public String getSubtype() {
        return subtype;
    }

    /**
     * <b>Setting the subtype will only have effect when you also set the
     * type</b>. When you set a subtype you will only receive events matching
     * both: type and subtype
     * 
     * @param subtype
     */
    public void setSubtype(String subtype) {
        this.subtype = subtype;
    }

    /**
     * @see setProperties
     * @return
     */
    public String getProperties() {
        return properties;
    }

    /**
     * <b>Setting properties has only effect when also setting type and
     * subtype</b>. You will only receive events matching type, subtype AND
     * properties.
     * <p/>
     * <b>NOTE:</b> properties will be saved as string.
     * <p/>
     * <b>NOTE:</b> No escaping is performed, so escape your values when needed
     * !
     * 
     * @param properties
     *            (syntax: key1=val1,key2=val2,key3=val3)
     * @see ConversionUtil
     */
    public void setProperties(String properties) {
        this.properties = properties;
    }

    /**
     * @see setProperties
     * @return
     */
    public Map<String, String> getPropertiesMap() {
        return ConversionUtil.stringToMap(properties);
    }

    /**
     * @see setProperties
     */
    public void setPropertiesMap(Map<String, String> propsMap) {
        properties = ConversionUtil.mapToString(propsMap);
    }

    /**
     * The id of the listener 'owning' this eventMask.
     * 
     * @param listenerId
     */
    public void setListenerId(String listenerId) {
        this.listenerId = listenerId;
    }

    /**
     * @see setListenerId
     * @return
     */
    public String getListenerId() {
        return listenerId;
    }

    /**
     * Two event masks are equal when listenerId, type, subtype and properties
     * are equal.
     */
    @Override
    public boolean equals(Object obj) {
        boolean equal = false;
        if (obj instanceof EventMaskEntity) {
            EventMaskEntity otherMask = (EventMaskEntity) obj;
            equal = ((type == null && otherMask.getType() == null) || (type != null && type.equals(otherMask.getType())))
                    && ((subtype == null && otherMask.getSubtype() == null) || (subtype != null && subtype
                            .equals(otherMask.getSubtype())))
                    && ((properties == null && otherMask.getProperties() == null) || (properties != null && properties
                            .equals(otherMask.getProperties())))
                    && ((listenerId == null && otherMask.getListenerId() == null) || (listenerId != null && listenerId
                            .equals(otherMask.getListenerId())));
        }
        return equal;
    }

    /**
     * @see equals
     */
    @Override
    public int hashCode() {
        String hash = "";
        hash += listenerId != null ? listenerId.hashCode() : "null";
        hash += type != null ? type.hashCode() : type;
        hash += subtype != null ? subtype.hashCode() : subtype;
        hash += properties != null ? properties.hashCode() : properties;
        return hash.hashCode();
    }

    /**
     * Custom string representation
     */
    public String toString() {
        return "" + listenerId + "->" + type + "," + subtype + " [" + properties + "]";
    }
}
