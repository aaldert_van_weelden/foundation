package nl.telecats.web.gwt.push.client.services;

import java.util.List;

import net.orcades.spring.gwt.security.client.GWTAuthorizationRequiredException;
import net.orcades.spring.gwt.security.client.GWTSecurityException;
import nl.telecats.web.gwt.push.common.entities.EventDispatcherConfigEntity;
import nl.telecats.web.gwt.push.common.entities.EventEntity;
import nl.telecats.web.gwt.push.common.entities.EventMaskEntity;
import nl.telecats.web.gwt.push.server.gwtservices.EventDispatcherGWTServiceImpl;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * @author Raymond Domingo
 * 
 * @see EventDispatcherGWTServiceImpl
 * 
 */
@RemoteServiceRelativePath("services/user/eventDispatcherGwtService.gwt")
public interface EventDispatcherGWTService extends RemoteService {

    public static class Util {
        public static EventDispatcherGWTServiceAsync getInstance() {
            return GWT.create(EventDispatcherGWTService.class);
        }
    }

    public List<String> getEventListeners() throws GWTAuthorizationRequiredException, GWTSecurityException;

    public List<EventEntity> getEventQueue(String listenerId) throws GWTAuthorizationRequiredException,
            GWTSecurityException;

    public List<EventMaskEntity> getEventMasks(String listenerId) throws GWTAuthorizationRequiredException,
            GWTSecurityException;

    // public long getQuietPeriod() throws GWTAuthorizationRequiredException,
    // GWTSecurityException;

    public void setQuietPeriode(long qp) throws GWTAuthorizationRequiredException, GWTSecurityException;

    public void setHeartbeatInterval(long interval) throws GWTAuthorizationRequiredException, GWTSecurityException;

    // public long getHeartbeatInterval() throws
    // GWTAuthorizationRequiredException, GWTSecurityException;

    public void setPurgeInterval(long interval) throws GWTAuthorizationRequiredException, GWTSecurityException;

    // public long getPurgeInterval() throws GWTAuthorizationRequiredException,
    // GWTSecurityException;

    // public long getExpirationTimeout() throws
    // GWTAuthorizationRequiredException, GWTSecurityException;

    public void setExpirationTimeout(long et) throws GWTAuthorizationRequiredException, GWTSecurityException;

    public void waitForEvent(final String listenerId) throws GWTAuthorizationRequiredException, GWTSecurityException;

    public List<EventEntity> fetchEvents(String listenerId) throws GWTAuthorizationRequiredException,
            GWTSecurityException;

    public String registerServiceEventListener() throws GWTAuthorizationRequiredException, GWTSecurityException;

    public void deregisterServiceEventListener(final String listenerId) throws GWTAuthorizationRequiredException,
            GWTSecurityException;

    public void addEventMask(final EventMaskEntity set) throws GWTAuthorizationRequiredException, GWTSecurityException;

    public void removeEventMask(final String listenerId, final EventMaskEntity set)
            throws GWTAuthorizationRequiredException, GWTSecurityException;

    public void fireEvent(final EventEntity se) throws GWTAuthorizationRequiredException, GWTSecurityException;

    public void setEventDispatcherConfig(long expiration, long heartbeat, long purge, long quiet, long lastNotification)
            throws GWTAuthorizationRequiredException, GWTSecurityException;

    public EventDispatcherConfigEntity getEventDispatcherConfig() throws GWTAuthorizationRequiredException,
            GWTSecurityException;
}
