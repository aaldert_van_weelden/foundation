package nl.telecats.web.gwt.push.common.entities;

import java.util.Map;

import nl.telecats.foundation.model.entities.IdentifiableObject;
import nl.telecats.web.gwt.push.common.util.ConversionUtil;

/**
 * By adding an EventFilter you can inform the dispatcher about the events a
 * specific owner is interested in (or not). This entity generalizes the
 * EventMaskEntity that has previously been introduced for listeners. The
 * EventFilter entity is introduced to give the purger the ability to exclude
 * some events.
 * 
 * @see EventMaskEntity
 * @see EventEntity
 * @author Gregor Ybema
 * 
 */
public class EventFilterEntity implements IdentifiableObject {

    private static final long serialVersionUID = 4016823378946409864L;

    private int version = 0;
    private String id;
    private String type;
    private String subtype;
    private String properties;
    private String ownerId;
    private FilterMode filterMode;

    public enum FilterMode {
        INCLUDE("INCLUDE"), EXCLUDE("EXCLUDE");

        private String value;

        /**
         * @param value
         */
        private FilterMode(String value) {
            this.setValue(value);
        }

        /**
         * @param value
         *            the value to set
         */
        public void setValue(String value) {
            this.value = value;
        }

        /**
         * @return the value
         */
        public String getValue() {
            return value;
        }
    }

    /**
     * Default constructor needed for serialization
     */
    public EventFilterEntity() {
        super();
    }

    /**
     * 
     * @param ownerId
     *            (required field)
     * @param type
     *            (optional)
     * @param subtype
     *            (optional)
     * @param properties
     *            (optional)
     */
    public EventFilterEntity(String ownerId, String type, String subtype, String properties, FilterMode filterMode) {
        this.ownerId = ownerId;
        this.type = type;
        this.subtype = subtype;
        this.properties = properties;
        this.filterMode = filterMode;
    }

    /**
     * @see setId
     */
    public String getId() {
        return id;
    }

    /**
     * Technical database id, don't use this
     */
    public void setId(String id) {
        this.id = (String) id;
    }

    /**
     * @see setVersion
     */
    public Integer getVersion() {
        return version;
    }

    /**
     * Technical database version, don't use this
     */
    public void setVersion(Integer v) {
        version = v;
    }

    /**
     * @see setType
     * @return
     */
    public String getType() {
        return type;
    }

    /**
     * When setting the type, you will only receive events matching the
     * specified type.
     * 
     * @param type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @see setSubtype
     * @return
     */
    public String getSubtype() {
        return subtype;
    }

    /**
     * <b>Setting the subtype will only have effect when you also set the
     * type</b>. When you set a subtype you will only receive events matching
     * both: type and subtype
     * 
     * @param subtype
     */
    public void setSubtype(String subtype) {
        this.subtype = subtype;
    }

    /**
     * @see setProperties
     * @return
     */
    public String getProperties() {
        return properties;
    }

    /**
     * <b>Setting properties has only effect when also setting type and
     * subtype</b>. You will only receive events matching type, subtype AND
     * properties.
     * <p/>
     * <b>NOTE:</b> properties will be saved as string.
     * <p/>
     * <b>NOTE:</b> No escaping is performed, so escape your values when needed
     * !
     * 
     * @param properties
     *            (syntax: key1=val1,key2=val2,key3=val3)
     * @see ConversionUtil
     */
    public void setProperties(String properties) {
        this.properties = properties;
    }

    /**
     * @see setProperties
     * @return
     */
    public Map<String, String> getPropertiesMap() {
        return ConversionUtil.stringToMap(properties);
    }

    /**
     * @see setProperties
     */
    public void setPropertiesMap(Map<String, String> propsMap) {
        properties = ConversionUtil.mapToString(propsMap);
    }

    /**
     * The id of the owner of this eventFilter.
     * 
     * @param ownerId
     */
    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    /**
     * @see setOwnerId
     * @return
     */
    public String getOwnerId() {
        return ownerId;
    }

    /**
     * Set the FilterMode of this EventFilter. EXCLUDE: the filter will accept
     * all events excluding the events containing the given properties INCLUDE:
     * the filter will only accept events containing the given properties
     * 
     * EXCLUDE trumps INCLUDE!
     * 
     * @param FilterMode
     */
    public void setFilterMode(FilterMode filterMode) {
        this.filterMode = filterMode;
    }

    /**
     * Set the filterMode as a string.
     * 
     * @param filterModeAsString
     * @throws IllegalArgumentException
     *             If no matching FilterMode is found for the provided value
     */
    public void setFilterModeAsString(String filterModeAsString) {
        if (filterModeAsString == null) {
            filterMode = null;
        } else {
            for (FilterMode i : FilterMode.values()) {
                if (i.getValue().equals(filterModeAsString)) {
                    filterMode = i;
                    return;
                }
            }
            throw new IllegalArgumentException("Unknown FilterMode provided: " + filterModeAsString);
        }
    }

    /**
     * @see setFilterMode
     * @return
     */
    public FilterMode getFilterMode() {
        return filterMode;
    }

    /**
     * Retrieve the string value of the filtermode
     * 
     * @return The string value of filtermode
     */
    public String getFilterModeAsString() {
        return (filterMode == null ? null : filterMode.getValue());
    }

    /**
     * Two event masks are equal when ownerId, type, subtype, properties and
     * filterMode are equal.
     */
    @Override
    public boolean equals(Object obj) {
        boolean equal = false;
        if (obj instanceof EventFilterEntity) {
            EventFilterEntity otherFilter = (EventFilterEntity) obj;
            equal = ((type == null && otherFilter.getType() == null) || (type != null && type.equals(otherFilter
                    .getType())))
                    && ((subtype == null && otherFilter.getSubtype() == null) || (subtype != null && subtype
                            .equals(otherFilter.getSubtype())))
                    && ((properties == null && otherFilter.getProperties() == null) || (properties != null && properties
                            .equals(otherFilter.getProperties())))
                    && ((ownerId == null && otherFilter.getOwnerId() == null) || (ownerId != null && ownerId
                            .equals(otherFilter.getOwnerId())))
                    && ((filterMode == null && otherFilter.getFilterMode() == null) || (filterMode != null && filterMode
                            .equals(otherFilter.getFilterMode())));
        }
        return equal;
    }

    /**
     * @see equals
     */
    @Override
    public int hashCode() {
        String hash = "";
        hash += ownerId != null ? ownerId.hashCode() : "null";
        hash += type != null ? type.hashCode() : type;
        hash += subtype != null ? subtype.hashCode() : subtype;
        hash += properties != null ? properties.hashCode() : properties;
        hash += filterMode != null ? filterMode.hashCode() : filterMode;
        return hash.hashCode();
    }

    /**
     * Custom string representation
     */
    public String toString() {
        return "" + ownerId + "->" + type + "," + subtype + " [" + properties + "] " + filterMode.getValue();
    }
}
