package nl.telecats.web.gwt.push.server.services;

public class NameSpace {

    public static final String nameSpace = "http://www.telecats.nl/nl.telecats.web.gwt.push.foundation-gwt-push";

    private NameSpace() {
    }
}
