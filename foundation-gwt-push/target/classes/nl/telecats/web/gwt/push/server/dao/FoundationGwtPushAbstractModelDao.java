package nl.telecats.web.gwt.push.server.dao;

import nl.telecats.foundation.model.dao.hibernate.AbstractGenericDaoImpl;
import nl.telecats.foundation.model.entities.IdentifiableObject;

import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Abstract DAO to set the sessionfactory to the sessionfactory defined in the context.
 * 
 * @author Laurens Satink
 * 
 * @param <T>
 */
public abstract class FoundationGwtPushAbstractModelDao<T extends IdentifiableObject> extends AbstractGenericDaoImpl<T> {

    private Logger log = LoggerFactory.getLogger(FoundationGwtPushAbstractModelDao.class);

    public FoundationGwtPushAbstractModelDao(Class<T> type) {
        super(type);
    }

    @Autowired
    public final void setFoundationGwtPushSessionFactory(SessionFactory foundationGwtPushSessionFactory) {
        if (foundationGwtPushSessionFactory == null) {
            log.warn("Provided SessionFactory is null");
        } else {
            log.info("Setting foundationGwtPushSessionFactory to " + foundationGwtPushSessionFactory);
        }

        super.setSessionFactory(foundationGwtPushSessionFactory);
    }
}
