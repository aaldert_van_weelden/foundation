package nl.telecats.web.gwt.push.server.dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import nl.telecats.foundation.model.exceptions.ObjectNotFoundException;
import nl.telecats.web.gwt.push.common.entities.EventEntity;
import nl.telecats.web.gwt.push.common.entities.EventFilterEntity;
import nl.telecats.web.gwt.push.common.entities.EventListenerEntity;
import nl.telecats.web.gwt.push.common.entities.EventMaskEntity;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Extend from the AbstractModelDao so we explicitly get the correct transaction manager, then propagate it in the @tTransactional
 * tag.
 * 
 * @see EventEntityDao
 * @author raymond
 * 
 */
@Repository
@Transactional(propagation = Propagation.REQUIRED, value = "foundationGwtPush-transactionManager")
public class EventEntityDaoHibernateImpl extends FoundationGwtPushAbstractModelDao<EventEntity> implements EventEntityDao {

    @Autowired
    private EventMaskEntityDao eventMaskDao;
    @Autowired
    private EventFilterEntityDao eventFilterDao;
    @Autowired
    private EventListenerEntityDao listenerDao;

    /**
     * Logger
     */
    private Logger log = Logger.getLogger(EventEntityDaoHibernateImpl.class);

    public EventEntityDaoHibernateImpl() {
        super(EventEntity.class);
    }

    /**
     * @see EventEntityDaoHibernateImpl#findByListenerMasksAndTimestamp(String)
     */
    @SuppressWarnings("unchecked")
    public List<EventEntity> findByListenerMasksAndTimestamp(String listenerId) {
        String logListenerId = (listenerId == null ? "<null>" : listenerId);
        log.debug("Finding events for listener " + logListenerId);

        // search listener
        EventListenerEntity listener;
        List<EventEntity> contents = null;
        try {
            listener = listenerDao.findObject(listenerId);
        } catch (ObjectNotFoundException onfe) {
            log.debug("Listener " + logListenerId + " not found; returning empty set");
            // listener was probably removed
            return new ArrayList<EventEntity>();
        }

        // find all event entities that :
        // - were added after listener.getLastActivity
        // - have matching masks; masks are matched by type, subtype and properties; null for any of these is treated as
        // a wildcard
        // order the result by event timestamp

        // search event masks
        List<EventMaskEntity> masks = eventMaskDao.findByListener(listenerId);
        List<Criterion> maskCriteriaList = new ArrayList<Criterion>();

        // no masks; return empty list.
        if (masks.isEmpty()) {
            log.debug("Listener " + logListenerId + " has no event masks; returning empty set");
            return new ArrayList<EventEntity>();
        }

        boolean findAll = false;
        for (EventMaskEntity eme : masks) {
            // filter by type, subtype and properties
            if ((!StringUtils.isEmpty(eme.getType())) && (!StringUtils.isEmpty(eme.getSubtype()))
                    && (!StringUtils.isEmpty(eme.getProperties()))) {
                maskCriteriaList.add(Restrictions.and(
                        Restrictions.eq(EventEntity.TYPE_FIELD, eme.getType()),
                        Restrictions.and(Restrictions.eq(EventEntity.SUBTYPE_FIELD, eme.getSubtype()),
                                Restrictions.eq("properties", eme.getProperties()))));

                // filter by type and subtype
            } else if ((!StringUtils.isEmpty(eme.getType())) && (!StringUtils.isEmpty(eme.getSubtype()))) {
                maskCriteriaList.add(Restrictions.and(Restrictions.eq(EventEntity.TYPE_FIELD, eme.getType()),
                        Restrictions.eq(EventEntity.SUBTYPE_FIELD, eme.getSubtype())));

                // filter by type
            } else if ((!StringUtils.isEmpty(eme.getType()))) {
                maskCriteriaList.add(Restrictions.eq(EventEntity.TYPE_FIELD, eme.getType()));

            } else {
                findAll = true;
            }
        }

        if (log.isDebugEnabled()) {
            if (findAll == true) {
                log.debug("Finding events for " + logListenerId + " using findAll, starting from "
                        + listener.getLastActivity());
            } else {
                log.debug("Finding events for " + logListenerId + " using " + masks.size() + " mask(s), starting from "
                        + listener.getLastActivity());
            }
        }

        DetachedCriteria criteria = DetachedCriteria.forClass(EventEntity.class);
        criteria.add(Restrictions.gt(EventEntity.TIMESTAMP_FIELD, listener.getLastActivity()));
        criteria.addOrder(Order.asc(EventEntity.TIMESTAMP_FIELD));

        if (!findAll) {
            criteria.add(createOrCriteria(maskCriteriaList));
        }

        // fetch events matching criteria
        contents = getHibernateTemplate().findByCriteria(criteria);

        log.debug("Found " + contents.size() + " event(s) for " + logListenerId);

        return contents;
    }

    private Criterion createOrCriteria(List<Criterion> maskCriteriaList) {
        if (maskCriteriaList.size() == 1) {
            return maskCriteriaList.get(0);
        } else if (maskCriteriaList.size() > 1) {
            Iterator<Criterion> ci = maskCriteriaList.iterator();
            return Restrictions.or(ci.next(), createOrCriteria(ci));
        } else {
            return null;
        }
    }

    private Criterion createOrCriteria(Iterator<Criterion> ci) {
        Criterion c = ci.next();
        if (ci.hasNext()) {
            return Restrictions.or(c, createOrCriteria(ci));
        } else {
            return c;
        }
    }

    /**
     * @see EventEntityDaoHibernateImpl#deleteAll(List)
     */
    public void deleteAll(List<EventEntity> list) {
        getHibernateTemplate().deleteAll(list);
    }

    /**
     * @see EventEntityDaoHibernateImpl#purgeExpiredEvents()
     */
    @SuppressWarnings("unchecked")
    public List<EventEntity> purgeExpiredEvents(String purgerId) {
        DetachedCriteria eventCriteria = DetachedCriteria.forClass(EventEntity.class);

        // search event filters
        List<EventFilterEntity> filters = eventFilterDao.findByOwner(purgerId);
        List<Criterion> includeCriteriaList = new ArrayList<Criterion>();
        List<Criterion> excludeCriteriaList = new ArrayList<Criterion>();

        boolean includeAll = false;
        boolean excludeAll = false;
        for (EventFilterEntity efe : filters) {
            if (EventFilterEntity.FilterMode.INCLUDE.equals(efe.getFilterMode())) {
                // filter by type, subtype and properties
                if ((!StringUtils.isEmpty(efe.getType())) && (!StringUtils.isEmpty(efe.getSubtype()))
                        && (!StringUtils.isEmpty(efe.getProperties()))) {
                    includeCriteriaList.add(Restrictions.and(
                            Restrictions.eq(EventEntity.TYPE_FIELD, efe.getType()),
                            Restrictions.and(Restrictions.eq(EventEntity.SUBTYPE_FIELD, efe.getSubtype()),
                                    Restrictions.eq("properties", efe.getProperties()))));

                    // filter by type and subtype
                } else if ((!StringUtils.isEmpty(efe.getType())) && (!StringUtils.isEmpty(efe.getSubtype()))) {
                    includeCriteriaList.add(Restrictions.and(Restrictions.eq(EventEntity.TYPE_FIELD, efe.getType()),
                            Restrictions.eq(EventEntity.SUBTYPE_FIELD, efe.getSubtype())));

                    // filter by type
                } else if ((!StringUtils.isEmpty(efe.getType()))) {
                    includeCriteriaList.add(Restrictions.eq(EventEntity.TYPE_FIELD, efe.getType()));

                } else {
                    includeAll = true;
                }
            } else if (EventFilterEntity.FilterMode.EXCLUDE.equals(efe.getFilterMode())) {
                // filter by type, subtype and properties
                if ((!StringUtils.isEmpty(efe.getType())) && (!StringUtils.isEmpty(efe.getSubtype()))
                        && (!StringUtils.isEmpty(efe.getProperties()))) {
                    excludeCriteriaList.add(Restrictions.and(
                            Restrictions.eq(EventEntity.TYPE_FIELD, efe.getType()),
                            Restrictions.and(Restrictions.eq(EventEntity.SUBTYPE_FIELD, efe.getSubtype()),
                                    Restrictions.eq("properties", efe.getProperties()))));

                    // filter by type and subtype
                } else if ((!StringUtils.isEmpty(efe.getType())) && (!StringUtils.isEmpty(efe.getSubtype()))) {
                    excludeCriteriaList.add(Restrictions.and(Restrictions.eq(EventEntity.TYPE_FIELD, efe.getType()),
                            Restrictions.eq(EventEntity.SUBTYPE_FIELD, efe.getSubtype())));

                    // filter by type
                } else if ((!StringUtils.isEmpty(efe.getType()))) {
                    excludeCriteriaList.add(Restrictions.eq(EventEntity.TYPE_FIELD, efe.getType()));

                } else {
                    excludeAll = true;
                }
            }
        }

        if (excludeAll) {
            // all events will be excluded, so don't delete anyting an return
            // empty list
            return new ArrayList<EventEntity>();
        } else if (includeAll) {
            // alle events will be included, except those that need to be
            // excluded
            if (!excludeCriteriaList.isEmpty()) {
                eventCriteria.add(Restrictions.not(createOrCriteria(excludeCriteriaList)));
            }
        } else if (includeCriteriaList.isEmpty()) {
            // no events will be included, so dont't delete anyting and return
            // empyt list
            return new ArrayList<EventEntity>();
        } else {
            // include events in the include list, except those that need to be
            // excluded
            if (excludeCriteriaList.isEmpty()) {
                eventCriteria.add(createOrCriteria(includeCriteriaList));
            } else {
                eventCriteria.add(Restrictions.and(createOrCriteria(includeCriteriaList),
                        Restrictions.not(createOrCriteria(excludeCriteriaList))));
            }
        }

        DetachedCriteria listenerCriteria = DetachedCriteria.forClass(EventListenerEntity.class);
        ProjectionList projList = Projections.projectionList();
        projList.add(Projections.min(EventListenerEntity.LAST_ACTIVITY_FIELD));
        listenerCriteria.setProjection(projList);

        // determine expiration timestamp so we can purge all events before this
        // timestamp
        List<Long> oldestListener = getHibernateTemplate().findByCriteria(listenerCriteria);
        List<EventEntity> outdatedEvents = null;
        if (!oldestListener.isEmpty() && oldestListener.get(0) != null) {
            Long expirationTimestamp = oldestListener.get(0);
            eventCriteria.add(Restrictions.lt(EventEntity.TIMESTAMP_FIELD, expirationTimestamp));
        }
        outdatedEvents = getHibernateTemplate().findByCriteria(eventCriteria);
        deleteAll(outdatedEvents);
        return outdatedEvents;
    }

    @SuppressWarnings("unchecked")
    public List<EventEntity> findBy(String type, String subType, Long startDateTime, Long endDateTime) {
        DetachedCriteria crit = DetachedCriteria.forClass(EventEntity.class);

        if (type != null) {
            crit.add(Restrictions.eq(EventEntity.TYPE_FIELD, type));
        }
        if (subType != null) {
            crit.add(Restrictions.eq(EventEntity.SUBTYPE_FIELD, subType));
        }

        if ((startDateTime != null) && endDateTime != null) {
            crit.add(Restrictions.between(EventEntity.TIMESTAMP_FIELD, startDateTime, endDateTime));
        } else if (startDateTime != null) {
            crit.add(Restrictions.ge(EventEntity.TIMESTAMP_FIELD, startDateTime));
        } else if (endDateTime != null) {
            crit.add(Restrictions.le(EventEntity.TIMESTAMP_FIELD, endDateTime));
        }

        List<EventEntity> events = getHibernateTemplate().findByCriteria(crit);

        return events;
    }
}
