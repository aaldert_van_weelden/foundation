package nl.telecats.web.gwt.push.client.services;

import nl.telecats.web.gwt.push.common.entities.EventDispatcherConfigEntity;

/**
 * @author Raymond Domingo
 * 
 * @see @see EventDispatcherService
 * 
 */
public interface EventDispatcherGWTServiceAsync {

    void addEventMask(nl.telecats.web.gwt.push.common.entities.EventMaskEntity set,
            com.google.gwt.user.client.rpc.AsyncCallback<java.lang.Void> arg2);

    void deregisterServiceEventListener(java.lang.String listenerId,
            com.google.gwt.user.client.rpc.AsyncCallback<java.lang.Void> arg2);

    void fetchEvents(
            java.lang.String listenerId,
            com.google.gwt.user.client.rpc.AsyncCallback<java.util.List<nl.telecats.web.gwt.push.common.entities.EventEntity>> arg2);

    void fireEvent(nl.telecats.web.gwt.push.common.entities.EventEntity se,
            com.google.gwt.user.client.rpc.AsyncCallback<java.lang.Void> arg2);

    void getEventMasks(
            java.lang.String listenerId,
            com.google.gwt.user.client.rpc.AsyncCallback<java.util.List<nl.telecats.web.gwt.push.common.entities.EventMaskEntity>> arg2);

    void getEventQueue(
            java.lang.String listenerId,
            com.google.gwt.user.client.rpc.AsyncCallback<java.util.List<nl.telecats.web.gwt.push.common.entities.EventEntity>> arg2);

    // void
    // getExpirationTimeout(com.google.gwt.user.client.rpc.AsyncCallback<java.lang.Long>
    // arg1);

    // void
    // getHeartbeatInterval(com.google.gwt.user.client.rpc.AsyncCallback<java.lang.Long>
    // arg1);

    void getEventListeners(com.google.gwt.user.client.rpc.AsyncCallback<java.util.List<java.lang.String>> arg1);

    // void
    // getPurgeInterval(com.google.gwt.user.client.rpc.AsyncCallback<java.lang.Long>
    // arg1);

    // void
    // getQuietPeriod(com.google.gwt.user.client.rpc.AsyncCallback<java.lang.Long>
    // arg1);

    void registerServiceEventListener(com.google.gwt.user.client.rpc.AsyncCallback<java.lang.String> arg1);

    void removeEventMask(java.lang.String listenerId, nl.telecats.web.gwt.push.common.entities.EventMaskEntity set,
            com.google.gwt.user.client.rpc.AsyncCallback<java.lang.Void> arg3);

    void setExpirationTimeout(long et, com.google.gwt.user.client.rpc.AsyncCallback<java.lang.Void> arg2);

    void setHeartbeatInterval(long interval, com.google.gwt.user.client.rpc.AsyncCallback<java.lang.Void> arg2);

    void setPurgeInterval(long interval, com.google.gwt.user.client.rpc.AsyncCallback<java.lang.Void> arg2);

    void setQuietPeriode(long qp, com.google.gwt.user.client.rpc.AsyncCallback<java.lang.Void> arg2);

    void waitForEvent(java.lang.String listenerId, com.google.gwt.user.client.rpc.AsyncCallback<java.lang.Void> arg2);

    void setEventDispatcherConfig(long expiration, long heartbeat, long purge, long quiet, long lastNotification,
            com.google.gwt.user.client.rpc.AsyncCallback<java.lang.Void> arg2);

    void getEventDispatcherConfig(com.google.gwt.user.client.rpc.AsyncCallback<EventDispatcherConfigEntity> config);
}