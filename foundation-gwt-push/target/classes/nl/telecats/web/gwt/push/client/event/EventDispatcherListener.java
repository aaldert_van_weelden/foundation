package nl.telecats.web.gwt.push.client.event;

import java.util.List;

import nl.telecats.web.gwt.push.common.entities.EventEntity;

/**
 * Listener used to listen for events from eventDispatcherService.
 * <p/>
 * Usage: You can register this listener at the eventDispatcherConnector
 * 
 * @author Raymond Domingo
 * 
 * @see EventDispatcherConnector
 */
public interface EventDispatcherListener {
    /**
     * You will be notied about events matching your eventMasks.
     * 
     * @param events
     */
    public void onDispatchedEvents(List<EventEntity> events);
}