package nl.telecats.web.gwt.push.server.services;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import nl.telecats.web.gwt.push.common.entities.EventFilterEntity;

/**
 * Distributed event dispatcher, used for pushing events to your gwt clients.
 * 
 * @author raymond
 * 
 */
@WebService(targetNamespace = NameSpace.nameSpace)
public interface PurgeService {

    public static final String serviceName = "PurgeService";
    public static final String portName = "PurgeServicePort";

    @WebMethod(operationName = "startPurging", action = "startPurging")
    public void startPurging();

    @WebMethod(operationName = "setEventFilters", action = "setEventFilters")
    public void setEventFilters(@WebParam(name = "eventFilters") List<EventFilterEntity> eventFilters);

    @WebMethod(operationName = "getEventFilters", action = "getEventFilters")
    @WebResult(name = "eventFilters")
    public List<EventFilterEntity> getEventFilters();

    @WebMethod(operationName = "getPurgeServiceId", action = "getPurgeServiceId")
    @WebResult(name = "id")
    public String getPurgeServiceId();

}
