package nl.telecats.web.gwt.push.server.services;

import javax.jws.WebMethod;
import javax.jws.WebService;

/**
 * Distributed event dispatcher, used for pushing events to your gwt clients.
 * 
 * @author raymond
 * 
 */
@WebService(targetNamespace = NameSpace.nameSpace)
public interface QuietPeriodeService {

    public static final String serviceName = "QuietPeriodeService";
    public static final String portName = "QuietPeriodeServicePort";

    @WebMethod(operationName = "startQuietPeriode", action = "startQueietPeriode")
    public void startQuietPeriode();

    @WebMethod(operationName = "isRunning", action = "isRunning")
    public boolean isRunning();
}
