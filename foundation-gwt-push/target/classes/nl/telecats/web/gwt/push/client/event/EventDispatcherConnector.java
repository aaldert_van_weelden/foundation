package nl.telecats.web.gwt.push.client.event;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.orcades.spring.gwt.security.client.GWTAuthentication;
import nl.telecats.web.gwt.client.security.SecuredAsyncCallback;
import nl.telecats.web.gwt.push.client.services.EventDispatcherGWTService;
import nl.telecats.web.gwt.push.client.services.EventDispatcherGWTServiceAsync;
import nl.telecats.web.gwt.push.common.entities.EventDispatcherConfigEntity;
import nl.telecats.web.gwt.push.common.entities.EventEntity;
import nl.telecats.web.gwt.push.common.entities.EventMaskEntity;
import nl.telecats.web.gwt.push.server.services.EventDispatcherService;

import com.allen_sauer.gwt.log.client.Log;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * Gwt client side part of EventDispatcher. Used to connect a gwt client to the EventDispatcher(GWT) service.
 * <p/>
 * By default one eventMask is automatically added. This is to prevent connection timeouts.
 * <p/>
 * <h3>Usage</h3>
 * <ul>
 * <li/>Register your listeners
 * <li/>Add your eventMasks
 * <li/>Start the eventDispatcherConnector
 * </ul>
 * 
 * @see EventDispatcherService
 * @see EventDispatcherListener
 * @see EventDispatcherConfigEntity#heartbeatInterval
 * 
 * @author Raymond Domingo
 */
public class EventDispatcherConnector {

    /**
     * Time to wait before re-trying after a failed register attemp with the eventDispatcherService. Currently hardcoded
     * to 2sec
     */
    private final static int RECONNECT_ATTEMPT_INTERVAL = 2000;

    private static EventDispatcherConnector singletonInstance;

    /**
     * Logging prefix
     */
    private static final String logPrefix = "EventDispatcherConnector: ";

    /**
     * The eventDispatcher gwt service (backend)
     */
    private EventDispatcherGWTServiceAsync edService = (EventDispatcherGWTServiceAsync) GWT
            .create(EventDispatcherGWTService.class);

    /**
     * Keep track of current dispatching status. If dispatching, don't allow to start dispatcher a second time. And if
     * set to false, stopDispatching as soon as 'waitForEvent' call returns.
     */
    private boolean dispatching = false;

    /**
     * listener id obtained from EventDispatcherService after registering.
     */
    private String listenerId;

    /**
     * All listeners registered with this EventDispatcherConnector
     */
    private List<EventDispatcherListener> listeners = new ArrayList<EventDispatcherListener>();

    /**
     * Don't add same mask twice, but increase counter when using same mask several times. This prevents you to receive
     * same event multiple times and saves band width.
     */
    private Map<EventMaskEntity, Integer> maskCount = new HashMap<EventMaskEntity, Integer>();

    /**
     * Boolean indicating if busyDialog should be used during calls to backend.
     * <p/>
     * <b>NOTE:</b> when the blocking waitForEvents method is called the busyDialog won't be used.
     * <p/>
     * Defaults to true
     */
    private boolean enableBusyDialog = true;

    /**
     * Booleaqn indicating if an erryrDialog should be shown after an error is received from server.
     * <p/>
     * Defaults to true
     */
    private boolean enableErrorDialog = true;

    /**
     * This boolean indicates if the eventDispatcherConnector should try to reconnect, when it got somehow disconnected.
     * <p/>
     * Default is true
     */
    private boolean enableAutoReconnect = true;

    /**
     * Singleton instance, don't allow to instantiate this singleton.
     * <p/>
     * Use getInstance to obtain instance to this singleton.
     */
    private EventDispatcherConnector() {
    }

    /**
     * @return singleton instance of EventDispatcherConnector
     */
    public static EventDispatcherConnector getInstance() {
        if (singletonInstance == null) {
            singletonInstance = new EventDispatcherConnector();
        }
        return singletonInstance;
    }

    /**
     * ListenerId received during registration with eventDispatcherService
     * 
     * @return listenerId
     */
    public String getListenerId() {
        return listenerId;
    }

    /**
     * Fire an event.
     * <p/>
     * <b>NOTE:</b>This event will be distributed by the eventDispatcherService to all other clients which have
     * registered for this kind of event.
     * 
     * @param event
     * @see EventEntity
     */
    public synchronized void fireEvent(final EventEntity event) {
        Log.info(logPrefix + "fireEvent:" + event);

        SecuredAsyncCallback<Void> secCallBack = new SecuredAsyncCallback<Void>(enableBusyDialog, enableErrorDialog) {
            @Override
            protected void doOnFailure(Throwable throwable) {
                Log.error(logPrefix + "Received errorResponse from server when calling fireEvent:" + event, throwable);
            }

            @Override
            public void doOnSuccess(Void object) {
                Log.debug(logPrefix + "Received successResponse for fireEvent:" + event);
            }
        };

        edService.fireEvent(event, secCallBack);
    }

    /**
     * Add eventMask to event dispatcher service, so you will be notified when matching events are fired. Calling this
     * method will update the listenerId in your EventMaskEntity, so it matches the listenerId of this
     * eventDispatcherConnector.
     * <p/>
     * When you are authenticated and the eventDispatcherConnector is started the event mask is added to the
     * eventDispatcherService, when not adding will be <b>prosponed</b> until starting the EventDispatcherConnector.
     * 
     * @param eventMask
     */
    public synchronized void addEventMask(final EventMaskEntity eventMask) {
        Log.info(logPrefix + "addEventMask:" + eventMask);
        if (maskCount.containsKey(eventMask)) {
            // when event mask already exists, increase counter
            int count = maskCount.get(eventMask) + 1;
            maskCount.put(eventMask, count);

        } else {
            // addEventMask and try to addEventMask to eventDispatcherService
            maskCount.put(eventMask, 1);
            callAddEventMask(eventMask);
        }
    }

    /**
     * Call addEventMask on eventDispatcherService when running (listenerId of eventMask is set).
     * 
     * @param eventMask
     *            the eventMaskEntity to add
     */
    private void callAddEventMask(final EventMaskEntity eventMask) {
        Log.info(logPrefix + "callAddEventMask:" + eventMask);
        if (listenerId != null) {
            eventMask.setListenerId(listenerId);
            edService.addEventMask(eventMask, new SecuredAsyncCallback<Void>(enableBusyDialog, enableErrorDialog) {
                @Override
                protected void doOnFailure(Throwable throwable) {
                    Log.error(logPrefix + "Recieved errorResponse from server while calling addEventMask:" + eventMask,
                            throwable);
                }

                @Override
                public void doOnSuccess(Void object) {
                    Log.debug(logPrefix + "Recieved successResponse from server when calling addEventMask:" + eventMask);
                }
            });
        }
    }

    /**
     * Remove an eventMask.
     * <p/>
     * NOTE: It will only be removed from eventDispatcherService when all added event masks (in
     * eventDispatcherConnector) are removed.
     * 
     * @param eventMask
     */
    public void removeEventMask(final EventMaskEntity eventMask) {
        Log.info(logPrefix + "removeEventMask:" + eventMask);
        if (maskCount.containsKey(eventMask)) {
            int count = maskCount.get(eventMask);
            count--;
            Log.debug(logPrefix + "new eventMask count:" + count);

            if (count > 0) {
                // there are other listeners still needing the mask
                // just decrease the count
                maskCount.put(eventMask, count);

            } else {
                // the mask is no longer needed, remove it from the index
                maskCount.remove(eventMask);

                callRemoveEventMask(eventMask);
            }
        }
    }

    /**
     * Invoke removeEventMask on eventDispatcherService
     * 
     * @param eventMask
     *            eventMask to remove
     */
    private void callRemoveEventMask(final EventMaskEntity eventMask) {
        Log.info(logPrefix + "callRemoveEventMask:" + eventMask);
        if (listenerId != null) {
            edService.removeEventMask(listenerId, eventMask, new SecuredAsyncCallback<Void>(enableBusyDialog,
                    enableErrorDialog) {
                @Override
                protected void doOnFailure(Throwable throwable) {
                    Log.error(logPrefix + "Received errorResponse from server while invoking removeEventMask:"
                            + eventMask, throwable);
                }

                @Override
                public void doOnSuccess(Void object) {
                    Log.debug(logPrefix + "Received successResponse from server when invoking removeEventmask:"
                            + eventMask);
                }
            });
        }
    }

    /**
     * @see EventDispatcherConnector#startDispatching(AsyncCallback, boolean)
     */
    public void startDispatching(final AsyncCallback<String> initialCallback) {
        Log.info(logPrefix + "startDispatching:" + initialCallback);
        startDispatching(initialCallback, enableAutoReconnect);
    }

    /**
     * Start dispatching, will start listening to the EventDispatcherService.
     * <p/>
     * All eventMasks added to this eventDispatcherConnector will be used ((re)added to eventDispatcherService if
     * needed).
     * <p/>
     * Listeners will start to receive event notifications.
     * <p/>
     * 
     * @param initialCallback
     *            to receive callback results (Applies only to first call) so you can detect if initial start of
     *            dispatching is a success or failure
     * 
     * @param initialCallback
     *            optional asyncCallback to inform of first call results
     * 
     * @param recoverFromDisconnect
     *            when true a (re)start will be triggerd when dispatcher is already dispatching
     * 
     * @see EventDispatcherConnector#setEnableAutoReconnect
     * @see EventDispatcherConnector#setEnableBusyDialog
     * @see EventDispatcherConnector#setEnableErrorDialog
     */
    private void startDispatching(final AsyncCallback<String> initialCallback, final boolean recoverFromDisconnect) {
        Log.info(logPrefix + "startDispatching (initialCallback,recoverFromDisconnect):" + initialCallback + ","
                + recoverFromDisconnect);

        if (recoverFromDisconnect && !enableAutoReconnect) {
            // recovering, but don't autoreconnect connect
            Log.warn(logPrefix
                    + "EventDispatcherConnector isn't allowed to recover from error error, enableAutoReconnect="
                    + enableAutoReconnect + " initialCallback=" + initialCallback);

            if (initialCallback != null) {
                initialCallback.onFailure(new Throwable("Disconnected and autoReconnect is disabled"));
            }

        } else if (recoverFromDisconnect || !dispatching) {
            // recovering and autoreconnect enabled OR initial start
            dispatching = true;
            edService
                    .registerServiceEventListener(new SecuredAsyncCallback<String>(enableBusyDialog, enableErrorDialog) {
                        @Override
                        protected void doOnFailure(Throwable throwable) {
                            Log.error(logPrefix
                                    + "Received errorResponse from server when calling registerServiceEventListener",
                                    throwable);
                            dispatching = false;
                            // perform initialCallback
                            if (initialCallback != null) {
                                initialCallback.onFailure(throwable);
                            }

                            // try to recover
                            if (recoverFromDisconnect) {
                                Timer reconnectTimer = new Timer() {

                                    @Override
                                    public void run() {
                                        Log.debug(logPrefix + "Reconnecting event dispatcher from timer for "
                                                + listenerId);
                                        startDispatching(null, true);
                                    }
                                };
                                reconnectTimer.schedule(RECONNECT_ATTEMPT_INTERVAL);
                                Log.debug(logPrefix + "Reconnect attempt scheduled, will try to reconnect after (ms):"
                                        + RECONNECT_ATTEMPT_INTERVAL);
                            } else {
                                Log.debug(logPrefix + "NO Reconnect attempt scheduled, enableAutoReconnect="
                                        + enableAutoReconnect);
                            }
                        }

                        @Override
                        public void doOnSuccess(String listenerId) {
                            Log.info(logPrefix
                                    + "Received successResponse when calling registerServiceEventListener, listenerId="
                                    + listenerId);

                            // remember listenerId
                            EventDispatcherConnector.this.listenerId = listenerId;

                            // re-add notifications masks if any exist
                            if (!maskCount.isEmpty()) {
                                Set<EventMaskEntity> eventMasks = maskCount.keySet();
                                for (EventMaskEntity eventMask : eventMasks) {
                                    eventMask.setListenerId(listenerId);
                                    callAddEventMask(eventMask);
                                }
                            }

                            // add notification mask for heartbeat
                            listenToHeartbeat();

                            // start listening for events
                            listenToEventDispatcherService();

                            // perform callback
                            if (initialCallback != null) {
                                initialCallback.onSuccess(listenerId);
                            }
                        }

                        @Override
                        public void doOnSuccessfulReauthentication(GWTAuthentication authentication) {
                            Log.debug(logPrefix + "reauthentication succesful; (re)starting dispatching for listener "
                                    + listenerId);
                            // seems we could re-login, retry to start
                            // dispatcher
                            startDispatching(null, true);
                        }
                    });
        } else {
            // dispatcher already started, can't start twice
            Log.warn(logPrefix + "Dispatcher already started, can't start twice:" + listenerId);
            if (initialCallback != null) {
                initialCallback.onFailure(new Throwable("Can't start dispatcher (twice), it is already started."));
            }
        }
    }

    /**
     * Stop listening and deregister from the EventDispatcherService.
     * <p/>
     * <b>NOTE</b>: The eventDispatcherConnector won't stop immediately, it will wait for last call to 'waitForEvent' to
     * return. After 'waitForEvent' returns the 'doStopDispatching' will be invoked
     * 
     * @see EventDispatcherConnector#doStopDispatching
     */
    public void stopDispatching() {
        Log.info(logPrefix + "stopDispatching:" + listenerId);
        dispatching = false;
    }

    /**
     * This will invoke deregisterServiceEventListener on EventDispatcherService. Your eventDispatcherConnector
     * listenerId will be set to NULL
     */
    private void doStopDispatching() {
        Log.info(logPrefix + "doStopDispatching:" + listenerId);
        edService.deregisterServiceEventListener(listenerId, new SecuredAsyncCallback<Void>(enableBusyDialog,
                enableErrorDialog) {
            @Override
            protected void doOnFailure(Throwable throwable) {
                Log.error(
                        logPrefix
                                + "Received errorResponse while calling deregisterServiceEventListener on eventDispatcherService:"
                                + listenerId, throwable);
            }

            @Override
            public void doOnSuccess(Void object) {
                // nothing to do
                Log.debug(logPrefix
                        + "Received successResponse when calling deregisterServiceEventListener on eventDispatcherService");
            }
        });
        listenerId = null;
    }

    /**
     * Listen for events.
     * <p/>
     * It can take some time before response is received from eventDispatcherService, because a webservice call will be
     * invoked which will 'block' (unitl events become available) some time before it returns.
     * 
     */
    private void listenToEventDispatcherService() {
        Log.debug(logPrefix + "listening for new events for listener " + listenerId);

        edService.waitForEvent(listenerId, new SecuredAsyncCallback<Void>(false, enableErrorDialog) {

            @Override
            protected void doOnFailure(Throwable throwable) {
                Log.error(logPrefix + "Received errorResponse when invoking waitForEvent on eventDispatcherService",
                        throwable);

                if (dispatching) {
                    // try to recover
                    startDispatching(null, enableAutoReconnect);
                } else {
                    // when not dispaching (any more) stop dispaching
                    doStopDispatching();
                }
            }

            @Override
            public void doOnSuccess(Void v) {
                if (dispatching) {
                    // if dispaching still enabled, fetch events\
                    Log.debug(logPrefix + "Invoke fetchEvents on eventDispatcherService:" + listenerId);
                    edService.fetchEvents(listenerId, new SecuredAsyncCallback<List<EventEntity>>(false, false) {

                        @Override
                        protected void doOnFailure(Throwable throwable) {
                            // if still dispatching, call listen again
                            Log.error(
                                    logPrefix
                                            + "Received errorResponse wehen invoking fetchEvents on eventDispatcherService for listener "
                                            + listenerId, throwable);
                            listenToEventDispatcherService();
                        }

                        @Override
                        protected void doOnSuccess(List<EventEntity> events) {
                            Log.debug(logPrefix
                                    + "Received successResponse when invoking fetchEvents on eventDispatcherService for listener "
                                    + listenerId);
                            // process received events
                            notifyListeners(events);
                            // continue listening
                            listenToEventDispatcherService();
                        }
                    });
                } else {
                    Log.debug(logPrefix + "event(s) received, but dispatching was already stopped for listener "
                            + listenerId);
                    // eventDispatcherConnector is no longer dispatching, call
                    // stop
                    doStopDispatching();
                }
            }

            @Override
            public void doOnSuccessfulReauthentication(GWTAuthentication authentication) {
                // seems we could re-login, (re)startDispatcher
                Log.debug(logPrefix + "Seems we could re-login, (re)startDispatcher for " + listenerId);
                startDispatching(null, true);
            }
        });
    }

    /**
     * This will inform registered listeners of specified events.
     * 
     * @param events
     */
    private void notifyListeners(final List<EventEntity> events) {
        Log.debug(logPrefix + "notifyListeners of listener " + listenerId);
        if (dispatching && events.size() > 0) {
            Log.debug(logPrefix + "notifying " + events.size() + " dispatchListener(s)");
            for (EventDispatcherListener sel : listeners) {
                try {
                    sel.onDispatchedEvents(events);
                } catch (Throwable t) {
                    Log.error(logPrefix + "Error while notifying dispatchListener " + (sel == null ? "<null>" : sel), t);
                }
            }
        } else {
            Log.debug(logPrefix + "no dispatchListeners, skipping notify");
        }
    }

    /**
     * Register an event listener. This listener will be notified when events are recieved.
     * 
     * @param listener
     * @see EventDispatcherConnector#startDispatching
     */
    public void registerListener(EventDispatcherListener listener) {
        listeners.add(listener);
    }

    /**
     * Remove a listener. The listener will no longer receive notifications about events
     * 
     * @param listener
     * @see EventDispatcherConnector#stopDispatching
     */
    public void deregisterListener(EventDispatcherListener listener) {
        listeners.remove(listener);
    }

    /**
     * 
     * @return true if errorDialog is enabled
     */
    public boolean isEnableErrorDialog() {
        return enableErrorDialog;
    }

    /**
     * Set this to false to prevent an error dialog to showup after an error received from async call.
     * <p/>
     * Default: true
     * 
     * @param enableErrorDialog
     */
    public void setEnableErrorDialog(boolean enableErrorDialog) {
        this.enableErrorDialog = enableErrorDialog;
    }

    /**
     * @return true if enabled
     */
    public boolean isEnableAutoReconnect() {
        return enableAutoReconnect;
    }

    /**
     * Set this to false to disable auto reconnect after failure
     * <p/>
     * Default: true
     * 
     * @param enableAutoReconnect
     */
    public void setEnableAutoReconnect(boolean enableAutoReconnect) {
        this.enableAutoReconnect = enableAutoReconnect;
    }

    /**
     * @return true if enabled
     */
    public boolean isEnableBusyDialog() {
        return enableBusyDialog;
    }

    /**
     * Set to false to disable busy dialog during async method invocations.
     * <p/>
     * Default: true
     * 
     * @param enableBusyDialog
     */
    public void setEnableBusyDialog(boolean enableBusyDialog) {
        this.enableBusyDialog = enableBusyDialog;
    }

    /**
     * This will add an event mask for heartbeats, this will cause the listen methode to return after each heartbeat
     * event.
     * <p/>
     * <ul>
     * <li/>EventMask Type: EventEntity.TYPE_DISPATCHING
     * <li/>EventMask Subtype: EventEntity.SUBTYPE_DISP_HEARTBEAT
     * </ul>
     * <p/>
     * This is a best practice to prevent browser timeout issues.
     * 
     * @see EventDispatcherConfigEntity#heartbeatInterval
     */
    private void listenToHeartbeat() {
        Log.info(logPrefix + "setting up listenToHeartbeat for " + listenerId);
        addEventMask(new EventMaskEntity(listenerId, EventEntity.TYPE_DISPATCHING, EventEntity.SUBTYPE_DISP_HEARTBEAT,
                null));
    }
}