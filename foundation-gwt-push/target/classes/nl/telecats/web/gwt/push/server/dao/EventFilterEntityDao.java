package nl.telecats.web.gwt.push.server.dao;

import java.util.List;

import nl.telecats.foundation.model.dao.GenericDao;
import nl.telecats.web.gwt.push.common.entities.EventFilterEntity;
import nl.telecats.web.gwt.push.common.entities.EventFilterEntity.FilterMode;

/**
 * Dao for accessing your EventFilter entities
 * 
 * @author Gregor Ybema
 * @see EventFilterEntityDao
 * @see GenericDao
 * 
 */
public interface EventFilterEntityDao extends GenericDao<EventFilterEntity> {

    /**
     * @param ownerId
     * @return All filters of specified owner
     */
    List<EventFilterEntity> findByOwner(String ownerId);

    /**
     * Find by some criteria (all parameters are optional) or empty list when no
     * criteria (all params are null) are specified.
     * 
     * @param ownerId
     * @param type
     * @param subtype
     * @param properties
     * @param filterMode
     * @return eventFilters matching specified criteria
     */
    List<EventFilterEntity> findByCriteria(String ownerId, String type, String subtype, String properties,
            FilterMode filterMode);

    /**
     * Delete multiple filters in one call
     * 
     * @param filters
     */
    void deleteAll(List<EventFilterEntity> eventFilters);

}
