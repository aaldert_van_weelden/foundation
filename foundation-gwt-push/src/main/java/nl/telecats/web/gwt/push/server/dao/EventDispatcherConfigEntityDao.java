package nl.telecats.web.gwt.push.server.dao;

import nl.telecats.foundation.model.dao.GenericDao;
import nl.telecats.web.gwt.push.common.entities.EventDispatcherConfigEntity;

/**
 * Dao for accessing your eventDispatcher configuration
 * 
 * @author Raymond Domingo
 * @see GenericDao
 * 
 */
public interface EventDispatcherConfigEntityDao extends GenericDao<EventDispatcherConfigEntity> {
}
