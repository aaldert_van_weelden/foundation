package nl.telecats.web.gwt.push.server.services;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.jws.WebService;

import nl.telecats.web.gwt.push.common.entities.EventDispatcherConfigEntity;
import nl.telecats.web.gwt.push.common.entities.EventFilterEntity;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * 
 * @see EventDispatcherService
 * @author Raymond Domingo
 */
@WebService(serviceName = PurgeService.serviceName, targetNamespace = NameSpace.nameSpace, portName = PurgeService.portName)
public class PurgeServiceImpl implements Serializable, PurgeService {

    private static final long serialVersionUID = 2673358396224796917L;

    /**
     * Run a process to clean expired listeners / outdated events
     */
    private transient Thread purgeDaemon = null;

    @Autowired
    private EventDispatcherService eventDispatcherService;

    private final String purgerId;
    private List<EventFilterEntity> eventFilters;

    public PurgeServiceImpl() {
        this.purgerId = UUID.randomUUID().toString();

        // define default filter that includes every event;
        EventFilterEntity defaultFilter = new EventFilterEntity(this.purgerId, null, null, null,
                EventFilterEntity.FilterMode.INCLUDE);
        List<EventFilterEntity> defaultFilters = new ArrayList<EventFilterEntity>();
        defaultFilters.add(defaultFilter);
        this.setEventFilters(defaultFilters);
    }

    public void setEventFilters(List<EventFilterEntity> eventFilters) {

        for (EventFilterEntity eventFilter : eventFilters) {
            // setting ownerId to link filter to purger
            eventFilter.setOwnerId(this.purgerId);
        }
        this.eventFilters = eventFilters;
    }

    public List<EventFilterEntity> getEventFilters() {
        return eventFilters;
    }

    public void startPurging() {

        purgeDaemon = new Thread() {

            public synchronized void run() {
                for (EventFilterEntity eventFilter : eventFilters) {
                    // eventfilters cannot be added earlier!
                    eventDispatcherService.addEventFilter(eventFilter);
                }
                while (true) {
                    try {
                        EventDispatcherConfigEntity config = eventDispatcherService.getEventDispatcherConfig();
                        try {
                            wait(config.getPurgeInterval());
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        eventDispatcherService.purgeExpiredListeners();
                        eventDispatcherService.purgeExpiredEvents(purgerId);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        try {
                            wait(250); // delay so system won't hang
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        };
        purgeDaemon.start();
    }

    public String getPurgeServiceId() {
        return this.purgerId;
    }

    /**
     * Cleanup the filters that are owned by this purger
     */
    public void cleanup() {
        List<EventFilterEntity> ownedFilters = eventDispatcherService.getEventFilters(this.purgerId);
        for (EventFilterEntity eventFilter : ownedFilters) {
            eventDispatcherService.removeEventFilter(eventFilter);
        }

    }
}
