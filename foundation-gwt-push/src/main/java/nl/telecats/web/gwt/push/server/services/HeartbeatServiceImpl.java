package nl.telecats.web.gwt.push.server.services;

import java.io.Serializable;

import javax.jws.WebService;

import nl.telecats.web.gwt.push.common.entities.EventDispatcherConfigEntity;
import nl.telecats.web.gwt.push.common.entities.EventEntity;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 
 * @see EventDispatcherService
 * @author Raymond Domingo
 */
@WebService(serviceName = HeartbeatService.serviceName, targetNamespace = NameSpace.nameSpace, portName = HeartbeatService.portName)
public class HeartbeatServiceImpl implements Serializable, HeartbeatService {

    private static final long serialVersionUID = -386398478744260740L;

    private static final Log log = LogFactory.getLog(HeartbeatServiceImpl.class);

    /**
     * Broadcast' heartbeat so:
     * <ul>
     * <li/>Keep connections to listeners open
     * <li/>Enable client to detect when service is down, so they can try to reconnect (heartbeat stops)
     * <li/>We can detect disconnected clients, by detecting they don't respond to heartbeat anymore.
     * </ul>
     */
    private transient Thread heartbeatDaemon = null;

    @Autowired
    private EventDispatcherService eventDispatcherService;

    public HeartbeatServiceImpl() {
    }

    /**
     * Start sending heart beat events. (EventEntity.TYPE_DISPATCHING, EventEntity.SUBTYPE_DISP_HEARTBEAT)
     */
    public void startHeartBeat() {
        heartbeatDaemon = new Thread() {
            public synchronized void run() {
                while (true) {
                    try {
                        EventDispatcherConfigEntity config = eventDispatcherService.getEventDispatcherConfig();
                        try {
                            wait(config.getHeartbeatInterval());
                        } catch (InterruptedException e) {
                            log.error("Interruption during regular wait", e);
                        }
                        eventDispatcherService.fireEvent(new EventEntity(EventEntity.TYPE_DISPATCHING,
                                EventEntity.SUBTYPE_DISP_HEARTBEAT, null, eventDispatcherService.getInstanceId()));
                    } catch (Exception ex) {
                        log.error("Error while running heartBeat daemon, daemon continues...", ex);
                        try {
                            wait(250); // delay so system won't hang
                        } catch (InterruptedException e) {
                            log.error("Interruption during exception wait", e);
                        }
                    }
                }
            }
        };
        heartbeatDaemon.start();
    }

}
