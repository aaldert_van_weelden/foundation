package nl.telecats.web.gwt.push.server.gwtservices;

import java.util.List;

import net.orcades.spring.gwt.security.client.GWTAuthorizationRequiredException;
import net.orcades.spring.gwt.security.client.GWTSecurityException;
import nl.telecats.web.gwt.push.client.services.EventDispatcherGWTService;
import nl.telecats.web.gwt.push.common.entities.EventDispatcherConfigEntity;
import nl.telecats.web.gwt.push.common.entities.EventEntity;
import nl.telecats.web.gwt.push.common.entities.EventMaskEntity;
import nl.telecats.web.gwt.push.server.services.EventDispatcherService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * All methods here are proxied to methods in EventDispatcherService
 * 
 * @author Raymond Domingo
 * @see EventDispatcherService
 * 
 */
@Component
@Scope("request")
public class EventDispatcherGWTServiceImpl implements EventDispatcherGWTService {

    private static final long serialVersionUID = 1L;

    @Autowired
    private EventDispatcherService eventDispatcherService;

    /**
     * @see EventDispatcherService#waitForEvent(String)
     */
    public void waitForEvent(final String listenerId) throws GWTSecurityException {
        eventDispatcherService.waitForEvent(listenerId);
    }

    /**
     * @see EventDispatcherService#fetchEvents(String)
     */
    public List<EventEntity> fetchEvents(String listenerId) throws GWTSecurityException {
        return eventDispatcherService.fetchEvents(listenerId);
    }

    /**
     * @see EventDispatcherService#addEventMask(EventMaskEntity)
     */
    public void addEventMask(final EventMaskEntity set) throws GWTSecurityException {
        eventDispatcherService.addEventMask(set);
    }

    /**
     * @see EventDispatcherService#removeEventMask(String, EventMaskEntity)
     */
    public void removeEventMask(final String listenerId, final EventMaskEntity set) throws GWTSecurityException {
        eventDispatcherService.removeEventMask(listenerId, set);
    }

    /**
     * @see EventDispatcherService#fireEvent(EventEntity)
     */
    public void fireEvent(final EventEntity se) throws GWTSecurityException {
        eventDispatcherService.fireEvent(se);
    }

    /**
     * @see EventDispatcherService#registerServiceEventListener()
     */
    public String registerServiceEventListener() {
        String result = eventDispatcherService.registerServiceEventListener();
        return result;
    }

    /**
     * @see EventDispatcherService#deregisterServiceEventListener(String)
     */
    public void deregisterServiceEventListener(String listenerId) {
        eventDispatcherService.deregisterServiceEventListener(listenerId);
    }

    /**
     * @see EventDispatcherService#getEventListeners()
     */
    public List<String> getEventListeners() {
        return eventDispatcherService.getEventListeners();
    }

    /**
     * @see EventDispatcherService#getEventMasks(String)
     */
    public List<EventMaskEntity> getEventMasks(String listenerId) {
        return eventDispatcherService.getEventMasks(listenerId);
    }

    /**
     * @see EventDispatcherService#getExpirationTimeout()
     */
    public long getExpirationTimeout() {
        return eventDispatcherService.getExpirationTimeout();
    }

    /**
     * @see EventDispatcherService#getEventQueue(String)
     */
    public List<EventEntity> getEventQueue(String listenerId) {
        return eventDispatcherService.getEventQueue(listenerId);
    }

    /**
     * @see EventDispatcherService#getQuietPeriode()
     */
    public long getQuietPeriod() {
        return eventDispatcherService.getQuietPeriode();
    }

    /**
     * @see EventDispatcherService#setExpirationTimeout(long)
     */
    public void setExpirationTimeout(long et) {
        eventDispatcherService.setExpirationTimeout(et);
    }

    /**
     * @see EventDispatcherService#setQuietPeriode(long)
     */
    public void setQuietPeriode(long qp) {
        eventDispatcherService.setQuietPeriode(qp);
    }

    /**
     * @see EventDispatcherService#getHeartbeatInterval()
     */
    public long getHeartbeatInterval() {
        return eventDispatcherService.getHeartbeatInterval();
    }

    /**
     * @see EventDispatcherService#setHeartbeatInterval(long)
     */
    public void setHeartbeatInterval(long interval) {
        eventDispatcherService.setHeartbeatInterval(interval);
    }

    /**
     * @see EventDispatcherService#getPurgeInterval()
     */
    public long getPurgeInterval() {
        return eventDispatcherService.getPurgeInterval();
    }

    /**
     * @see EventDispatcherService#setPurgeInterval(long)
     */
    public void setPurgeInterval(long interval) {
        eventDispatcherService.setPurgeInterval(interval);
    }

    /**
     * @see EventDispatcherService#setEventDispatcherConfig(long , long , long ,
     *      long , long )
     */
    public void setEventDispatcherConfig(long expiration, long heartbeat, long purge, long quiet, long lastNotification)
            throws GWTAuthorizationRequiredException, GWTSecurityException {
        eventDispatcherService.setEventDispatcherConfig(expiration, heartbeat, purge, quiet, lastNotification);
    }

    /**
     * @see EventDispatcherService#getEventDispatcherConfig()
     */
    public EventDispatcherConfigEntity getEventDispatcherConfig() throws GWTAuthorizationRequiredException,
            GWTSecurityException {
        return eventDispatcherService.getEventDispatcherConfig();
    }
}