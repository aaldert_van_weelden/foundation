package nl.telecats.web.gwt.push.common.entities;

import nl.telecats.foundation.model.entities.IdentifiableObject;

/**
 * This entity contains all configuration information of a
 * EventDispatcherService.
 * <p/>
 * It is persisted in database so instances are stateless and failover scenarios
 * can be supported.
 * 
 * @author Raymond Domingo
 * 
 */
public class EventDispatcherConfigEntity implements IdentifiableObject {

    private static final long serialVersionUID = 2449073552985096259L;

    /**
     * Technical database id, don't use it unless you know exactly what you are
     * doing.
     */
    private String id;

    /**
     * Technical database version, don't use it unless you know exactly what you
     * are doing.
     */
    private int version = 0;

    /**
     * Keep track of lastNotification (timestamp).
     * 
     * @see System#currentTimeMillis()
     */
    private long lastNotification = 0;

    /**
     * Number of ms to wait before notifying listeners of new events (forced
     * idle period to prevent the system to be flooded by event notifications)
     * <p/>
     * Defaults to 250ms
     */
    private long quietPeriode = 250;

    /**
     * Interval of heartBeat event generation (ms).
     * <p/>
     * EventDispatcherService will generate following events:
     * <ul>
     * <li/>EventEntity.TYPE_DISPATCHING
     * <li/>EventEntity.SUBTYPE_DISP_HEARTBEAT
     * </ul>
     * <p/>
     * Defaults to 30 sec
     * <p/>
     * See: <a href=
     * "http://code.google.com/p/google-web-toolkit-incubator/wiki/ServerPushFAQ"
     * >http://code.google.com/p/google-web-toolkit-incubator/wiki/ServerPushFAQ
     * </a>
     * 
     */
    private long heartbeatInterval = 30000;

    /**
     * Allowed Idle timeout of listeners before they expire (ms).
     * <p/>
     * Defaults to 15 minutes
     * 
     * @see purteInterval
     */
    private long expirationTimeout = 15 * 60 * 1000;

    /**
     * Interval between runs of the purge process (ms). This purge process
     * cleans expired listeners and events that are already sended to the
     * listeners.
     * <p/>
     * Defaults to 5 minutes
     */
    private long purgeInterval = 5 * 60 * 1000;

    /**
     * Defaut constructor needed for serialization
     */
    public EventDispatcherConfigEntity() {
        super();
    }

    /**
     * @return id
     * 
     * @see EventDispatcherConfigEntity#id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id
     * 
     * @see EventDispatcherConfigEntity#id
     */
    public void setId(String id) {
        this.id = (String) id;
    }

    /**
     * @return version
     * 
     * @see EventDispatcherConfigEntity#version
     */
    public Integer getVersion() {
        return version;
    }

    /**
     * S@see EventDispatcherConfigEntity#version
     * 
     * @param version
     */
    public void setVersion(Integer version) {
        this.version = version;
    }

    /**
     * @return purgeInterval
     * 
     * @see EventDispatcherConfigEntity#purgeInterval
     */
    public long getPurgeInterval() {
        return purgeInterval;
    }

    /**
     * @param purgeInterval
     * @see EventDispatcherConfigEntity#purgeInterval
     */
    public void setPurgeInterval(long purgeInterval) {
        this.purgeInterval = purgeInterval;
    }

    /**
     * @return lastNotification timestamp
     * @see EventDispatcherConfigEntity#lastNotification
     */
    public long getLastNotification() {
        return lastNotification;
    }

    /**
     * 
     * @param lastNotification
     * @see EventDispatcherConfigEntity#lastNotification
     */
    public void setLastNotification(long lastNotification) {
        this.lastNotification = lastNotification;
    }

    /**
     * 
     * @return quietPeriode
     * @see EventDispatcherConfigEntity#quietPeriode
     */
    public long getQuietPeriode() {
        return quietPeriode;
    }

    /**
     * 
     * @param quietPeriode
     * @see EventDispatcherConfigEntity#quietPeriode
     */
    public void setQuietPeriode(long quietPeriode) {
        this.quietPeriode = quietPeriode;
    }

    /**
     * 
     * @return heartbeatInterval
     * @see EventDispatcherConfigEntity#heartbeatInterval
     */
    public long getHeartbeatInterval() {
        return heartbeatInterval;
    }

    /**
     * 
     * @param heartbeatInterval
     * @see EventDispatcherConfigEntity#heartbeatInterval
     */
    public void setHeartbeatInterval(long heartbeatInterval) {
        this.heartbeatInterval = heartbeatInterval;
    }

    /**
     * 
     * @return expirationTimeout
     * @see EventDispatcherConfigEntity#expirationTimeout
     */
    public long getExpirationTimeout() {
        return expirationTimeout;
    }

    /**
     * 
     * @param expirationTimeout
     * @see EventDispatcherConfigEntity#expirationTimeout
     */
    public void setExpirationTimeout(long expirationTimeout) {
        this.expirationTimeout = expirationTimeout;
    }

}
