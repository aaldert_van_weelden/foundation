package nl.telecats.web.gwt.push.server.services;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import nl.telecats.web.gwt.push.client.event.EventDispatcherConnector;
import nl.telecats.web.gwt.push.common.entities.EventDispatcherConfigEntity;
import nl.telecats.web.gwt.push.common.entities.EventEntity;
import nl.telecats.web.gwt.push.common.entities.EventFilterEntity;
import nl.telecats.web.gwt.push.common.entities.EventMaskEntity;

/**
 * Distributed event dispatcher, used for pushing events to your gwt clients.
 * 
 * @author Raymond Domingo
 * 
 * @see EventDispatcherConfigEntity
 * @see EventDispatcherConnector
 * @see EventDispatcherServiceImpl
 * 
 */
@WebService(targetNamespace = NameSpace.nameSpace)
public interface EventDispatcherService extends EventDispatcherProducerService {

    public static final String serviceName = "EventDispatcherService";
    public static final String portName = "EventDispatcherServicePort";

    /**
     * Obtain configuration.
     * 
     * @return eventDispatcherConfig
     */
    @WebMethod
    @WebResult(name = "eventDispatcherConfigEntity")
    public EventDispatcherConfigEntity getEventDispatcherConfig();

    /**
     * Request notification of listeners. When quietPeriode is expired events will be pushed to the listeners.
     * <p/>
     * <b>Note:</b>You probably never have to call this method manually, it's invoked automatically.
     */
    @WebMethod
    @WebResult(name = "notifyListeners")
    public void notifyListeners();

    /**
     * An instance of the EventDispatcherService has an unique id. This can be usefull when using multiple instances to
     * identify each instance.
     * 
     * @return id
     */
    @WebMethod(operationName = "getInstanceid", action = "getInstanceid")
    @WebResult(name = "instanceid")
    public String getInstanceId();

    /**
     * Delete all listeners not active for configured ExpirationTimeout.
     * 
     */
    @WebMethod(operationName = "purgeExpiredListeners", action = "purgeExpiredListeners")
    public void purgeExpiredListeners();

    /**
     * Purge exprired events. Events become expired when they are sended to all listeners.
     * 
     * @param purgerId
     *            Id of the purger-service that requested the purging. This will be used to find eventfilters
     */
    @WebMethod(operationName = "purgeExpiredEvents", action = "purgeExpiredEvents")
    public void purgeExpiredEvents(@WebParam(name = "purgerId") String purgerId);

    /**
     * @return list of listenerId's, id's of listeners currently listenering
     */
    @WebMethod
    @WebResult(name = "EventListeners")
    public List<String> getEventListeners();

    /**
     * Events in queue (will be send to client during next notification) for specified listener.
     * 
     * @param listenerId
     * @return queued events for specified listener
     */
    @WebMethod
    @WebResult(name = "eventQueue")
    public List<EventEntity> getEventQueue(@WebParam(name = "listenerId") String listenerId);

    /**
     * @return The number of milliseconds required to have passed since the last listener notification before the next
     *         notification can occur.
     */
    @WebMethod
    @WebResult(name = "quietPeriode")
    public long getQuietPeriode();

    /**
     * @param periode
     *            The number of milliseconds required to have passed since the last listener notification before the
     *            next notification can occur.
     */
    @WebMethod
    public void setQuietPeriode(@WebParam(name = "periode") long periode);

    /**
     * ms between heart beat events
     * 
     * @param interval
     *            in ms
     */
    @WebMethod
    public void setHeartbeatInterval(@WebParam(name = "interval") long interval);

    /**
     * ms between heart beat events
     * 
     * @return interval in ms
     */
    @WebMethod
    @WebResult(name = "heartbeatInterval")
    public long getHeartbeatInterval();

    /**
     * ms between runs of purge processes
     * 
     * @return
     */
    @WebMethod
    @WebResult(name = "purgeInterval")
    public long getPurgeInterval();

    /**
     * ms between runs of purge processes
     * 
     * @param interval
     */
    @WebMethod
    public void setPurgeInterval(@WebParam(name = "interval") long interval);

    /**
     * Time a listener may be inactive before it's purged.
     * <p/>
     * Inactive means it hasn't called listen again after it was notified.
     * 
     * @return time in ms
     */
    @WebMethod
    @WebResult(name = "expirationTimeout")
    public long getExpirationTimeout();

    /**
     * Time a listener may be inactive before it's purged.
     * <p/>
     * Inactive means it hasn't called listen again after it was notified.
     * 
     * @param time
     *            in ms
     */
    @WebMethod
    public void setExpirationTimeout(@WebParam(name = "time") long time);

    /**
     * This method blocks until an event (matching the listeners event mask) is available.
     * <p/>
     * Note: To prevent timeouts we use heartBeat events
     * 
     * @see EventDispatcherService#addEventMask(String, EventMask)
     * 
     * @param listenerId
     */
    @WebMethod
    @WebResult(name = "waitForEvent")
    public void waitForEvent(@WebParam(name = "listenerId") final String listenerId);

    /**
     * Fetch events queued for specified listener.
     * 
     * @param listenerId
     * @return
     */
    @WebMethod
    @WebResult(name = "fetchEvents")
    public List<EventEntity> fetchEvents(@WebParam(name = "listenerId") final String listenerId);

    /**
     * Listeners can register to obtain an Id which they can use to use the event manager.
     * 
     * @return id for the listener, or null when listener couldn't be registered
     */
    @WebMethod(operationName = "registerServiceEventListenerMessage", action = "registerServiceEventListenerMessage")
    @WebResult(name = "listenerId")
    public String registerServiceEventListener();

    /**
     * Deregister the listener (and remove all it's event masks)
     * 
     * @param listenerId
     */
    public void deregisterServiceEventListener(@WebParam(name = "listenerId", partName = "listenerId") String listenerId);

    /**
     * Event masks registered by specified listener
     * 
     * @param listenerId
     * @return
     */
    @WebMethod
    @WebResult(name = "eventMasks")
    public List<EventMaskEntity> getEventMasks(@WebParam(name = "listenerId") String listenerId);

    /**
     * Tell dispatcher which events you are interested in, by adding an eventMask
     * 
     * @param listenerId
     * @param set
     */
    @WebMethod
    public void addEventMask(@WebParam(name = "eventMask") final EventMaskEntity eventMask);

    /**
     * Inform dispatcher you are no longer interested in receiving notifications of events matching the provided event
     * mask.
     * <p/>
     * <h2>All event masks MATCHING the specified event mask will be removed!!!</h2>
     * <p/>
     * <b>NOTE:</b> When no eventMask specified, all mask for specified listener will be removed.
     * <p/>
     * <b>NOTE:</b> This only works if you have added the eventMask earlier.
     * 
     * @param listenerId
     * @param eventMask
     *            to remove
     */
    @WebMethod
    public void removeEventMask(@WebParam(name = "listenerId") final String listenerId,
            @WebParam(name = "eventMask") final EventMaskEntity eventMask);

    /**
     * Get Event filters
     * 
     * @return
     */
    @WebMethod
    @WebResult(name = "eventFilters")
    public List<EventFilterEntity> getEventFilters(@WebParam(name = "ownerId") String ownerId);

    /**
     * Add an event filter
     * 
     * @param eventFilter
     */
    @WebMethod
    public void addEventFilter(@WebParam(name = "eventFilter") final EventFilterEntity eventFilter);

    /**
     * Remove an event filter
     * 
     * @param eventFilter
     *            to remove
     */
    @WebMethod
    public void removeEventFilter(@WebParam(name = "eventFilter") final EventFilterEntity eventFilter);

    /**
     * Changing the eventDispatcherConfig should be synchronized !
     * 
     * @param expiration
     *            -1 will ignore this parameter and current value won't be changed
     * @param heartbeat
     *            -1 will ignore this parameter and current value won't be changed
     * @param purge
     *            -1 will ignore this parameter and current value won't be changed
     * @param quiet
     *            -1 will ignore this parameter and current value won't be changed
     * @param lastNotification
     *            -1 will ignore this parameter and current value won't be changed
     */
    @WebMethod
    public void setEventDispatcherConfig(long expiration, long heartbeat, long purge, long quiet, long lastNotification);

    /**
     * Retrieve the list of events for a provided type and subtype
     * 
     * @param type
     *            The type of the events to retrieve
     * 
     * @return The list of EventEntities that match the provided criteria (can be empty)
     */
    @WebMethod(operationName = "findByType", action = "findByType")
    @WebResult(name = "EventList")
    public List<EventEntity> findByType(@WebParam(name = "type") String type);

    /**
     * Retrieve the list of events for a provided type and subtype
     * 
     * @param type
     *            The type of the events to retrieve
     * @param subType
     *            The subtype of the events to retrieve
     * @return The list of EventEntities that match the provided criteria (can be empty)
     */
    @WebMethod(operationName = "findByTypeAndSubType", action = "findByTypeAndSubType")
    @WebResult(name = "EventList")
    public List<EventEntity> findByTypeAndSubType(@WebParam(name = "type") String type,
            @WebParam(name = "subType") String subType);

    /**
     * Retrieve the list of events for a provided type and subtype
     * 
     * @param type
     *            The type of the events to retrieve
     * @param subType
     *            The subtype of the events to retrieve
     * @param startDateTime
     *            The time of the events should be past this time (ISO format)
     * @param endDateTime
     *            The events have to occur before this date and time (ISO format)
     * @return The list of EventEntities that match the provided criteria (can be empty)
     */
    @WebMethod(operationName = "findByTypeSubTypeAndTime", action = "findByTypeSubTypeAndTime")
    @WebResult(name = "EventList")
    public List<EventEntity> findByTypeSubTypeAndTime(@WebParam(name = "type") String type,
            @WebParam(name = "subType") String subType, @WebParam(name = "startDateTime") String startDateTime,
            @WebParam(name = "endDateTime") String endDateTime);

    /**
     * Retrieve the list of EventEntities that math the provided content
     * 
     * @param type
     *            The type of the events to retrieve
     * @param subType
     *            The subtype of the events to retrieve
     * @param startDateTime
     *            The time of the events should be past this time (ISO format)
     * @param endDateTime
     *            The events have to occur before this date and time (ISO format)
     * @param content
     *            The content to search for
     * @return The list of EvenEntities that match the provided criteria (can be empty)
     */
    @WebMethod(operationName = "findByContent", action = "findByContent")
    @WebResult(name = "EventList")
    public List<EventEntity> findByContent(@WebParam(name = "type") String type,
            @WebParam(name = "subType") String subType, @WebParam(name = "startDateTime") String startDateTime,
            @WebParam(name = "endDateTime") String endDateTime, @WebParam(name = "content") String content);

    /**
     * Retrieve the list of EventEntities that math the provided content in the provided field. Assumes the separator to
     * be the default ('|')
     * 
     * @param type
     *            The type of the events to retrieve
     * @param subType
     *            The subtype of the events to retrieve
     * @param startDateTime
     *            The time of the events should be past this time (ISO format)
     * @param endDateTime
     *            The events have to occur before this date and time (ISO format)
     * @param fieldNr
     *            The field within the content field to search for
     * @param content
     *            The content to search for in the designated field
     * 
     * @return The list of EvenEntities that match the provided criteria (can be empty)
     */
    @WebMethod(operationName = "findByFieldAndContent", action = "findByFieldAndContent")
    @WebResult(name = "EventList")
    public List<EventEntity> findByFieldAndContent(@WebParam(name = "type") String type,
            @WebParam(name = "subType") String subType, @WebParam(name = "startDateTime") String startDateTime,
            @WebParam(name = "endDateTime") String endDateTime, @WebParam(name = "fieldNr") int fieldNr,
            @WebParam(name = "content") String content);

    /**
     * Retrieve the list of events that match the content in the designated field (splitted by fieldNr)
     * 
     * @param type
     *            The type of the events to retrieve
     * @param subType
     *            The subtype of the events to retrieve
     * @param startDateTime
     *            The time of the events should be past this time (ISO format)
     * @param endDateTime
     *            The events have to occur before this date and time (ISO format)
     * @param separator
     *            The separator to split the content field with
     * @param fieldNr
     *            The field within the content to search for (starting with 0)
     * @param content
     *            The content to search for in the designated field
     * @return The list of EvenEntities that match the provided criteria (can be empty)
     */
    @WebMethod(operationName = "findBySepFieldAndContent", action = "findBySepFieldAndContent")
    @WebResult(name = "EventList")
    public List<EventEntity> findBySepFieldAndContent(@WebParam(name = "type") String type,
            @WebParam(name = "subType") String subType, @WebParam(name = "startDateTime") String startDateTime,
            @WebParam(name = "endDateTime") String endDateTime, @WebParam(name = "separator") String separator,
            @WebParam(name = "fieldNr") int fieldNr, @WebParam(name = "content") String content);

}
