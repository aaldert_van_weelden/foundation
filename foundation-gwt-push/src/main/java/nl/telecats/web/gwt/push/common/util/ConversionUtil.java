package nl.telecats.web.gwt.push.common.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Utility class performing some data conversions
 * 
 * @author Raymond Domingo
 * 
 */
public class ConversionUtil {
    private final static String ENTRY_SEPARATOR = ",";
    private final static String KEY_VALUE_SEPARATOR = "=";

    /**
     * Convert string to map, straight forward implementation. InputMap
     * shouldn't contain any special characters because it will be converted as
     * follow: [key1]=[value1],[key2]=[value2]...
     * <p/>
     * <b>Note:</b> This class is used in GWT and isn't allowed to use 'more
     * complex' api's.
     * 
     * @param inputMap
     * 
     * @return String [key1]=[value1],[key2]=[value2]...
     */
    static public String mapToString(Map<String, String> inputMap) {
        StringBuffer sb = new StringBuffer();
        if (inputMap != null) {
            List<String> keys = new ArrayList<String>(inputMap.keySet());
            Collections.sort(keys);
            Iterator<String> keyIter = keys.iterator();
            while (keyIter.hasNext()) {
                // TODO [p2] unescape, see also escaping below
                String escKey = keyIter.next();
                String escValue = inputMap.get(escKey);
                sb.append(escKey + KEY_VALUE_SEPARATOR + escValue);
                if (keyIter.hasNext()) {
                    sb.append(ENTRY_SEPARATOR);
                }
            }
        }
        return sb.toString();
    }

    /**
     * This will convert a string using following sematics to a map:
     * [key1]=[value1],[key2]=[value2]...
     * <p/>
     * <b>Note:</b> This class is used in GWT and isn't allowed to use 'more
     * complex' api's.
     * 
     * @param inputString
     *            [key1]=[value1],[key2]=[value2]...
     * @return
     */
    static public Map<String, String> stringToMap(String inputString) {
        HashMap<String, String> result = new HashMap<String, String>();
        if (inputString != null) {
            // TODO [p2] escaping
            String[] entries = inputString.split(ENTRY_SEPARATOR);
            for (int n = 0; n < entries.length; n++) {
                String entry = entries[n];
                String[] keyValue = entry.split(KEY_VALUE_SEPARATOR);
                String key = keyValue[0];
                String value = keyValue.length > 1 ? keyValue[1] : null;
                result.put(key, value);
            }
        }
        return result;
    }
}
