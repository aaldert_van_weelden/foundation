package nl.telecats.web.gwt.push.server.services;

import java.util.ArrayList;
import java.util.List;

import nl.telecats.foundation.statusservice.services.StatusDetailServable;
import nl.telecats.foundation.statusservice.services.model.Status;
import nl.telecats.foundation.statusservice.services.model.StatusDetail;
import nl.telecats.foundation.statusservice.services.model.StatusDetailImpl;
import nl.telecats.web.gwt.push.common.entities.EventDispatcherConfigEntity;
import nl.telecats.web.gwt.push.common.entities.EventEntity;
import nl.telecats.web.gwt.push.common.entities.EventMaskEntity;

import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class StatusListener implements StatusDetailServable {

    private static final Log log = LogFactory.getLog(StatusListener.class);

    @Autowired
    private EventDispatcherService eventDispatcherService;

    @Autowired
    private PropertiesConfiguration pc;

    private final static String HEARTBEAT_INTERVAL_MARGIN_SECS_KEY = "status.heartbeatIntervalMarginInSeconds";
    private final static String STATUS_LISTENER_ACTIVE_KEY = "status.statusListenerActive";

    private StatusClient client;
    private boolean running = true;
    private long lastHeartBeat = 0L;

    private String errMsg = null;

    private class StatusClient extends Thread {
        private String listenerId;

        public StatusClient() {

        }

        @Override
        public void run() {
            // register on event dispatcher as listener
            listenerId = eventDispatcherService.registerServiceEventListener();
            // add eventmask for HeartBeat events
            eventDispatcherService.addEventMask(new EventMaskEntity(listenerId, EventEntity.TYPE_DISPATCHING,
                    EventEntity.SUBTYPE_DISP_HEARTBEAT, null));
            // start listener loop
            while (running) {
                // wait for event
                waitForEvent(listenerId);
                // get events
                List<EventEntity> entities = fetchEvents(listenerId);
                if (entities.size() > 0) {
                    for (EventEntity e : entities) {
                        if (EventEntity.TYPE_DISPATCHING.equals(e.getType())
                                && EventEntity.SUBTYPE_DISP_HEARTBEAT.equals(e.getSubtype())) {
                            // heart beat event
                            lastHeartBeat = System.currentTimeMillis();
                            log.info("Received HeartBeat Event");
                        }
                    }
                } else {
                    log.warn("No events received calling fetchEvents");
                }
            }
        }
    }

    private void waitForEvent(String listenerId) {
        try {
            eventDispatcherService.waitForEvent(listenerId);
            errMsg = null;
        } catch (Throwable t) {
            errMsg = "Exception while invoking eventDispatcherService.waitForEvent: " + t.getClass().getSimpleName()
                    + ": " + t.getMessage();
            // log explicitly also for local logging
            log.error(errMsg);
        }
    }

    private List<EventEntity> fetchEvents(String listenerId) {
        List<EventEntity> res = new ArrayList<EventEntity>();
        try {
            res = eventDispatcherService.fetchEvents(listenerId);
            errMsg = null;
            return res;
        } catch (Throwable t) {
            errMsg = "Exception while invoking eventDispatcherService.fetchEvents: " + t.getClass().getSimpleName()
                    + ": " + t.getMessage();
            // log explicitly also for local logging
            log.error(errMsg);
            return res;
        }
    }

    public void init() {
        if (pc.getBoolean(STATUS_LISTENER_ACTIVE_KEY, false)) {
            log.info("Starting the StatusListener to check on heartbeat events");
            client = new StatusClient();
            client.start();
        }
    }

    public void destroy() {
        running = false;
        if (client != null) {
            try {
                log.info("Stopping the StatusListener..");
                client.join(5000L);
                log.info("Stopped.");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public List<StatusDetail> getStatusDetails() {
        EventDispatcherConfigEntity config = eventDispatcherService.getEventDispatcherConfig();
        long hbInterval = config.getHeartbeatInterval();
        long hbIntMargin = pc.getLong(HEARTBEAT_INTERVAL_MARGIN_SECS_KEY);
        long maxDelay = hbInterval + hbIntMargin;

        List<StatusDetail> details = new ArrayList<StatusDetail>();
        StatusDetailImpl status = new StatusDetailImpl();
        details.add(status);

        if (lastHeartBeat == 0) {
            status.setStatus(Status.UNKNOWN);
            status.setShortDescription("Never received HB");
            status.setShortDescription("The StatusClient listener has not yet received any heartbeat events");
        } else {
            long timeSinceLastHb = System.currentTimeMillis() - lastHeartBeat;
            if (timeSinceLastHb > maxDelay) {
                status.setStatus(Status.CRITICAL);
                status.setShortDescription("TSLHB: " + timeSinceLastHb + ">" + maxDelay);
                status.setData("Time since last heartbeat: " + timeSinceLastHb + " exceeds limit of " + maxDelay
                        + " millis");
            } else {
                status.setStatus(Status.OK);
                status.setShortDescription("TSLHB: " + timeSinceLastHb);
                status.setData("Time since last heartbeat: " + timeSinceLastHb + " within limit of " + maxDelay
                        + " millis");
            }
        }

        if (errMsg != null) {
            StatusDetailImpl error = new StatusDetailImpl();
            details.add(error);
            error.setShortDescription("Error invoking EventDispatcher");
            error.setShortDescription(errMsg);
            error.setStatus(Status.CRITICAL);
        }

        return details;
    }
}
