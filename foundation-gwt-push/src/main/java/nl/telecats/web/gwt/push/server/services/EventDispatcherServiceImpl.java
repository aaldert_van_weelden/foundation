package nl.telecats.web.gwt.push.server.services;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.jws.WebParam;
import javax.jws.WebService;

import nl.telecats.foundation.model.exceptions.DatabaseException;
import nl.telecats.foundation.model.exceptions.ObjectNotFoundException;
import nl.telecats.foundation.statusservice.services.StatusDetailServable;
import nl.telecats.foundation.statusservice.services.model.Status;
import nl.telecats.foundation.statusservice.services.model.StatusDetail;
import nl.telecats.foundation.statusservice.services.model.StatusDetailImpl;
import nl.telecats.web.gwt.push.common.entities.EventDispatcherConfigEntity;
import nl.telecats.web.gwt.push.common.entities.EventEntity;
import nl.telecats.web.gwt.push.common.entities.EventFilterEntity;
import nl.telecats.web.gwt.push.common.entities.EventFilterEntity.FilterMode;
import nl.telecats.web.gwt.push.common.entities.EventListenerEntity;
import nl.telecats.web.gwt.push.common.entities.EventMaskEntity;
import nl.telecats.web.gwt.push.server.dao.EventDispatcherConfigEntityDao;
import nl.telecats.web.gwt.push.server.dao.EventEntityDao;
import nl.telecats.web.gwt.push.server.dao.EventFilterEntityDao;
import nl.telecats.web.gwt.push.server.dao.EventListenerEntityDao;
import nl.telecats.web.gwt.push.server.dao.EventMaskEntityDao;

import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @see EventDispatcherService
 * @author Raymond Domingo
 */
@WebService(serviceName = EventDispatcherService.serviceName, targetNamespace = NameSpace.nameSpace, portName = EventDispatcherService.portName)
public class EventDispatcherServiceImpl implements Serializable, EventDispatcherService, StatusDetailServable {

    private static final long serialVersionUID = 8856026665403934910L;

    private static final int MAX_WAIT = 120000;

    private static final Log log = LogFactory.getLog(EventDispatcherServiceImpl.class);

    // list of threads currently waiting, for monitoring purposes only
    private List<Thread> waiting = new ArrayList<Thread>();

    final DateFormat df_ISO_8601 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    private final String EVENT_VALUE_DEFAULT_SEPERATOR = "|";

    /**
     * This is used to support multiple instances of the EventDispatcherServiceImpl. We must distinguish heartbeats of
     * each instance.
     */
    private static final String instanceId = UUID.randomUUID().toString();

    @Autowired
    private EventListenerEntityDao listenerDao;
    @Autowired
    private EventMaskEntityDao eventMaskDao;
    @Autowired
    private EventFilterEntityDao eventFilterDao;
    @Autowired
    private EventEntityDao eventDao;
    @Autowired
    private EventDispatcherConfigEntityDao configDao;
    @Autowired
    private QuietPeriodeService quietPeriodeService;

    @Autowired
    private PropertiesConfiguration pc;

    private final static String LOW_THREAD_TRESHOLD_KEY = "status.LowThreadThreshold";
    private final static String MED_THREAD_TRESHOLD_KEY = "status.MedThreadThreshold";
    private final static String HIGH_THREAD_TRESHOLD_KEY = "status.HighThreadThreshold";
    private final static String MAX_THREAD_TRESHOLD_KEY = "status.MaxThreadThreshold";

    public EventDispatcherServiceImpl() {
    }

    /**
     * @See {@link EventDispatcherService#registerServiceEventListener()}
     */
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class, value = "foundationGwtPush-transactionManager")
    public String registerServiceEventListener() {
        log.info("registerServiceEventListener");
        try {
            String listenerId = createListener();
            log.info("registerServiceEventListener, allocated listenerId:" + listenerId);
            EventListenerEntity listener = listenerDao.findObject(listenerId);
            listener = updateLastActivity(listener);
            return listenerId.toString();
        } catch (DatabaseException e) {
            log.error("Coudn't createListener", e);
            return null;
        }
    }

    /**
     * @See {@link EventDispatcherService#deregisterServiceEventListener(String)}
     * 
     */
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class, value = "foundationGwtPush-transactionManager")
    public void deregisterServiceEventListener(@WebParam(name = "listenerId", partName = "listenerId") String listenerId) {
        log.info("deregisterServiceEventListener:" + listenerId);
        removeListenerAndEventMasks(listenerId);

        // wakeup listeners to release thread of the
        // deregistered listener
        wakeupListeners();
    }

    /**
     * @See {@link EventDispatcherService#waitForEvent(String)}
     */
    public synchronized void waitForEvent(final String listenerId) {
        log.debug("waitForEvent:" + listenerId);
        // Wait for event
        try {
            waiting.add(Thread.currentThread());
            wait(MAX_WAIT);
            waiting.remove(Thread.currentThread());
        } catch (InterruptedException e) {
            log.warn("Waiting interupted", e);
        }
    }

    /**
     * @See {@link EventDispatcherService#fetchEvents(String)

     */
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class, value = "foundationGwtPush-transactionManager")
    public List<EventEntity> fetchEvents(final String listenerId) {
        log.debug("fetchEvents:" + listenerId);
        List<EventEntity> notifications = new ArrayList<EventEntity>();
        if (listenerId != null) {
            try {
                notifications = prepareQueuedEventsForTransport(listenerId);

            } catch (DatabaseException e) {
                log.error("Couldn't access database to prepare notifications for transport", e);
            }
        }
        log.debug("fetchEvents (" + listenerId + ") fetched:" + notifications);
        return notifications;

    }

    /**
     * @See {@link EventDispatcherService#addEventMask(String, EventMask)}
     */
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class, value = "foundationGwtPush-transactionManager")
    public void addEventMask(final EventMaskEntity eventMask) {
        log.info("addEventMask:" + eventMask);
        try {
            eventMaskDao.save(eventMask);
        } catch (DatabaseException e) {
            log.error("Couldn't save eventMask", e);
        }
    }

    /**
     * @See {@link EventDispatcherService#removeEventMask(String, EventMask)}
     */
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class, value = "foundationGwtPush-transactionManager")
    public void removeEventMask(@WebParam(name = "listenerId") final String listenerId,
            @WebParam(name = "eventMask") final EventMaskEntity eventMask) {
        log.info("removeEventMask(listenerId,eventMask):" + listenerId + "," + eventMask);
        String type = eventMask != null ? eventMask.getType() : null;
        String subtype = eventMask != null ? eventMask.getSubtype() : null;
        String properties = eventMask != null ? eventMask.getProperties() : null;
        List<EventMaskEntity> eventMaskEntities = eventMaskDao.findByCiteria(listenerId, type, subtype, properties);
        for (EventMaskEntity eme : eventMaskEntities) {
            eventMaskDao.delete(eme);
        }
    }

    /**
     * @See {@link EventDispatcherService#fireEvent(ServiceEvent)}
     */
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class, value = "foundationGwtPush-transactionManager")
    public synchronized void fireEvent(@WebParam(name = "event") final EventEntity event) {
        log.debug("fireEvent:" + event);
        event.setTimestamp(System.currentTimeMillis());
        try {
            @SuppressWarnings("unused")
            EventEntity ee = eventDao.save(event);
            // flush here, to make sure the event is available for all reading threads
            eventDao.getSessionFactory().getCurrentSession().flush();
        } catch (DatabaseException e) {
            log.error("Couldn't save new event:" + event, e);
        }
        notifyListeners();
    }

    /**
     * @See {@link EventDispatcherService#notifyListeners()}
     */
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class, value = "foundationGwtPush-transactionManager")
    public synchronized void notifyListeners() {
        log.debug("notifyListeners");
        EventDispatcherConfigEntity config = getEventDispatcherConfig();
        boolean delayNotification = config.getLastNotification() + config.getQuietPeriode() > System
                .currentTimeMillis();
        if (delayNotification) {
            quietPeriodeService.startQuietPeriode();

        } else {
            // forward event(s) to listeners
            setEventDispatcherConfig(-1, -1, -1, -1, System.currentTimeMillis());

            final Transaction tx = eventDao.getSessionFactory().getCurrentSession().getTransaction();
            if (tx.isActive()) {
                tx.commit();
            }
            wakeupListeners();
        }
    }

    /**
     * @See {@link EventDispatcherService#getEventQueue(String)}
     */
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class, value = "foundationGwtPush-transactionManager")
    public List<EventEntity> getEventQueue(@WebParam(name = "listenerId") String listenerId) {
        log.info("getEventQueue:" + listenerId);
        List<EventEntity> queuedEvents = eventDao.findByListenerMasksAndTimestamp(listenerId);
        return eventDao.getHibernateUtils().initAndstrip(queuedEvents);
    }

    /**
     * @See {@link EventDispatcherService#getEventMasks(String)}
     */
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class, value = "foundationGwtPush-transactionManager")
    public List<EventMaskEntity> getEventMasks(@WebParam(name = "listenerId") String listenerId) {
        log.info("getEventMasks:" + listenerId);
        List<EventMaskEntity> masks = eventMaskDao.findByListener(listenerId);
        return eventMaskDao.getHibernateUtils().initAndstrip(masks);
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class, value = "foundationGwtPush-transactionManager")
    public void purgeExpiredListeners() {
        long now = System.currentTimeMillis();
        List<String> deletedListeners = listenerDao.deleteExpired(now
                - getEventDispatcherConfig().getExpirationTimeout());
        log.debug("Purged listeners:" + deletedListeners);
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class, value = "foundationGwtPush-transactionManager")
    public void purgeExpiredEvents(String purgerId) {
        List<EventEntity> purgeExpiredEvents = eventDao.purgeExpiredEvents(purgerId);
        log.debug("Purged events:" + purgeExpiredEvents);
    }

    // *************************************
    // Helper methods
    // *************************************

    /**
     * Update lastActivity timestamp of listener to current time.
     * 
     * @param listener
     *            to update
     * @return updated listener
     * @throws DatabaseException
     *             on database error
     */
    @Transactional
    private EventListenerEntity updateLastActivity(EventListenerEntity listener) throws DatabaseException {
        return updateLastActivity(listener, System.currentTimeMillis());
    }

    /**
     * Update lastActivity timestamp of listener to specific time.
     * 
     * @param listener
     *            to update
     * @param lastActivityTS
     *            specific time
     * @return updated listener
     * @throws DatabaseException
     *             on database error
     */
    @Transactional
    private EventListenerEntity updateLastActivity(EventListenerEntity listener, long lastActivityTS)
            throws DatabaseException {
        listener.setLastActivity(lastActivityTS);
        return listenerDao.save(listener);
    }

    private String createListener() throws DatabaseException {
        EventListenerEntity listener = new EventListenerEntity();
        listener = listenerDao.save(listener);
        return (String) listener.getId();
    }

    private void removeListenerAndEventMasks(String listenerId) {
        removeEventMask(listenerId, null);

        // delete listener
        try {
            EventListenerEntity listener = listenerDao.findObject(listenerId);
            listenerDao.delete(listener);
        } catch (ObjectNotFoundException e) {
            // nothing to delete
        }
    }

    /**
     * Find all events currently queued for specified listener.
     * 
     * @param listenerId
     * @return
     * @throws DatabaseException
     */
    private List<EventEntity> prepareQueuedEventsForTransport(String listenerId) throws DatabaseException {
        log.debug("Perparing queued events for transport for listener " + listenerId);

        EventListenerEntity listener;
        try {
            listener = listenerDao.findObject(listenerId);

            // find all events that have not been sent to the listener
            // register this moment; events that have been received after this moment, will go in the next run

            // default timestamp; all events up to now will be fetched
            long timestampOfLastEventSendToClient = System.currentTimeMillis();

            // queue refresh timer
            List<EventEntity> queuedEvents = eventDao.findByListenerMasksAndTimestamp(listenerId);
            log.debug("Found " + queuedEvents.size() + " events for listener " + listenerId);

            List<EventEntity> filteredQueuedEvents = filterForeignHeartBeats(queuedEvents);
            log.debug("Filtered to " + filteredQueuedEvents.size() + " events for listener " + listenerId);

            // we might have found fired events after 'timestampOfLastEventSendToClient', so if we find events we must
            // actualize the timestampOfLastEventSendToClient so we are sure it isn't send twice to the listener

            // make sure no events will be missed; all events received after the last set of pushed events will go in
            // the next run; this prevents a race condition
            if (filteredQueuedEvents.size() > 0) {
                timestampOfLastEventSendToClient = filteredQueuedEvents.get(filteredQueuedEvents.size() - 1)
                        .getTimestamp();
                log.debug("Updating last activity timestamp of " + listenerId + " to that of last event: "
                        + timestampOfLastEventSendToClient);
            } else {
                log.debug("Using current timestamp as last activity timestamp for " + listenerId + ": "
                        + timestampOfLastEventSendToClient);
            }

            filteredQueuedEvents = eventDao.getHibernateUtils().initAndstrip(filteredQueuedEvents);
            updateLastActivity(listener, timestampOfLastEventSendToClient);

            return filteredQueuedEvents;
        } catch (ObjectNotFoundException e) {
            // listener probably deregistered
            return new ArrayList<EventEntity>();
        }
    }

    /**
     * Filter heartBeats of other dispatcher instances
     * 
     * @param queuedEvents
     * @return
     */
    private List<EventEntity> filterForeignHeartBeats(List<EventEntity> queuedEvents) {
        List<EventEntity> filteredQueue = new ArrayList<EventEntity>();
        for (EventEntity ee : queuedEvents) {
            if (!(EventEntity.TYPE_DISPATCHING.equals(ee.getType())
                    && EventEntity.SUBTYPE_DISP_HEARTBEAT.equals(ee.getSubtype()) && !instanceId.equals(ee.getValue()))) {
                filteredQueue.add(ee);
            } else {
                log.debug("Filtereing heartbeat of instance:" + ee.getValue());
            }
        }
        return filteredQueue;
    }

    private synchronized void wakeupListeners() {
        notifyAll();
    }

    // *************************************
    // getters / setters
    // *************************************
    /**
     * @see EventDispatcherService#getEventDispatcherConfig()
     */
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class, value = "foundationGwtPush-transactionManager")
    public EventDispatcherConfigEntity getEventDispatcherConfig() {
        log.debug("getEventDispatcherConfig");
        EventDispatcherConfigEntity config = null;
        if (configDao != null) {
            configDao.getSessionFactory().getCurrentSession().flush();
            List<EventDispatcherConfigEntity> configs = configDao.findAll();
            if (!configs.isEmpty()) {
                config = configs.get(0);
            }
        }

        if (config == null) {
            // config not loaded, create new config
            config = new EventDispatcherConfigEntity();

            // when configDao available, store initial config
            try {
                config = configDao.save(config);
            } catch (DatabaseException e) {
                log.error("Could not save initial dispatcher configuration, continue using defaults", e);
            }
        }
        return config;
    }

    /**
     * @See {@link EventDispatcherService#getEventListeners()

     */
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class, value = "foundationGwtPush-transactionManager")
    public List<String> getEventListeners() {
        log.debug("getEventListeners");
        List<EventListenerEntity> listeners = listenerDao.findAll();
        List<String> result = new ArrayList<String>();
        for (EventListenerEntity l : listeners) {
            result.add(l.getId().toString());
        }
        return result;
    }

    /**
     * @See {@link EventDispatcherService#setQuietPeriode(long)}
     */
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class, value = "foundationGwtPush-transactionManager")
    public void setQuietPeriode(@WebParam(name = "periode") long periode) {
        log.info("setQuietPeriode:" + periode);
        setEventDispatcherConfig(-1, -1, -1, periode, -1);
    }

    /**
     * @See {@link EventDispatcherService#getQuietPeriode()}
     */
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class, value = "foundationGwtPush-transactionManager")
    public long getQuietPeriode() {
        return getEventDispatcherConfig().getQuietPeriode();
    }

    /**
     * @See {@link EventDispatcherService#setHeartbeatInterval(long)}
     */
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class, value = "foundationGwtPush-transactionManager")
    public void setHeartbeatInterval(@WebParam(name = "interval") long interval) {
        log.info("setHeartbeatInterval:" + interval);
        setEventDispatcherConfig(-1, interval, -1, -1, -1);
    }

    /**
     * @See {@link EventDispatcherService#getHeartbeatInterval()}
     */
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class, value = "foundationGwtPush-transactionManager")
    public long getHeartbeatInterval() {
        return getEventDispatcherConfig().getHeartbeatInterval();
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class, value = "foundationGwtPush-transactionManager")
    public void setPurgeInterval(@WebParam(name = "interval") long interval) {
        log.info("setPurgeInterval:" + interval);
        setEventDispatcherConfig(-1, -1, interval, -1, -1);
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class, value = "foundationGwtPush-transactionManager")
    public long getPurgeInterval() {
        return getEventDispatcherConfig().getPurgeInterval();
    }

    /**
     * Changing the eventDispatcherConfig should be synchronized !
     * 
     * @param expiration
     *            -1 will ignore this parameter and current value won't be changed
     * @param heartbeat
     *            -1 will ignore this parameter and current value won't be changed
     * @param purge
     *            -1 will ignore this parameter and current value won't be changed
     * @param quiet
     *            -1 will ignore this parameter and current value won't be changed
     * @param lastNotification
     *            -1 will ignore this parameter and current value won't be changed
     */
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class, value = "foundationGwtPush-transactionManager")
    public synchronized void setEventDispatcherConfig(long expiration, long heartbeat, long purge, long quiet,
            long lastNotification) {
        EventDispatcherConfigEntity config = getEventDispatcherConfig();
        if (expiration >= 0) {
            config.setExpirationTimeout(expiration);
        }
        if (heartbeat >= 0) {
            config.setHeartbeatInterval(heartbeat);
        }
        if (quiet >= 0) {
            config.setQuietPeriode(quiet);
        }
        if (purge >= 0) {
            config.setPurgeInterval(purge);
        }
        if (lastNotification >= 0) {
            config.setLastNotification(lastNotification);
        }
        try {
            configDao.save(config);
        } catch (DatabaseException e) {
            log.error("Couldn't save EventDispatcherConfig:" + e);
        }
    }

    /**
     * @See {@link EventDispatcherService#setExpirationTimeout(long)}
     */
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class, value = "foundationGwtPush-transactionManager")
    public void setExpirationTimeout(@WebParam(name = "time") long time) {
        log.info("setExpirationTimeout:" + time);
        setEventDispatcherConfig(time, -1, -1, -1, -1);
    }

    /**
     * @See {@link EventDispatcherService#getExpirationTimeout()}
     */
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class, value = "foundationGwtPush-transactionManager")
    public long getExpirationTimeout() {
        return getEventDispatcherConfig().getExpirationTimeout();
    }

    public String getInstanceId() {
        return instanceId;
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class, value = "foundationGwtPush-transactionManager")
    public List<EventEntity> findByType(String type) {
        return findByTypeAndSubType(type, null);
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class, value = "foundationGwtPush-transactionManager")
    public List<EventEntity> findByTypeAndSubType(String type, String subType) {
        return findByTypeSubTypeAndTime(type, subType, null, null);
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class, value = "foundationGwtPush-transactionManager")
    public List<EventEntity> findByTypeSubTypeAndTime(String type, String subType, String startDateTime,
            String endDateTime) {

        Long startTimestamp = null;
        if (startDateTime != null) {
            try {
                startTimestamp = new Long(df_ISO_8601.parse(startDateTime).getTime());
            } catch (ParseException e) {
                String message = "findBy: startDatetime not in correct ISO format: " + startDateTime;
                log.error(message);
                throw new IllegalArgumentException(message);
            }
        }
        Long endTimestamp = null;
        if (endDateTime != null) {
            try {
                endTimestamp = new Long(df_ISO_8601.parse(endDateTime).getTime());
            } catch (ParseException e) {
                String message = "findBy: endDatetime not in correct ISO format: " + endDateTime;
                log.error(message);
                throw new IllegalArgumentException(message);
            }
        }

        log.debug("findBy" + ", type=" + (type == null ? "null" : type) + ", subType="
                + (subType == null ? "null" : subType) + ", startDateTime="
                + (startDateTime == null ? "null" : startDateTime) + ", endDateTime="
                + (endDateTime == null ? "null" : endDateTime));

        List<EventEntity> events = eventDao.findBy(type, subType, startTimestamp, endTimestamp);
        log.debug("findBy EventEntity returned " + String.valueOf(events.size()) + " items");
        return eventDao.getHibernateUtils().initAndstrip(events);

    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class, value = "foundationGwtPush-transactionManager")
    public List<EventEntity> findByContent(String type, String subType, String startDateTime, String endDateTime,
            String content) {

        Long startTimestamp = null;
        if (startDateTime != null) {
            try {
                startTimestamp = new Long(df_ISO_8601.parse(startDateTime).getTime());
            } catch (ParseException e) {
                String message = "findBy: startDatetime not in correct ISO format: " + startDateTime;
                log.error(message);
                throw new IllegalArgumentException(message);
            }
        }
        Long endTimestamp = null;
        if (endDateTime != null) {
            try {
                endTimestamp = new Long(df_ISO_8601.parse(endDateTime).getTime());
            } catch (ParseException e) {
                String message = "findBy: endDatetime not in correct ISO format: " + endDateTime;
                log.error(message);
                throw new IllegalArgumentException(message);
            }
        }

        if (content == null) {
            return findByTypeSubTypeAndTime(type, subType, startDateTime, endDateTime);
        }

        log.debug("findByContent" + ", type=" + (type == null ? "null" : type) + ", subType="
                + (subType == null ? "null" : subType) + ", startDateTime="
                + (startDateTime == null ? "null" : startDateTime) + ", endDateTime="
                + (endDateTime == null ? "null" : endDateTime) + ", content=" + content);
        List<EventEntity> events = eventDao.findBy(type, subType, startTimestamp, endTimestamp);

        // filter returned events by content
        List<EventEntity> filteredEvents = new ArrayList<EventEntity>();
        for (EventEntity event : events) {
            if (content.equals(event.getValue())) {
                filteredEvents.add(event);
            }
        }
        log.debug("findByContent eventEntity returned " + String.valueOf(filteredEvents.size()) + " items");
        return eventDao.getHibernateUtils().initAndstrip(filteredEvents);
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class, value = "foundationGwtPush-transactionManager")
    public List<EventEntity> findByFieldAndContent(String type, String subType, String startDateTime,
            String endDateTime, int fieldNr, String content) {
        return findBySepFieldAndContent(type, subType, startDateTime, endDateTime, EVENT_VALUE_DEFAULT_SEPERATOR,
                fieldNr, content);
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class, value = "foundationGwtPush-transactionManager")
    public List<EventEntity> findBySepFieldAndContent(String type, String subType, String startDateTime,
            String endDateTime, String seperator, int fieldNr, String content) {

        Long startTimestamp = null;
        if (startDateTime != null) {
            try {
                startTimestamp = new Long(df_ISO_8601.parse(startDateTime).getTime());
            } catch (ParseException e) {
                String message = "findBy: startDatetime not in correct ISO format: " + startDateTime;
                log.error(message);
                throw new IllegalArgumentException(message);
            }
        }
        Long endTimestamp = null;
        if (endDateTime != null) {
            try {
                endTimestamp = new Long(df_ISO_8601.parse(endDateTime).getTime());
            } catch (ParseException e) {
                String message = "findBy: endDatetime not in correct ISO format: " + endDateTime;
                log.error(message);
                throw new IllegalArgumentException(message);
            }
        }

        if ((seperator == null) || (seperator.equals(""))) {
            String message = "findbyContent: seperator has no value";
            log.error(message);
            throw new IllegalArgumentException(message);
        }

        if (fieldNr <= 0) {
            String message = "findbyContent: fieldNr has a value lower then 1: " + Integer.toString(fieldNr);
            log.error(message);
            throw new IllegalArgumentException(message);
        }

        if (content == null) {
            String message = "findbyContent: content has no value";
            log.error(message);
            throw new IllegalArgumentException(message);
        }

        log.debug("findByContent" + ", type=" + (type == null ? "null" : type) + ", subType="
                + (subType == null ? "null" : subType) + ", startDateTime="
                + (startDateTime == null ? "null" : startDateTime) + ", endDateTime="
                + (endDateTime == null ? "null" : endDateTime) + ", seperator=" + seperator + ", fieldNr="
                + Integer.toString(fieldNr) + ", content=" + content);
        List<EventEntity> events = eventDao.findBy(type, subType, startTimestamp, endTimestamp);

        // escaping seperator for creating correct regular expression
        seperator = seperator.replace("\\", "\\\\");
        seperator = seperator.replace("[", "\\[");
        seperator = seperator.replace("^", "\\^");
        seperator = seperator.replace("$", "\\$");
        seperator = seperator.replace(".", "\\.");
        seperator = seperator.replace("|", "\\|");
        seperator = seperator.replace("?", "\\?");
        seperator = seperator.replace("*", "\\*");
        seperator = seperator.replace("+", "\\+");
        seperator = seperator.replace("(", "\\(");
        seperator = seperator.replace(")", "\\)");

        // filter returned events by content
        List<EventEntity> filteredEvents = new ArrayList<EventEntity>();
        for (EventEntity event : events) {
            String value = event.getValue();
            if (value != null) {

                String[] allFields = value.split(seperator);
                if (allFields.length > fieldNr) {
                    if (content.equals(allFields[fieldNr])) {
                        filteredEvents.add(event);
                    }
                }
            }
        }
        log.debug("findByContent eventEntity returned " + String.valueOf(filteredEvents.size()) + " items");
        return eventDao.getHibernateUtils().initAndstrip(filteredEvents);
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class, value = "foundationGwtPush-transactionManager")
    public List<EventFilterEntity> getEventFilters(@WebParam(name = "ownerId") String ownerId) {
        log.info("getEventFilters:" + ownerId);
        List<EventFilterEntity> filters = eventFilterDao.findByOwner(ownerId);
        return eventFilterDao.getHibernateUtils().initAndstrip(filters);

    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class, value = "foundationGwtPush-transactionManager")
    public void addEventFilter(EventFilterEntity eventFilter) {
        log.info("addEventFilter:" + eventFilter);
        try {
            eventFilterDao.save(eventFilter);
        } catch (DatabaseException e) {
            log.error("Couldn't save eventFilter", e);
        }
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class, value = "foundationGwtPush-transactionManager")
    public void removeEventFilter(EventFilterEntity eventFilter) {
        String ownerId = eventFilter.getOwnerId();
        log.info("removeEventFilter(ownerId,eventFilter):" + ownerId + "," + eventFilter);
        String type = eventFilter != null ? eventFilter.getType() : null;
        String subtype = eventFilter != null ? eventFilter.getSubtype() : null;
        String properties = eventFilter != null ? eventFilter.getProperties() : null;
        FilterMode filterMode = eventFilter != null ? eventFilter.getFilterMode() : null;
        List<EventFilterEntity> eventFilterEntities = eventFilterDao.findByCriteria(ownerId, type, subtype, properties,
                filterMode);
        for (EventFilterEntity efe : eventFilterEntities) {
            eventMaskDao.delete(efe);
        }
    }

    @Override
    public List<StatusDetail> getStatusDetails() {
        List<StatusDetail> details = new ArrayList<StatusDetail>();
        int waitingThreads = waiting.size();
        int low = pc.getInt(LOW_THREAD_TRESHOLD_KEY, 1);
        int mid = pc.getInt(MED_THREAD_TRESHOLD_KEY, 100);
        int high = pc.getInt(HIGH_THREAD_TRESHOLD_KEY, 150);
        int max = pc.getInt(MAX_THREAD_TRESHOLD_KEY, 200);
        StatusDetailImpl detail = new StatusDetailImpl("Threads waiting in waitOnEvent(): " + waiting.size() + "/"
                + max, "Threads: " + waiting.size() + "/" + max, Status.UNKNOWN);
        details.add(detail);
        if (waitingThreads >= low && waitingThreads < mid)
            detail.setStatus(Status.OK);
        else if (waitingThreads >= mid && waitingThreads < high) {
            detail.setStatus(Status.WARNING);
            detail.setData(detail.getData() + " meets WARNING treshold of " + mid);
        } else if (waitingThreads >= high) {
            detail.setStatus(Status.CRITICAL);
            detail.setData(detail.getData() + " meets CRITICAL treshold of " + high);
        }
        return details;
    }

}
