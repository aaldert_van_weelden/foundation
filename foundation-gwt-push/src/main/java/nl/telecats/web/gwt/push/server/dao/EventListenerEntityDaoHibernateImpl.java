package nl.telecats.web.gwt.push.server.dao;

import java.util.ArrayList;
import java.util.List;

import nl.telecats.web.gwt.push.common.entities.EventListenerEntity;
import nl.telecats.web.gwt.push.common.entities.EventMaskEntity;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Extend from the AbstractModelDao so we explicitly get the correct transaction
 * manager
 * 
 * @see EventListenerEntityDao
 * @author Raymond Domingo
 * 
 */
@Repository
@Transactional(propagation = Propagation.REQUIRED, value = "foundationGwtPush-transactionManager")
public class EventListenerEntityDaoHibernateImpl extends FoundationGwtPushAbstractModelDao<EventListenerEntity> implements
        EventListenerEntityDao {

    @Autowired
    private EventMaskEntityDao eventMaskDao;

    public EventListenerEntityDaoHibernateImpl() {
        super(EventListenerEntity.class);
    }

    /**
     * @see EventListenerEntityDao#deleteExpired(long)
     */
    @SuppressWarnings("unchecked")
    public List<String> deleteExpired(long timestamp) {
        DetachedCriteria criteria = DetachedCriteria.forClass(EventListenerEntity.class);
        criteria.add(Restrictions.lt("lastActivity", timestamp));
        List<EventListenerEntity> listeners = getHibernateTemplate().findByCriteria(criteria);
        List<String> listenerIds = new ArrayList<String>();
        for (EventListenerEntity l : listeners) {
            listenerIds.add((String) l.getId());
            List<EventMaskEntity> eventMasks = eventMaskDao.findByCiteria((String) l.getId(), null, null, null);
            eventMaskDao.deleteAll(eventMasks);
        }
        getHibernateTemplate().deleteAll(listeners);
        return listenerIds;
    }
}
