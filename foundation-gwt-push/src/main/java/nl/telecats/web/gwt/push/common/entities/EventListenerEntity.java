package nl.telecats.web.gwt.push.common.entities;

import nl.telecats.foundation.model.entities.IdentifiableObject;

/**
 * This entity contains information aboutServiceEventListener instances
 * 
 * @author Raymond Domingo
 * 
 */
public class EventListenerEntity implements IdentifiableObject {

    private static final long serialVersionUID = 6063225747927936237L;

    // field helper, used to construct queries in your dao's
    public static final String LAST_ACTIVITY_FIELD = "lastActivity";

    /**
     * Technical database version, don't use it unless you know exactly what you
     * are doing.
     */
    private int version = 0;

    /**
     * Technical database id, don't use it unless you know exactly what you are
     * doing.
     */
    private String id;

    /**
     * Last detected activity timestamp of this listener
     */
    private long lastActivity = 0;

    /**
     * Defaut constructor needed for serialization
     */
    public EventListenerEntity() {
        super();
    }

    /**
     * @return id
     * @see EventListenerEntity#id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id
     * @see EventListenerEntity#id
     */
    public void setId(String id) {
        this.id = (String) id;
    }

    /**
     * @return version
     * @see EventListenerEntity#version
     */
    public Integer getVersion() {
        return version;
    }

    /**
     * 
     * @param version
     * @see EventListenerEntity#version
     */
    public void setVersion(Integer v) {
        version = v;
    }

    /**
     * @return lastActivity
     * @see EventListenerEntity#lastActivity
     */
    public long getLastActivity() {
        return lastActivity;
    }

    /**
     * @param lastActivity
     * @see EventListenerEntity#lastActivity
     */
    public void setLastActivity(long lastActivity) {
        this.lastActivity = lastActivity;
    }
}
