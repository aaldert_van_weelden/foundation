package nl.telecats.web.gwt.push.server.dao;

import java.util.ArrayList;
import java.util.List;

import nl.telecats.web.gwt.push.common.entities.EventFilterEntity;
import nl.telecats.web.gwt.push.common.entities.EventFilterEntity.FilterMode;

import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/***
 * Extend from the AbstractModelDao so we explicitly get the correct transaction
 * manager
 * 
 * @author Gregor Ybema
 * @see EventFilterEntityDao
 * 
 */
@Repository
@Transactional(propagation = Propagation.REQUIRED, value = "foundationGwtPush-transactionManager")
public class EventFilterEntityDaoHibernateImpl extends FoundationGwtPushAbstractModelDao<EventFilterEntity> implements
        EventFilterEntityDao {

    public EventFilterEntityDaoHibernateImpl() {
        super(EventFilterEntity.class);
    }

    /**
     * @see EventFilterEntityDao#findByOwner(String)
     */
    public List<EventFilterEntity> findByOwner(String ownerId) {
        return this.findByCriteria(ownerId, null, null, (String) null, null);
    }

    /**
     * @see EventFilterEntityDao#findByCiteria(String, String, String, String,
     *      FilterMode)
     */
    @SuppressWarnings("unchecked")
    public List<EventFilterEntity> findByCriteria(String ownerId, String type, String subtype, String properties,
            FilterMode filterMode) {
        boolean hasCriteria = false;
        DetachedCriteria crit = DetachedCriteria.forClass(EventFilterEntity.class);
        if (!StringUtils.isEmpty(type)) {
            hasCriteria = true;
            crit.add(Restrictions.eq("type", type));
        }
        if (!StringUtils.isEmpty(subtype)) {
            hasCriteria = true;
            crit.add(Restrictions.eq("subtype", subtype));
        }
        if (!StringUtils.isEmpty(properties)) {
            hasCriteria = true;
            crit.add(Restrictions.eq("properties", properties));
        }
        if (!StringUtils.isEmpty(ownerId)) {
            hasCriteria = true;
            crit.add(Restrictions.eq("ownerId", ownerId));
        }
        if (filterMode != null) {
            hasCriteria = true;
            crit.add(Restrictions.eq("filterModeAsString", filterMode.getValue()));
        }

        List<EventFilterEntity> contents = null;
        if (hasCriteria) {
            contents = getHibernateTemplate().findByCriteria(crit);
        } else {
            contents = new ArrayList<EventFilterEntity>();
        }
        return contents;
    }

    /**
     * @see EventFilterEntityDao#deleteAll(List)
     */
    public void deleteAll(List<EventFilterEntity> eventFilters) {
        getHibernateTemplate().deleteAll(eventFilters);
    }
}
