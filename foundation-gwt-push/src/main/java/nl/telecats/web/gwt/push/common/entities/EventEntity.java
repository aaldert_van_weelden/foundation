package nl.telecats.web.gwt.push.common.entities;

import java.util.Map;

import nl.telecats.foundation.model.entities.IdentifiableObject;
import nl.telecats.web.gwt.push.common.util.ConversionUtil;

/**
 * The eventEntity contains all information about an event.
 * 
 * @author Raymond Domingo
 * 
 * @see EventMaskEntity
 * 
 */
public class EventEntity implements IdentifiableObject {

    private static final long serialVersionUID = 2096888975642279485L;

    // *******************************************
    // TYPES
    // *******************************************
    // events related to event dispatching
    public static final String TYPE_DISPATCHING = "TYPE_DISPATCHING";

    // *******************************************
    // SUBTYPES
    // *******************************************
    // the listener (id) is not known to the dispatcher
    public static final String SUBTYPE_DISP_HEARTBEAT = "SUBTYPE_DISP_HEARTBEAT";
    public static final String SUBTYPE_DISP_UNKNOWN_LISTENER = "SUBTYPE_DISP_UNKNOWN_LISTENER";

    // field helpers, used to construct queries in your dao's
    public static final String TYPE_FIELD = "type";
    public static final String SUBTYPE_FIELD = "subtype";
    public static final String TIMESTAMP_FIELD = "timestamp";

    /**
     * Technical database version, don't use it unless you know exactly what you
     * are doing.
     */
    private int version = 0;

    /**
     * Technical database id, don't use it unless you know exactly what you are
     * doing.
     */
    private String id;

    /**
     * (optional) Indicate the application throwing the event, example:
     * <p/>
     * nl.telecats.experimental.messenger
     */
    private String type;

    /**
     * (optional) Describes the event, example: new_message
     */
    private String subtype;

    /**
     * (optional) Additional / custom properties like:
     * <p/>
     * host=pc-raymond,user=raymond
     * 
     * @see ConversionUtil.stringToMap
     */
    private String properties;

    /**
     * An event might contain a value, it is up to the receiver of the event to
     * interpet this value. You may add up to '1024' characters (specified in
     * hbm file)
     */
    private String value;

    /**
     * Timestamp the event is fired, don't use this field unless you know
     * exactly what you are doing. It is used by the EventDispatcherService.
     */
    private long timestamp;

    /**
     * Default constructor needed for serialization
     */
    public EventEntity() {
        this(null, null);
    }

    /**
     * 
     * @param type
     *            eventType
     * @param subtype
     *            eventSubType
     */
    public EventEntity(String type, String subtype) {
        this(type, subtype, null, null);
    }

    /**
     * 
     * @param type
     * @param subtype
     * @param properties
     * @param value
     */
    public EventEntity(String type, String subtype, Map<String, String> properties, String value) {
        this.type = type;
        this.subtype = subtype;
        this.value = value;
        setPropertiesMap(properties);
    }

    /**
     * @return id
     * @see EventEntity#id
     * 
     */
    public String getId() {
        return id;
    }

    /**
     * @param id
     * @see EventEntity#id
     */
    public void setId(String id) {
        this.id = (String) id;
    }

    /**
     * @return version
     * @see EventEntity#version
     */
    public Integer getVersion() {
        return version;
    }

    /**
     * @param version
     * @see EventEntity#
     */
    public void setVersion(Integer version) {
        this.version = version;
    }

    /**
     * @return type
     * @see EventEntity#type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type
     * @see EventEntity#type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return subtype
     * @see EventEntity#subtype
     */
    public String getSubtype() {
        return subtype;
    }

    public void setSubtype(String subtype) {
        this.subtype = subtype;
    }

    /**
     * 
     * @return properties
     * @see EventEntity#properties
     * @see ConversionUtil.stringToMap
     */
    public String getProperties() {
        return properties;
    }

    /**
     * @param properties
     * @see @see EventEntity#properties
     * @see ConversionUtil.stringToMap
     */
    public void setProperties(String properties) {
        this.properties = properties;
    }

    /**
     * @return properties
     * @see EventEntity#properties
     * @see ConversionUtil.stringToMap
     */
    public Map<String, String> getPropertiesMap() {
        return ConversionUtil.stringToMap(properties);
    }

    /**
     * Note: keys and values incontent map shouldn't contain any special chars.
     * The map will be persisted as a string.
     * 
     * @param propsMap
     * @see EventEntity#properties
     * @see ConversionUtil.mapToString
     */
    public void setPropertiesMap(Map<String, String> propsMap) {
        properties = ConversionUtil.mapToString(propsMap);
    }

    /**
     * 
     * @return value
     * @see EventEntity#value
     */
    public String getValue() {
        return value;
    }

    /**
     * @param value
     * @see EventEntity#value
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * @return timestamp
     * @see EventEntity#timestamp
     */
    public long getTimestamp() {
        return timestamp;
    }

    /**
     * 
     * @param timestamp
     * @see EventEntity#timestamp
     */
    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * Override toString
     * 
     * @return type,subtype,[properties],value
     */
    public String toString() {
        return type + "," + subtype + " [" + properties + "] ," + value;
    }
}
