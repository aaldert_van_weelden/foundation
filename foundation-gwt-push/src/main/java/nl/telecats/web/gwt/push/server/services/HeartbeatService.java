package nl.telecats.web.gwt.push.server.services;

import javax.jws.WebMethod;
import javax.jws.WebService;

/**
 * Distributed event dispatcher, used for pushing events to your gwt clients.
 * 
 * @author raymond
 * 
 */
@WebService(targetNamespace = NameSpace.nameSpace)
public interface HeartbeatService {

    public static final String serviceName = "HeartbeatService";
    public static final String portName = "HeartbeatServicePort";

    @WebMethod(operationName = "startHeartBeat", action = "startHeartBeat")
    public void startHeartBeat();
}
