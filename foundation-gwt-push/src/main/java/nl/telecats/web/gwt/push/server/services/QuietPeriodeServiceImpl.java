package nl.telecats.web.gwt.push.server.services;

import java.io.Serializable;

import javax.jws.WebService;

import nl.telecats.web.gwt.push.common.entities.EventDispatcherConfigEntity;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * 
 * @see EventDispatcherService
 * @author Raymond Domingo
 */
@WebService(serviceName = QuietPeriodeService.serviceName, targetNamespace = NameSpace.nameSpace, portName = QuietPeriodeService.portName)
public class QuietPeriodeServiceImpl implements Serializable, QuietPeriodeService {

    private static final long serialVersionUID = 9132229791690585680L;

    private transient Thread quietPeriodeDaemon = null;

    @Autowired
    private EventDispatcherService eventDispatcherService;

    public QuietPeriodeServiceImpl() {
    }

    public boolean isRunning() {
        return quietPeriodeDaemon != null;
    }

    /**
     * Start sending heart beat events. (EventEntity.TYPE_DISPATCHING,
     * EventEntity.SUBTYPE_DISP_HEARTBEAT)
     */
    public void startQuietPeriode() {
        EventDispatcherConfigEntity config = eventDispatcherService.getEventDispatcherConfig();
        final long remainingQuietPeriode = config.getLastNotification() + config.getQuietPeriode()
                - System.currentTimeMillis();
        if (remainingQuietPeriode > 0) {
            if (quietPeriodeDaemon == null) {
                quietPeriodeDaemon = new Thread() {
                    @Override
                    public synchronized void run() {
                        try {
                            if (remainingQuietPeriode > 0) {
                                wait(remainingQuietPeriode);
                            }
                        } catch (InterruptedException e) {
                        }
                        // cleanup the quietPeriodeDaemon
                        quietPeriodeDaemon = null;
                        eventDispatcherService.notifyListeners();
                    }
                };
                quietPeriodeDaemon.start();
            } else {
                // quietDaemon already running
            }
        } else {
            // quietPeriode expired
            eventDispatcherService.notifyListeners();
        }
    }
}
