package nl.telecats.web.gwt.push.server.dao;

import java.util.List;

import nl.telecats.foundation.model.dao.GenericDao;
import nl.telecats.web.gwt.push.common.entities.EventListenerEntity;

/**
 * Dao for accessing your eventListener entities
 * 
 * @author Raymond Domingo
 * @see GenericDao
 * 
 */
public interface EventListenerEntityDao extends GenericDao<EventListenerEntity> {

    /**
     * Delete all listeners containing an older lastActivity as specified
     * timestamp.
     * 
     * @param timestamp
     * @return deleted listener id's
     */
    List<String> deleteExpired(long timestamp);

}
