package nl.telecats.web.gwt.test.config;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestConfigOverride {
    @Test
    public void testConfigDefault() {
        ApplicationContext ac = new ClassPathXmlApplicationContext(new String[] { "classpath:context-test.xml" });
        ConfigTestBean ctb = (ConfigTestBean) ac.getBean("configTestBean");
        Assert.assertTrue("value 1 ok", ctb.getValue1().equals("default_value1"));
        Assert.assertTrue("value 2 ok", ctb.getValue2().equals("default_value2"));
    }

    @Test
    public void testConfigCustomSettings1() {
        ApplicationContext ac = new ClassPathXmlApplicationContext(new String[] { "classpath:context-test.xml",
                "classpath:context-test-custom_settings1.xml" });
        ConfigTestBean ctb = (ConfigTestBean) ac.getBean("configTestBean");
        Assert.assertTrue("value 1 ok", ctb.getValue1().equals("custom1_value1"));
        Assert.assertTrue("value 2 ok", ctb.getValue2().equals("default_value2"));
    }

    @Test
    public void testConfigCustomSettings2() {
        ApplicationContext ac = new ClassPathXmlApplicationContext(new String[] { "classpath:context-test.xml",
                "classpath:context-test-custom_settings2.xml" });
        ConfigTestBean ctb = (ConfigTestBean) ac.getBean("configTestBean");
        Assert.assertTrue("value 1 ok", ctb.getValue1().equals("custom2_value1"));
        Assert.assertTrue("value 2 ok", ctb.getValue2().equals("custom2_value2"));
    }
}