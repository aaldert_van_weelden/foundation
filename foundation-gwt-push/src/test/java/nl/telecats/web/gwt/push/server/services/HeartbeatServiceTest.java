package nl.telecats.web.gwt.push.server.services;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:foundation-gwt-push-test-context.xml" })
@TransactionConfiguration(transactionManager = "foundationGwtPush-transactionManager", defaultRollback = false)
public class HeartbeatServiceTest {

    @Autowired
    private HeartbeatService service;

    @Test
    public void testAutowire() {
        Assert.assertNotNull(service);
    }

    // TODO actually add unittest to validate the inner workings of the
    // HeartbeatService

}
