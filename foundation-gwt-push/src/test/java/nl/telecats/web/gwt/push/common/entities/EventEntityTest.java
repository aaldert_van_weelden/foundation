package nl.telecats.web.gwt.push.common.entities;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

import junit.framework.Assert;
import nl.telecats.foundation.model.exceptions.DatabaseException;
import nl.telecats.web.gwt.push.server.dao.EventEntityDao;
import nl.telecats.web.gwt.push.server.dao.EventFilterEntityDao;
import nl.telecats.web.gwt.push.server.dao.EventListenerEntityDao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

/**
 * Testsuite for the EventEntity
 * 
 * @author Gregor Ybema
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:foundation-gwt-push-model-test-context.xml" })
@TransactionConfiguration(transactionManager = "foundationGwtPush-transactionManager", defaultRollback = false)
public class EventEntityTest {

    @Autowired
    private EventEntityDao eventEntityDao;
    @Autowired
    private EventFilterEntityDao eventFilterDao;
    @Autowired
    private EventListenerEntityDao eventListenerDao;

    private static Serializable eventEntity1Id;

    private final String TEST_DUMMY_TYPE = "test_dummy_type";
    private final String TEST_DUMMY_SUBTYPE = "test_dummy_type";

    private long timestamp;

    @Test
    public void testAutowire() {
        Assert.assertNotNull(eventEntityDao);
        Assert.assertNotNull(eventFilterDao);
        Assert.assertNotNull(eventListenerDao);

        Assert.assertNotNull(eventEntityDao.getSessionFactory());
    }

    @Test
    @Transactional
    public void testFindBy() {

        try {
            init();

            EventEntity e = eventEntityDao.findObject(eventEntity1Id);
            Assert.assertNotNull("EventEntity not found", e.getId());

            // test finding eventEntity by correct type
            List<EventEntity> events = eventEntityDao.findBy(TEST_DUMMY_TYPE, null, null, null);
            int size = events.size();
            Assert.assertTrue("Type - Excpected to find one EventEntity (" + size + ")", size == 1);

            // test finding eventEntity by incorrect type
            events = eventEntityDao.findBy("INCORRECT_TYPE", null, null, null);
            size = events.size();
            Assert.assertTrue("Type - Excpected to find no EventEntities (" + size + ")", size == 0);

            // test finding eventEntity by correct subType
            events = eventEntityDao.findBy(null, TEST_DUMMY_SUBTYPE, null, null);
            size = events.size();
            Assert.assertTrue("SubType - Excpected to find one EventEntity (" + size + ")", size == 1);

            // test finding eventEntity by incorrect subType
            events = eventEntityDao.findBy(null, "INCORRECT_SUBTYPE", null, null);
            size = events.size();
            Assert.assertTrue("SubType - Excpected to find no EventEntities (" + size + ")", size == 0);

            // test finding eventEntity by correct startDateTime
            events = eventEntityDao.findBy(null, null, new Long(timestamp - 100000), null);
            size = events.size();
            Assert.assertTrue("StartDateTime - Excpected to find one EventEntity (" + size + ")", size == 1);

            // test finding eventEntity by incorrect startDateTime
            events = eventEntityDao.findBy(null, null, new Long(timestamp + 100000), null);
            size = events.size();
            Assert.assertTrue("StartDateTime - Excpected to find no EventEntities (" + size + ")", size == 0);

            // test finding eventEntity by correct endDateTime
            events = eventEntityDao.findBy(null, null, null, new Long(timestamp + 100000));
            size = events.size();
            Assert.assertTrue("EndDateTime - Excpected to find one EventEntity (" + size + ")", size == 1);

            // test finding eventEntity by incorrect endDateTime
            events = eventEntityDao.findBy(null, null, null, new Long(timestamp - 100000));
            size = events.size();
            Assert.assertTrue("EndDateTime - Excpected to find no EventEntities (" + size + ")", size == 0);

            // test finding eventEntity by correct startDate and correct
            // endDateTime
            events = eventEntityDao.findBy(null, null, new Long(timestamp - 100000), new Long(timestamp + 100000));
            size = events.size();
            Assert.assertTrue("StartDateTime and EndDatTime - Excpected to find one EventEntity (" + size + ")",
                    size == 1);

            // test finding eventEntity by incorrect startDate and endDateTime
            // (before)
            events = eventEntityDao.findBy(null, null, new Long(timestamp + 100000), new Long(timestamp + 200000));
            size = events.size();
            Assert.assertTrue("StartDateTime and EndDatTime (before)- Excpected to find no EventEntities (" + size
                    + ")", size == 0);

            // test finding eventEntity by incorrect startDate and endDateTime
            // (after)
            events = eventEntityDao.findBy(null, null, new Long(timestamp - 200000), new Long(timestamp - 100000));
            size = events.size();
            Assert.assertTrue("StartDateTime and EndDatTime (after) - Excpected to find no EventEntities (" + size
                    + ")", size == 0);

            // test finding eventEntity by all properties
            events = eventEntityDao.findBy(TEST_DUMMY_TYPE, TEST_DUMMY_SUBTYPE, new Long(timestamp - 100000), new Long(
                    timestamp + 100000));
            size = events.size();
            Assert.assertTrue("All properties - Excpected to find one EventEntity (" + size + ")", size == 1);

            cleanUp();

        } catch (DatabaseException e) {
            Assert.fail("Error reading/writing entities to the database: " + e.getMessage());
        }

    }

    @Test
    @Transactional
    public void testPurgeExpiredEvents() {

        try {
            init();
            String testPurgerId = UUID.randomUUID().toString();

            // test that event exists
            EventEntity e = eventEntityDao.findObject(eventEntity1Id);
            Assert.assertNotNull("EventEntity not found", e.getId());

            // test purging eventEntity (no includes)
            eventEntityDao.purgeExpiredEvents(testPurgerId);
            List<EventEntity> events = eventEntityDao.findAll();
            int size = events.size();
            Assert.assertTrue("Excpected to have purged no EventEntities (" + size + ")", size == 1);

            // add an eventEntityFilter that includes all
            EventFilterEntity includeAllFilter = new EventFilterEntity(testPurgerId, null, null, null,
                    EventFilterEntity.FilterMode.INCLUDE);
            eventFilterDao.save(includeAllFilter);

            // test purging eventEntity (include all)
            eventEntityDao.purgeExpiredEvents(testPurgerId);
            events = eventEntityDao.findAll();
            size = events.size();
            Assert.assertTrue("Excpected to have purged the EventEntity (" + size + ")", size == 0);

            // init again (to be sure event is added again)
            init();

            // add an eventEntityFilter that excludes all
            EventFilterEntity excludeAllFilter = new EventFilterEntity(testPurgerId, null, null, null,
                    EventFilterEntity.FilterMode.EXCLUDE);
            eventFilterDao.save(excludeAllFilter);

            // test purging eventEntity (exclude all)
            eventEntityDao.purgeExpiredEvents(testPurgerId);
            events = eventEntityDao.findAll();
            size = events.size();
            Assert.assertTrue("Excpected to have purged no EventEntities (" + size + ")", size == 1);

            // remove all filters!
            eventFilterDao.deleteAll(eventFilterDao.findAll());

            // add an eventEntityFilter that includes events of another type
            EventFilterEntity includeOtherFilter = new EventFilterEntity(testPurgerId, "other_type", null, null,
                    EventFilterEntity.FilterMode.INCLUDE);
            eventFilterDao.save(includeOtherFilter);

            // test purging eventEntity (include other)
            eventEntityDao.purgeExpiredEvents(testPurgerId);
            events = eventEntityDao.findAll();
            size = events.size();
            Assert.assertTrue("Excpected to have purged no EventEntity (" + size + ")", size == 1);

            // add an eventEntityFilter that includes events of the right type
            EventFilterEntity includeRightFilter = new EventFilterEntity(testPurgerId, TEST_DUMMY_TYPE, null, null,
                    EventFilterEntity.FilterMode.INCLUDE);
            eventFilterDao.save(includeRightFilter);

            // test purging eventEntity (include right)
            eventEntityDao.purgeExpiredEvents(testPurgerId);
            events = eventEntityDao.findAll();
            size = events.size();
            Assert.assertTrue("Excpected to have purged the EventEntity (" + size + ")", size == 0);

            // init again (to be sure event is added again)
            init();

            // add an eventEntityFilter that excludes events of another type
            EventFilterEntity excludeOtherFilter = new EventFilterEntity(testPurgerId, "other_type", null, null,
                    EventFilterEntity.FilterMode.EXCLUDE);
            eventFilterDao.save(excludeOtherFilter);

            // test purging eventEntity (exclude other)
            eventEntityDao.purgeExpiredEvents(testPurgerId);
            events = eventEntityDao.findAll();
            size = events.size();
            Assert.assertTrue("Excpected to have purged one EventEntity (" + size + ")", size == 0);

            // init again (to be sure event is added again)
            init();

            // add an eventEntityFilter that includes events of the right type
            EventFilterEntity excludeRightFilter = new EventFilterEntity(testPurgerId, TEST_DUMMY_TYPE, null, null,
                    EventFilterEntity.FilterMode.EXCLUDE);
            eventFilterDao.save(excludeRightFilter);

            // test purging eventEntity (exclude right)
            eventEntityDao.purgeExpiredEvents(testPurgerId);
            events = eventEntityDao.findAll();
            size = events.size();
            Assert.assertTrue("Excpected to have purged no EventEntities (" + size + ")", size == 1);

            EventListenerEntity listener = new EventListenerEntity();
            listener.setLastActivity(timestamp - 10000);
            eventListenerDao.save(listener);

            // remove all filters and add includeAllFitler!
            eventFilterDao.deleteAll(eventFilterDao.findAll());
            eventFilterDao.save(includeAllFilter);

            // test purging eventEntity (listener has earlier timestamp)
            eventEntityDao.purgeExpiredEvents(testPurgerId);
            events = eventEntityDao.findAll();
            size = events.size();
            Assert.assertTrue("Excpected to have purged no EventEntities (" + size + ")", size == 1);

            // add listener again
            eventListenerDao.deleteExpired(System.currentTimeMillis());
            listener.setLastActivity(timestamp + 10000);
            listener = eventListenerDao.save(listener);
            Assert.assertTrue("LastActivity must be larger then timestamp", timestamp < listener.getLastActivity());

            // test purging eventEntity (listener has later timestamp)
            eventEntityDao.purgeExpiredEvents(testPurgerId);
            events = eventEntityDao.findAll();
            size = events.size();
            Assert.assertTrue("Excpected to have purged one EventEntity (" + size + ")", size == 0);

            // SKIPPED checking filtering (include and exclude) on subType and
            // properties!!

            // be sure there are no eventFilters present anymore
            eventFilterDao.deleteAll(eventFilterDao.findAll());

            // delete the eventListeners
            eventListenerDao.deleteExpired(System.currentTimeMillis());

            // not needed
            // cleanUp();
        } catch (DatabaseException e) {
            Assert.fail("Error reading/writing entities to the database: " + e.getMessage());
        }

    }

    /**
     * Create eventEntity objects to test with persist them
     */
    protected void init() {
        try {
            EventEntity eventEntity1 = new EventEntity();

            eventEntity1.setType(TEST_DUMMY_TYPE);
            eventEntity1.setSubtype(TEST_DUMMY_SUBTYPE);

            timestamp = System.currentTimeMillis();
            eventEntity1.setTimestamp(timestamp);

            eventEntity1 = eventEntityDao.save(eventEntity1);
            eventEntity1Id = eventEntity1.getId();

            eventEntityDao.getSessionFactory().getCurrentSession().flush();

        } catch (DatabaseException e) {
            Assert.fail("Error creating context: " + e.getMessage());
        }
    }

    /**
     * Remove the created eventEntity objects
     */
    protected void cleanUp() {
        try {
            eventEntityDao.delete(eventEntityDao.findObject(eventEntity1Id));

        } catch (DatabaseException e) {
            Assert.fail("Error cleaing up context: " + e.getMessage());
        }

    }

}
