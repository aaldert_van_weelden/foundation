package nl.telecats.web.gwt.push.test.common.entities;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

import junit.framework.Assert;
import nl.telecats.foundation.model.exceptions.DatabaseException;
import nl.telecats.web.gwt.push.common.entities.EventFilterEntity;
import nl.telecats.web.gwt.push.common.entities.EventFilterEntity.FilterMode;
import nl.telecats.web.gwt.push.server.dao.EventFilterEntityDao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

/**
 * Testsuite for the EventFilterEntity
 * 
 * @author Gregor Ybema
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:foundation-gwt-push-model-test-context.xml" })
@TransactionConfiguration(transactionManager = "foundationGwtPush-transactionManager", defaultRollback = false)
public class EventFilterEntityTest {

    @Autowired
    private EventFilterEntityDao eventFilterEntityDao;

    private static Serializable eventFilterEntity1Id;

    private static String OWNERID = UUID.randomUUID().toString();

    private final String TEST_DUMMY_TYPE = "test_dummy_type";
    private final String TEST_DUMMY_SUBTYPE = "test_dummy_type";

    private final String TEST_PROPERTIES = "key1=val1";

    private final FilterMode FILTER_MODE_USED = FilterMode.EXCLUDE;
    private final FilterMode FILTER_MODE_NOTUSED = FilterMode.INCLUDE;

    @Test
    @Transactional
    public void testFindByOwner() {

        try {
            init();

            EventFilterEntity e = eventFilterEntityDao.findObject(eventFilterEntity1Id);
            Assert.assertNotNull("EventFilterEntity not found", e.getId());

            // test finding eventFilterEntity by owner
            List<EventFilterEntity> eventFilters = eventFilterEntityDao.findByOwner(OWNERID);
            int size = eventFilters.size();
            Assert.assertTrue("Owner - Excpected to find one EventFilterEntity (" + size + ")", size == 1);

            // test finding eventFilterEntity by incorrect type
            eventFilters = eventFilterEntityDao.findByOwner(UUID.randomUUID().toString());
            size = eventFilters.size();
            Assert.assertTrue("Owner - Excpected to find no EventFilterEntities (" + size + ")", size == 0);

            cleanUp();

        } catch (DatabaseException e) {
            Assert.fail("Error reading/writing entities to the database: " + e.getMessage());
        }

    }

    @Test
    @Transactional
    public void testFindByCriteria() {

        init();

        // test finding eventFilterEntity by owner
        List<EventFilterEntity> eventFilters = eventFilterEntityDao.findByCriteria(OWNERID, null, null, null, null);
        int size = eventFilters.size();
        Assert.assertTrue("Owner - Excpected to find one EventFilterEntity (" + size + ")", size == 1);

        // test finding eventFilterEntity by incorrect owner
        eventFilters = eventFilterEntityDao.findByCriteria(UUID.randomUUID().toString(), null, null, null, null);
        size = eventFilters.size();
        Assert.assertTrue("Owner - Excpected to find no EventFilterEntities (" + size + ")", size == 0);

        // test finding eventFilterEntity by type
        eventFilters = eventFilterEntityDao.findByCriteria(null, TEST_DUMMY_TYPE, null, null, null);
        size = eventFilters.size();
        Assert.assertTrue("Type - Excpected to find one EventFilterEntity (" + size + ")", size == 1);

        // test finding eventFilterEntity by incorrect type
        eventFilters = eventFilterEntityDao.findByCriteria(null, "test_non_existent_type", null, null, null);
        size = eventFilters.size();
        Assert.assertTrue("Type - Excpected to find no EventFilterEntities (" + size + ")", size == 0);

        // test finding eventFilterEntity by subtype
        eventFilters = eventFilterEntityDao.findByCriteria(null, null, TEST_DUMMY_SUBTYPE, null, null);
        size = eventFilters.size();
        Assert.assertTrue("Subtype - Excpected to find one EventFilterEntity (" + size + ")", size == 1);

        // test finding eventFilterEntity by incorrect subtype
        eventFilters = eventFilterEntityDao.findByCriteria(null, null, "test_non_existent_subtype", null, null);
        size = eventFilters.size();
        Assert.assertTrue("Subtype - Excpected to find no EventFilterEntities (" + size + ")", size == 0);

        // test finding eventFilterEntity by properties
        eventFilters = eventFilterEntityDao.findByCriteria(null, null, null, TEST_PROPERTIES, null);
        size = eventFilters.size();
        Assert.assertTrue("Properties - Excpected to find one EventFilterEntity (" + size + ")", size == 1);

        // test finding eventFilterEntity by incorrect properties
        eventFilters = eventFilterEntityDao.findByCriteria(null, null, null, "otherKey=otherValue", null);
        size = eventFilters.size();
        Assert.assertTrue("Properties - Excpected to find no EventFilterEntities (" + size + ")", size == 0);

        // test finding eventFilterEntity by filtermode
        eventFilters = eventFilterEntityDao.findByCriteria(null, null, null, null, FILTER_MODE_USED);
        size = eventFilters.size();
        Assert.assertTrue("FilterMode - Excpected to find one EventFilterEntity (" + size + ")", size == 1);

        // test finding eventFilterEntity by incorrect filtermode
        eventFilters = eventFilterEntityDao.findByCriteria(null, null, null, null, FILTER_MODE_NOTUSED);
        size = eventFilters.size();
        Assert.assertTrue("FilterMode - Excpected to find no EventFilterEntities (" + size + ")", size == 0);

        // test finding eventFilterEntity by all arguments
        eventFilters = eventFilterEntityDao.findByCriteria(OWNERID, TEST_DUMMY_TYPE, TEST_DUMMY_SUBTYPE,
                TEST_PROPERTIES, FILTER_MODE_USED);
        size = eventFilters.size();
        Assert.assertTrue("All arguments - Excpected to find one EventFilterEntity (" + size + ")", size == 1);

        cleanUp();

    }

    @Test
    @Transactional
    public void testDeleteAll() {

        init();

        // find all
        List<EventFilterEntity> eventFilters = eventFilterEntityDao.findAll();
        int size = eventFilters.size();
        Assert.assertTrue("Before - Excpected to find one EventFilterEntity (" + size + ")", size == 1);

        eventFilterEntityDao.deleteAll(eventFilters);

        // test after deleting
        eventFilters = eventFilterEntityDao.findAll();
        size = eventFilters.size();
        Assert.assertTrue("After - Excpected to find one EventFilterEntity (" + size + ")", size == 0);

        if (size != 0) {
            cleanUp();
        }
    }

    /**
     * Create eventFilterEntity objects to test with persist them
     */
    protected void init() {
        try {

            // be sure there are no eventFilters present
            List<EventFilterEntity> eventFilters = eventFilterEntityDao.findAll();
            eventFilterEntityDao.deleteAll(eventFilters);

            EventFilterEntity eventFilterEntity1 = new EventFilterEntity(OWNERID, TEST_DUMMY_TYPE, TEST_DUMMY_SUBTYPE,
                    TEST_PROPERTIES, FILTER_MODE_USED);

            eventFilterEntity1 = eventFilterEntityDao.save(eventFilterEntity1);
            eventFilterEntity1Id = eventFilterEntity1.getId();

            eventFilterEntityDao.getSessionFactory().getCurrentSession().flush();

        } catch (DatabaseException e) {
            Assert.fail("Error creating context: " + e.getMessage());
        }
    }

    /**
     * Remove the created eventFilterEntity objects
     */
    protected void cleanUp() {
        try {
            eventFilterEntityDao.delete(eventFilterEntityDao.findObject(eventFilterEntity1Id));

        } catch (DatabaseException e) {
            Assert.fail("Error cleaing up context: " + e.getMessage());
        }

    }

}
