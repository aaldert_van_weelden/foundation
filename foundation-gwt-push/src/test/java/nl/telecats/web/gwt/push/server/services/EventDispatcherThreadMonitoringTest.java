package nl.telecats.web.gwt.push.server.services;

import java.util.List;

import nl.telecats.foundation.statusservice.services.StatusDetailServable;
import nl.telecats.foundation.statusservice.services.model.Status;
import nl.telecats.foundation.statusservice.services.model.StatusDetail;
import nl.telecats.web.gwt.push.common.entities.EventEntity;
import nl.telecats.web.gwt.push.server.dao.EventEntityDao;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.After;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;

@Ignore
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:foundation-gwt-push-test-context.xml" })
@TransactionConfiguration(transactionManager = "foundationGwtPush-transactionManager", defaultRollback = false)
public class EventDispatcherThreadMonitoringTest {

    private static final Log log = LogFactory.getLog(EventDispatcherThreadMonitoringTest.class);

    @Autowired
    private EventDispatcherService service;

    @Autowired
    private EventEntityDao dao;

    @After
    public void after() {
        for (EventEntity e : dao.findAll()) {
            dao.delete(e);
        }
    }

    @Test
    public synchronized void testEventDispatcherService() throws InterruptedException {

        // String listenerId = service.registerServiceEventListener();

        StatusDetailServable status = (StatusDetailServable) service;

        // status with no listeners waiting
        StatusDetail d = getDetail(status.getStatusDetails());
        Assert.assertEquals(Status.UNKNOWN, d.getStatus());

        registerClients(1);
        wait(250);

        // status with 1 listener waiting
        d = getDetail(status.getStatusDetails());
        Assert.assertEquals(Status.OK, d.getStatus());

        registerClients(99);
        wait(250);

        // status with 100 listeners waiting
        d = getDetail(status.getStatusDetails());
        Assert.assertEquals(Status.WARNING, d.getStatus());

        registerClients(50);
        wait(250);

        // status with 150 listeners waiting
        d = getDetail(status.getStatusDetails());
        Assert.assertEquals(Status.CRITICAL, d.getStatus());

        service.fireEvent(new EventEntity("BLA-TYPE", "SUBTYPE", null, "value"));

        wait(250);
        d = getDetail(status.getStatusDetails());
        Assert.assertEquals(Status.OK, d.getStatus());

    }

    private void registerClients(int amount) {
        for (int i = 0; i < amount; i++) {
            TestClient c1 = new TestClient(String.valueOf(i));
            c1.start();
        }
    }

    private StatusDetail getDetail(List<StatusDetail> details) {
        Assert.assertEquals(1, details.size());
        StatusDetail d = details.get(0);
        log.info("\nStatus:\n" + d.getStatus() + "\nInfo:\n" + d.getShortDescription() + "\nData: " + d.getData()
                + "\n");
        return details.get(0);
    }

    private class TestClient extends Thread {

        private String n = "";

        public TestClient(String n) {
            this.n = n;
        }

        @Override
        public void run() {
            String listenerId = service.registerServiceEventListener();
            System.out.println(n + ": Registered");
            service.waitForEvent(listenerId);
            System.out.println(n + ": Wait finished !");
        }

    }

}
