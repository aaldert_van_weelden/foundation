package nl.telecats.web.gwt.push.server.services;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;

@Ignore
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:foundation-gwt-push-test-context.xml" })
@TransactionConfiguration(transactionManager = "foundationGwtPush-transactionManager", defaultRollback = false)
public class StatusListenerTest {

    private static final Log log = LogFactory.getLog(StatusListenerTest.class);

    @Autowired
    private EventDispatcherService service;

    @Autowired
    private StatusListener status;

    @Test
    public synchronized void testEventDispatcherService() throws InterruptedException {

        // TODO: test the inner workings of the status monitoring

    }

}
