<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<groupId>nl.telecats.foundation</groupId>
	<artifactId>foundation-commons</artifactId>
	<version>2.7.8-SNAPSHOT</version>
	<packaging>jar</packaging>
	<url>http://maven.apache.org</url>

	<name>foundation-commons</name>
	<description>Foundation commons api</description>

	<parent>
		<groupId>nl.telecats.foundation</groupId>
		<artifactId>foundation-core-apis</artifactId>
		<version>2.7.8-SNAPSHOT</version>
	</parent>

	<properties>
		<!-- 3rd party -->
		<commons-lang.version>2.5</commons-lang.version>
        	<gwt.version>2.3.0</gwt.version>
        	<joda-time.version>2.1</joda-time.version>
		<junit.version>4.8.1</junit.version>
        	<log4j.version>1.2.15</log4j.version>
		<maven-antrun-plugin.version>1.3</maven-antrun-plugin.version>
		<maven-javadoc-plugin.version>2.8</maven-javadoc-plugin.version>
		<maven-site-plugin.version>3.0</maven-site-plugin.version>
        	<slf4j.version>1.6.3</slf4j.version>
		<spring.version>3.0.2.RELEASE</spring.version>

		<!-- telecats -->
		<releaseInfoPlugin.version>1.0</releaseInfoPlugin.version>
	</properties>

	<build>
		<plugins>
			<!-- <plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-site-plugin</artifactId>
				<version>${maven-site-plugin.version}</version>
				<executions>
					<execution>
						<id>site</id>
						<phase>deploy</phase>
						<goals>
							<goal>site</goal>
							<goal>deploy</goal>
						</goals>
					</execution>
				</executions>
			</plugin>-->
			<plugin>
				<groupId>nl.telecats.maven.plugins</groupId>
				<artifactId>releaseInfoPlugin</artifactId>
				<version>${releaseInfoPlugin.version}</version>
				<executions>
					<execution>
						<phase>prepare-package</phase>
						<goals>
							<goal>generaterelease</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<artifactId>maven-antrun-plugin</artifactId>
				<version>${maven-antrun-plugin.version}</version>
				<executions>
					<execution>
						<id>fix_classpath</id>
						<phase>compile</phase>
						<goals>
							<goal>run</goal>
						</goals>
						<configuration>
							<tasks>
								<echo message="fix_classpath" />
								<replace dir=".">
									<include name=".classpath" />
									<replacetoken><![CDATA[	<classpathentry excluding="**" kind="src" output="target/classes" path="src/main/resources" />]]></replacetoken>
									<replacevalue><![CDATA[	<classpathentry kind="src" output="target/classes" path="src/main/resources" />]]></replacevalue>
								</replace>
								<replace dir=".">
									<include name=".classpath" />
									<replacetoken><![CDATA[	<classpathentry excluding="**" kind="src" output="target/test-classes" path="src/test/resources" />]]></replacetoken>
									<replacevalue><![CDATA[	<classpathentry kind="src" output="target/test-classes" path="src/test/resources" />]]></replacevalue>
								</replace>
							</tasks>
						</configuration>
					</execution>
				</executions>
			</plugin>
		</plugins>
	</build>

	<dependencies>
		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>slf4j-api</artifactId>
			<version>${slf4j.version}</version>
		</dependency>
		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>slf4j-log4j12</artifactId>
			<version>${slf4j.version}</version>
			<scope>test</scope>
		</dependency>
		<dependency>
            <groupId>log4j</groupId>
            <artifactId>log4j</artifactId>
            <version>${log4j.version}</version>
            <scope>test</scope>
        </dependency>
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-context</artifactId>
			<version>${spring.version}</version>
		</dependency>
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-test</artifactId>
			<version>${spring.version}</version>
			<type>jar</type>
			<optional>false</optional>
		</dependency>
		<dependency>
			<groupId>commons-lang</groupId>
			<artifactId>commons-lang</artifactId>
			<version>${commons-lang.version}</version>
			<type>jar</type>
			<scope>compile</scope>
		</dependency>
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<version>${junit.version}</version>
			<scope>test</scope>
		</dependency>
		<dependency>
            <groupId>joda-time</groupId>
            <artifactId>joda-time</artifactId>
            <version>${joda-time.version}</version>
        </dependency>
	</dependencies>

	<reporting>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-javadoc-plugin</artifactId>
				<version>${maven-javadoc-plugin.version}</version>
			</plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-project-info-reports-plugin</artifactId>
                <configuration>
                    <dependencyLocationsEnabled>false</dependencyLocationsEnabled>
                </configuration>
            </plugin>
		</plugins>
	</reporting>

	<profiles>
		<profile>
			<activation>
				<property>
					<name>hudson</name>
				</property>
			</activation>
			<build>
				<plugins>
					<plugin>
						<groupId>nl.telecats.maven.plugins</groupId>
						<artifactId>releaseInfoPlugin</artifactId>
						<version>${releaseInfoPlugin.version}</version>
						<executions>
							<execution>
								<phase>compile</phase>
								<goals>
									<goal>generaterelease</goal>
								</goals>
							</execution>
						</executions>
					</plugin>
				</plugins>
			</build>
		</profile>
	</profiles>
</project>
