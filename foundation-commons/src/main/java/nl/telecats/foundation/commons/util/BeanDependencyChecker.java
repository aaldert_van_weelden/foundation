package nl.telecats.foundation.commons.util;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

/**
 * This class can be used to force a certain bean (ID) is available.
 * <p/>
 * For example when your model needs a 'myModel-transactionManager' you can use this class to check there is a bean with
 * id 'myModel-transactionManager' configured.
 * 
 * Usage:
 * <p/>
 * 
 * <pre>
 * &lt;bean id="beanDependencyChecker" class="nl.telecats.foundation.commons.util.BeanDependencyChecker"
 * init-method="checkDependencies"&gt;
 *       &lt;constructor-arg&gt;
 *            &lt;list&gt;
 *                &lt;value&gt;springBeanLogger&lt;/value&gt;
 *                &lt;value&gt;beanDependencyChecker&lt;/value&gt;
 *            &lt;/list&gt;
 *        &lt;/constructor-arg&gt; 
 * &lt;/bean&gt;
 * </pre>
 * 
 * @author Raymond
 * 
 */
public class BeanDependencyChecker {
    private final static Log LOG = LogFactory.getLog(BeanDependencyChecker.class);

    @Autowired
    ApplicationContext appContext;

    private List<String> requiredBeanIds = null;

    public BeanDependencyChecker(List<String> requiredBeanIds) {
        this.requiredBeanIds = requiredBeanIds;
    }

    public void checkDependencies() {
        LOG.info("================================");
        LOG.info("[start] BeanDependencyChecker]");
        LOG.info("================================");

        if (requiredBeanIds == null) {
            LOG.info("- No beans required.");
        } else {
            LOG.info("Required beans:");
            String missingBeans = "";
            for (String beanId : requiredBeanIds) {
                try {
                    appContext.getBean(beanId);
                    LOG.info("- " + beanId + "... OK");
                } catch (NoSuchBeanDefinitionException nsbde) {
                    LOG.error("- " + beanId + "... FAIL, bean not available.");
                    if (missingBeans.length() > 0) {
                        missingBeans += ", ";
                    }
                    missingBeans += beanId;
                }
            }

            if (missingBeans.length() > 0) {
                LOG.error("Some beans you are depending on are missing, please check your configuration and configure following beans : "
                        + missingBeans);
                throw new NoSuchBeanDefinitionException(missingBeans);
            }
        }

        LOG.info("================================");
        LOG.info("[end] BeanDependencyChecker]");
        LOG.info("================================");
    }

    protected String[] findBeanDefinitions() {
        return appContext.getBeanDefinitionNames();
    }
}