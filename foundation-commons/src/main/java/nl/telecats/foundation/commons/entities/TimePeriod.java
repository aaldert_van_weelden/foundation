package nl.telecats.foundation.commons.entities;

import java.io.Serializable;
import java.util.Date;

/**
 * Represents a period of time, by supplying a start and an end.<br />
 * If start is <code>null</code> it means 'from the beginning of time'.<br />
 * If end is <code>null</code> it means no limit.<br />
 * 
 * @author Danny Dam Wichers
 */
public class TimePeriod implements Serializable {

    /**
     * Serialization version
     */
    private static final long serialVersionUID = -941776936028067807L;

    /**
     * Start time.
     */
    private Date start;

    /**
     * End time.
     */
    private Date end;

    /**
     * Constructor
     */
    public TimePeriod() {
    }

    /**
     * Constructor, setting all instance variables.
     * 
     * @param start
     *            start time.
     * @param end
     *            end time.
     */
    public TimePeriod(Date start, Date end) {
        setStart(start);
        setEnd(end);
    }

    /**
     * @param start
     *            the start to set
     */
    public void setStart(Date start) {
        this.start = start;
    }

    /**
     * Retrieve start; can be null, which means 'from the absolute start'.
     * 
     * @return the start
     */
    public Date getStart() {
        return start;
    }

    /**
     * @param end
     *            the end to set
     */
    public void setEnd(Date end) {
        this.end = end;
    }

    /**
     * Retrieve end; can be null, which means 'up until now'.
     * 
     * @return the end
     */
    public Date getEnd() {
        return end;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {

        return (getStart() == null ? "The absolute beginning" : getStart().toString()) + " - "
                + (getEnd() == null ? "no limit" : getEnd().toString());
    }
}