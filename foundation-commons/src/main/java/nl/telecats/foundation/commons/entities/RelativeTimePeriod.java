package nl.telecats.foundation.commons.entities;


/**
 * A collection of relative time periods, which can be turned into actual {@link TimePeriod} entities, using
 * {@link TimePeriodFactory}.
 * 
 * @author Danny Dam Wichers
 */
public enum RelativeTimePeriod {

    /**
     * The last 60 seconds.
     */
    PAST_MINUTE(1),

    /**
     * The last 300 seconds.
     */
    PAST_FIVE_MINUTES(2),

    /**
     * The last 900 seconds.
     */
    PAST_FIFTEEN_MINUTES(3),

    /**
     * The current hour, starting at <current hour>:00:00, length depending on current minute and second.
     */
    THIS_HOUR(4),

    /**
     * The last 60 minutes.
     */
    PAST_HOUR(5),

    /**
     * The last hour, ending at <current hour>:00:00, 60 minutes long.
     */
    LAST_HOUR(6),

    /**
     * All seconds since 00:00:00 today.
     */
    TODAY(7),

    /**
     * 00:00:00 to 23:59:59 yesterday.
     */
    YESTERDAY(8),

    /**
     * The current week, starting at Sunday 00:00:00.
     */
    THIS_WEEK(9),

    /**
     * The past week, starting at the Sunday before last Sunday at 00:00:00 and ending last Saturday at 23:59:59.
     */
    LAST_WEEK(10),

    /**
     * The current month, starting at <current month>-01 00:00:00
     */
    THIS_MONTH(11),

    /**
     * The last month, ending at <last month>-(28/29/30/31) 23:59:59
     */
    LAST_MONTH(12),

    /**
     * The current quarter, starting at either 01-01, 04-01, 07-01 or 10-01 at 00:00:00; date is chosen as closest date
     * that has passed.
     */
    THIS_QUARTER(13),

    /**
     * The current quarter, starting at either 01-01, 04-01, 07-01 or 10-01 at 00:00:00; date is chosen as second
     * closest date that has passed. The end date is the closest date that has passed at 23:59:59.
     */
    LAST_QUARTER(14),

    /**
     * The current year, starting at 01-01 00:00:00
     */
    THIS_YEAR(15),

    /**
     * Last year, starting at <last year>-01-01 00:00:00 and ending at <last year>-12-31 23:59:59
     */
    LAST_YEAR(16);

    /**
     * Internal value, mostly used for consistent storage.
     */
    private Integer value;

    /**
     * Constructor
     * 
     * @param value
     *            Create a new RelativeTimePeriod based on this value
     */
    private RelativeTimePeriod(Integer value) {
        this.setValue(value);
    }

    /**
     * Set the value
     * 
     * @param value
     *            the value to set
     */
    public void setValue(Integer value) {
        this.value = value;
    }

    /**
     * Retrieve the value
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }

    /**
     * The original toString implementation
     * 
     * @return toString; the String representation of the enum value.
     */
    public String stringValue() {
        return super.toString();
    }

    /**
     * Returns the string part corresponding to the int value of the RelativeTimePeriod enum
     * 
     * @param type
     * @return The enum value as string
     */
    public static String getTypeAsString(int type) {

        for (RelativeTimePeriod t : RelativeTimePeriod.values()) {
            if (type == t.getValue()) {
                return t.stringValue();
            }
        }
        return null;
    }
}