package nl.telecats.foundation.commons.util;

import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang.time.FastDateFormat;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * ISO 8601 date / time utility
 * 
 * <h2>ChangeLog:</h2>
 * 
 * 2.0:
 * <ul>
 * <li>initial release</li>
 * </ul>
 * 2.1:
 * <ul>
 * <li>you need to get an instance of this class</li>
 * <li>uses {@link FastDateFormat} for better performance / thread safety</li>
 * </ul>
 */
public class DateTimeUtil {
    /**
     * Zomer- / wintertijd support. <br/>
     * yyyy-MM-dd'T'HH:mm:ssZ
     */
    public static final String DATEFORMAT = "yyyy-MM-dd'T'HH:mm:ssZ";

    /**
     * yyyy-MM-dd'T'HH:mm:ss
     */
    public static final String DATEFORMAT_NOTZ = "yyyy-MM-dd'T'HH:mm:ss";

    private static final Log logger = LogFactory.getLog(DateTimeUtil.class);

    private static final FastDateFormat iso8601formatter = FastDateFormat.getInstance(DATEFORMAT);

    // private static final Map<String, FastDateFormat> formatterCache;

    private static final Map<DateTimeFields, Format> formatters = getAllFormatters();

    public static enum DateTimeFields {
        YEAR, MONTH, DAY, WEEK_OF_YEAR, DAY_OF_WEEK_IN_MONTH, DAY_IN_WEEK, HOUR, MINUTE, SECOND, QUARTER_OF_YEAR, QUARTER_OF_HOUR
    }

    private Locale mLocale;

    /**
     * Private contructor
     */
    private DateTimeUtil() {
    }

    private DateTimeUtil(Locale locale) {
        mLocale = locale;
    }

    /**
     * Init the cached map of formatters used
     * 
     * @return
     */
    private static Map<DateTimeFields, Format> getAllFormatters() {
        // final Format df_full_date =
        // FastDateFormat.getInstance("d MMMM yyyy");
        final Format df_year = FastDateFormat.getInstance("yyyy");
        final Format df_month = FastDateFormat.getInstance("MM");
        final Format df_day = FastDateFormat.getInstance("dd");
        final Format df_weekOfYear = FastDateFormat.getInstance("w");
        final Format df_dayOfWeekInMonth = FastDateFormat.getInstance("F");

        // final Format df_full_time = FastDateFormat.getInstance("HH:mm:ss");
        final Format df_hour = FastDateFormat.getInstance("HH");
        final Format df_minute = FastDateFormat.getInstance("mm");
        final Format df_second = FastDateFormat.getInstance("ss");

        Map<DateTimeFields, Format> map = new HashMap<DateTimeFields, Format>();
        map.put(DateTimeFields.YEAR, df_year);
        map.put(DateTimeFields.MONTH, df_month);
        map.put(DateTimeFields.DAY, df_day);
        map.put(DateTimeFields.WEEK_OF_YEAR, df_weekOfYear);
        map.put(DateTimeFields.DAY_OF_WEEK_IN_MONTH, df_dayOfWeekInMonth);
        map.put(DateTimeFields.HOUR, df_hour);
        map.put(DateTimeFields.MINUTE, df_minute);
        map.put(DateTimeFields.SECOND, df_second);
        return map;
    }

    /**
     * Get an instance of the DateTimeUtil with the system wide default Locale (see {@link Locale#getDefault()}.
     * 
     * @return a DateTimeUtil object
     * @since 2.1
     */
    public static DateTimeUtil getInstance() {
        return getInstance(Locale.getDefault());
    }

    /**
     * Get an instance of the DateTimeUtil, given a {@link Locale}.
     * 
     * @param locale
     *            The Locale to use when calculating week of year
     * @return a DateTimeUtil object
     * @since 2.1
     */
    public static DateTimeUtil getInstance(Locale locale) {
        return new DateTimeUtil(locale);

    }

    /**
     * @return Current date/time in iso 8601 format
     * @since 2.0
     */
    public String getNowIso8601() {
        String nowIso8601 = iso8601formatter.format(new Date());
        logger.debug("nowIso8601:" + nowIso8601);
        return nowIso8601;
    }

    /**
     * Parse iso8601 date/time string
     * 
     * @param iso8601
     * @return
     * @throws ParseException
     *             if string can't be parsed
     * @since 2.0
     */
    public Date parseIso8601(String iso8601) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat(DATEFORMAT);
        try {
            return formatter.parse(iso8601);
        } catch (ParseException e) {
            // now try the old method
            SimpleDateFormat formatter_notz = new SimpleDateFormat(DATEFORMAT_NOTZ);
            try {
                Date d = formatter_notz.parse(iso8601);
                logger.warn("Parsing date " + iso8601
                        + " needed the NOTZ format. This is deprecated, always use a timezone in your strings.");
                return d;
            } catch (ParseException e1) {
                throw e1;
            }
        }
    }

    /**
     * 
     * @param date
     * @return Iso8601 string representation of date object
     * @since 2.0
     */
    public String formatIso8601(Date date) {

        return iso8601formatter.format(date);
    }

    /**
     * Convert a non TZ aware string to a string with timezone information. Uses the currect timezone we are in right
     * now.<br>
     * <br>
     * E.g.: 2010-10-31T01:59:59 -> 2010-10-31T01:59:59, +0200 (summer time)
     * 
     * @param iso8601NoTz
     *            the non TZ aware time string
     * @return the same string with the timezone information
     * @throws ParseException
     *             if the iso8604NoTz does not parse
     * @since 2.0
     */
    public String convertToTimezoneStamp(String iso8601NoTz) throws ParseException {
        SimpleDateFormat formatter_notz = new SimpleDateFormat(DATEFORMAT_NOTZ);
        return iso8601formatter.format(formatter_notz.parse(iso8601NoTz));
    }

    /**
     * Formats an iso8601 formatted datetime string to another format accepted by {@link SimpleDateFormat#format(Date)}
     * 
     * @param iso8601
     *            The iso8601 string to format
     * @param format
     *            The format to format into
     * @return the formatted string
     * @throws ParseException
     *             if the iso8601 string cannot be parsed
     * @since 2.0
     */
    public String convertTo(String iso8601, String format) throws ParseException {
        FastDateFormat customFormatter = FastDateFormat.getInstance(format);
        return customFormatter.format(parseIso8601(iso8601));
    }

    /**
     * Formats a date string in the given format to an iso8601 string
     * 
     * @param date
     *            The date to format
     * @param format
     *            The format the date is in
     * @return An iso8601 formatted date representing the given date
     * @throws ParseException
     *             if the given date cannot be parsed with the given format
     * @since 2.1
     */
    public String convertFrom(String date, String format) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat(format);
        return formatIso8601(formatter.parse(date));
    }

    /**
     * Formats an iso8601 formated datetime string to {@link #DATEFORMAT_NOTZ} format. Effectively stripping the +0100
     * from the supplied string
     * 
     * @param iso8601
     *            The iso8601 string to format
     * @return The formatted string
     * @throws ParseException
     * @since 2.0
     */
    public String convertToNoTz(String iso8601) throws ParseException {
        return convertTo(iso8601, DATEFORMAT_NOTZ);
    }

    /**
     * @param t1
     *            iso dateTime
     * @param t2
     *            iso dateTime
     * @return seconds between t1 and t2, if t1 after t2 OR if incorrect iso dateTime is specified, result will be -1
     * @since 2.0
     */
    public int secondsBetween(String isoStartDateTime, String isoEndDateTime) {
        Date t1;
        Date t2;
        try {
            t1 = parseIso8601(isoStartDateTime);
        } catch (ParseException e) {
            logger.error("Parse exception in secondsBetween function for date " + isoStartDateTime);
            return -1;
        }
        try {
            t2 = parseIso8601(isoEndDateTime);
        } catch (ParseException e) {
            logger.error("Parse exception in secondsBetween function for date " + isoEndDateTime);
            return -1;
        }

        long ts1 = t1.getTime();
        long ts2 = t2.getTime();

        if (ts1 > ts2) {
            logger.warn("ts1 > ts2 when calculating secondsBetween");
            return -1;
        }
        return Math.round((ts2 - ts1) / 1000);
    }

    /**
     * Get a field from the given dateTime as iso8601 string
     * 
     * @param iso8601
     *            the dateTime to extract the field from
     * @param field
     *            a {@link DateTimeFields} field
     * @return the value for the field. Note that all values which CAN be 2 or more in length will be padded with 0.
     *         E.g. "2" will become "02" if it represents a day.
     * @throws ParseException
     */
    public String getField(String iso8601, DateTimeFields field) throws ParseException {
        return getField(parseIso8601(iso8601), field);
    }

    /**
     * Get a field from the given dateTime
     * 
     * @param dateTime
     *            the dateTime to extract the field from
     * @param field
     *            a {@link DateTimeFields} field
     * @return the value for the field. Note that all values which CAN be 2 or more in length will be padded with 0.
     *         E.g. "2" will become "02" if it represents a day
     * @since 2.1
     */
    public String getField(Date dateTime, DateTimeFields field) {
        if (dateTime == null) {
            return null;
        }
        if (field == DateTimeFields.QUARTER_OF_YEAR) {
            // QUARTER_OF_YEAR
            Integer month = Integer.parseInt(getField(dateTime, DateTimeFields.MONTH));
            if ((month >= 1) && (month <= 3)) {
                return "1";
            } else if ((month >= 4) && (month <= 6)) {
                return "2";
            } else if ((month >= 7) && (month <= 9)) {
                return "3";
            } else if ((month >= 10) && (month <= 12)) {
                return "4";
            } else {
                throw new IllegalArgumentException("Could not determine quarterOfYear for provided date '"
                        + dateTime.toString() + "'");
            }
        } else if (field == DateTimeFields.QUARTER_OF_HOUR) {
            // QUARTER_OF_HOUR
            Integer minute = Integer.parseInt(getField(dateTime, DateTimeFields.MINUTE));
            if ((minute >= 0) && (minute <= 14)) {
                return "1";
            } else if ((minute >= 15) && (minute <= 29)) {
                return "2";
            } else if ((minute >= 30) && (minute <= 44)) {
                return "3";
            } else if ((minute >= 45) && (minute <= 59)) {
                return "4";
            } else
                throw new IllegalArgumentException("Could not determine quarter of hour for provided date '"
                        + dateTime.toString() + "'");
        } else if (field == DateTimeFields.DAY_IN_WEEK) {
            // DAY_IN_WEEK
            final Format df_dayInWeek = FastDateFormat.getInstance("EEEE", mLocale);
            Calendar c = new GregorianCalendar(mLocale);
            c.setTime(dateTime);
            return df_dayInWeek.format(c);
        } else {
            Format f = formatters.get(field);
            if (f == null) {
                throw new IllegalArgumentException("Could not get a formatter for field " + field);
            }
            Calendar c = new GregorianCalendar(mLocale);
            c.setTime(dateTime);
            return f.format(c);
        }
    }

    /**
     * Function for your convenience (tm) to get a certain field from the give Date
     * 
     * @param dateTime
     * @return
     * @since 2.1
     */
    public String getYear(Date dateTime) {
        return getField(dateTime, DateTimeFields.YEAR);
    }

    /**
     * Function for your convenience (tm) to get a certain field from the give Date
     * 
     * @param dateTime
     * @return
     * @since 2.1
     */

    public String getDay(Date dateTime) {
        return getField(dateTime, DateTimeFields.DAY);
    }

    /**
     * Function for your convenience (tm) to get a certain field from the give Date
     * 
     * @param dateTime
     * @return
     * @since 2.1
     */
    public String getHour(Date dateTime) {
        return getField(dateTime, DateTimeFields.HOUR);
    }

    /**
     * Function for your convenience (tm) to get a certain field from the give Date
     * 
     * @param dateTime
     * @return
     * @since 2.1
     */
    public String getMinute(Date dateTime) {
        return getField(dateTime, DateTimeFields.MINUTE);
    }

    /**
     * Function for your convenience (tm) to get a certain field from the give Date
     * 
     * @param dateTime
     * @return
     * @since 2.1
     */
    public String getMonth(Date dateTime) {
        return getField(dateTime, DateTimeFields.MONTH);
    }

    /**
     * Function for your convenience (tm) to get a certain field from the give Date
     * 
     * @param dateTime
     * @return
     * @since 2.1
     */
    public String getQuarterOfHour(Date dateTime) {
        return getField(dateTime, DateTimeFields.QUARTER_OF_HOUR);
    }

    /**
     * Function for your convenience (tm) to get a certain field from the give Date
     * 
     * @param dateTime
     * @return
     * @since 2.1
     */
    public String getSecond(Date dateTime) {
        return getField(dateTime, DateTimeFields.SECOND);
    }

    /**
     * Function for your convenience (tm) to get a certain field from the give Date
     * 
     * @param dateTime
     * @return
     * @since 2.1
     */
    public String getQuarterOfYear(Date dateTime) {
        return getField(dateTime, DateTimeFields.QUARTER_OF_YEAR);
    }

    /**
     * Function for your convenience (tm) to get a certain field from the give Date
     * 
     * @param dateTime
     * @return
     * @since 2.1
     */
    public String getWeekOfYear(Date dateTime) {
        return getField(dateTime, DateTimeFields.WEEK_OF_YEAR);
    }

    /**
     * Function for your convenience (tm) to get a certain field from the given Date, this will return 2 if it is the
     * second saturday in the month, for example
     * 
     * @param dateTime
     * @return
     * @since 2.1
     */
    public String getDayOfWeekInMonth(Date dateTime) {
        return getField(dateTime, DateTimeFields.DAY_OF_WEEK_IN_MONTH);
    }

    /**
     * Function for your convenience (tm) to get a certain field from the given Date, this will return the day in the
     * week (for example 'Tuesday'). Depends on your locales!
     * 
     * @param dateTime
     * @return
     */
    public String getDayInWeek(Date dateTime) {
        return getField(dateTime, DateTimeFields.DAY_IN_WEEK);
    }

    /**
     * Example:isDateBefore(20101220T...,20101219T...) -> false
     * <p/>
     * Example:isDateBefore(20101219T...,20101220T...) -> true
     * <p/>
     * NOTE: This method converts all to GMT0 before comparing.
     * 
     * @param isoDate1
     * @param isoDate2
     * @return true if date1 before date2
     * @throws ParseException
     */
    public boolean isDateBefore(String isoDate1, String isoDate2) throws ParseException {
        String isoDate1GMT0 = convertToNoTz(isoDate1);
        String isoDate2GMT0 = convertToNoTz(isoDate2);
        return isoDate1GMT0.compareToIgnoreCase(isoDate2GMT0) < 0;
    }

    /**
     * Subtract number of seconds without changing timezone info
     * 
     * @param iso8601
     * @param secondsToSubtract
     * @return
     * @throws ParseException
     */
    public String subtractSeconds(String iso8601, int secondsToSubtract) throws ParseException {
        // ignore timezone's (don't change them), subtraction is relative
        SimpleDateFormat formatterNoTZ = new SimpleDateFormat(DATEFORMAT_NOTZ);

        Date date = formatterNoTZ.parse(iso8601);
        date.setTime(date.getTime() - (1000 * secondsToSubtract));
        String result = formatterNoTZ.format(date);

        // restore timezone
        if (iso8601.contains("+")) {
            result += iso8601.substring(iso8601.indexOf("+"));
        }

        return result;
    }

    /**
     * Add number of seconds without changing timezone info
     * 
     * @param iso8601
     * @param secondsToAdd
     * @return
     * @throws ParseException
     */
    public String addSeconds(String iso8601, int secondsToAdd) throws ParseException {
        // ignore timezone's (don't change them), subtraction is relative
        SimpleDateFormat formatterNoTZ = new SimpleDateFormat(DATEFORMAT_NOTZ);

        Date date = formatterNoTZ.parse(iso8601);
        date.setTime(date.getTime() + (1000 * secondsToAdd));
        String result = formatterNoTZ.format(date);

        // restore timezone
        if (iso8601.contains("+")) {
            result += iso8601.substring(iso8601.indexOf("+"));
        }

        return result;
    }
}