package nl.telecats.foundation.commons.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

/**
 * This class can be used to log your spring beans when your spring context is loaded.
 * 
 * Usage:
 * <p/>
 * &lt;bean id="springBeanLogger" class="nl.telecats.foundation.commons.util.SpringBeanLogger"
 * init-method="logBeans"/&gt; </pre>
 * 
 * @author Raymond
 * 
 */
public class SpringBeanLogger {
    private final static Logger LOG = LoggerFactory.getLogger(SpringBeanLogger.class);

    @Autowired
    ApplicationContext appContext;

    public void logBeans() {
        LOG.debug("================================");
        LOG.debug("[start] SpringBeanLogger]");
        
        String[] beanDefinitionNames = findBeanDefinitions();
        if (beanDefinitionNames == null) {
            LOG.warn("- No Spring BeanDefinitions found. Your spring context might be incorrect.");
        } else {
            LOG.debug("Found beans:");
            for (String beanDef : beanDefinitionNames) {
                LOG.debug("- Bean: " + beanDef);
            }
        }

        LOG.debug("[end] SpringBeanLogger]");
        LOG.debug("================================");
    }

    protected String[] findBeanDefinitions() {
        return appContext.getBeanDefinitionNames();
    }
}