package nl.telecats.foundation.commons.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

/**
 * This class contains helper methods for timezone related date operations.
 */
public class TimeZoneHelper {

    public final static String ISO8601TZ_FORMAT = "yyyy-MM-dd'T'HH:mm:ssZ";
    private final static String GMT0 = "Etc/GMT-0";

    private static Map<Integer, String> timeZones = new HashMap<Integer, String>();

    private final static DateTimeUtil dtu = DateTimeUtil.getInstance();

    /**
     * 
     * @param iso8601tz
     * @return offset of timezone (gmt)
     * @throws ParseException
     *             when iso8601tz param can't be parsed according to format specified in ISO8601TZ_FORMAT
     */
    public static Integer getTimeZoneAsInteger(String iso8601tz) throws ParseException {
        Integer offset = null;
        if (iso8601tz != null) {

            int pos = iso8601tz.length() - 5;
            String tz = iso8601tz.substring(iso8601tz.length() - 5);

            if (!tz.matches("(\\+|-)\\d{4}")) {
                throw new ParseException("Timezone: " + tz + " has invalid format, required is (+|-)dddd", pos);
            }

            int sign = 1;
            String signSymbol = iso8601tz.substring(pos, pos + 1);
            String tzhh = iso8601tz.substring(pos + 1, pos + 3);
            String tzmm = iso8601tz.substring(pos + 3, pos + 5);

            if (signSymbol.indexOf('-') >= 0)
                sign = -1;

            int hh = Integer.parseInt(tzhh);
            hh = hh * 3600000;
            int mm = Integer.parseInt(tzmm);
            mm = mm * 60000;

            offset = (hh + mm) * sign;
        }

        return offset;
    }

    /**
     * 
     * @param iso8601tz
     * @return iso8601 date in gmt0 formatted as specified in ISO8601TZ_FORMAT
     * @throws ParseException
     *             when iso8601tz param can't be parsed using ISO8601TZ_FORMAT
     */
    public static String timeToGmt0(String iso8601tz) throws ParseException {
        if (iso8601tz != null && !isGmt0(iso8601tz)) {
            Calendar local = new GregorianCalendar();
            local.setTime(dtu.parseIso8601(iso8601tz));

            Calendar gmt0 = new GregorianCalendar(TimeZone.getTimeZone(GMT0));
            gmt0.setTimeInMillis(local.getTimeInMillis());

            DateFormat df = new SimpleDateFormat(ISO8601TZ_FORMAT);
            df.setTimeZone(TimeZone.getTimeZone(GMT0));
            iso8601tz = df.format(gmt0.getTime());
        }
        return iso8601tz;
    }

    /**
     * 
     * @param iso8601tz
     * @return true if iso8601tz param is in gmt0
     */
    public static boolean isGmt0(String iso8601tz) {
        return (iso8601tz == null ? false : ((iso8601tz.length() == 24) && (iso8601tz.endsWith("0000"))));
    }

    /**
     * Convert iso8601 date/time to other timezone.
     * 
     * @param iso8601tz
     * @param newTz
     * @return
     * @throws ParseException
     */
    public static String changeTimeZone(String iso8601tz, Integer newTz) throws ParseException {
        String converted = null;
        String timeZoneId = null;
        if (iso8601tz != null && newTz != null) {
            // TODO: Voor berekening maakt niet uit, maar nu wordt automatisch
            // de lexografisch eerste tijdzonenaam gekozen
            timeZoneId = getTimezone(newTz);
            iso8601tz = timeToGmt0(iso8601tz);

            Date date = dtu.parseIso8601(iso8601tz);
            Calendar local = new GregorianCalendar(TimeZone.getTimeZone(GMT0));
            local.setTime(date);

            Calendar gmt0 = new GregorianCalendar(TimeZone.getTimeZone(timeZoneId));
            gmt0.setTimeInMillis(local.getTimeInMillis());

            DateFormat df = new SimpleDateFormat(ISO8601TZ_FORMAT);
            df.setTimeZone(TimeZone.getTimeZone(timeZoneId));

            converted = df.format(gmt0.getTime());
        }

        return converted;

    }

    private static String getTimezone(int offsetMs) {
        // cached timezone string?
        if (timeZones.containsKey(offsetMs)) {
            return timeZones.get(offsetMs);
        }
        // try to find GMT string for zone offset
        String tz = null;
        String tzIds[] = TimeZone.getAvailableIDs(offsetMs);
        for (int i = 0; i < tzIds.length; i++) {
            if (tzIds[i].matches(".*GMT.*")) {
                timeZones.put(offsetMs, tzIds[i]);
                return tzIds[i];
            }
        }
        // return the first possible timezone string
        if (tzIds.length > 0)
            tz = tzIds[0];
        return tz;
    }

    /**
     * 
     * 
     * @return
     */
    public static boolean isValidIso8601tzFormat(String time) {
        String iso8601pattern = "\\d\\d\\d\\d\\-[0-1]\\d\\-[0-3]\\dT[0-2]\\d\\:[0-5]\\d:[0-5]\\d\\+\\d\\d\\d\\d";
        return time.matches(iso8601pattern);
    }

}
