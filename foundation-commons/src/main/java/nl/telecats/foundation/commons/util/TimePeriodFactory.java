package nl.telecats.foundation.commons.util;

import java.util.Date;
import java.util.TimeZone;

import nl.telecats.foundation.commons.entities.RelativeTimePeriod;
import nl.telecats.foundation.commons.entities.TimePeriod;

import org.joda.time.DateTimeZone;
import org.joda.time.LocalDateTime;

/**
 * Creates {@link TimePeriod} entities based on input.<br>
 * <br>
 * For instance: creates a time period from a {@link RelativeTimePeriod}
 * 
 * @author Danny Dam Wichers
 */
public class TimePeriodFactory {

    /**
     * Creates a {@link TimePeriod} based on the provided relative time and a base time of 'now'.
     * 
     * @param relativeTime
     *            the relative time to create the time period for.
     * @param timezone
     *            the time zone the conversion needs to be in. If <code>null</code>, the current timezone is used.
     * @return the time period.
     */
    public static nl.telecats.foundation.commons.entities.TimePeriod fromRelativeTimePeriod(
            RelativeTimePeriod relativeTime, TimeZone timezone) {
        timezone = timezone == null ? TimeZone.getDefault() : timezone;

        return fromRelativeTimePeriodWithBase(relativeTime, new Date(), timezone);
    }

    /**
     * Creates a {@link TimePeriod} based on the provided relative time, base date and time zone.<br />
     * <br /> {@link #fromRelativeTimePeriod(RelativeTimePeriod, TimeZone)} should suffice for most uses.
     * 
     * @param relativeTime
     *            the relative time to create the period for.
     * @param baseTime
     *            the time to calculate the relative time from.
     * @param timezone
     *            the time zone the conversion needs to be in. If <code>null</code>, the current timezone is used.
     * @return the time period.
     */
    public static TimePeriod fromRelativeTimePeriodWithBase(RelativeTimePeriod relativeTime, Date baseTime,
            TimeZone timezone) {
        TimePeriod result = new TimePeriod();

        baseTime = baseTime == null ? new Date() : baseTime;
        LocalDateTime baseDT = LocalDateTime.fromDateFields(baseTime);

        DateTimeZone dtz = timezone == null ? DateTimeZone.getDefault() : DateTimeZone.forTimeZone(timezone);

        // Set start time to now.
        LocalDateTime start = baseDT;

        // Set end time to now.
        LocalDateTime end = baseDT;

        // calculate
        switch (relativeTime) {
        case PAST_MINUTE:
            start = baseDT.minusMinutes(1);
            break;
        case PAST_FIVE_MINUTES:
            start = baseDT.minusMinutes(5);
            break;
        case PAST_FIFTEEN_MINUTES:
            start = baseDT.minusMinutes(15);
            break;
        case THIS_HOUR:
            start = baseDT.withMinuteOfHour(0).withSecondOfMinute(0);
            break;
        case PAST_HOUR:
            start = baseDT.minusHours(1);
            break;
        case LAST_HOUR:
            start = baseDT.withMinuteOfHour(0).withSecondOfMinute(0).minusHours(1);
            end = start.withMinuteOfHour(59).withSecondOfMinute(59);
            break;
        case TODAY:
            start = baseDT.withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0);
            break;
        case YESTERDAY:
            start = baseDT.withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0).minusDays(1);
            end = start.withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59);
            break;
        case THIS_WEEK:
            start = baseDT.withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0).withDayOfWeek(1);
            break;
        case LAST_WEEK:
            start = baseDT.withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0).withDayOfWeek(1).minusWeeks(1);
            end = start.withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59).withDayOfWeek(7);
            break;
        case THIS_MONTH:
            start = baseDT.withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0).withDayOfMonth(1);
            break;
        case LAST_MONTH:
            start = baseDT.withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0).withDayOfMonth(1).minusMonths(1);
            end = start.withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59)
                    .withDayOfMonth(start.dayOfMonth().getMaximumValue());
            break;
        case THIS_QUARTER:
            int startQuarter = ((int) Math.floor((start.getMonthOfYear() - 1) / 3) * 3) + 1;

            start = baseDT.withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0).withDayOfMonth(1)
                    .withMonthOfYear(startQuarter);
            break;
        case LAST_QUARTER:
            int lastQuarterStart = ((int) Math.floor((start.getMonthOfYear() - 1) / 3) * 3) - 2;
            int minusYears = lastQuarterStart < 0 ? 1 : 0;
            lastQuarterStart = lastQuarterStart < 0 ? lastQuarterStart + 12 : lastQuarterStart;

            start = baseDT.withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0).withMonthOfYear(lastQuarterStart)
                    .withDayOfMonth(1).minusYears(minusYears);
            end = baseDT.withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59)
                    .withMonthOfYear(lastQuarterStart + 2)
                    .withDayOfMonth(start.plusMonths(2).dayOfMonth().getMaximumValue()).minusYears(minusYears);
            break;
        case THIS_YEAR:
            start = baseDT.withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0).withDayOfMonth(1)
                    .withMonthOfYear(1);
            break;
        case LAST_YEAR:
            start = baseDT.withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0).withMonthOfYear(1)
                    .withDayOfMonth(1).minusYears(1);
            end = baseDT.withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59).withMonthOfYear(12)
                    .withDayOfMonth(31).minusYears(1);
            break;
        default:
            break;
        }

        result.setStart(start.toDateTime(dtz).toDate());
        result.setEnd(end.toDateTime(dtz).toDate());

        return result;
    }

    /**
     * Creates a {@link TimePeriod} based on the provided relative time, recalculating everything to UTC.
     * 
     * @param relativeTime
     *            the relative time to create the time period for.
     * @return the time period.
     */
    public static TimePeriod fromRelativeTimePeriodUTC(RelativeTimePeriod relativeTime) {
        return TimePeriodFactory.fromRelativeTimePeriod(relativeTime, TimeZone.getTimeZone("UTC"));
    }
}