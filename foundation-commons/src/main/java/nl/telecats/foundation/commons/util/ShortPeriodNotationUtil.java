package nl.telecats.foundation.commons.util;

import java.text.ParseException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * ShortPeriodNotationUtil enables the use of so called 'Jira time notation', such as '1d 5h'. This util will translate
 * such notations to milliseconds.<br>
 * <br>
 * Valid periods are:
 * <dl>
 * <dt>s, second, seconds</dt>
 * <dd>Second</dd>
 * <dt>m, minute, minutes</dt>
 * <dd>Minute</dd>
 * <dt>h, hour, hours</dt>
 * <dd>Hour</dd>
 * <dt>d, day, days</dt>
 * <dd>Day</dd>
 * <dt>w, week, weeks</dt>
 * <dd>Week</dd>
 * </dl>
 * 
 * @author Danny Dam Wichers
 */
public class ShortPeriodNotationUtil {

    /**
     * Regex pattern for matching notations.
     */
    private static final Pattern periodPattern = Pattern.compile("(\\d+)([A-Za-z]+)");

    /**
     * Internal service enum.
     * 
     * @author Danny Dam Wichers
     * 
     */
    protected enum PeriodNotation {

        SECOND(new String[] { "s", "second", "seconds" }, 1000L), MINUTE(
                new String[] { "m", "min", "minute", "minutes" }, 60000L), HOUR(new String[] { "h", "hour", "hours" },
                3600000L), DAY(new String[] { "d", "day", "days" }, 86400000L), WEEK(new String[] { "w", "week",
                "weeks" }, 604800000L);

        /**
         * Notations used for the period.
         */
        private String[] notations;

        /**
         * Number of milliseconds for this period.
         */
        private long millis;

        /**
         * Constructor
         * 
         * @param notations
         *            notations that can be used for this period.
         * @param millis
         *            nNumber of milliseconds for this period.
         */
        private PeriodNotation(String[] notations, long millis) {
            this.notations = notations;
            this.millis = millis;
        }

        /**
         * Retrieve all notations for this period.
         * 
         * @return all notations for this period.
         */
        public String[] getNotations() {
            return notations;
        }

        /**
         * Retrieve the number of milliseconds for this period.
         * 
         * @return the number of milliseconds for this period.
         */
        public long getMillis() {
            return millis;
        }

        /**
         * Retrieve the number of milliseconds for a provided notation for a period.
         * 
         * @param notation
         *            the notation.
         * @return number of milliseconds.
         * @throws IllegalArgumentException
         *             if the notation did not correspond to a period.
         */
        public static long getMillisForNotation(String notation) throws IllegalArgumentException {
            if (notation != null) {
                for (PeriodNotation pn : PeriodNotation.values()) {
                    for (String n : pn.getNotations()) {
                        if (notation.equals(n)) {
                            return pn.getMillis();
                        }
                    }
                }
            }
            throw new IllegalArgumentException("Unknown notation for a period: "
                    + (notation == null ? "<null>" : notation));
        }
    }

    /**
     * Parse a notation of periods into milliseconds. For instance: '2h 30s' would become 7230000.
     * 
     * @param fullPeriod
     *            all parts of a period.
     * @return the milliseconds that the notation describes.
     * @throws ParseException
     *             if any part of the notation could be parsed into milliseconds.
     */
    public static long parseNotation(String fullPeriod) throws ParseException {
        long result = 0L;

        if (fullPeriod == null || fullPeriod.length() == 0) {
            return result;
        }

        for (String subPeriod : fullPeriod.split("\\s")) {
            Matcher subPeriodMatcher = periodPattern.matcher(subPeriod);
            if (subPeriodMatcher.find()) {
                int periodLength = Integer.parseInt(subPeriodMatcher.group(1));
                String subPeriodNotation = subPeriodMatcher.group(2);

                try {
                    long periodMillis = PeriodNotation.getMillisForNotation(subPeriodNotation);
                    result = result + (periodLength * periodMillis);
                } catch (IllegalArgumentException iae) {
                    throw new ParseException("Unknown notation for a period " + subPeriodNotation + " in period "
                            + fullPeriod, 0);
                }
            } else {
                throw new ParseException("Supplied period does not match [period duration][period notation]; error in "
                        + subPeriod + " (" + fullPeriod + ")", 0);
            }
        }

        return result;
    }
}