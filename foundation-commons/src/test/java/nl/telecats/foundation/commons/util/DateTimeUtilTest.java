package nl.telecats.foundation.commons.util;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.junit.Assert;
import org.junit.Test;

public class DateTimeUtilTest {
    @Test
    public void testNow() {
        Calendar cal = Calendar.getInstance();
        String year = "" + cal.get(Calendar.YEAR);
        String month = "" + (cal.get(Calendar.MONTH) + 1);
        while (month.length() < 2)
            month = "0" + month;
        String day = "" + cal.get(Calendar.DAY_OF_MONTH);
        while (day.length() < 2)
            day = "0" + day;
        String hour = "" + cal.get(Calendar.HOUR_OF_DAY);
        while (hour.length() < 2)
            hour = "0" + hour;
        DateTimeUtil dateTimeUtil = DateTimeUtil.getInstance();
        String result = dateTimeUtil.getNowIso8601();
        String expectedResult = year + "-" + month + "-" + day + "T" + hour;
        System.out.println("result        :" + result);
        System.out.println("expectedResult:" + expectedResult);
        Assert.assertTrue("Correct time format", result.startsWith(expectedResult));
    }

    @Test
    public void testSecondsBetween() throws ParseException {
        DateTimeUtil dateTimeUtil = DateTimeUtil.getInstance();
        String t1 = "2009-09-24T07:35:54";
        String t2 = "2009-09-24T07:36:55";
        int result = dateTimeUtil.secondsBetween(t1, t2);
        Assert.assertEquals(61, result);

        result = dateTimeUtil.secondsBetween(t2, t1);
        Assert.assertEquals(-1, result);
    }

    @Test
    public void convertTest() throws ParseException {
        DateTimeUtil dateTimeUtil = DateTimeUtil.getInstance();
        String t1 = "2010-03-28T01:59:59";
        Assert.assertEquals("2010-03-28T01:59:59+0100", dateTimeUtil.convertToTimezoneStamp(t1));

        t1 = "2010-03-28T02:59:59"; // tijd bestaat niet, de zomertijd is nu
        // ingegaan
        Assert.assertEquals("2010-03-28T03:59:59+0200", dateTimeUtil.convertToTimezoneStamp(t1));
    }

    @Test
    public void winterNaarZomerTijdTest() throws ParseException {
        DateTimeUtil dateTimeUtil = DateTimeUtil.getInstance();
        String t1 = "2010-03-28T01:59:59"; // wintertijd
        String t2 = "2010-03-28T03:00:01"; // zomertijd

        int result = dateTimeUtil.secondsBetween(t1, t2);
        Assert.assertEquals(2, result); // zou dus 2 seconden moeten zijn

        result = dateTimeUtil.secondsBetween(t2, t1);
        Assert.assertEquals(-1, result);

    }

    @Test
    public void zomerNaarWinterTijdTest() {
        DateTimeUtil dateTimeUtil = DateTimeUtil.getInstance();
        String t1 = "2010-10-31T01:59:59";
        String t2 = "2010-10-31T03:00:01";
        int result = dateTimeUtil.secondsBetween(t1, t2);
        Assert.assertEquals(7202, result);

        t1 = "2010-10-31T01:59:59+0200";
        t2 = "2010-10-31T03:00:01+0100";
        result = dateTimeUtil.secondsBetween(t1, t2);
        Assert.assertEquals(7202, result);

        result = dateTimeUtil.secondsBetween(t2, t1);
        Assert.assertEquals(-1, result);

        t1 = "2010-10-31T02:16:00";
        t2 = "2010-10-31T02:15:00";
        result = dateTimeUtil.secondsBetween(t1, t2);
        // this will go wrong ofcourse
        Assert.assertEquals(-1, result);

        t1 = "2010-10-31T02:16:00+0200";
        t2 = "2010-10-31T02:15:00+0100";
        result = dateTimeUtil.secondsBetween(t1, t2);
        // this should go ok now
        Assert.assertEquals(59 * 60, result);

    }

    @Test
    public void testLocales() throws ParseException {
        DateTimeUtil dateTimeUtil = DateTimeUtil.getInstance(new Locale("en"));
        String t1 = "2010-09-08T01:22:33+0200";
        Date dateTime = dateTimeUtil.parseIso8601(t1);
        Assert.assertEquals("Week of year should be 37 in locale EN", "37", dateTimeUtil.getWeekOfYear(dateTime));
        dateTimeUtil = DateTimeUtil.getInstance(new Locale("nl"));
        Assert.assertEquals("Week of year should be 36 in locale NL", "36", dateTimeUtil.getWeekOfYear(dateTime));
    }

    @Test
    public void fields() throws ParseException {
        DateTimeUtil dateTimeUtil = DateTimeUtil.getInstance(new Locale("nl"));
        String t1 = "2010-09-08T01:22:33+0200";
        Date dateTime = dateTimeUtil.parseIso8601(t1);
        Assert.assertEquals("2010", dateTimeUtil.getYear(dateTime));
        Assert.assertEquals("09", dateTimeUtil.getMonth(dateTime));
        Assert.assertEquals("08", dateTimeUtil.getDay(dateTime));
        Assert.assertEquals("3", dateTimeUtil.getQuarterOfYear(dateTime));
        Assert.assertEquals("36", dateTimeUtil.getWeekOfYear(dateTime));
        Assert.assertEquals("2", dateTimeUtil.getDayOfWeekInMonth(dateTime));
        Assert.assertEquals("01", dateTimeUtil.getHour(dateTime));
        Assert.assertEquals("22", dateTimeUtil.getMinute(dateTime));
        Assert.assertEquals("33", dateTimeUtil.getSecond(dateTime));
        Assert.assertEquals("2", dateTimeUtil.getQuarterOfHour(dateTime));

    }

    @Test
    public void testDateBefore() throws ParseException {
        String isoDate1 = "2010-10-05T20:46:21+0200";
        String isoDate2 = "2010-10-05T20:46:22+0200";
        Assert.assertTrue("Date before", DateTimeUtil.getInstance().isDateBefore(isoDate1, isoDate2));
    }

    @Test
    public void testDateBeforeTZ() throws ParseException {
        String isoDate1 = "2010-10-05T20:46:23+0300";
        String isoDate2 = "2010-10-05T20:46:22+0200";
        Assert.assertTrue("Date before", DateTimeUtil.getInstance().isDateBefore(isoDate1, isoDate2));
    }

    @Test
    public void testDateNotBefore() throws ParseException {
        String isoDate1 = "2010-10-05T20:46:22+0200";
        String isoDate2 = "2010-10-05T20:46:21+0200";
        Assert.assertTrue("Date after", !DateTimeUtil.getInstance().isDateBefore(isoDate1, isoDate2));
    }

    @Test
    public void testDateNotBeforeTZ() throws ParseException {
        String isoDate1 = "2010-10-05T20:46:20+0100";
        String isoDate2 = "2010-10-05T20:46:21+0200";
        Assert.assertTrue("Date after", !DateTimeUtil.getInstance().isDateBefore(isoDate1, isoDate2));
    }

    @Test
    public void testSubtractSeconds() throws ParseException {
        String isoDate = "2010-10-05T20:46:05+0100";
        System.out.println("isoDate:" + isoDate);
        int secondsToSubtract = 5;
        String expectedIsoDate = "2010-10-05T20:46:00+0100";

        String result = DateTimeUtil.getInstance().subtractSeconds(isoDate, secondsToSubtract);

        System.out.println("expectedIsoDate :" + expectedIsoDate);
        System.out.println("result          :" + result);
        Assert.assertTrue("Correct subtraction, expected/actual:" + expectedIsoDate + "," + result,
                result.equals(expectedIsoDate));
    }

    @Test
    public void testAddSeconds() throws ParseException {
        String isoDate = "2010-10-05T20:46:05+0100";
        System.out.println("isoDate:" + isoDate);
        int secondsToAdd = 5;
        String expectedIsoDate = "2010-10-05T20:46:10+0100";

        String result = DateTimeUtil.getInstance().addSeconds(isoDate, secondsToAdd);

        System.out.println("expectedIsoDate :" + expectedIsoDate);
        System.out.println("result          :" + result);
        Assert.assertTrue("Correct added, expected/actual:" + expectedIsoDate + "," + result,
                result.equals(expectedIsoDate));
    }

    @Test
    public void testConvertTo() throws ParseException {
        String isoDate = "2010-10-05T20:46:05+0100";
        String format = "yyyy";
        String expected = "2010";

        String result = DateTimeUtil.getInstance().convertTo(isoDate, format);
        Assert.assertTrue("Date formated, expected:" + expected + " but got:" + result, expected.equals(result));
    }

    @Test
    public void testGetDayOfWeekInMonth() throws ParseException {
        String isoDate = "2011-01-08T20:46:05+0100";
        Date date = DateTimeUtil.getInstance().parseIso8601(isoDate);
        String expected = "2";

        String result = DateTimeUtil.getInstance().getDayOfWeekInMonth(date);
        Assert.assertEquals(expected, result);
    }

    @Test
    public void testGetDayInWeek() throws ParseException {
        String isoDate = "2011-01-08T20:46:05+0100";
        Date date = DateTimeUtil.getInstance().parseIso8601(isoDate);
        String expected = "Saturday";

        Locale currentLocale = Locale.getDefault();
        Locale locale = new Locale("en", "US");

        String result = DateTimeUtil.getInstance(locale).getDayInWeek(date);
        Assert.assertEquals(expected, result);

        // Check if default locale does not get changed systemwide, somehow
        Assert.assertEquals(currentLocale, Locale.getDefault());
    }
}
