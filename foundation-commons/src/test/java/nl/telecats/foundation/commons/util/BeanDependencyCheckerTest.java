package nl.telecats.foundation.commons.util;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.ExpectedException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Test BeanDependencyChecker is functioning as expected
 * <p/>
 * NOTE: We don't actually test console output.
 * 
 * @author raymond
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:foundation-common-test-beandependencychecker-missingbean-context.xml" })
public class BeanDependencyCheckerTest {

    @Autowired
    BeanDependencyChecker beanDependencyChecker;

    @ExpectedException(NoSuchBeanDefinitionException.class)
    @Test
    public void testSpringBeanLogger() {
        beanDependencyChecker.checkDependencies();
    }
}
