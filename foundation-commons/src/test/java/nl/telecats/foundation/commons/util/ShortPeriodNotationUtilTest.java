package nl.telecats.foundation.commons.util;

import java.text.ParseException;

import junit.framework.Assert;

import org.junit.Test;

/**
 * Tests for {@link ShortPeriodNotationUtil}
 * 
 * @author Danny Dam Wichers
 */
public class ShortPeriodNotationUtilTest {

    @Test
    public void testSeconds() throws ParseException {
        Assert.assertEquals(1000L, ShortPeriodNotationUtil.parseNotation("1s"));
        Assert.assertEquals(15000L, ShortPeriodNotationUtil.parseNotation("15second"));
        Assert.assertEquals(2000L, ShortPeriodNotationUtil.parseNotation("2seconds"));
    }

    @Test
    public void testMinutes() throws ParseException {
        Assert.assertEquals(60000L, ShortPeriodNotationUtil.parseNotation("1m"));
        Assert.assertEquals(900000L, ShortPeriodNotationUtil.parseNotation("15min"));
        Assert.assertEquals(120000L, ShortPeriodNotationUtil.parseNotation("2minute"));
        Assert.assertEquals(180000L, ShortPeriodNotationUtil.parseNotation("3minutes"));
    }

    @Test
    public void testHours() throws ParseException {
        Assert.assertEquals(3600000L, ShortPeriodNotationUtil.parseNotation("1h"));
        Assert.assertEquals(54000000L, ShortPeriodNotationUtil.parseNotation("15hour"));
        Assert.assertEquals(7200000L, ShortPeriodNotationUtil.parseNotation("2hours"));
    }

    @Test
    public void testDays() throws ParseException {
        Assert.assertEquals(86400000L, ShortPeriodNotationUtil.parseNotation("1d"));
        Assert.assertEquals(1296000000L, ShortPeriodNotationUtil.parseNotation("15day"));
        Assert.assertEquals(172800000L, ShortPeriodNotationUtil.parseNotation("2days"));
    }

    @Test
    public void testWeeks() throws ParseException {
        Assert.assertEquals(604800000L, ShortPeriodNotationUtil.parseNotation("1w"));
        Assert.assertEquals(9072000000L, ShortPeriodNotationUtil.parseNotation("15week"));
        Assert.assertEquals(1209600000L, ShortPeriodNotationUtil.parseNotation("2weeks"));
    }

    @Test
    public void testFull() throws ParseException {
        Assert.assertEquals(777601000L, ShortPeriodNotationUtil.parseNotation("1w 2d 1s"));

        Assert.assertEquals(1300320000L, ShortPeriodNotationUtil.parseNotation("15d 12minutes 1hour"));

        Assert.assertEquals(1209660000L, ShortPeriodNotationUtil.parseNotation("2w 1min"));
    }

    @Test(expected = ParseException.class)
    public void testFalseNotation1() throws ParseException {
        ShortPeriodNotationUtil.parseNotation("abc1");
    }

    @Test(expected = ParseException.class)
    public void testFalseNotation2() throws ParseException {
        ShortPeriodNotationUtil.parseNotation("2d 1son");
    }
}