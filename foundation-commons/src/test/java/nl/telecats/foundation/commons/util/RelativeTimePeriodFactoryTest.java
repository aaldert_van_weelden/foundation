package nl.telecats.foundation.commons.util;

import java.text.SimpleDateFormat;
import java.util.Date;

import junit.framework.Assert;
import nl.telecats.foundation.commons.entities.RelativeTimePeriod;
import nl.telecats.foundation.commons.entities.TimePeriod;

import org.junit.Test;

public class RelativeTimePeriodFactoryTest {

    /**
     * Date format for checking dates as String
     */
    private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    /**
     * 2010-05-25 18:23:41+0200
     */
    private final Date dateTest = new Date(1274804621000L);

    /**
     * 2010-05-25 00:00:00+0200
     */
    private final Date dateMidnight = new Date(1274738400000L);

    /**
     * 2010-05-25 23:59:59+0200
     */
    private final Date dateBeforeMidnight = new Date(1274824799000L);

    /**
     * 2010-05-26 00:00:01+0200
     */
    private final Date dateAfterMidnight = new Date(1274824801000L);

    /**
     * 2008-02-29 08:42:57
     */
    private final Date leapDay = new Date(1204270977000L);

    /**
     * 2008-03-01 08:42:57
     */
    private final Date afterLeap = new Date(1204530177000L);

    /**
     * 2010-01-01 00:00:00
     */
    private final Date yearStart = new Date(1262300400000L);

    @Test
    public void testPastMinute() {
        TimePeriod result;

        result = TimePeriodFactory.fromRelativeTimePeriodWithBase(RelativeTimePeriod.PAST_MINUTE, dateTest, null);
        Assert.assertEquals("2010-05-25 18:22:41", sdf.format(result.getStart()));
        Assert.assertEquals("2010-05-25 18:23:41", sdf.format(result.getEnd()));

        result = TimePeriodFactory.fromRelativeTimePeriodWithBase(RelativeTimePeriod.PAST_MINUTE, dateAfterMidnight,
                null);
        Assert.assertEquals("2010-05-25 23:59:01", sdf.format(result.getStart()));
        Assert.assertEquals("2010-05-26 00:00:01", sdf.format(result.getEnd()));

        result = TimePeriodFactory.fromRelativeTimePeriodWithBase(RelativeTimePeriod.PAST_MINUTE, yearStart, null);
        Assert.assertEquals("2009-12-31 23:59:00", sdf.format(result.getStart()));
        Assert.assertEquals("2010-01-01 00:00:00", sdf.format(result.getEnd()));
    }

    @Test
    public void testPastFiveMinutes() {
        TimePeriod result;

        result = TimePeriodFactory.fromRelativeTimePeriodWithBase(RelativeTimePeriod.PAST_FIVE_MINUTES, dateTest, null);
        Assert.assertEquals("2010-05-25 18:18:41", sdf.format(result.getStart()));
        Assert.assertEquals("2010-05-25 18:23:41", sdf.format(result.getEnd()));

        result = TimePeriodFactory.fromRelativeTimePeriodWithBase(RelativeTimePeriod.PAST_FIVE_MINUTES,
                dateAfterMidnight, null);
        Assert.assertEquals("2010-05-25 23:55:01", sdf.format(result.getStart()));
        Assert.assertEquals("2010-05-26 00:00:01", sdf.format(result.getEnd()));

        result = TimePeriodFactory
                .fromRelativeTimePeriodWithBase(RelativeTimePeriod.PAST_FIVE_MINUTES, yearStart, null);
        Assert.assertEquals("2009-12-31 23:55:00", sdf.format(result.getStart()));
        Assert.assertEquals("2010-01-01 00:00:00", sdf.format(result.getEnd()));
    }

    @Test
    public void testPastFifteenMinutes() {
        TimePeriod result;

        result = TimePeriodFactory.fromRelativeTimePeriodWithBase(RelativeTimePeriod.PAST_FIFTEEN_MINUTES, dateTest,
                null);
        Assert.assertEquals("2010-05-25 18:08:41", sdf.format(result.getStart()));
        Assert.assertEquals("2010-05-25 18:23:41", sdf.format(result.getEnd()));

        result = TimePeriodFactory.fromRelativeTimePeriodWithBase(RelativeTimePeriod.PAST_FIFTEEN_MINUTES,
                dateAfterMidnight, null);
        Assert.assertEquals("2010-05-25 23:45:01", sdf.format(result.getStart()));
        Assert.assertEquals("2010-05-26 00:00:01", sdf.format(result.getEnd()));

        result = TimePeriodFactory.fromRelativeTimePeriodWithBase(RelativeTimePeriod.PAST_FIFTEEN_MINUTES, yearStart,
                null);
        Assert.assertEquals("2009-12-31 23:45:00", sdf.format(result.getStart()));
        Assert.assertEquals("2010-01-01 00:00:00", sdf.format(result.getEnd()));
    }

    @Test
    public void testCurrentHour() {
        TimePeriod result;

        result = TimePeriodFactory.fromRelativeTimePeriodWithBase(RelativeTimePeriod.THIS_HOUR, dateTest, null);
        Assert.assertEquals("2010-05-25 18:00:00", sdf.format(result.getStart()));
        Assert.assertEquals("2010-05-25 18:23:41", sdf.format(result.getEnd()));

        result = TimePeriodFactory.fromRelativeTimePeriodWithBase(RelativeTimePeriod.THIS_HOUR, dateMidnight, null);
        Assert.assertEquals("2010-05-25 00:00:00", sdf.format(result.getStart()));
        Assert.assertEquals("2010-05-25 00:00:00", sdf.format(result.getEnd()));

        result = TimePeriodFactory.fromRelativeTimePeriodWithBase(RelativeTimePeriod.THIS_HOUR, dateBeforeMidnight,
                null);
        Assert.assertEquals("2010-05-25 23:00:00", sdf.format(result.getStart()));
        Assert.assertEquals("2010-05-25 23:59:59", sdf.format(result.getEnd()));
    }

    @Test
    public void testPastHour() {
        TimePeriod result;

        result = TimePeriodFactory.fromRelativeTimePeriodWithBase(RelativeTimePeriod.PAST_HOUR, dateTest, null);
        Assert.assertEquals("2010-05-25 17:23:41", sdf.format(result.getStart()));
        Assert.assertEquals("2010-05-25 18:23:41", sdf.format(result.getEnd()));

        result = TimePeriodFactory.fromRelativeTimePeriodWithBase(RelativeTimePeriod.PAST_HOUR, dateMidnight, null);
        Assert.assertEquals("2010-05-24 23:00:00", sdf.format(result.getStart()));
        Assert.assertEquals("2010-05-25 00:00:00", sdf.format(result.getEnd()));

        result = TimePeriodFactory
                .fromRelativeTimePeriodWithBase(RelativeTimePeriod.PAST_HOUR, dateAfterMidnight, null);
        Assert.assertEquals("2010-05-25 23:00:01", sdf.format(result.getStart()));
        Assert.assertEquals("2010-05-26 00:00:01", sdf.format(result.getEnd()));

        result = TimePeriodFactory.fromRelativeTimePeriodWithBase(RelativeTimePeriod.PAST_HOUR, yearStart, null);
        Assert.assertEquals("2009-12-31 23:00:00", sdf.format(result.getStart()));
        Assert.assertEquals("2010-01-01 00:00:00", sdf.format(result.getEnd()));
    }

    @Test
    public void testLastHour() {
        TimePeriod result;

        result = TimePeriodFactory.fromRelativeTimePeriodWithBase(RelativeTimePeriod.LAST_HOUR, dateTest, null);
        Assert.assertEquals("2010-05-25 17:00:00", sdf.format(result.getStart()));
        Assert.assertEquals("2010-05-25 17:59:59", sdf.format(result.getEnd()));

        result = TimePeriodFactory.fromRelativeTimePeriodWithBase(RelativeTimePeriod.LAST_HOUR, dateMidnight, null);
        Assert.assertEquals("2010-05-24 23:00:00", sdf.format(result.getStart()));
        Assert.assertEquals("2010-05-24 23:59:59", sdf.format(result.getEnd()));

        result = TimePeriodFactory.fromRelativeTimePeriodWithBase(RelativeTimePeriod.LAST_HOUR, dateBeforeMidnight,
                null);
        Assert.assertEquals("2010-05-25 22:00:00", sdf.format(result.getStart()));
        Assert.assertEquals("2010-05-25 22:59:59", sdf.format(result.getEnd()));
    }

    @Test
    public void testToday() {
        TimePeriod result;

        result = TimePeriodFactory.fromRelativeTimePeriodWithBase(RelativeTimePeriod.TODAY, dateTest, null);
        Assert.assertEquals("2010-05-25 00:00:00", sdf.format(result.getStart()));
        Assert.assertEquals("2010-05-25 18:23:41", sdf.format(result.getEnd()));

        result = TimePeriodFactory.fromRelativeTimePeriodWithBase(RelativeTimePeriod.TODAY, dateMidnight, null);
        Assert.assertEquals("2010-05-25 00:00:00", sdf.format(result.getStart()));
        Assert.assertEquals("2010-05-25 00:00:00", sdf.format(result.getEnd()));

        result = TimePeriodFactory.fromRelativeTimePeriodWithBase(RelativeTimePeriod.TODAY, dateBeforeMidnight, null);
        Assert.assertEquals("2010-05-25 00:00:00", sdf.format(result.getStart()));
        Assert.assertEquals("2010-05-25 23:59:59", sdf.format(result.getEnd()));
    }

    @Test
    public void testYesterday() {
        TimePeriod result;

        result = TimePeriodFactory.fromRelativeTimePeriodWithBase(RelativeTimePeriod.YESTERDAY, dateTest, null);
        Assert.assertEquals("2010-05-24 00:00:00", sdf.format(result.getStart()));
        Assert.assertEquals("2010-05-24 23:59:59", sdf.format(result.getEnd()));

        result = TimePeriodFactory.fromRelativeTimePeriodWithBase(RelativeTimePeriod.YESTERDAY, dateMidnight, null);
        Assert.assertEquals("2010-05-24 00:00:00", sdf.format(result.getStart()));
        Assert.assertEquals("2010-05-24 23:59:59", sdf.format(result.getEnd()));

        result = TimePeriodFactory.fromRelativeTimePeriodWithBase(RelativeTimePeriod.YESTERDAY, dateBeforeMidnight,
                null);
        Assert.assertEquals("2010-05-24 00:00:00", sdf.format(result.getStart()));
        Assert.assertEquals("2010-05-24 23:59:59", sdf.format(result.getEnd()));

        // 2008-03-01 08:42:57
        Date afterLeap = new Date(1204357377000L);
        result = TimePeriodFactory.fromRelativeTimePeriodWithBase(RelativeTimePeriod.YESTERDAY, afterLeap, null);
        Assert.assertEquals("2008-02-29 00:00:00", sdf.format(result.getStart()));
        Assert.assertEquals("2008-02-29 23:59:59", sdf.format(result.getEnd()));
    }

    @Test
    public void testThisWeek() {
        TimePeriod result;

        result = TimePeriodFactory.fromRelativeTimePeriodWithBase(RelativeTimePeriod.THIS_WEEK, dateTest, null);
        Assert.assertEquals("2010-05-24 00:00:00", sdf.format(result.getStart()));
        Assert.assertEquals("2010-05-25 18:23:41", sdf.format(result.getEnd()));

        result = TimePeriodFactory.fromRelativeTimePeriodWithBase(RelativeTimePeriod.THIS_WEEK, dateMidnight, null);
        Assert.assertEquals("2010-05-24 00:00:00", sdf.format(result.getStart()));
        Assert.assertEquals("2010-05-25 00:00:00", sdf.format(result.getEnd()));

        result = TimePeriodFactory.fromRelativeTimePeriodWithBase(RelativeTimePeriod.THIS_WEEK, dateBeforeMidnight,
                null);
        Assert.assertEquals("2010-05-24 00:00:00", sdf.format(result.getStart()));
        Assert.assertEquals("2010-05-25 23:59:59", sdf.format(result.getEnd()));

        // 2010-01-01 08:42:57
        Date weekDate = new Date(1262331777000L);

        result = TimePeriodFactory.fromRelativeTimePeriodWithBase(RelativeTimePeriod.THIS_WEEK, weekDate, null);
        Assert.assertEquals("2009-12-28 00:00:00", sdf.format(result.getStart()));
        Assert.assertEquals("2010-01-01 08:42:57", sdf.format(result.getEnd()));

        result = TimePeriodFactory.fromRelativeTimePeriodWithBase(RelativeTimePeriod.THIS_WEEK, leapDay, null);
        Assert.assertEquals("2008-02-25 00:00:00", sdf.format(result.getStart()));
        Assert.assertEquals("2008-02-29 08:42:57", sdf.format(result.getEnd()));
    }

    @Test
    public void testLastWeek() {
        TimePeriod result;

        result = TimePeriodFactory.fromRelativeTimePeriodWithBase(RelativeTimePeriod.LAST_WEEK, dateTest, null);
        Assert.assertEquals("2010-05-17 00:00:00", sdf.format(result.getStart()));
        Assert.assertEquals("2010-05-23 23:59:59", sdf.format(result.getEnd()));

        result = TimePeriodFactory.fromRelativeTimePeriodWithBase(RelativeTimePeriod.LAST_WEEK, dateMidnight, null);
        Assert.assertEquals("2010-05-17 00:00:00", sdf.format(result.getStart()));
        Assert.assertEquals("2010-05-23 23:59:59", sdf.format(result.getEnd()));

        result = TimePeriodFactory.fromRelativeTimePeriodWithBase(RelativeTimePeriod.LAST_WEEK, dateBeforeMidnight,
                null);
        Assert.assertEquals("2010-05-17 00:00:00", sdf.format(result.getStart()));
        Assert.assertEquals("2010-05-23 23:59:59", sdf.format(result.getEnd()));

        // 2010-01-01 08:42:57
        Date weekDate = new Date(1262331777000L);

        result = TimePeriodFactory.fromRelativeTimePeriodWithBase(RelativeTimePeriod.LAST_WEEK, weekDate, null);
        Assert.assertEquals("2009-12-21 00:00:00", sdf.format(result.getStart()));
        Assert.assertEquals("2009-12-27 23:59:59", sdf.format(result.getEnd()));

        result = TimePeriodFactory.fromRelativeTimePeriodWithBase(RelativeTimePeriod.LAST_WEEK, leapDay, null);
        Assert.assertEquals("2008-02-18 00:00:00", sdf.format(result.getStart()));
        Assert.assertEquals("2008-02-24 23:59:59", sdf.format(result.getEnd()));

        result = TimePeriodFactory.fromRelativeTimePeriodWithBase(RelativeTimePeriod.LAST_WEEK, afterLeap, null);
        Assert.assertEquals("2008-02-25 00:00:00", sdf.format(result.getStart()));
        Assert.assertEquals("2008-03-02 23:59:59", sdf.format(result.getEnd()));
    }

    @Test
    public void testThisMonth() {
        TimePeriod result;

        result = TimePeriodFactory.fromRelativeTimePeriodWithBase(RelativeTimePeriod.THIS_MONTH, dateTest, null);
        Assert.assertEquals("2010-05-01 00:00:00", sdf.format(result.getStart()));
        Assert.assertEquals("2010-05-25 18:23:41", sdf.format(result.getEnd()));

        result = TimePeriodFactory.fromRelativeTimePeriodWithBase(RelativeTimePeriod.THIS_MONTH, dateMidnight, null);
        Assert.assertEquals("2010-05-01 00:00:00", sdf.format(result.getStart()));
        Assert.assertEquals("2010-05-25 00:00:00", sdf.format(result.getEnd()));

        result = TimePeriodFactory.fromRelativeTimePeriodWithBase(RelativeTimePeriod.THIS_MONTH, dateBeforeMidnight,
                null);
        Assert.assertEquals("2010-05-01 00:00:00", sdf.format(result.getStart()));
        Assert.assertEquals("2010-05-25 23:59:59", sdf.format(result.getEnd()));
    }

    @Test
    public void testLastMonth() {
        TimePeriod result;

        result = TimePeriodFactory.fromRelativeTimePeriodWithBase(RelativeTimePeriod.LAST_MONTH, dateTest, null);
        Assert.assertEquals("2010-04-01 00:00:00", sdf.format(result.getStart()));
        Assert.assertEquals("2010-04-30 23:59:59", sdf.format(result.getEnd()));

        result = TimePeriodFactory.fromRelativeTimePeriodWithBase(RelativeTimePeriod.LAST_MONTH, dateMidnight, null);
        Assert.assertEquals("2010-04-01 00:00:00", sdf.format(result.getStart()));
        Assert.assertEquals("2010-04-30 23:59:59", sdf.format(result.getEnd()));

        result = TimePeriodFactory.fromRelativeTimePeriodWithBase(RelativeTimePeriod.LAST_MONTH, dateBeforeMidnight,
                null);
        Assert.assertEquals("2010-04-01 00:00:00", sdf.format(result.getStart()));
        Assert.assertEquals("2010-04-30 23:59:59", sdf.format(result.getEnd()));

        result = TimePeriodFactory.fromRelativeTimePeriodWithBase(RelativeTimePeriod.LAST_MONTH, afterLeap, null);
        Assert.assertEquals("2008-02-01 00:00:00", sdf.format(result.getStart()));
        Assert.assertEquals("2008-02-29 23:59:59", sdf.format(result.getEnd()));

        result = TimePeriodFactory.fromRelativeTimePeriodWithBase(RelativeTimePeriod.LAST_MONTH, yearStart, null);
        Assert.assertEquals("2009-12-01 00:00:00", sdf.format(result.getStart()));
        Assert.assertEquals("2009-12-31 23:59:59", sdf.format(result.getEnd()));

    }

    @Test
    public void testThisQuarter() {
        TimePeriod result;

        result = TimePeriodFactory.fromRelativeTimePeriodWithBase(RelativeTimePeriod.THIS_QUARTER, dateTest, null);
        Assert.assertEquals("2010-04-01 00:00:00", sdf.format(result.getStart()));
        Assert.assertEquals("2010-05-25 18:23:41", sdf.format(result.getEnd()));

        result = TimePeriodFactory.fromRelativeTimePeriodWithBase(RelativeTimePeriod.THIS_QUARTER, dateMidnight, null);
        Assert.assertEquals("2010-04-01 00:00:00", sdf.format(result.getStart()));
        Assert.assertEquals("2010-05-25 00:00:00", sdf.format(result.getEnd()));

        result = TimePeriodFactory.fromRelativeTimePeriodWithBase(RelativeTimePeriod.THIS_QUARTER, dateBeforeMidnight,
                null);
        Assert.assertEquals("2010-04-01 00:00:00", sdf.format(result.getStart()));
        Assert.assertEquals("2010-05-25 23:59:59", sdf.format(result.getEnd()));

        result = TimePeriodFactory.fromRelativeTimePeriodWithBase(RelativeTimePeriod.THIS_QUARTER, yearStart, null);
        Assert.assertEquals("2010-01-01 00:00:00", sdf.format(result.getStart()));
        Assert.assertEquals("2010-01-01 00:00:00", sdf.format(result.getEnd()));
    }

    @Test
    public void testLastQuarter() {
        TimePeriod result;

        result = TimePeriodFactory.fromRelativeTimePeriodWithBase(RelativeTimePeriod.LAST_QUARTER, dateTest, null);
        Assert.assertEquals("2010-01-01 00:00:00", sdf.format(result.getStart()));
        Assert.assertEquals("2010-03-31 23:59:59", sdf.format(result.getEnd()));

        result = TimePeriodFactory.fromRelativeTimePeriodWithBase(RelativeTimePeriod.LAST_QUARTER, dateMidnight, null);
        Assert.assertEquals("2010-01-01 00:00:00", sdf.format(result.getStart()));
        Assert.assertEquals("2010-03-31 23:59:59", sdf.format(result.getEnd()));

        result = TimePeriodFactory.fromRelativeTimePeriodWithBase(RelativeTimePeriod.LAST_QUARTER, dateBeforeMidnight,
                null);
        Assert.assertEquals("2010-01-01 00:00:00", sdf.format(result.getStart()));
        Assert.assertEquals("2010-03-31 23:59:59", sdf.format(result.getEnd()));

        result = TimePeriodFactory.fromRelativeTimePeriodWithBase(RelativeTimePeriod.LAST_QUARTER, afterLeap, null);
        Assert.assertEquals("2007-10-01 00:00:00", sdf.format(result.getStart()));
        Assert.assertEquals("2007-12-31 23:59:59", sdf.format(result.getEnd()));

        result = TimePeriodFactory.fromRelativeTimePeriodWithBase(RelativeTimePeriod.LAST_QUARTER, yearStart, null);
        Assert.assertEquals("2009-10-01 00:00:00", sdf.format(result.getStart()));
        Assert.assertEquals("2009-12-31 23:59:59", sdf.format(result.getEnd()));
    }

    @Test
    public void testThisYear() {
        TimePeriod result;

        result = TimePeriodFactory.fromRelativeTimePeriodWithBase(RelativeTimePeriod.THIS_YEAR, dateTest, null);
        Assert.assertEquals("2010-01-01 00:00:00", sdf.format(result.getStart()));
        Assert.assertEquals("2010-05-25 18:23:41", sdf.format(result.getEnd()));

        result = TimePeriodFactory.fromRelativeTimePeriodWithBase(RelativeTimePeriod.THIS_YEAR, dateMidnight, null);
        Assert.assertEquals("2010-01-01 00:00:00", sdf.format(result.getStart()));
        Assert.assertEquals("2010-05-25 00:00:00", sdf.format(result.getEnd()));

        result = TimePeriodFactory.fromRelativeTimePeriodWithBase(RelativeTimePeriod.THIS_YEAR, dateBeforeMidnight,
                null);
        Assert.assertEquals("2010-01-01 00:00:00", sdf.format(result.getStart()));
        Assert.assertEquals("2010-05-25 23:59:59", sdf.format(result.getEnd()));

        result = TimePeriodFactory.fromRelativeTimePeriodWithBase(RelativeTimePeriod.THIS_YEAR, yearStart, null);
        Assert.assertEquals("2010-01-01 00:00:00", sdf.format(result.getStart()));
        Assert.assertEquals("2010-01-01 00:00:00", sdf.format(result.getEnd()));
    }

    @Test
    public void testLastYear() {
        TimePeriod result;

        result = TimePeriodFactory.fromRelativeTimePeriodWithBase(RelativeTimePeriod.LAST_YEAR, dateTest, null);
        Assert.assertEquals("2009-01-01 00:00:00", sdf.format(result.getStart()));
        Assert.assertEquals("2009-12-31 23:59:59", sdf.format(result.getEnd()));

        result = TimePeriodFactory.fromRelativeTimePeriodWithBase(RelativeTimePeriod.LAST_YEAR, dateMidnight, null);
        Assert.assertEquals("2009-01-01 00:00:00", sdf.format(result.getStart()));
        Assert.assertEquals("2009-12-31 23:59:59", sdf.format(result.getEnd()));

        result = TimePeriodFactory.fromRelativeTimePeriodWithBase(RelativeTimePeriod.LAST_YEAR, dateBeforeMidnight,
                null);
        Assert.assertEquals("2009-01-01 00:00:00", sdf.format(result.getStart()));
        Assert.assertEquals("2009-12-31 23:59:59", sdf.format(result.getEnd()));

        result = TimePeriodFactory.fromRelativeTimePeriodWithBase(RelativeTimePeriod.LAST_YEAR, afterLeap, null);
        Assert.assertEquals("2007-01-01 00:00:00", sdf.format(result.getStart()));
        Assert.assertEquals("2007-12-31 23:59:59", sdf.format(result.getEnd()));

        result = TimePeriodFactory.fromRelativeTimePeriodWithBase(RelativeTimePeriod.LAST_YEAR, yearStart, null);
        Assert.assertEquals("2009-01-01 00:00:00", sdf.format(result.getStart()));
        Assert.assertEquals("2009-12-31 23:59:59", sdf.format(result.getEnd()));
    }
}