package nl.telecats.foundation.commons.util;

import java.text.ParseException;

import junit.framework.Assert;

import org.junit.Test;

// @RunWith(SpringJUnit4ClassRunner.class)
// @ContextConfiguration(locations = { "classpath:rating-commons-test-context.xml" })
public class TimeZoneTest {

    @Test
    public void testIso8691tzFormat() {
        String validTime = "2010-01-01T00:00:00+0100";
        String invalidTime = "2010-10-01T00:00:00+0100";
        boolean valid = TimeZoneHelper.isValidIso8601tzFormat(validTime);
        boolean invalid = TimeZoneHelper.isValidIso8601tzFormat(invalidTime);

        Assert.assertTrue("Valid iso8601tz time", valid);
        Assert.assertTrue("Invalid iso8601tz time", invalid);

    }

    @Test
    public void testTimeZone() {

        String time1 = "2011-01-01T00:00:00+0100";
        String time1gmt0 = "2010-12-31T23:00:00+0000";

        String time2 = "2011-01-01T00:00:00-0100";
        String time2gmt0 = "2011-01-01T01:00:00+0000";
        String time3 = "2011-01-01T00:00:00+0200";
        String time3gmt0 = "2010-12-31T22:00:00+0000";
        String time4 = "2011-01-01T00:00:00+0000";
        String time4gmt0 = "2011-01-01T00:00:00+0000";

        Assert.assertFalse("Time is incorrecly recognized as GMT0", TimeZoneHelper.isGmt0(time1));
        Assert.assertFalse("Time is incorrecly recognized as GMT0", TimeZoneHelper.isGmt0(time2));
        Assert.assertFalse("Time is incorrecly recognized as GMT0", TimeZoneHelper.isGmt0(time3));
        Assert.assertTrue("Time is incorrecly recognized as not GMT0", TimeZoneHelper.isGmt0(time4));

        Assert.assertTrue("Time is incorrecly recognized as not GMT0", TimeZoneHelper.isGmt0(time1gmt0));
        Assert.assertTrue("Time is incorrecly recognized as not GMT0", TimeZoneHelper.isGmt0(time2gmt0));
        Assert.assertTrue("Time is incorrecly recognized as not GMT0", TimeZoneHelper.isGmt0(time3gmt0));
        Assert.assertTrue("Time is incorrecly recognized as not GMT0", TimeZoneHelper.isGmt0(time4gmt0));

        int t1offset = testTimeZoneOffset(time1, 3600000);
        int t2offset = testTimeZoneOffset(time2, -3600000);
        int t3offset = testTimeZoneOffset(time3, 7200000);
        int t4offset = testTimeZoneOffset(time4, 0);

        testTimeToGmt0(time1, time1gmt0);
        testTimeToGmt0(time2, time2gmt0);
        testTimeToGmt0(time3, time3gmt0);
        testTimeToGmt0(time4, time4gmt0);

        testChangeTimeZone(time1gmt0, t1offset, time1);
        testChangeTimeZone(time2gmt0, t2offset, time2);
        testChangeTimeZone(time3gmt0, t3offset, time3);
        testChangeTimeZone(time4gmt0, t4offset, time4);

    }

    private void testTimeToGmt0(String time, String result) {
        try {
            String gmt0 = TimeZoneHelper.timeToGmt0(time);
            // System.out.println();
            Assert.assertEquals("Unexpected result in toGmt0 conversion: Time: [" + time + "] Expected: [" + result
                    + "] Actual: [" + gmt0 + "]", result, gmt0);

        } catch (ParseException e) {
            Assert.fail("Parse exception in time: " + time);
            e.printStackTrace();
        }

    }

    private int testTimeZoneOffset(String time, int timezone) {
        int offset = 0;
        try {
            offset = TimeZoneHelper.getTimeZoneAsInteger(time);
            Assert.assertEquals("Timezone " + time + " integer value not equal", timezone, offset);

        } catch (ParseException e) {
            Assert.fail("Parse exception in time: " + time);
            e.printStackTrace();
        }
        return offset;
    }

    private void testChangeTimeZone(String time, int offset, String result) {
        try {
            String newTime = TimeZoneHelper.changeTimeZone(time, offset);
            Assert.assertEquals("Unexpected result in changeTimezone for time: " + time + ", offset: " + offset
                    + " expected: " + result + "actual: " + newTime, result, newTime);
        } catch (ParseException e) {
            Assert.fail("Parse exception in time: " + time);
            e.printStackTrace();
        }
    }

}
