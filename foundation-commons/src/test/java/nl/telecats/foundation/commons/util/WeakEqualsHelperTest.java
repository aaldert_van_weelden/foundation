package nl.telecats.foundation.commons.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import junit.framework.Assert;

import org.junit.Test;

public class WeakEqualsHelperTest {

    /*------------TEST------------*/
    public class TestClass {
        public class TestNestClass extends TestClass {
            public TestNestClass(int a, int b, ArrayList list) {
                super(a, b, list);
            }

            public void setD(int d) {
                this.d = d;
            }

            public void putMap(String s, Integer i) {
                map.put(s, i);
            }

            private int d;
            private Map<String, Integer> map = new HashMap<String, Integer>();
        }

        private int a;
        private int b;
        private ArrayList list = new ArrayList();
        private String c = "Hello!!!";

        public TestClass(int a, int b, ArrayList list) {
            this.a = a;
            this.b = b;
            this.list = list;
        }

        public int getA() {
            return a;
        }

        public int getB() {
            return b;
        }

        public String getC() {
            return c;
        }

        public void setC(String c) {
            this.c = c;
        }

        public ArrayList getList() {
            return list;
        }

        public TestNestClass createNest() {
            return new TestNestClass(a, b, list);
        }
    }

    public TestClass createTestClass(int a, int b, ArrayList list) {
        return new TestClass(a, b, list);
    }

    @Test
    public void test() {
        ArrayList<Integer> ar1 = new ArrayList<Integer>();
        ar1.add(1);
        ar1.add(5);
        ar1.add(7);
        ArrayList<Integer> ar2 = new ArrayList<Integer>();
        ar2.add(1);
        ar2.add(5);
        ar2.add(7);
        TestClass tc1 = createTestClass(1, 15, ar1);
        TestClass tc2 = createTestClass(1, 15, ar2);
        TestClass.TestNestClass tnc = tc1.createNest();
        TestClass.TestNestClass tnc2 = tc2.createNest();
        tnc.putMap("Hello", 1);
        tnc2.putMap("Hello", 1);
        tnc.putMap("goodbye", 1);
        tnc2.putMap("goodbye", 1);
        Assert.assertTrue("tnc != tnc2", WeakEqualsHelper.doEquals(tnc, tnc2));

    }
}
