package nl.telecats.foundation.commons.util;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * TestSpringBeanLogger is functioning as expected
 * <p/>
 * NOTE: We don't actually test console output.
 * 
 * @author raymond
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:foundation-common-test-context.xml" })
public class SpringBeanLoggerTest {

    private final static int EXPECTED_BEAN_SIZE = 6;

    @Autowired
    SpringBeanLogger springBeanLogger;

    @Test
    public void testSpringBeanLogger() {
        String[] beans = springBeanLogger.findBeanDefinitions();
        Assert.assertTrue("found " + EXPECTED_BEAN_SIZE + " beans:" + beans.length, beans.length == EXPECTED_BEAN_SIZE);

        Assert.assertEquals(beans[0], "springBeanLogger");
        Assert.assertEquals(beans[1], "beanDependencyChecker");
        Assert.assertEquals(beans[2], "org.springframework.context.annotation.internalConfigurationAnnotationProcessor");
        Assert.assertEquals(beans[3], "org.springframework.context.annotation.internalAutowiredAnnotationProcessor");
        Assert.assertEquals(beans[4], "org.springframework.context.annotation.internalRequiredAnnotationProcessor");
        Assert.assertEquals(beans[5], "org.springframework.context.annotation.internalCommonAnnotationProcessor");
    }
}
