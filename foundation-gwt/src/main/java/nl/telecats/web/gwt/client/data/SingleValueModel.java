package nl.telecats.web.gwt.client.data;

import com.extjs.gxt.ui.client.data.BaseModelData;

/**
 * Single value model which can be used in a ListStore. This helps you implementing your ListViews
 * 
 * @author raymond
 *
 * @param <V>
 */
public class SingleValueModel <V> extends BaseModelData {
    
    private static final long serialVersionUID = -1085851970634919776L;
    
    public static final String VALUE = "VALUE";
    
    public SingleValueModel(V value) {
        setValue(value);
    }

    @SuppressWarnings("unchecked") // the cast IS needed for GWT
    public V getValue() {
        return (V)get(VALUE);
    }
    
    public void setValue(V value) {
        set(VALUE, value);
    }
}
