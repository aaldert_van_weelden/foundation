package nl.telecats.web.gwt.client.security;


public interface SecuredAsyncCallbackListener {
    /**
     * Called when a failure occurs, after the {@link SecuredAsyncCallback}
     * handle.
     * 
     * @param throwable
     */
    public void doOnFailure(Throwable throwable);
    
    /**
     * Called when a response is received successfully
     */
    public void doOnSuccess(Object object);
}