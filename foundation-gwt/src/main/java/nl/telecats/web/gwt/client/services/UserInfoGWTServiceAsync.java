package nl.telecats.web.gwt.client.services;

import nl.telecats.web.gwt.server.services.UserInfoGWTServiceImpl;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * @see UserInfoGWTServiceImpl
 * @author raymond
 * 
 */
public interface UserInfoGWTServiceAsync {

    public void showUserInfo(AsyncCallback<UserInfoDTO> callback);

    public void isSsoEnabled(AsyncCallback<Boolean> callback);

    public void hasRole(String role, AsyncCallback<Boolean> callback);
}
