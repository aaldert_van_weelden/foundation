package nl.telecats.web.gwt.client.widgets.form;

import nl.telecats.web.gwt.client.data.KeyValueModel;

import com.extjs.gxt.ui.client.widget.form.Field;

public class ComboBoxKeyValueModelValidator<W extends KeyValueModel<? extends Object, ? extends Object>>
        extends AbstractComboBoxValidator<W> {

    public ComboBoxKeyValueModelValidator(Field<W> field) {
        super(field);
    }

    @Override
    protected Object getValue(W selectedEntry) {
        return selectedEntry.getValue();
    }



}
