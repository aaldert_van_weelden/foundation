package nl.telecats.web.gwt.client.services;

import nl.telecats.web.gwt.client.data.paged.PagedList;
import nl.telecats.web.gwt.server.services.PagedListDataGWTServiceImpl;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * @see PagedListDataGWTServiceImpl
 * @author Raymond Domingo
 * 
 */
@RemoteServiceRelativePath("services/user/pagedListDataGwtService.gwt")
public interface PagedListDataGWTService extends RemoteService {

    public static class Util {

        public static PagedListDataGWTServiceAsync getInstance() {
            return GWT.create(PagedListDataGWTService.class);
        }
    }

    PagedList fetchPagedList(String cacheId, int nextOffset, int maxDataSize);

}
