package nl.telecats.web.gwt.server.data.paged;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import nl.telecats.web.gwt.client.data.paged.PagedList;

/**
 * Server side cache for supporting paged sets of data to be send to client in
 * chunks.
 * 
 * TODO clean cache entry after inactive for X minuges
 * 
 * @author Raymond Domingo
 * 
 * @see PagedListSender, PagedListReceiver
 */
public class PagedListServerCache {
    private static Map<String, CacheEntry> cache = new HashMap<String, CacheEntry>();

    private static PagedListServerCache singleton;

    private PagedListServerCache() {
    }

    /**
     * @return singleton instance of this cache
     */
    public static PagedListServerCache getInstance() {
        if (singleton == null) {
            singleton = new PagedListServerCache();
        }
        return singleton;
    }

    /**
     * Push data to this ache
     * 
     * @param data
     *            to cache
     * @return id of this data in the cache, needed for retrieving the data at a
     *         later time
     */
    public String push(List data) {
        String id = UUID.randomUUID().toString();
        cache.put(id, new CacheEntry(data));
        return id;
    }

    /**
     * 'Pop' data from the cache.
     * 
     * @param cacheEntryId
     *            of cache entry
     * @param offset
     *            of data
     * @return subset of data
     */
    @SuppressWarnings("unchecked")
    public PagedList pop(String cacheEntryId, int offset, int maxDataSize) {
        CacheEntry cacheEntry = cache.get(cacheEntryId);
        ArrayList subList = new ArrayList();
        int index = offset;
        for (; index < offset + maxDataSize && index < cacheEntry.getData().size(); index++) {
            subList.add(cacheEntry.getData().get(index));
        }

        if (offset + subList.size() == cacheEntry.getData().size()) {
            // no need to keep data in cache any longer
            cache.remove(cacheEntryId);
        } else {
            // refresh inactivity ts
            cacheEntry.setInactivityTS(System.currentTimeMillis());
        }
        return new PagedList(subList, offset, cacheEntry.getData().size(), cacheEntryId);
    }

    class CacheEntry {
        List data;
        long ts = System.currentTimeMillis();

        public CacheEntry(List data) {
            this.data = data;
        }

        public long getTS() {
            return ts;
        }

        public void setInactivityTS(long ts) {
            this.ts = ts;
        }

        protected List getData() {
            return data;
        }
    }
}