package nl.telecats.web.gwt.client.security;

import java.util.ArrayList;

import net.orcades.spring.gwt.security.client.GWTAccessDeniedException;
import net.orcades.spring.gwt.security.client.GWTAuthentication;
import net.orcades.spring.gwt.security.client.GWTSecurityException;
import net.orcades.spring.gwt.security.client.GWTSecurityModule;
import net.orcades.spring.gwt.security.client.IGWTSecurityExceptionVisitor;
import nl.telecats.web.gwt.client.widgets.BusyIndicator;
import nl.telecats.web.gwt.client.widgets.error.ErrorDialog;
import nl.telecats.web.gwt.client.widgets.login.DefaultLoginDialog;
import nl.telecats.web.gwt.client.widgets.login.LoginDialog;

import com.allen_sauer.gwt.log.client.Log;
import com.extjs.gxt.ui.client.Registry;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.ClickListener;

/**
 * Adapter Generic callback to handle security exception.
 * 
 * Can optionally be used together with the {@link BusyIndicator} and the {@link ErrorDialog}. Use the appropriate
 * constructors.
 * 
 * @author NOUGUIER Olivier olivier@orcades.net, olivier.nouguier@gmail.com
 * 
 * @author Raymond Domingo (improved login dialog, added listener mechanism)
 * 
 * @author Raymond Domingo (added support for josso, solution might also work for other systems) See GWTSecurityModule
 * 
 * @param <T>
 * 
 * @see net.orcades.spring.gwt.security.client.rpc.SecuredAsyncCallback
 * @see net.orcades.spring.gwt.security.client.GWTSecurityModule
 */
public abstract class SecuredAsyncCallback<T> implements AsyncCallback<T> {

    private ArrayList<SecuredAsyncCallbackListener> listeners = new ArrayList<SecuredAsyncCallbackListener>();

    /**
     * Exception Visitor.
     */
    private IGWTSecurityExceptionVisitor securityExceptionVisitor;

    /**
     * Constructor. When using this constructor, the request will be lost if the user is not yet authenticated. A
     * {@link BusyIndicator} and an {@link ErrorDialog} will be used.
     * 
     */
    public SecuredAsyncCallback() {
        this(null, true, true);
    }

    /**
     * Constructor. When using this constructor and if parameter is not null, the clickListener will be invoked on a
     * successful authentication. A {@link BusyIndicator} and an {@link ErrorDialog} will be used.
     * 
     * @deprecated ClickListener is deprecated
     * @param clickListener
     *            to call on successful authentication, may be null.
     */
    public SecuredAsyncCallback(ClickListener clickListener) {
        this(clickListener, true, true);
    }

    /**
     * Constructor. Use this constructor to indicate whether or not to use the {@link BusyIndicator} and the
     * {@link ErrorDialog}
     * 
     * @param useBusyIndicator
     *            use the busy indicator
     * @param useErrorDialog
     *            use the error dialog
     */
    public SecuredAsyncCallback(boolean useBusyIndicator, boolean useErrorDialog) {
        this(null, useBusyIndicator, useErrorDialog);
    }

    /**
     * Constructor. The clickListener will be invoked on a successful authentication. ALso use this constructor to
     * indicate whether or not to use the {@link BusyIndicator} and the {@link ErrorDialog}
     * 
     * @deprecated ClickListener is deprecated
     * 
     * @param clickListener
     *            to call on successful authentication, may be null.
     * @param useBusyIndicator
     *            use the busy indicator
     * @param useErrorDialog
     *            use the error dialog
     */
    public SecuredAsyncCallback(ClickListener clickListener, boolean useBusyIndicator, boolean useErrorDialog) {
        Log.trace("created SecuredAsyncCallback, clickListener:" + clickListener + ", useBusyIndicator:"
                + useBusyIndicator + ", useErrorDialog:" + useErrorDialog);
        securityExceptionVisitor = new SecurityExceptionVisitor(clickListener);
        if (useBusyIndicator) {
            BusyIndicator.getInstance().monitor(this);
        }
        if (useErrorDialog) {
            ErrorDialog.Util.getInstance().monitor(this);
        }
    }

    /**
     * Handle the exceptions, the function {@link SecuredAsyncCallback#doOnFailure(Throwable)} must be overridden to do
     * something on failure.
     */
    public final void onFailure(Throwable throwable) {
        Log.debug("onFailure:" + throwable.getMessage(), throwable);
        boolean securityRedirectPerformed = false;

        for (SecuredAsyncCallbackListener listener : listeners) {
            listener.doOnFailure(throwable);
        }

        if (throwable instanceof GWTSecurityException) {
            Log.info("onFailure, recognized GWTSecurityException. forward to securityExceptionVisitor");
            securityRedirectPerformed = true;
            GWTSecurityException securityException = (GWTSecurityException) throwable;
            securityException.accept(securityExceptionVisitor);

        } else if (GWTSecurityModule.isSecurityRedirect(throwable)) {
            // support redirection from 3rth party security frameworks
            Log.info("onFailure, recognized securityRedirect. ");
            securityRedirectPerformed = GWTSecurityModule.handleSecurityRedirect(throwable);

        }

        if (securityRedirectPerformed) {
            // prevent the permanent masked screen
            Log.info("onFailure, security redirect has been performed; busy indicator updated");
            BusyIndicator.getInstance().doOnFailure(throwable);
        } else {
            // When exception didn't trigger the securityRedirect, handle it
            Log.error(
                    "onFailure, unexpected exception. Inform listeners and invoke doOnFailure:"
                            + throwable.getMessage(), throwable);
            doOnFailure(throwable);
        }
    }

    native void redirect(String url)
    /*-{
        $wnd.location.replace(url);

    }-*/;

    /**
     * Called when a failure occurs, after the {@link SecuredAsyncCallback} handle.
     * 
     * @param throwable
     */
    abstract protected void doOnFailure(Throwable throwable);

    /**
     * Handle the success. The function {@link SecuredAsyncCallback#doOnSuccess(T)} must be overridden to do something
     * on success.
     */
    public final void onSuccess(T object) {
        Log.info("onSuccess:" + object);
        doOnSuccess(object);
        for (SecuredAsyncCallbackListener listener : listeners) {
            listener.doOnSuccess(object);
        }
    }

    /**
     * Called when a response is received successfully
     */
    abstract protected void doOnSuccess(T object);

    /**
     * When a secured async call is performed but the user can't be authenticated, by default the login dialog is
     * displayed. After the user re-authenticates we can't resubmit the sec asyn call.
     * <p/>
     * To detect a successful re-authentication this method is called. This enables the developer to resubmit the sec
     * async call if needed.
     * 
     * @param authentication
     */
    public void doOnSuccessfulReauthentication(GWTAuthentication authentication) {
        Log.info("doOnSuccessfulReauthentication:" + authentication);
    }

    public void registerListener(SecuredAsyncCallbackListener listener) {
        listeners.add(listener);
    }

    public void deregisterListener(SecuredAsyncCallbackListener listener) {
        listeners.remove(listener);
    }

    public class SecurityExceptionVisitor implements IGWTSecurityExceptionVisitor {

        private ClickListener clickListener;

        /**
         * @deprecated ClickListener is deprecated
         * @param clickListener
         * @param useBusyIndicator
         */
        public SecurityExceptionVisitor(ClickListener clickListener) {
            this.clickListener = clickListener;
        }

        public void visit(GWTSecurityException authorizationRequiredException) {
            Log.info("SecurityExceptionVisitor (Auth required): visit(GWTSecurityException authorizationRequiredException)");
            LoginDialog loginPanel = instantiateLoginPanel();
            loginPanel.setAuthServiceEndPoint(authorizationRequiredException.getAuthServiceEndPoint());
            loginPanel.setMessage(authorizationRequiredException.getMessage());
            loginPanel.setClickListener(clickListener);
            loginPanel.setSecuredAsyncCallback(SecuredAsyncCallback.this);
            loginPanel.show();
        }

        public void visit(GWTAccessDeniedException accessDeniedException) {
            Log.info("SecurityExceptionVisitor (Access denied): visit(GWTAccessDeniedException accessDeniedException)");
            LoginDialog loginPanel = instantiateLoginPanel();
            loginPanel.setAuthServiceEndPoint(accessDeniedException.getAuthServiceEndPoint());
            loginPanel.setMessage(accessDeniedException.getMessage());
            loginPanel.setClickListener(clickListener);
            loginPanel.setSecuredAsyncCallback(SecuredAsyncCallback.this);
            loginPanel.show();
        }

        private LoginDialog instantiateLoginPanel() {
            Log.info("SecurityExceptionVisitor: instantiateLoginPanel");
            LoginDialog lp = (LoginDialog) Registry.get(LoginDialog.LOGIN_POPUP_PANEL);
            if (lp != null) {
                Log.info("SecurityExceptionVisitor: instantiate custom LoginPanel found in Registry:" + lp);
                lp.reset();
                return lp;
            } else {
                // the default login panel
                Log.info("SecurityExceptionVisitor: instantiate DefaultLoginDialog");
                return DefaultLoginDialog.getInstance();
            }
        }
    }
}
