package nl.telecats.web.gwt.client.utils;

import com.extjs.gxt.ui.client.widget.Info;

/**
 * Shows info balloons containing messages
 * 
 * @author raymond
 */
public class NotificationManager {

	private static NotificationManager instance;
	private boolean enabled = true;

	private NotificationManager() {
	}
	
	public boolean isEnabled() {
		return enabled;
	}
	
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	
	public static NotificationManager getInstance() {
		if (instance==null) {
			instance = new NotificationManager();
		}
		return instance;
	}
	
	public void display(String title, String message, String... params) {
		if (enabled) Info.display(title, message, params);
	}
}
