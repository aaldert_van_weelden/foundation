package nl.telecats.web.gwt.client.widgets.panel;

import nl.telecats.web.gwt.client.TCF;

import com.extjs.gxt.ui.client.widget.MessageBox;
import com.extjs.gxt.ui.client.widget.form.Field;
import com.extjs.gxt.ui.client.widget.form.FormPanel;

/**
 * An extension on {@link FormPanel}. It adds the function
 * {@link #isFormValid()} to the Panel, which checks if all the parent fields
 * are valid.
 * 
 * @author "Eelco Brolman"
 * 
 */
public class ValidationFormPanel extends FormPanel {

    /**
     * Check if all the Fields of the FormPanel do validate.
     * 
     * @param showDefaultError
     *            Show a default error box when the validation turned out to be
     *            not valid
     * @return true if all fields validate, false otherwise
     */
    @SuppressWarnings("unchecked")
    public boolean isFormValid(boolean showDefaultError) {
        boolean validated = true;
        Field focusField = null;
        for (Field field : this.getFields()) {
            if (focusField == null)
                focusField = field;
            validated &= field.validate();

        }
        if (!validated && showDefaultError) {
            MessageBox.alert(TCF.MESSAGES.FormDoesNotValidateTitle(),
                    TCF.MESSAGES.FormDoesNotValidate(), null);
            if (focusField != null) {
                focusField.focus();
            }
        }
        return validated;
    }

    /**
     * Check if all the Fields of the FormPanel do validate. Show the default
     * error message if the form does not validate.
     * 
     * @return true if all fields validate, false otherwise
     */
    public boolean isFormValid() {
        return isFormValid(true);
    }

    /**
     * Clear all the invalid flags for all form fields
     */
    @SuppressWarnings("unchecked")
    public void clearInvalid() {
        for (Field field : this.getFields()) {
            field.clearInvalid();
        }
    }
}
