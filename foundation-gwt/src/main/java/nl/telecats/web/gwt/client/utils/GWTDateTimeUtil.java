package nl.telecats.web.gwt.client.utils;

import java.util.Date;

import com.google.gwt.i18n.client.DateTimeFormat;

/**
 * Convenient class for formatting dates.
 * 
 * TODO, move this to generic foundation and create non-GWT counterpart
 * 
 * @author eelco
 * 
 */
public class GWTDateTimeUtil {

    public static final DateTimeFormat df_ISO_8601 = DateTimeFormat
            .getFormat("yyyy-MM-dd'T'HH:mm:ss");
    public static final DateTimeFormat df_full_date = DateTimeFormat
            .getFormat("EEEE d MMMM yyyy");
    public static final DateTimeFormat df_full_date_time = DateTimeFormat
            .getFormat("EEEE d MMMM yyyy HH:mm:ss");

    /**
     * Returns the full date for an iso 8601 date string. E.g.:
     * 2009-04-07T12:33:00 -> dinsdag 7 april 2009 (locale dependant)
     * 
     * @param iso8601DateTime
     *            the date in ISO 8601 format
     * @return the full date string, or null if the iso8601DateTime was null
     */
    public static String getFullDate(String iso8601DateTime) {
        if (iso8601DateTime == null) {
            return null;
        }
        return getFullDate(df_ISO_8601.parse(iso8601DateTime));
    }

    /**
     * Returns the full date for an iso 8601 date string. E.g.:
     * 2009-04-07T12:33:00 -> dinsdag 7 april 2009 (locale dependant)
     * 
     * @param iso8601DateTime
     *            the date in ISO 8601 format
     * @return the full date string, or null if the iso8601DateTime was null
     */
    public static String getFullDate(Date iso8601DateTime) {
        if (iso8601DateTime == null) {
            return null;
        }
        return df_full_date.format(iso8601DateTime);
    }

    /**
     * Gets now in ISO 8601 format
     * 
     * @return the current date in ISO 8601 format
     */
    public static String getNowISO8601() {
        return df_ISO_8601.format(new Date());
    }

    /**
     * Returns the date and time
     * 
     * @param iso8601DateTime
     *            the date in ISO 8601 format
     * @return the full date time string, or null if the iso8601DateTime was
     *         null
     */
    public static String getFullDateTime(String iso8601DateTime) {
        if (iso8601DateTime == null) {
            return null;
        }
        return getFullDateTime(df_ISO_8601.parse(iso8601DateTime));
    }

    /**
     * Returns the date and time
     * 
     * @param iso8601DateTime
     *            the date in ISO 8601 format
     * @return the full date time string, or null if the iso8601DateTime was
     *         null
     */
    public static String getFullDateTime(Date iso8601DateTime) {
        if (iso8601DateTime == null) {
            return null;
        }
        return df_full_date_time.format(iso8601DateTime);
    }

    /**
     * Get a Date object from an iso 8601 date
     * 
     * @param iso8601DateTime
     *            the date in ISO 8601 format
     * @return the Date object, or null if the iso8601DateTime was null
     */
    public static Date getDate(String iso8601DateTime) {
        if (iso8601DateTime == null) {
            return null;
        }
        return df_ISO_8601.parse(iso8601DateTime);
    }
}
