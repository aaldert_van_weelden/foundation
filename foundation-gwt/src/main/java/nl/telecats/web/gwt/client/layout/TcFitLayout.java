package nl.telecats.web.gwt.client.layout;

import com.extjs.gxt.ui.client.core.El;
import com.extjs.gxt.ui.client.util.Margins;
import com.extjs.gxt.ui.client.util.Size;
import com.extjs.gxt.ui.client.widget.Component;
import com.extjs.gxt.ui.client.widget.ComponentHelper;
import com.extjs.gxt.ui.client.widget.Container;
import com.extjs.gxt.ui.client.widget.Info;
import com.extjs.gxt.ui.client.widget.Layout;
import com.extjs.gxt.ui.client.widget.layout.LayoutData;

/**
 * This is a base class for layouts that contain a single item that automatically expands to fill the layout's
 * container.
 * 
 * <p />
 * Child Widgets are:
 * <ul>
 * <li><b>Sized</b> : Yes - expands to fill parent container, sizing can be limited by using TcFitData and SizeLimits</li>
 * <li><b>Positioned</b> : Yes, margins can be applied to</li>
 * </ul>
 * 
 * <p />
 * In the following code, the child panel will be limited to width between 600 and 1000 and height between 200 and
 * 400.</br> A margin of 30 pixels will be applied to every side of the panel:
 * 
 * <code><pre>
   TcLayoutContainer container = new TcLayoutContainer();
   container.setLayout(new TcFitLayout());
  
   ContentPanel panel = new ContentPanel();
   container.add(panel, new TcFitData(new SizeLimits(600, 1000, 200, 400), new Margins(30)));
  
   
 * </pre></code>
 * 
 * @see TcFitData
 * @see SizeLimits
 * @see Margins
 * 
 * @author Aaldert van Weelden
 */
public class TcFitLayout extends Layout {

    private Size parentSize;
    private int width;
    private int height;
    private boolean isScrollEnabled;
	private boolean isScrollBarVisible;
	public static final int SCROLLBAR_WIDTH = 18;

    /**
     * Creates a new fit layout instance.
     */
    public TcFitLayout() {
        monitorResize = true;
    }

    @Override
    protected void onLayout(Container<?> container, El target) {

        if (container.getItemCount() == 0) {
            return;
        }
        activeItem = activeItem != null ? activeItem : container.getItem(0);
        super.onLayout(container, target);
        // getting layout data
        TcFitData layoutData = null;

        LayoutData d = ComponentHelper.getLayoutData(activeItem);
        if (d != null && d instanceof TcFitData) {
            layoutData = (TcFitData) d;
        }

        if (layoutData == null) {
            layoutData = new TcFitData();
        }

        // getting sizing limits
        SizeLimits limits = layoutData.getSizeLimits();
        isScrollEnabled = layoutData.isScrollEnabled();
        isScrollBarVisible=false;
        parentSize = target.getStyleSize();
        if (limits != null) {
            int minW = limits.getMinWidth();
            int maxW = limits.getMaxWidht();
            int minH = limits.getMinHeight();
            int maxH = limits.getMaxHeight();

            int parentW = parentSize.width;
            int parentH = parentSize.height;

            int w = parentW;
            int h = parentH;
            if (parentW < minW && minW != -1)
                w = minW;
            if (parentW > maxW && maxW != -1)
                w = maxW;
            if (parentH < minH && minH != -1) {
                // subtract scrollbar width here
                if (isScrollEnabled){
                	isScrollBarVisible=true;
                    w -= SCROLLBAR_WIDTH;
                }
                h = minH;
               
            }
            if (parentH > maxH && maxH != -1){
                h = maxH;
            }
            Size size = new Size(w, h);
            setItemSize(activeItem, size);
        } else {
            setItemSize(activeItem, parentSize);
        }
    }

    protected void setItemSize(Component item, Size size) {
        if (item != null && item.isRendered()) {
            size.width -= getSideMargins(item);
            size.height -= item.el().getMargins("tb");
            width = size.width;
            height = size.height;
            setSize(item, size.width, size.height);
        }
    }

    /**
     * Returns the size of the parent container
     * 
     * @return
     */
    public Size getParentSize() {
        return parentSize;
    }

    /**
     * returns the container width, with optional offset and minimum
     * 
     * @param minWidth
     *            the minimum width to set, value -1 sets no limit
     * @param maxWidth
     *            the maximum width to set, value -1 sets no limit
     * @param offset
     *            The offset added to the returned width
     * @return the width in pixels
     */
    public int getWidth(int minWidth, int maxWidth, int offset) {
        int w = width + offset;
        if (w < minWidth && minWidth != -1)
            w = minWidth;
        if (w > maxWidth && maxWidth != -1) {
            w = maxWidth;
        }
        return w;
    }

    /**
     * returns the container height as set by the parent, with optional offset.
     * 
     * @param minHeight
     *            The minimum height to set, value -1 sets no limit
     * @param maxHeight
     *            The maximm height to set, value -1 sets no limit
     * @param offset
     *            The offset added to the returned height
     * @return the height in pixels
     */
    public int getHeight(int minHeight, int maxHeight, int offset) {
        int h = height + offset;
        if (h < minHeight && minHeight != -1)
            h = minHeight;
        if (h > maxHeight && maxHeight != -1)
            h = maxHeight;
        return h;
    }

    /**
     * returns the container height set by the parent, with optional offset.
     * 
     * @param offset
     *            The offset added to the returned height
     * @return the height in pixels
     */
    public int getHeight(int offset) {
        return height + offset;

    }

    /**
     * returns the container width as set by the parent.
     * 
     * @return the width in pixels
     */
    public int getWidth() {
        return width;
    }

    /**
     * returns the container height as set by the parent.
     * 
     * @return the height in pixels
     */
    public int getHeight() {
        return height;
    }
    
    /**
     * Returns true if a vertical scrollbar is added
     * @return
     */
    public boolean isScrollBarVisible(){
    	return this.isScrollBarVisible;
    }
}