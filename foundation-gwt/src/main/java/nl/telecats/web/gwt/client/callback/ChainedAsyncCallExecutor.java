package nl.telecats.web.gwt.client.callback;

import java.util.HashMap;
import java.util.Map;

/**
 * This class is responsible for executing ChainedAsyncCalls
 * 
 * @author raymond
 * 
 */
public class ChainedAsyncCallExecutor {
    /**
     * Chain these together and execute the chain.
     * <p/>
     * NOTE: This will instantiate a new ChainRegistry for this execution.
     * 
     * @param chainedCalls
     */
    public static void executeChain(ChainedAsyncCall... chainedCalls) {
        Map<String, Object> chainRegistryInstance = new HashMap<String, Object>();
        executeChain(chainRegistryInstance, chainedCalls);
    }

    /**
     * Chain these together and execute the chain.
     * 
     * @param chainRegistryInstance
     *            , chainRegistry to use during the execution of this chain
     * @param chainedCalls
     */
    public static void executeChain(Map<String, Object> chainRegistryInstance, ChainedAsyncCall... chainedCalls) {
        if (chainedCalls.length > 0) {

            // chain the calls and set registry
            chainedCalls[0].setChainRegistry(chainRegistryInstance);
            if (chainedCalls.length > 1) {
                for (int cursor = 1; cursor < (chainedCalls.length); cursor++) {
                    chainedCalls[cursor - 1].setSuccessorAsyncCall(chainedCalls[cursor]);
                    chainedCalls[cursor].setChainRegistry(chainRegistryInstance);
                }
            }

            // execute the chain
            chainedCalls[0].execute();
        }
    }
}