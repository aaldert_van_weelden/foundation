package nl.telecats.web.gwt.client.widgets.grid;

import com.extjs.gxt.ui.client.data.LoadEvent;
import com.extjs.gxt.ui.client.data.PagingLoader;
import com.extjs.gxt.ui.client.event.LoadListener;
import com.extjs.gxt.ui.client.util.IconHelper;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.toolbar.LabelToolItem;
import com.extjs.gxt.ui.client.widget.toolbar.PagingToolBar;

/**
 * The current version (2.1.1) of the GXT {@link PagingToolBar} has a few shortcomings.
 * <p/>
 * setMessages does not actually set the labels of the tool bar. This is fixed in GXT SVN revision 2126.
 * <p/>
 * If the a load is triggered before another one is finished, the paging tool bar gets permanently disabled. GXT 3
 * should have a solution for this.
 * <p/>
 * The {@link PatchedPagingToolBar} was created to overcome these problems.
 * 
 * @author danny
 */
public class PatchedPagingToolBar extends PagingToolBar {

    protected boolean savedEnableState = false;
    protected boolean isLoading = false;

    /**
     * Constructor
     * 
     * @param pageSize
     */
    public PatchedPagingToolBar(int pageSize) {
        super(pageSize);
    }

    /**
     * Patched version of setMessages, that actually updates the component messages (is fixed in GXT SVN revision 2126).
     * 
     * @see com.extjs.gxt.ui.client.widget.toolbar.PagingToolBar#setMessages(com.extjs.gxt.ui.client.widget.toolbar.PagingToolBar.PagingToolBarMessages)
     */
    @Override
    public void setMessages(PagingToolBarMessages messages) {
        super.setMessages(messages);

        ((Button) getItem(0)).setToolTip(messages.getFirstText());
        ((Button) getItem(1)).setToolTip(messages.getPrevText());
        ((Button) getItem(7)).setToolTip(messages.getNextText());
        ((Button) getItem(8)).setToolTip(messages.getLastText());
        ((Button) getItem(10)).setToolTip(messages.getRefreshText());
        ((LabelToolItem) getItem(3)).setLabel(messages.getBeforePageText());
    }

    /**
     * Patched version of bind, that does not get confused by double loading (is likely to be fixed in GXT 3). N.B.:
     * double loading should not occur in the first place; always check your application first as to why a load gets
     * triggered while another one has not returned yet.
     * 
     * @see com.extjs.gxt.ui.client.widget.toolbar.PagingToolBar#bind(com.extjs.gxt.ui.client.data.PagingLoader)
     */
    @Override
    public void bind(PagingLoader<?> loader) {
        if (this.loader != null) {
            this.loader.removeLoadListener(loadListener);
        }
        this.loader = loader;
        if (loader != null) {
            loader.setLimit(pageSize);
            if (loadListener == null) {
                loadListener = new LoadListener() {
                    public void loaderBeforeLoad(LoadEvent le) {
                        if (!isLoading) {
                            savedEnableState = isEnabled();
                            isLoading = true;
                        }

                        if (isEnabled()) {
                            setEnabled(false);
                        }

                        refresh.setIcon(IconHelper.createStyle("x-tbar-loading"));
                    }

                    public void loaderLoad(LoadEvent le) {
                        refresh.setIcon(getImages().getRefresh());
                        setEnabled(savedEnableState);
                        isLoading = false;
                        onLoad(le);
                    }

                    public void loaderLoadException(LoadEvent le) {
                        refresh.setIcon(getImages().getRefresh());
                        isLoading = false;
                        setEnabled(savedEnableState);
                    }
                };
            }
            loader.addLoadListener(loadListener);
        }
    }
}