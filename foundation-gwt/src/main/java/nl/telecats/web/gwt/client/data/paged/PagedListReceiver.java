package nl.telecats.web.gwt.client.data.paged;

import nl.telecats.web.gwt.client.security.SecuredAsyncCallback;
import nl.telecats.web.gwt.client.services.PagedListDataGWTService;

import com.allen_sauer.gwt.log.client.Log;
import com.extjs.gxt.ui.client.widget.Dialog;
import com.extjs.gxt.ui.client.widget.MessageBox;

/**
 * 
 * @author Raymond Domingo
 * @see PagedList
 * 
 */
public class PagedListReceiver {

    /**
     * Recieve data of pagedList instance
     * 
     * @param pagedList
     *            instance received from server
     * @param dataHandler
     *            your datahandler for data in pagedList
     */
    public static void receive(PagedList pagedList, int maxDataSize, PagedListDataHandler dataHandler) {
        dataHandler.handleData(pagedList);

        if (pagedList.getFragmentOffset() + pagedList.getFragmentSize() < pagedList.getTotalSize()) {
            // there is more data to fetch
            // show dialog
            PagedListReceiverMonitor monitor = new PagedListReceiverMonitor();
            monitor.show();
            monitor.center();

            // start fetching
            fetchNextPage(pagedList, maxDataSize, monitor, dataHandler);
        }
    }

    private static void fetchNextPage(final PagedList pl, final int maxDataSize, final PagedListReceiverMonitor pm,
            final PagedListDataHandler dh) {
        // update progress feedback
        double progress = (pl.getFragmentOffset() + pl.getFragmentSize()) / (double) pl.getTotalSize();
        pm.getProgressBar().updateProgress(progress, "");

        // prepare async data fetch
        SecuredAsyncCallback<PagedList> dataSecAsCB = new SecuredAsyncCallback<PagedList>(false, true) {
            @Override
            protected void doOnFailure(Throwable throwable) {
                pm.hide();
                Log.error("Error receiving paged data for:" + pl.getCacheId(), throwable);
            }

            @Override
            protected void doOnSuccess(final PagedList pagedList) {
                dh.handleData(pagedList);
                if (!pm.isCancelled()
                        && pagedList.getFragmentOffset() + pagedList.getFragmentSize() < pagedList.getTotalSize()) {
                    // not cancelled and still date remaining to fetch
                    fetchNextPage(pagedList, maxDataSize, pm, dh);
                } else if (pm.isCancelled()) {
                    // cancelled
                    pm.hide();
                    MessageBox infoBox = MessageBox.info("Systeem", "Uw opdracht is geannuleerd", null);
                    infoBox.getDialog().getButtonById(Dialog.OK).setId("infoDialogOkButton");
                    infoBox.getDialog().getButtonById(Dialog.OK).setItemId("infoDialogOkButton");

                    // InfoDialog.display("Systeem",
                    // "Uw opdracht is geannuleerd");
                    Log.debug("cancelled paged data retreive");
                } else {
                    // no more data to fetch
                    pm.hide();
                }
            }
        };

        // perform async data fetch
        PagedListDataGWTService.Util.getInstance().fetchPagedList(pl.getCacheId(),
                pl.getFragmentOffset() + pl.getFragmentSize(), maxDataSize, dataSecAsCB);
    }
}