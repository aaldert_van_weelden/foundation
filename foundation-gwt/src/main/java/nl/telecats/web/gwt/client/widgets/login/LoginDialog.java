package nl.telecats.web.gwt.client.widgets.login;

import nl.telecats.web.gwt.client.security.SecuredAsyncCallback;

import com.google.gwt.user.client.ui.ClickListener;

/**
 * To support customized login popup panels we hide the specific implementation
 * by using an interface.
 * 
 * @see AbstractLoginDialog
 * 
 * @author Raymond Domingo
 */
public interface LoginDialog {
    public static final String LOGIN_POPUP_PANEL = "LOGIN_POPUP_PANEL";

    /**
     * Show the popup
     */
    public void show();

    /**
     * Reset the data in the popup so it can be re-used
     */
    public void reset();

    /**
     * Hide the popup
     */
    public void hide();

    public void setAuthServiceEndPoint(String authServiceEndPoint);

    /**
     * @deprecated using a clickListener is no longer supported
     * @param clickListener
     */
    public void setClickListener(ClickListener clickListener);

    public void setMessage(String message);

    public void setSecuredAsyncCallback(SecuredAsyncCallback<?> secAsyncCallback);
}
