package nl.telecats.web.gwt.client.layout;

import com.extjs.gxt.ui.client.util.Margins;
import com.extjs.gxt.ui.client.widget.layout.MarginData;

/**
 * Layout data for <code>TcFitLayout</code></br> Class to encapsulate layoutdata and setting the minimum and maximum
 * size of the rendered child</br> Also used for setting a margin around the container</br> Margins will be subtracted
 * from the size limits
 * 
 * @author Aaldert van Weelden
 */
public class TcFitData extends MarginData {

    private SizeLimits sizeLimits = null;
    private boolean isScrollEnabled = false;
    private int offsetLeft = 0;
    private int offsetTop = 0;

    /**
     * Instantiates an empty FitData object
     */
    public TcFitData() {
        super();
    }

    /**
     * Adds margins to the sides of the container accordingly a margins object</br> Example:
     * <code>new TcFitData( new Margins(10,10,20,20))</code> see {@link Margins}
     * 
     * @param margins
     *            The Margin object.
     */
    public TcFitData(Margins margins) {
        super(margins);
    }

    /**
     * Adds margins to the sides of the container accordingly a margins object</br> Example:
     * <code>new TcFitData( new Margins(10,10,20,20))</code> see {@link Margins}
     * 
     * @param margins
     *            The Margin object.
     * @params offsetLeft The left offset in pixels
     * @params offsetTop The top offset in pixels
     */
    public TcFitData(Margins margins, int offsetLeft, int offsetTop) {
        super(margins);
        this.setOffsetLeft(offsetLeft);
        this.setOffsetTop(offsetTop);
    }

    /**
     * Sets the sizing limits of the container</br> Example:
     * <code>new TcFitData(new SizeLimits(100,200,500,1000))</code> see {@link SizeLimits}
     * 
     * @param limits
     *            new SizeLimits(minWidth, maxWidth, minHeight, maxHeight)
     */
    public TcFitData(SizeLimits limits) {
        this.setSizeLimits(limits);
    }

    /**
     * Sets the sizing limits of the container as well as the posion offset</br> Example:
     * <code>new TcFitData(new SizeLimits(100,200,500,1000),100,50)</code> see {@link SizeLimits}
     * 
     * @param limits
     *            new SizeLimits(minWidth, maxWidth, minHeight, maxHeight)
     * @params offsetLeft The left offset in pixels
     * @params offsetTop The top offset in pixels
     */
    public TcFitData(SizeLimits limits, int offsetLeft, int offsetTop) {
        this.setSizeLimits(limits);
        this.setOffsetLeft(offsetLeft);
        this.setOffsetTop(offsetTop);
    }

    /**
     * Sets the sizing limits of the container as well as the margins by using a Margins object</br>Note: Margins will
     * be subtracted from the container size</br> Example:
     * <code>new TcFitData(new SizeLimits(100,200,500,1000),new Margins(10,20,20,10))</code> see {@link SizeLimits} and
     * {@link Margins}
     * 
     * @param limits
     *            new SizeLimits(minWidth, maxWidth, minHeight, maxHeight)
     * @param margin
     *            The margin to set as a Margins object new Margins(top, right, bottom, left);
     */
    public TcFitData(SizeLimits limits, Margins margins) {
        super(margins);
        this.setSizeLimits(limits);
    }

    /**
     * Sets the sizing limits of the container as well as the margins by using a Margins object</br>Note: Margins will
     * be subtracted from the container size</br> Example:
     * <code>new TcFitData(new SizeLimits(100,200,500,1000),new Margins(10,20,20,10))</code> see {@link SizeLimits} and
     * {@link Margins}
     * 
     * @param limits
     *            new SizeLimits(minWidth, maxWidth, minHeight, maxHeight)
     * @param margin
     *            The margin to set as a Margins object new Margins(top, right, bottom, left);
     * @params offsetLeft The left offset in pixels
     * @params offsetTop The top offset in pixels
     */
    public TcFitData(SizeLimits limits, Margins margins, int offsetLeft, int offsetTop) {
        super(margins);
        this.setSizeLimits(limits);
        this.setOffsetLeft(offsetLeft);
        this.setOffsetTop(offsetTop);
    }

    /**
     * Sets the sizing limits of the container</br> Example:
     * <code>new TcFitData(new SizeLimits(100,200,500,1000))</code> see {@link SizeLimits}
     * 
     * @param limits
     *            new SizeLimits(minWidth, maxWidth, minHeight, maxHeight)
     * @param isScrollEnabled
     *            Resizes width of child container if vertical scrollbar is enabled to prevent scrollbar covering right
     *            side of child container if true
     */
    public TcFitData(SizeLimits limits, boolean isScrollEnabled) {
        this.setSizeLimits(limits);
        this.setScrollEnabled(isScrollEnabled);
    }

    /**
     * Sets the sizing limits of the container</br> Example:
     * <code>new TcFitData(new SizeLimits(100,200,500,1000))</code> see {@link SizeLimits}
     * 
     * @param limits
     *            new SizeLimits(minWidth, maxWidth, minHeight, maxHeight)
     * @param isScrollEnabled
     *            Resizes width of child container if vertical scrollbar is enabled to prevent scrollbar covering right
     *            side of child container if true
     * @params offsetLeft The left offset in pixels
     * @params offsetTop The top offset in pixels
     */
    public TcFitData(SizeLimits limits, boolean isScrollEnabled, int offsetLeft, int offsetTop) {
        this.setSizeLimits(limits);
        this.setScrollEnabled(isScrollEnabled);
        this.setOffsetLeft(offsetLeft);
        this.setOffsetTop(offsetTop);
    }

    /**
     * Sets the sizing limits of the container as well as the margins by using a Margins object</br>Note: Margins will
     * be subtracted from the container size</br> Example:
     * <code>new TcFitData(new SizeLimits(100,200,500,1000),new Margins(10,20,20,10))</code> see {@link SizeLimits} and
     * {@link Margins}
     * 
     * @param limits
     *            new SizeLimits(minWidth, maxWidth, minHeight, maxHeight)
     * @param isScrollEnabled
     *            Resizes width of child container if vertical scrollbar is enabled to prevent scrollbar covering right
     *            side of child container if true
     * @param margin
     *            The margin to set as a Margins object new Margins(top, right, bottom, left);
     */
    public TcFitData(SizeLimits limits, boolean isScrollEnabled, Margins margins) {
        super(margins);
        this.setSizeLimits(limits);
        this.setScrollEnabled(isScrollEnabled);
    }

    /**
     * Sets the sizing limits of the container as well as the margins by using a Margins object</br>Note: Margins will
     * be subtracted from the container size</br> Example:
     * <code>new TcFitData(new SizeLimits(100,200,500,1000),new Margins(10,20,20,10))</code> see {@link SizeLimits} and
     * {@link Margins}
     * 
     * @param limits
     *            new SizeLimits(minWidth, maxWidth, minHeight, maxHeight)
     * @param isScrollEnabled
     *            Resizes width of child container if vertical scrollbar is enabled to prevent scrollbar covering right
     *            side of child container if true
     * @param margin
     *            The margin to set as a Margins object new Margins(top, right, bottom, left);
     * @params offsetLeft The left offset in pixels
     * @params offsetTop The top offset in pixels
     */
    public TcFitData(SizeLimits limits, boolean isScrollEnabled, Margins margins, int offsetLeft, int offsetTop) {
        super(margins);
        this.setSizeLimits(limits);
        this.setScrollEnabled(isScrollEnabled);
        this.setOffsetLeft(offsetLeft);
        this.setOffsetTop(offsetTop);
    }

    /**
     * Sets the size limits of the child container
     * 
     * @param sizeLimits
     *            new SizeLimits(minWidth, maxWidth, minHeight, maxHeight)
     */
    public void setSizeLimits(SizeLimits sizeLimits) {
        this.sizeLimits = sizeLimits;
    }

    /**
     * Gets the size limits applied by the child container
     * 
     * @return
     */
    public SizeLimits getSizeLimits() {
        return sizeLimits;
    }

    public void setScrollEnabled(boolean isScrollEnabled) {
        this.isScrollEnabled = isScrollEnabled;
    }

    public boolean isScrollEnabled() {
        return isScrollEnabled;
    }

    public void setOffsetLeft(int offsetLeft) {
        this.offsetLeft = offsetLeft;
    }

    public int getOffsetLeft() {
        return offsetLeft;
    }

    public void setOffsetTop(int offsetTop) {
        this.offsetTop = offsetTop;
    }

    public int getOffsetTop() {
        return offsetTop;
    }

}
