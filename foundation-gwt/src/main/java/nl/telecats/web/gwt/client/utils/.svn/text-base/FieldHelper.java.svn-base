package nl.telecats.web.gwt.client.utils;

import com.extjs.gxt.ui.client.widget.form.Field;

/**
 * Helper class for various {@link Field} related functions.
 * 
 * @author "Eelco Brolman"
 * 
 */
public class FieldHelper {

    /**
     * This function applies the given value to a {@link Field}. Both the value
     * as the originalValue are set, and the validation is cleared from the
     * Field.
     * 
     * @param <D>
     * @param field
     *            The field to set the values on
     * @param value
     *            The value to apply on the field
     */
    public static <D extends Object> void applyValues(Field<D> field, D value) {
        field.setValue(value);
        field.setOriginalValue(value);
        field.clearInvalid();
    }
}
