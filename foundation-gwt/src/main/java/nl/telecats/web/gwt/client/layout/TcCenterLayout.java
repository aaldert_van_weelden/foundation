package nl.telecats.web.gwt.client.layout;

import com.extjs.gxt.ui.client.core.El;
import com.extjs.gxt.ui.client.util.Margins;
import com.extjs.gxt.ui.client.util.Point;
 import com.extjs.gxt.ui.client.widget.ComponentHelper;
import com.extjs.gxt.ui.client.widget.Container;
import com.extjs.gxt.ui.client.widget.layout.LayoutData;

/**
 * This is a base class for layouts that contain a single item that automatically expands to fill the layout's
 * container.
 * 
 * <p />
 * Child Widgets are:
 * <ul>
 * <li><b>Sized</b> : Yes - expands to fill parent container, sizing can be limited by using TcFitData and SizeLimits</li>
 * <li><b>Positioned</b> : Yes, child container will be centered, and margins can be applied to</li>
 * </ul>
 * 
 * <p />
 * In the following code, the child panel will be limited to width between 600 and 1000 and height between 200 and
 * 400.</br> A margin of 30 pixels will be applied to every side of the panel and the container will be centered:
 * 
 * <code><pre>
   TcLayoutContainer container = new TcLayoutContainer();
   container.setLayout(new TcCenterLayout());
  
   ContentPanel panel = new ContentPanel();
   container.add(panel, new TcFitData(new SizeLimits(600, 1000, 200, 400), new Margins(30)));
  
   
 * </pre></code>
 * 
 * @see TcFitLayout
 * @see TcFitData
 * @see SizeLimits
 * @see Margins
 * 
 * @author Aaldert van Weelden
 */
public class TcCenterLayout extends TcFitLayout {

    /**
     * Creates a new fit layout instance.
     */
    public TcCenterLayout() {
        componentStyleName = "x-abs-layout-item";
        targetStyleName = "x-abs-layout-container";
        monitorResize = true;
    }

    @Override
    protected void onLayout(Container<?> container, El target) {
        if (container.getItemCount() == 0) {
            return;
        }
        activeItem = activeItem != null ? activeItem : container.getItem(0);
        super.onLayout(container, target);

        // make sure the container holds only a single widget
        while (container.getItemCount() > 1) {
            container.getItem(1).removeFromParent();

        }

        // getting layout data
        TcFitData layoutData = null;

        LayoutData d = ComponentHelper.getLayoutData(activeItem);
        if (d != null && d instanceof TcFitData) {
            layoutData = (TcFitData) d;
        }

        if (layoutData == null) {
            layoutData = new TcFitData();
        }

        if (activeItem != null) {
            callLayout(activeItem, false);
            // positioning child, make sure parent boundaries are not crossed
            Point p = activeItem.el().getAlignToXY(target.dom, "c-c", null);
            p = activeItem.el().translatePoints(p);
            p.x += layoutData.getOffsetLeft();
            p.y += layoutData.getOffsetTop();
            if (p.x < 0)
                p.x = 0;
            if (p.y < 0)
                p.y = 0;
            setPosition(activeItem, p.x, p.y);
        }
    }
}