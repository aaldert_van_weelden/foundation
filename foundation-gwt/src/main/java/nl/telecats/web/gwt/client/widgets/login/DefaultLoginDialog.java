package nl.telecats.web.gwt.client.widgets.login;

import net.orcades.spring.gwt.security.client.ui.StringUtils;
import nl.telecats.web.gwt.client.TCF;
import nl.telecats.web.gwt.client.security.ReauthenticationException;

import com.extjs.gxt.ui.client.Style.Orientation;
import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.ComponentEvent;
import com.extjs.gxt.ui.client.event.KeyListener;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.Text;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.form.TextField;
import com.extjs.gxt.ui.client.widget.layout.FitData;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.extjs.gxt.ui.client.widget.layout.FormLayout;
import com.extjs.gxt.ui.client.widget.layout.RowData;
import com.extjs.gxt.ui.client.widget.layout.RowLayout;
import com.google.gwt.event.dom.client.KeyCodes;

/**
 * This is an out of the box implementation of the AbstractLoginPopupPanel. This implementation will be used by default.
 * TODO i18n
 * 
 * @see AbstractLoginDialog
 * @author Raymond Domingo
 * 
 */
public class DefaultLoginDialog extends AbstractLoginDialog {

    private static DefaultLoginDialog instance;

    private Text logMessage = new Text();
    private TextField<String> loginTextBox = new TextField<String>();
    private TextField<String> passwordTextBox = new TextField<String>();
    Button loginButton = new Button(TCF.MESSAGES.Login());
    Button cancelButton = new Button(TCF.MESSAGES.Cancel());

    /**
     * Constructor.
     * 
     * @param authServiceEndPoint
     *            authentication end point.
     * @param message
     *            Message to show in header.
     * @param clickListener
     *            to call on successful authentication, may be null.
     */
    public DefaultLoginDialog() {
        super(false, true);
        initGui();
    }

    public void reset() {
        logMessage.setText("");
        loginTextBox.setValue("");
        passwordTextBox.setValue("");
    }

    private void initGui() {
        setResizable(false);
        setDraggable(false);
        setClosable(false);

        FormLayout fl = new FormLayout();
        fl.setDefaultWidth(155);
        LayoutContainer formContainer = new LayoutContainer(fl);
        loginTextBox.setId("loginTextBox");
        loginTextBox.setFieldLabel(TCF.MESSAGES.Username());
        loginTextBox.addKeyListener(new KeyListener() {
            @Override
            public void componentKeyPress(ComponentEvent event) {
                if (event.getKeyCode() == KeyCodes.KEY_ENTER) {
                    doLogin();
                }
            }
        });
        formContainer.add(loginTextBox);

        passwordTextBox.setId("passwordTextBox");
        passwordTextBox.setFieldLabel(TCF.MESSAGES.Password());
        passwordTextBox.setPassword(true);
        passwordTextBox.addKeyListener(new KeyListener() {
            @Override
            public void componentKeyPress(ComponentEvent event) {
                if (event.getKeyCode() == KeyCodes.KEY_ENTER) {
                    doLogin();
                }
            }
        });
        formContainer.add(passwordTextBox);

        LayoutContainer buttonBar = new LayoutContainer();
        buttonBar.setHeight(25);
        buttonBar.setLayout(new RowLayout(Orientation.HORIZONTAL));
        buttonBar.add(new LayoutContainer(), new RowData(1.0, 0));
        formContainer.add(buttonBar, new RowData(1.0, 0.0));
        loginButton.setId("loginButton");
        cancelButton.setId("cancelButton");
        loginButton.addSelectionListener(new SelectionListener<ButtonEvent>() {
            @Override
            public void componentSelected(ButtonEvent ce) {
                doLogin();
            }
        });
        cancelButton.addSelectionListener(new SelectionListener<ButtonEvent>() {
            @Override
            public void componentSelected(ButtonEvent ce) {
                DefaultLoginDialog.this.hide();
                if (getSecuredAsyncCallback() != null) {
                    getSecuredAsyncCallback().onFailure(
                            new ReauthenticationException(TCF.MESSAGES.ReauthenticationCancelled()));
                }
            }
        });
        buttonBar.add(loginButton, new RowData(-1, -1));
        buttonBar.add(cancelButton, new RowData(-1, -1));

        formContainer.add(logMessage);
        setLayout(new FitLayout());
        add(formContainer, new FitData(10));
    }

    private void doLogin() {
        if (StringUtils.isEmptyOrBlank(loginTextBox.getValue())
                || StringUtils.isEmptyOrBlank(passwordTextBox.getValue())) {
            setMessage(TCF.MESSAGES.LoginAndPasswordMustBeProvided());
            return;
        }
        authenticate(loginTextBox.getValue(), passwordTextBox.getValue());
    }

    @Override
    public void show() {
        super.show();
        passwordTextBox.setValue(""); // always clean password field before
        // showing the dialog
        center();
        loginTextBox.focus();
    }

    @Override
    public void setMessage(String message) {

        if ("Bad credentials".equals(message)) {
            message = TCF.MESSAGES.badCredentials();
        } else if ("Auth required".equals(message)) {
            message = TCF.MESSAGES.authRequired();
        }

        super.setMessage(message);
        logMessage.setText(message);
    }

    /**
     * Get an instance of the DefaultLoginDialog
     * 
     * @return
     */
    public static LoginDialog getInstance() {
        if (instance == null) {
            instance = new DefaultLoginDialog();
        }
        return instance;
    }
}
