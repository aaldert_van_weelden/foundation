package nl.telecats.web.gwt.client.utils;

/**
 * This utility contains some needed native javscript functions
 */
public class NativeUtils {
    private static NativeUtils singleton;
    
    private NativeUtils() {        
    }
    
    public static NativeUtils getInstance() {
        if (singleton==null) {
            singleton = new NativeUtils();
        }
        return singleton;
    }
    
    public native void triggerPageRefresh() /*-{
        $wnd.location.reload();
    }-*/;
    
    public native void pageRedirect(String url)
    /*-{
        $wnd.location.replace(url);
    }-*/;

}
