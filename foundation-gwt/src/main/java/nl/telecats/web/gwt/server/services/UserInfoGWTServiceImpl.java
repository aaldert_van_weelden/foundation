package nl.telecats.web.gwt.server.services;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import net.orcades.spring.gwt.security.client.GWTAuthorizationRequiredException;
import net.orcades.spring.gwt.security.client.GWTSecurityException;
import nl.telecats.web.gwt.client.services.UserInfoDTO;
import nl.telecats.web.gwt.client.services.UserInfoGWTService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

/**
 * This service returns the username.
 * 
 * @author raymond
 * 
 */
@Component
@Scope("request")
public class UserInfoGWTServiceImpl extends RemoteServiceServlet implements UserInfoGWTService {

    @Autowired
    ApplicationContext ac;

    private static final long serialVersionUID = 1L;

    public UserInfoDTO showUserInfo() throws GWTAuthorizationRequiredException {
        Principal p = SecurityContextHolder.getContext().getAuthentication();
        return new UserInfoDTO(p != null ? p.getName() : "");
    }

    @Override
    public Boolean isSsoEnabled() throws GWTSecurityException {
        return ac != null && ac.containsBean("jossoAuthenticationManager");
    }

    @Override
    public boolean hasRole(String role) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Collection<GrantedAuthority> authorities = authentication.getAuthorities();
        List<String> result = new ArrayList<String>();
        for (GrantedAuthority ga : authorities) {
            result.add(ga.getAuthority());
        }
        return result.contains(role);
    }
}
