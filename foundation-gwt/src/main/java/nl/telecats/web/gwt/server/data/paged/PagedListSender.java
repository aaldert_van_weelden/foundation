package nl.telecats.web.gwt.server.data.paged;

import java.util.List;

import nl.telecats.web.gwt.client.data.paged.PagedList;

/**
 * Send a list in paged data chunks to the client (client will poll for data chunks).
 * 
 * @author Raymond Domingo
 * 
 * @See PagedList, PagedListReceiver
 */
public class PagedListSender {

    @SuppressWarnings("unchecked")
    public static PagedList send(List list, int maxDataSize) {
        PagedList resultList = null;
        if (list != null) {
            if (list.size() > maxDataSize) {
                // list exceeds max data size, we need to page the data
                String cacheId = PagedListServerCache.getInstance().push(list);
                // pop first chunk
                resultList = (PagedList) PagedListServerCache.getInstance().pop(cacheId, 0, maxDataSize);

            } else {
                // list doesn't exceed the max_size, so there is no need to page
                // the data
                resultList = new PagedList(list);
            }
        }
        return resultList;
    }
}