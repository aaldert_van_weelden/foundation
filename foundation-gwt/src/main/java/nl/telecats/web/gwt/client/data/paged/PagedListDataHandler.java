package nl.telecats.web.gwt.client.data.paged;

/**
 * This handler is used to handle paged data chunks.
 * 
 * @author Raymond Domingo
 * 
 * @see PagedListSender, PagedListReceiver
 */
public abstract class PagedListDataHandler {
    /**
     * 
     * @param pagedList
     *            paged data chunk received from server
     */
    public abstract void handleData(PagedList pagedList);
}