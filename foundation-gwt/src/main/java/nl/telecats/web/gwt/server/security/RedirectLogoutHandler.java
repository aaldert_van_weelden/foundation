package nl.telecats.web.gwt.server.security;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutHandler;

/**
 * You can use this filter as a logout filter.
 * <p/>
 * Where you normally would use something like:<br/>
 * 
 * &lt;bean class=
 * "org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler"
 * /&gt;
 * <p/>
 * You can now use the RedirectLougoutHandler and redirect to you single signon
 * system's logoff page.
 * 
 * @author Raymond Domingo
 * 
 */
public class RedirectLogoutHandler implements LogoutHandler {

    private String url;
    Logger log = LoggerFactory.getLogger(RedirectLogoutHandler.class);

    public RedirectLogoutHandler(String url) {
        this.url = url;
    }

    public void logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
        try {
            if (url != null && url.startsWith("/")) {
                response.sendRedirect(request.getContextPath() + url);
            } else {
                response.sendRedirect(url);
            }
        } catch (IOException e) {
            log.error("Couldn't send redirect", e);
        }
    }

}