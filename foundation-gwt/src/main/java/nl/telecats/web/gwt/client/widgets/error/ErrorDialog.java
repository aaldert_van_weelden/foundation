package nl.telecats.web.gwt.client.widgets.error;

import nl.telecats.web.gwt.client.security.SecuredAsyncCallbackListener;

import com.extjs.gxt.ui.client.Registry;
import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * To support customized error popup panels we hide the specific implementation
 * by using an interface. This implements the
 * {@link SecuredAsyncCallbackListener} interface. Notice that onFailure can be
 * called multiple times, so if you want to show all the errors, you have to
 * gather them in a list yourself
 * 
 * @see AbstractErrorDialog
 * 
 * @author Eelco Brolman
 */
public interface ErrorDialog extends SecuredAsyncCallbackListener {

    public static class Util {
        private static ErrorDialog instance;

        /**
         * @return an errorpanel, either the custom one
         *         (Registry.get(ErrorDialog.ERROR_POPUP_PANEL)) or the default
         *         implementation
         */
        public static ErrorDialog getInstance() {
            if (instance == null)
                instance = instantiateErrorPanel();
            return instance;
        }

        private static ErrorDialog instantiateErrorPanel() {
            ErrorDialog ep = (ErrorDialog) Registry
                    .get(ErrorDialog.ERROR_POPUP_PANEL);
            if (ep != null) {
                return ep;
            } else {
                // the default error panel
                return new DefaultErrorDialog();
            }
        }
    }

    public static final String ERROR_POPUP_PANEL = "ERROR_POPUP_PANEL";

    /**
     * The callback this ErrorDialog will monitor. <b>NOTE: This will only work
     * for SecuredAsyncCallback, param is of type AsyncCallback for your
     * convenience.</b>
     * 
     * @param cb
     *            <b>SecuredAsyncCallback</b> to monitor
     */
    @SuppressWarnings("unchecked")
    public void monitor(final AsyncCallback cb);
}
