package nl.telecats.web.gwt.server.services;

import nl.telecats.web.gwt.client.data.paged.PagedList;
import nl.telecats.web.gwt.client.services.PagedListDataGWTService;
import nl.telecats.web.gwt.server.data.paged.PagedListServerCache;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

/**
 * This service can be used to fetch data chunks of paged lists
 * 
 * @author Raymond Doingo
 * 
 * @see PagedListReceiver, PagedListSender
 */
@Component
@Scope("request")
public class PagedListDataGWTServiceImpl extends RemoteServiceServlet implements PagedListDataGWTService {

    private static final long serialVersionUID = 1L;

    public PagedList fetchPagedList(String cacheId, int nextOffset, int maxDataSize) {
        PagedList pop = PagedListServerCache.getInstance().pop(cacheId, nextOffset, maxDataSize);
        return pop;
    }

}
