package nl.telecats.web.gwt.client;

import com.google.gwt.i18n.client.Messages;

/**
 * Define your localized strings related to the foundation here.
 * <p/>
 * Check also:
 * <ul>
 * <li/>FoundationMessages.properties
 * <li/>FoundationMessages_en.properties
 * </ul>
 * 
 * @author Raymond Domingo
 * 
 * @see TCF
 * 
 */
public interface FoundationMessages extends Messages {
    String Login();

    String Ok();

    String Cancel();

    String Password();

    String LoginAndPasswordMustBeProvided();

    String Username();

    String ErrorOccured();

    String ErrorDetails();

    String StatusCodeException(String statusCode);

    String CantConnectToGwtService();

    String Details();

    String InvocationException();

    String IncompatibleRemoteService();

    String PleaseBePatient();

    String Close();

    String FormDoesNotValidate();

    String FormDoesNotValidateTitle();

    String combobox_nothingselected();

    String combobox_invalidselected();

    String authRequired();

    String badCredentials();

    String ReauthenticationCancelled();

}
