package nl.telecats.web.gwt.client.widgets.form;

import java.util.List;

import nl.telecats.web.gwt.client.TCF;
import nl.telecats.web.gwt.client.model.AbstractWrapper;

import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.SelectionChangedEvent;
import com.extjs.gxt.ui.client.widget.form.ComboBox;
import com.extjs.gxt.ui.client.widget.form.Field;
import com.extjs.gxt.ui.client.widget.form.Validator;

/**
 * This class can be used to perform validation on ComboBoxes, where you want to
 * validate a selection against a set of valid choices. Optionally, the null
 * value (as "real" value, for instance inside a Wrapper (see
 * {@link AbstractWrapper}) can be allowed or disallowed as valid value.<br/>
 * <br/>
 * <b>Implementing</b><br/>
 * Classes extending this class must implement the {@link #getValue(ModelData)}
 * method, to obtain the "real" value of the combobox model, so it can be
 * compared.<br/>
 * <br/>
 * <b>Usage</b><br/>
 * To use an implementation of this class, call
 * {@link ComboBox#setValidator(Validator)} on a newly created object.<br/>
 * <br/>
 * <b>Event Listener</b> A sidenote on why this class implements
 * {@link Listener} for {@link SelectionChangedEvent}:<br/>
 * On the ComboBox implementation you can set ValidateOnBlur to false. This
 * implies that the validate is only called when explicitly calling the
 * validate() function on the Field. However, when the selection of a ComboBox
 * changes, the setValue() function is called, and immediately fires the
 * validate(). So we listen to selection change events, and clear the invalid
 * tag when the validateOnBlur is set to false for this field.
 * 
 * @author "Eelco Brolman"
 * 
 * @param <W>
 */
public abstract class AbstractComboBoxValidator<W extends ModelData> implements
        Validator, Listener<SelectionChangedEvent<W>> {

    protected boolean allowNull;
    protected List<W> validValues;

    protected String nothingSelectedText = TCF.MESSAGES
            .combobox_nothingselected();
    protected String invalidSelectedText = TCF.MESSAGES
            .combobox_invalidselected();
    private Field<W> field;

    /**
     * Set the nothing selected Text (shown if really nothing is selected, or if
     * the selected entry has a null value).
     * 
     * @param nothingSelectedText
     */
    final public void setNothingSelectedText(String nothingSelectedText) {
        this.nothingSelectedText = nothingSelectedText;
    }

    /**
     * Set the invalid Text (shown if the selected entry is not in the valid
     * list (and not null))
     * 
     * @param invalidSelectedText
     */
    final public void setInvalidSelectedText(String invalidSelectedText) {
        this.invalidSelectedText = invalidSelectedText;
    }

    /**
     * Constructor. Does not allow null values to be selected.
     */
    public AbstractComboBoxValidator(Field<W> field) {
        this(field, false);
    }

    /**
     * Constructor
     * 
     * @param allowNull
     *            whether or not to allow null values
     * @param validValues
     *            a List with valid values
     */
    public AbstractComboBoxValidator(Field<W> field, boolean allowNull,
            List<W> validValues) {
        super();
        this.allowNull = allowNull;
        this.validValues = validValues;
        this.field = field;
        field.addListener(Events.SelectionChange, this);

    }

    /**
     * Constructor
     * 
     * @param allowNull
     *            whether or not to allow null values
     */
    public AbstractComboBoxValidator(Field<W> field, boolean allowNull) {
        this(field, allowNull, null);
    }

    /**
     * @param allowNull
     *            whether or not to allow null values
     */
    final public void setAllowNull(boolean allowNull) {
        this.allowNull = allowNull;
    }

    /**
     * @param validValues
     *            a List with valid values
     */
    final public void setValidValues(List<W> validValues) {
        this.validValues = validValues;
    }

    @SuppressWarnings("unchecked")
    public String validate(Field<?> field, String value) {
        // TODO We're now casting field.getValue() implicitly, where a proper
        // check would have been in order.
        W selectedEntry = (W) field.getValue();
        if (selectedEntry == null || getValue(selectedEntry) == null) {
            // nothing selected
            if (allowNull) {
                return null;
            }
            return nothingSelectedText;
        }
        if (validValues == null || validValues.contains(selectedEntry)) {
            return null;
        }
        return invalidSelectedText;

    }

    /**
     * This function must be extended in the SuperClass. Provide the real value
     * for the selectedEntry of type W.
     * 
     * @param selectedEntry
     * @return
     */
    abstract protected Object getValue(W selectedEntry);

    final public void handleEvent(SelectionChangedEvent<W> be) {
        if (be.getType() == Events.SelectionChange) {
            if (!field.getValidateOnBlur()) {
                field.clearInvalid();
            }
        }

    }

}
