package nl.telecats.web.gwt.client.callback;

import java.util.HashMap;
import java.util.Map;

/**
 * This chain helps you to execute a sequence of AsyncCalls, so you don't have to nest each call in its successor.
 * <p/>
 * Note: You can use the chainRegistry to pass data from one call in the chain to the next one. There will be one
 * instance of this registry during the complete execution of this chain.
 * 
 * @author raymond
 *         <p>
 *         <b><u>Example of use</u></b>:
 * 
 *         <pre>
 * /*
 *  * just encapsulate the asynchronous calls into a ChainedAsyncCall subsequentially, sequence of calls is defined in the
 *  * executor, no need to preserve call sequence here besides transparent coding.
 *  &#42;/
 * 
 * ChainedAsyncCall doSomething = new ChainedAsyncCall(true, false) {
 *     public void execute() {
 *         myService.doSomething(parameter, new SecuredAsyncCallback&lt;String&gt;() {
 * 
 *             &#064;Override
 *             protected void doOnFailure(Throwable throwable) {
 *                 Log.debug(&quot;could not do something&quot;);
 *                 continueAsyncChain(false);
 *             }
 * 
 *             &#064;Override
 *             protected void doOnSuccess(Object obj) {
 *                 Log.debug(&quot;did something successfully, storing information in chainregistry&quot;);
 *                 // put information in ChainRegistry
 *                 getChainRegistry().put(&quot;MY_INSTANCE&quot;, obj.getMyInstance());
 *                 continueAsyncChain(true);
 * 
 *             }
 *         });
 *     }
 * };
 * 
 * ChainedAsyncCall doAnotherThing = new ChainedAsyncCall(true, false) {
 *     public void execute() {
 *         // retrieve information from ChainRegistry
 *         final MyInstance myInstance = (MyInstance) getChainRegistry().get(&quot;MY_INSTANCE&quot;);
 *         myService.doAnotherThing(myInstance, new SecuredAsyncCallback&lt;String&gt;() {
 * 
 *             &#064;Override
 *             protected void doOnFailure(Throwable throwable) {
 *                 Log.debug(&quot;could not do another thing&quot;);
 *                 continueAsyncChain(false);
 *             }
 * 
 *             &#064;Override
 *             protected void doOnSuccess(Object obj) {
 *                 Log.debug(&quot;did another thing successfully&quot;);
 *                 continueAsyncChain(true);
 *             }
 *         });
 *     }
 * };
 * 
 * // execute the chain
 * ChainedAsyncCallExecutor.executeChain(doSomething, doAnotherThing);
 * </pre>
 * 
 *         </p>
 */
public class ChainedAsyncCall {
    private ChainedAsyncCall successorAsyncCall;
    private boolean executeOnSuccess = false;
    private boolean executeOnFailure = false;
    private Map<String, Object> chainRegistry = new HashMap<String, Object>();

    /**
     * @param doOnSuccess
     *            , should this call be executed when precessor completes successfull
     * @param doOnFailure
     *            , should this call be executed when precessor completes un-successfull
     */
    public ChainedAsyncCall(boolean doOnSuccess, boolean doOnFailure) {
        this(doOnSuccess, doOnFailure, null);
    }

    /**
     * @param doOnSuccess
     *            , should this call be executed when precessor completes successfull
     * @param doOnFailure
     *            , should this call be executed when precessor completes un-successfull
     * @param successor
     *            , next call to execute
     */
    public ChainedAsyncCall(boolean doOnSuccess, boolean doOnFailure, ChainedAsyncCall successor) {
        executeOnSuccess = doOnSuccess;
        executeOnFailure = doOnFailure;
        successorAsyncCall = successor;
    }

    /**
     * Call this from your async callback to inform the chain of the outcome of your callback.
     * 
     * @param success
     */
    public void continueAsyncChain(boolean success) {
        if (successorAsyncCall != null) {
            if (success && executeOnSuccess) {
                successorAsyncCall.execute();
            } else if (!success && executeOnFailure) {
                successorAsyncCall.execute();
            }
        }
    }

    /**
     * You should override this method in your own implementation.
     * <p/>
     * This is where your logic should be placed.
     */
    public void execute() {
    };

    public Map<String, Object> getChainRegistry() {
        return chainRegistry;
    }

    public void setChainRegistry(Map<String, Object> chainRegistry) {
        this.chainRegistry = chainRegistry;
    }

    public ChainedAsyncCall getSuccessorAsyncCall() {
        return successorAsyncCall;
    }

    public void setSuccessorAsyncCall(ChainedAsyncCall successorAsyncCall) {
        this.successorAsyncCall = successorAsyncCall;
    }
}