package nl.telecats.web.gwt.client.i18n;

import com.extjs.gxt.ui.client.messages.XMessages;

/**
 * @author "Eelco Brolman" Use this class for custom (Dutch only for now)
 *         language for Ext-GWT. Use somewhere in your Application:<br/>
 *         GXT.MESSAGES = (MyMessages) GWT.create(ExtJSMessages.class);
 */
public interface ExtJSMessages extends XMessages {

}
