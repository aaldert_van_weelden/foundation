package nl.telecats.web.gwt.client.security;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import net.orcades.spring.gwt.security.client.GWTAuthentication;
import net.orcades.spring.gwt.security.client.GWTAuthenticationListener;
import net.orcades.spring.gwt.security.client.GWTSecurityModule;

import com.allen_sauer.gwt.log.client.Log;
import com.extjs.gxt.ui.client.widget.Component;

/**
 * Enable / Disable gui components according to user roles.
 * @author raymond
 */
public class ComponentSecurityManager implements GWTAuthenticationListener {

	private static ComponentSecurityManager singleton;
	private ArrayList<Component> securedCompList = new ArrayList<Component>();

	// when comp should be enabled when user is NOT in role (!ROLE) then the inverse will be set
	private Map<Component, Boolean> inversedByComp = new HashMap<Component, Boolean>();
	private Map<Component, String> roleByComp = new HashMap<Component, String>();

	private ComponentSecurityManager() {
	    // singleton pattern
	}

	public static ComponentSecurityManager getInstance() {
		if (singleton == null) {
			singleton = new ComponentSecurityManager();
			GWTSecurityModule.addAuthenticationListener(singleton);
		}
		return singleton;
	}

	/**
	 * @param comp to secure
	 * @param role during authentication the user roles are determined, if user has the 
	 * specified role comp will be enabled, else it will be disabled
	 */
	public void secure(final Component comp, String role) {
		if (Log.isDebugEnabled()) {
			comp.setStyleAttribute("border", "2px dotted red");
		}
		index(comp, role);
		GWTSecurityModule.applyAuthentication(new GWTAuthenticationListener() {
			public void authenticated(GWTAuthentication authentication) {
				authenticateComponent(comp, authentication);
			}
		});

	}

	/**
	 * Inform the ComponentSecurityManager a component shouldn't be secured anymore
	 * @param comp
	 */
	public void unsecure(Component comp) {
		unindex(comp);
	}

	private void index(Component comp, String rawRole) {
		String role = rawRole;

		boolean inverse = false;
		if (role.startsWith("!")) {
			inverse = true;
			role = role.substring(1);
		} else {
			inverse = false;
		}

		securedCompList.add(comp);
		inversedByComp.put(comp, inverse);
		roleByComp.put(comp, role);
	}

	private void unindex(Component comp) {
		securedCompList.remove(comp);
		inversedByComp.remove(comp);
		roleByComp.remove(comp);
	}

	/**
	 * This method will be triggered by the GWTSecurityModule
	 */
	public void authenticated(GWTAuthentication authentication) {
		for (Component comp : securedCompList) {
			authenticateComponent(comp, authentication);
		}
	}

	/**
	 * this method will check if user is in role and enable / disable components
	 * @param comp
	 * @param authentication
	 */
	private void authenticateComponent(Component comp, GWTAuthentication authentication) {
		if (authentication == null) {
			boolean inverse = inversedByComp.get(comp);
			comp.setEnabled(inverse);
		} else {
			boolean inverse = inversedByComp.get(comp);
			String role = roleByComp.get(comp);
			comp.setEnabled(authentication.userInRole(role) ^ inverse);
		}

	}
}
