package nl.telecats.web.gwt.client.widgets.grid;

import java.util.Arrays;
import java.util.List;

import com.extjs.gxt.ui.client.core.El;
import com.extjs.gxt.ui.client.data.BeanModel;
import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.event.GridEvent;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.grid.CheckBoxSelectionModel;
import com.extjs.gxt.ui.client.widget.grid.EditorGrid;
import com.google.gwt.user.client.Event;

/**
 * Use this class instead of {@link CheckBoxSelectionModel} if you want to
 * change the behaviour as follows:<br/>
 * <ul>
 * <li>When clicking on a row entry without any modifier key (Shift, Ctrl of
 * Alt), the row is appended to the selected rows (if not already).
 * <li>When clicking with a modifier key, the underlying mechanism is used
 * </ul>
 * This changes the default behaviour: normally when single clicking a row which
 * is <b>not</b> selected, this becomes the <b>only</b> selected row. This is
 * (especially with an {@link EditorGrid}) not desirable.
 * 
 * @author Eelco Brolman
 * 
 * @param <M>
 *            The modelData to work with
 */
public class CheckBoxSelectionModelSingleClick<M extends ModelData> extends
        CheckBoxSelectionModel<M> {

    private M lastClicked;

    @SuppressWarnings("unchecked")
    @Override
    protected void handleMouseDown(GridEvent e) {
        // cast store to liststore
        store = (ListStore) store;

        // store the last clicked row for context menu's etc.
        El row = e.getTarget(".x-grid3-row", 15);

        if (row != null) {
            lastClicked = ((ListStore<M>) store).getAt(e.getRowIndex());
        }

        // ignore the right click
        if (e.isRightClick()) {
            return;
        }

        // overrule the left click without modifiers
        if (e.getEvent().getButton() == Event.BUTTON_LEFT
                && !isClickModifier(e)) {
            if (row != null) {

                M m = ((ListStore<M>) store).getAt(e.getRowIndex());

                if (isSelected(m)) {
                    // only deselected if the checkbox was clicked
                    if (e.getTarget().getClassName().equals(
                            "x-grid3-row-checker")) {
                        doDeselect(Arrays.asList(m), false);
                    }
                } else {
                    doSelect(Arrays.asList(m), true, false);
                }
            }
        } else {
            super.handleMouseDown(e);
        }
    }

    // This method is no longer needed or used; kept for history.
    // @SuppressWarnings("unchecked")
    // @Override
    /*
     * protected void onContextMenu(GridEvent e) { if (locked) return; if
     * (e.getRowIndex() != -1) { if (isSelected(((ListStore<M>)
     * store).getAt(e.getRowIndex())) && selectionMode != SelectionMode.SINGLE)
     * { return; } doSelect(Arrays.asList(((ListStore<M>) store)
     * .getAt(e.getRowIndex())), true, false); } }
     */

    private boolean isClickModifier(GridEvent<BeanModel> e) {
        return (e.isAltKey() || e.isControlKey() || e.isShiftKey());
    }

    /**
     * Select an item, optionally keeping the orginal selectin (append to
     * selection). See also {@link CheckBoxSelectionModel#select(ModelData)}.
     * 
     * @param item
     *            the item to select
     * @param keepExisting
     *            whether to keep the existing selecting (append) or not
     */
    @SuppressWarnings("unchecked")
    public void select(M item, boolean keepExisting) {
        select(Arrays.asList(item), keepExisting);
    }

    /**
     * Select items, optionally keeping the orginal selectin (append to
     * selection). See also {@link CheckBoxSelectionModel#select(List)}.
     * 
     * @param items
     *            the items to select
     * @param keepExisting
     *            whether to keep the existing selecting (append) or not
     */
    public void select(List<M> items, boolean keepExisting) {
        doSelect(items, true, false);
    }

    /**
     * Returns the ModelData of the last clicked entry (right or left)
     * 
     * @return
     */
    public M getLastClicked() {
        return lastClicked;
    }

}
