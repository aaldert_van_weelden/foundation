package nl.telecats.web.gwt.client.data.paged;

import com.extjs.gxt.ui.client.Style;
import com.extjs.gxt.ui.client.Style.HorizontalAlignment;
import com.extjs.gxt.ui.client.Style.Orientation;
import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.util.Margins;
import com.extjs.gxt.ui.client.widget.Dialog;
import com.extjs.gxt.ui.client.widget.Html;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.ProgressBar;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.button.ButtonBar;
import com.extjs.gxt.ui.client.widget.layout.CenterLayout;
import com.extjs.gxt.ui.client.widget.layout.RowData;
import com.extjs.gxt.ui.client.widget.layout.RowLayout;

public class PagedListReceiverMonitor extends Dialog {
    ProgressBar progressBar = new ProgressBar();
    boolean cancelled = false;
    Button cancelButton = new Button("Annuleren");

    public PagedListReceiverMonitor() {
        setDraggable(true);
        setClosable(false);
        setSize(400, 175);
        setModal(true);
        getButtonBar().removeAll();

        LayoutContainer infoMessagePanel = new LayoutContainer();
        infoMessagePanel.setHeight("50");
        infoMessagePanel.setLayout(new CenterLayout());

        Html htmlText = new Html("<h3>Uw gegevens worden geladen, een ogenblik geduld a.u.b...</h3>");
        infoMessagePanel.add(htmlText);
        add(infoMessagePanel);

        LayoutContainer progressFeedbackPanel = new LayoutContainer();
        progressFeedbackPanel.setHeight("40");
        progressFeedbackPanel.setLayout(new RowLayout(Orientation.VERTICAL));

        progressFeedbackPanel.add(progressBar, new RowData(Style.DEFAULT, Style.DEFAULT, new Margins(10, 10, 10, 10)));
        progressBar.updateProgress(0.0, "");
        add(progressFeedbackPanel);

        ButtonBar buttonBar = new ButtonBar();
        buttonBar.setEnableOverflow(false);
        buttonBar.setAlignment(HorizontalAlignment.CENTER);

        cancelButton.addSelectionListener(new SelectionListener<ButtonEvent>() {
            public void componentSelected(ButtonEvent ce) {
                cancelled = true;
                cancelButton.setEnabled(false);
            }
        });
        cancelButton.setId("cancelButton");
        cancelButton.setItemId("cancelButton");
        buttonBar.add(cancelButton);
        add(buttonBar, new RowData(Style.DEFAULT, Style.DEFAULT, new Margins(20, 5, 5, 5)));
    }

    public ProgressBar getProgressBar() {
        return progressBar;
    }

    public void setProgressBar(ProgressBar progressBar) {
        this.progressBar = progressBar;
    }

    public boolean isCancelled() {
        return cancelled;
    }

    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

}