package nl.telecats.web.gwt.client.security;

public class ReauthenticationException extends Exception {

    private static final long serialVersionUID = 1L;

    public ReauthenticationException() {
        super();
    }

    public ReauthenticationException(String message) {
        super(message);
    }
}