package nl.telecats.web.gwt.client.services;

import nl.telecats.web.gwt.client.data.paged.PagedList;
import nl.telecats.web.gwt.server.services.PagedListDataGWTServiceImpl;

/**
 * @see PagedListDataGWTServiceImpl
 * @author Raymond Domingo
 * 
 */
public interface PagedListDataGWTServiceAsync {

    void fetchPagedList(java.lang.String cacheId, int nextOffset, int maxDataSize,
            com.google.gwt.user.client.rpc.AsyncCallback<PagedList> arg3);
}
