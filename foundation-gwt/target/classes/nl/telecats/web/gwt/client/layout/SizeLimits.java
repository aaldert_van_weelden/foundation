package nl.telecats.web.gwt.client.layout;

/**
 * Wrapper for values used for limiting the container size
 * 
 * @param minWidth
 *            The minimum width of the container
 * @param maxWidht
 *            The maximum width of the container
 * @param minHeight
 *            The minimum height of the container
 * @param maxHeight
 *            The maximum height of the container
 * 
 * @author Aaldert van Weelden
 */
public class SizeLimits {

    private int minWidth = 0;
    private int maxWidht = 0;
    private int minHeight = 0;
    private int maxHeight = 0;

    /**
     * Wrapper constructor for values used for limiting the container size
     * 
     * @param minWidth
     *            The minimum width of the container
     * @param maxWidht
     *            The maximum width of the container
     * @param minHeight
     *            The minimum height of the container
     * @param maxHeight
     *            The maximum height of the container
     */
    public SizeLimits(int minWidth, int maxWidht, int minHeight, int maxHeight) {
        super();
        this.minWidth = minWidth;
        this.maxWidht = maxWidht;
        this.minHeight = minHeight;
        this.maxHeight = maxHeight;
    }

    public int getMinWidth() {
        return minWidth;
    }

    public void setMinWidth(int minWidth) {
        this.minWidth = minWidth;
    }

    public int getMaxWidht() {
        return maxWidht;
    }

    public void setMaxWidht(int maxWidht) {
        this.maxWidht = maxWidht;
    }

    public int getMinHeight() {
        return minHeight;
    }

    public void setMinHeight(int minHeight) {
        this.minHeight = minHeight;
    }

    public int getMaxHeight() {
        return maxHeight;
    }

    public void setMaxHeight(int maxHeight) {
        this.maxHeight = maxHeight;
    }

}
