package nl.telecats.web.gwt.client.callback;

/**
 * A simple callback class. Use this to register your callback with another module.
 * 
 * <pre class="code">
 * SimpleCallBack scb = new SimpleCallBackImpl&lt;String&gt;() {
 *      &#64;Override
 *      public void doCallBack(String result) {
 *          // ... do something
 *      };
 * </pre>
 * 
 * @author eelco
 *
 * @param <T> The type of the return object for the doCallBack function
 */
public abstract class SimpleCallBackImpl<T> implements SimpleCallBack<T> {

    public abstract void doCallBack(T object);
    
}
