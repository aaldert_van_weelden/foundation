package nl.telecats.web.gwt.client.model;

import nl.telecats.web.gwt.client.utils.FieldHelper;

import com.extjs.gxt.ui.client.data.BaseModel;

/**
 * Use this class as a base for your listStore object wrappers. It has a
 * convenient equals compare function, so that code like
 * {@link FieldHelper#applyValues(com.extjs.gxt.ui.client.widget.form.Field, Object)}
 * works with newly create Wrappers (the listStore contains a List of other
 * Wrapper object).
 * 
 * @author "Eelco Brolman"
 * 
 * @param <T>
 */
public abstract class AbstractWrapper<T> extends BaseModel {

    private static final long serialVersionUID = 5469196471320334475L;

    @SuppressWarnings("unchecked")
    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (!other.getClass().equals(getClass())) {
            return false;
        }
        AbstractWrapper<T> otherT = (AbstractWrapper<T>) other;
        if (getObject() == null) {
            // Hmm, true if both null?
            return otherT.getObject() == null;
        }
        return getObject().equals(otherT.getObject());
    }

    public abstract T getObject();
}
