package nl.telecats.web.gwt.client.callback;


/**
 * A simple callBack interface for objects. Use this to create your own callback
 * mechanism, which can call the function doCallBack(T)
 * 
 * @author eelco
 * 
 * @param <T> The type of the return object for the doCallBack function
 */
public abstract interface SimpleCallBack<T> {

    /**
     * Function to be called on callback 
     * @param object
     */
    public void doCallBack(T object);
}
