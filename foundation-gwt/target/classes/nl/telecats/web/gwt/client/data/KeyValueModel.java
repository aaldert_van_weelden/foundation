package nl.telecats.web.gwt.client.data;

import com.extjs.gxt.ui.client.data.BaseModelData;

/**
 * Key value model which can be used in a ListStore. This helps you implementing your ListViews
 * 
 * @author raymond
 *
 * @param <K>
 * @param <V>
 */
public class KeyValueModel <K,V> extends BaseModelData {
    
    private static final long serialVersionUID = 4350729901146407077L;
    
    public static final String KEY = "KEY";
    public static final String VALUE = "VALUE";
    
    public KeyValueModel(K key, V value) {
        setKey(key);
        setValue(value);
    }
    
    public void setKey(K key) {
        set(KEY, key);
    }
    
    @SuppressWarnings("unchecked") // the cast IS needed for GWT
    public K getKey() {
        return (K)get(KEY);
    }
    
    public void setValue(V value) {
        set(VALUE, value);
    }
    
    @SuppressWarnings("unchecked") // the cast IS needed for GWT
    public V getValue() {
        return (V)get(VALUE);
    }
}
