package nl.telecats.web.gwt.client.widgets.error;

import nl.telecats.web.gwt.client.security.SecuredAsyncCallback;

import com.extjs.gxt.ui.client.widget.Dialog;
import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * This class helps you to quickly implement your own customized version of a
 * error popup panel.
 * <p/>
 * When you add it to the registry it will be used automatically.
 * <p/>
 * <b>Example:</b><br/>
 * Registry.register(ErrorDialog.ERROR_POPUP_PANEL, new
 * MyErrorPopupPanel());
 * <p/>
 * 
 * @see DefaultErrorDialog
 * @author Eelco Brolman
 * 
 */
public abstract class AbstractErrorDialog extends Dialog implements ErrorDialog {

    public AbstractErrorDialog(boolean autoHide, boolean modal) {
        super();
        setAutoHide(autoHide);
        setModal(modal);
    }

    @SuppressWarnings("unchecked")
    final public void monitor(final AsyncCallback cb) {
        if (cb instanceof SecuredAsyncCallback) {
            SecuredAsyncCallback scb = (SecuredAsyncCallback) cb;
            scb.registerListener(this);
        }
    }

    final public void doOnSuccess(Object object) {
        // do nothing, this handles errors, not successes...
    }
}
