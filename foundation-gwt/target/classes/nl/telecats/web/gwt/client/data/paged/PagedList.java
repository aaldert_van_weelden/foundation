package nl.telecats.web.gwt.client.data.paged;

import java.io.Serializable;
import java.util.List;

/**
 * This is a data holder for a fragment of data. This might be a subset of the
 * complete set of data, but could also be the complete set of data. <br/>
 * <b>Usage:</b> <br/>
 * In your service impl you shouldn't return your list directly but use the
 * sender to wrap it in a PagedList:
 * 
 * <pre>
 * public PagedList findAllPaged() {
 *     return PagedListSender.send(dummyDataStore, 250);
 * }
 * </pre>
 * 
 * <br/>
 * In your presenter you should use a PagedListReceiver to recieve the paged
 * list:
 * 
 * <pre>
 * &#064;Override
 * protected void doOnSuccess(PagedList pagedList) {
 * 
 *     PagedListReceiver.receive(pagedList, 250, new PagedListDataHandler() {
 *         &#064;Override
 *         public void handleData(PagedList pl) {
 *             view.addPersons(pl.getDataFragment());
 *         }
 *     });
 * }
 * </pre>
 * 
 * @author Raymond
 * 
 * @param <T>
 *            type of this PagedList's content
 * 
 * @see PagedListReceiver, PagedListSender
 */
public class PagedList implements Serializable {
    List dataFragment;
    int fragmentOffset = 0;
    int fragmentSize = 0;
    int totalSize = 0;
    String cacheId;

    /**
     * Empty constructor to support serialization
     */
    public PagedList() {
        super();
    }

    /**
     * Convenience constructor for situations when there is no need to chunk the
     * data. (size <= MAX_SIZE)
     * 
     * @param dataFragment
     */
    public PagedList(List dataFragment) {
        this(dataFragment, 0, dataFragment.size(), null);
    }

    /**
     * Create a pagedList instance
     * 
     * @param dataFragment
     * @param fragmentOffset
     * @param totalSize
     */
    public PagedList(List dataFragment, int fragmentOffset, int totalSize, String cacheId) {
        this.dataFragment = dataFragment;
        this.fragmentOffset = fragmentOffset;
        this.totalSize = totalSize;
        this.fragmentSize = dataFragment.size();
        this.cacheId = cacheId;
    }

    public List getDataFragment() {
        return dataFragment;
    }

    public void setDataFragment(List dataFragment) {
        this.dataFragment = dataFragment;
    }

    public int getFragmentOffset() {
        return fragmentOffset;
    }

    public void setFragmentOffset(int fragmentOffset) {
        this.fragmentOffset = fragmentOffset;
    }

    public int getFragmentSize() {
        return fragmentSize;
    }

    public void setFragmentSize(int fragmentSize) {
        this.fragmentSize = fragmentSize;
    }

    public int getTotalSize() {
        return totalSize;
    }

    public void setTotalSize(int totalSize) {
        this.totalSize = totalSize;
    }

    public String getCacheId() {
        return cacheId;
    }

    public void setCacheId(String cacheId) {
        this.cacheId = cacheId;
    }

}