package nl.telecats.web.gwt.client.widgets.grid;

import com.extjs.gxt.ui.client.data.BeanModel;
import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.GridEvent;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.BoxComponent;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;

/**
 * Render button in table cell.
 * <p/>
 * <b>Example:</b>
 * 
 * <pre>
 * ColumnConfig detailButtonColumn = new ColumnConfig();
 * detailsButtonRenderer = new ButtonCellRenderer&lt;BeanModel&gt;(messages.ShowDetails(), null);
 * detailButtonColumn.setRenderer(detailsButtonRenderer);
 * columnConfigs.add(detailButtonColumn);
 * </pre>
 * 
 * @author raymond
 * 
 * @param <C>
 */
public class ButtonCellRenderer<C extends BeanModel> implements GridCellRenderer<C> {
    boolean init = false;
    private String buttonTitle;
    private String buttonTooltipText;
    private C model;
    private SelectionListener<ButtonEvent> selectionListener;

    public ButtonCellRenderer(String buttonTitle, String buttonTooltipText) {
        this.buttonTitle = buttonTitle;
        this.buttonTooltipText = buttonTooltipText;
    }

    public Object render(final C model, String property, ColumnData config, int rowIndex, int colIndex,
            ListStore<C> store, Grid<C> grid) {
        this.model = model;
        if (!init) {
            init = true;
            grid.addListener(Events.ColumnResize, new Listener<GridEvent<C>>() {

                public void handleEvent(GridEvent<C> be) {
                    for (int i = 0; i < be.getGrid().getStore().getCount(); i++) {
                        if (be.getGrid().getView().getWidget(i, be.getColIndex()) != null
                                && be.getGrid().getView().getWidget(i, be.getColIndex()) instanceof BoxComponent) {
                            ((BoxComponent) be.getGrid().getView().getWidget(i, be.getColIndex())).setWidth(be
                                    .getWidth() - 10);
                        }
                    }
                }
            });
        }
        Button button = new Button();
        button.setWidth(grid.getColumnModel().getColumnWidth(colIndex) - 10);
        if (buttonTitle != null) {
            button.setText(buttonTitle);
        }
        if (buttonTooltipText != null) {
            button.setToolTip(buttonTooltipText);
        }
        button.addSelectionListener(new SelectionListener<ButtonEvent>() {
            @Override
            public void componentSelected(ButtonEvent ce) {
                if (selectionListener != null) {
                    ce.setSource(model.getBean());
                    selectionListener.componentSelected(ce);
                }
            }
        });
        return button;
    }

    /**
     * SelectionListener to inform when button is pressed.
     * <p/>
     * The source of ButtonEvent will contain the bean related to the row of the
     * pressed button.
     * 
     * @param selectionListener
     */
    public void setSelectionListener(SelectionListener<ButtonEvent> selectionListener) {
        this.selectionListener = selectionListener;
    }

    public C getModel() {
        return model;
    }
}