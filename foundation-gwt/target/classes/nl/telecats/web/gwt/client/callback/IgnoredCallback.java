package nl.telecats.web.gwt.client.callback;

import nl.telecats.web.gwt.client.security.SecuredAsyncCallback;

/**
 * In some situations you are not interested in the callback. Just fire and forget. You can use the 'IgnoredCallback' to
 * do this.
 * 
 * @author Raymond
 * 
 */
public class IgnoredCallback<T> extends nl.telecats.web.gwt.client.security.SecuredAsyncCallback<T> {

    public IgnoredCallback() {
        super(false, false);
    }

    @Override
    protected void doOnFailure(Throwable throwable) {
        // Do nothing
    }

    @Override
    protected void doOnSuccess(T object) {
        // Do nothing
    }
}
