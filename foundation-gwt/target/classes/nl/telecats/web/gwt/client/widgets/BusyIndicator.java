package nl.telecats.web.gwt.client.widgets;

import nl.telecats.web.gwt.client.TCF;
import nl.telecats.web.gwt.client.security.SecuredAsyncCallback;
import nl.telecats.web.gwt.client.security.SecuredAsyncCallbackListener;

import com.extjs.gxt.ui.client.widget.Text;
import com.extjs.gxt.ui.client.widget.Window;
import com.extjs.gxt.ui.client.widget.layout.CenterLayout;
import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * This BusyIndicator helps you to show a busy indicator while your rpc-call are performed.
 * TODO implement timeout so BusyIndicator will always be hidden after some period
 * 
 * @author raymond
 */
public class BusyIndicator extends Window implements
        SecuredAsyncCallbackListener {

    private static BusyIndicator instance = null;
    private int unfinishedProcessCount = 0;

    private BusyIndicator() {
        super();
        initGui();
    }
    
    private void initGui() {
        setLayout(new CenterLayout());
        setModal(true);
        add(new Text(TCF.MESSAGES.PleaseBePatient()));
        setHeight(30);
        setWidth(200);
        setClosable(false);
        setDraggable(false);
    }

    public static BusyIndicator getInstance() {
        if (instance == null) {
            instance = new BusyIndicator();
        }
        return instance;
    }

    /**
     * Show a busy indicator until all monitored callbacks are finished (failed or succeeded)<br/>
     * <b>NOTE: This will only work for SecuredAsyncCallback, param is of type AsyncCallback for your convenience.</b>
     * @param cb <b>SecuredAsyncCallback</b> to monitor
     */
    @SuppressWarnings("unchecked")
	public void monitor(final AsyncCallback cb) {
        if (cb instanceof SecuredAsyncCallback) {
            SecuredAsyncCallback scb = (SecuredAsyncCallback)cb;
            unfinishedProcessCount++;
            scb.registerListener(this);
            if (!isVisible()) {
                show();
                center();
            }
        }
    }

    public void doOnFailure(Throwable throwable) {
        asynCallReturned();
    }

    public void doOnSuccess(Object object) {
        asynCallReturned();
    }

    /**
     * Hide the busy indicator when all rpc calls are done
     */
    private void asynCallReturned() {
        unfinishedProcessCount--;
        if (unfinishedProcessCount == 0 ) {
            hide();
        }
    }
}