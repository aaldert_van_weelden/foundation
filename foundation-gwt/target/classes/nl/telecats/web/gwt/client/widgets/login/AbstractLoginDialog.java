package nl.telecats.web.gwt.client.widgets.login;

import net.orcades.spring.gwt.security.client.GWTAuthService;
import net.orcades.spring.gwt.security.client.GWTAuthServiceAsync;
import net.orcades.spring.gwt.security.client.GWTAuthentication;
import net.orcades.spring.gwt.security.client.GWTAuthenticationFailedException;
import net.orcades.spring.gwt.security.client.GWTSecurityModule;
import nl.telecats.web.gwt.client.security.SecuredAsyncCallback;

import com.allen_sauer.gwt.log.client.Log;
import com.extjs.gxt.ui.client.widget.Dialog;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.ClickListener;

/**
 * This class helps you to quickly implement your own customized version of a
 * login popup panel.
 * <p/>
 * When you add it to the registry it will be used automatically.
 * <p/>
 * <b>Example:</b><br/>
 * Registry.register(LoginPopupPanel.LOGIN_POPUP_PANEL, new
 * MyLoginPopupPanel());
 * <p/>
 * <b>Note:</b>Defautl value authServiceEndPoint = /security-auth2.gwt
 * 
 * @see DefaultLoginDialog
 * @author Raymond Domingo
 * 
 */
public abstract class AbstractLoginDialog extends Dialog implements LoginDialog {

    private String authServiceEndPoint = "/security-auth2.gwt"; // default value
    private ClickListener clickListener = null;
    @SuppressWarnings("unused")
    private String message = null;
    private SecuredAsyncCallback<?> secAsyncCallback;

    public AbstractLoginDialog(boolean autoHide, boolean modal) {
        super();
        setAutoHide(autoHide);
        setModal(modal);
        setButtons("");
    }

    /**
     * This will try to authenticate the user. Use a GWTAuthenticationListener
     * to listen for the authentication to fail / succeed
     * <p/>
     * When an securedAsyncCallback is set, onSuccess will call
     * SecuredAsyncCallBack.doOnSuccessfulReauthentication
     * 
     * @see SecuredAsyncCallback#doOnSuccessfulReauthentication(GWTAuthentication)
     * 
     * @param username
     * @param password
     */
    final protected void authenticate(final String username, final String password) {
        GWTAuthServiceAsync authService = GWT.create(GWTAuthService.class);
        ServiceDefTarget serviceDefTarget = (ServiceDefTarget) authService;
        serviceDefTarget.setServiceEntryPoint(GWT.getModuleBaseURL() + authServiceEndPoint);
        authService.autenticate(username, password, new AsyncCallback<GWTAuthentication>() {

            public void onFailure(Throwable caught) {
                if (caught instanceof GWTAuthenticationFailedException) {
                    setMessage(caught.getMessage());
                    Log.info(caught.getMessage());

                } else {
                    setMessage("Error on login");
                    Log.error("Error on login", caught);
                }
            }

            public void onSuccess(GWTAuthentication authentication) {
                Log.debug(authentication.toString());
                GWTSecurityModule.fireAuthenticationPerformed(authentication);
                setMessage("success");
                AbstractLoginDialog.this.hide();
                if (secAsyncCallback != null) {
                    secAsyncCallback.doOnSuccessfulReauthentication(authentication);
                }
                if (clickListener != null) {
                    clickListener.onClick(null);
                }
            }
        });
    }

    public void setAuthServiceEndPoint(final String authServiceEndPoint) {
        this.authServiceEndPoint = authServiceEndPoint;
    }

    public void setClickListener(final ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public void setMessage(final String message) {
        this.message = message;
    }

    public void setSecuredAsyncCallback(SecuredAsyncCallback<?> secAsyncCallback) {
        this.secAsyncCallback = secAsyncCallback;
    }

    public SecuredAsyncCallback<?> getSecuredAsyncCallback() {
        return this.secAsyncCallback;
    }

}
