package nl.telecats.web.gwt.client.widgets.form;

import nl.telecats.web.gwt.client.model.AbstractWrapper;

import com.extjs.gxt.ui.client.widget.form.Field;

public class ComboBoxWrapperValidator<W extends AbstractWrapper<? extends Object>>
        extends AbstractComboBoxValidator<W> {

    public ComboBoxWrapperValidator(Field<W> field) {
        super(field);
    }

    @Override
    protected Object getValue(W selectedEntry) {
        return selectedEntry.getObject();
    }

}
