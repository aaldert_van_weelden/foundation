package nl.telecats.web.gwt.client.services;

import net.orcades.spring.gwt.security.client.GWTAuthorizationRequiredException;
import net.orcades.spring.gwt.security.client.GWTSecurityException;
import nl.telecats.web.gwt.server.services.UserInfoGWTServiceImpl;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * @see UserInfoGWTServiceImpl
 * @author raymond
 * 
 */
@RemoteServiceRelativePath("services/user/userInfoGwtService.gwt")
public interface UserInfoGWTService extends RemoteService {

    public static class Util {

        public static UserInfoGWTServiceAsync getInstance() {
            return GWT.create(UserInfoGWTService.class);
        }
    }

    public UserInfoDTO showUserInfo() throws GWTSecurityException, GWTAuthorizationRequiredException;

    public Boolean isSsoEnabled() throws GWTSecurityException;

    boolean hasRole(String role);
}
