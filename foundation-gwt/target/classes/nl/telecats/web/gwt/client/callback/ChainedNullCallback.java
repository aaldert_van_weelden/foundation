package nl.telecats.web.gwt.client.callback;

/**
 * We can use this for example to chain saving UserActions.
 * 
 * @author Raymond
 */
public class ChainedNullCallback extends nl.telecats.web.gwt.client.security.SecuredAsyncCallback<Void> {

    ChainedAsyncCall chainedAsyncCall;

    public ChainedNullCallback(ChainedAsyncCall chainedAC) {
        this.chainedAsyncCall = chainedAC;
    }

    @Override
    protected void doOnFailure(Throwable throwable) {
        chainedAsyncCall.continueAsyncChain(false);
    }

    @Override
    protected void doOnSuccess(Void object) {
        chainedAsyncCall.continueAsyncChain(false);
    }
}
