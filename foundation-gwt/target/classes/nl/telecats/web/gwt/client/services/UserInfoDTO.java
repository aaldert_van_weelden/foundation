package nl.telecats.web.gwt.client.services;

import java.io.Serializable;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * DTO for login name of user.
 * 
 * @author raymond
 *
 */
public class UserInfoDTO implements Serializable, IsSerializable {

	private static final long serialVersionUID = 1L;
	/**
	 * Login.
	 */
	private String login;

	@Deprecated
	public UserInfoDTO() {
	
	}
	
	public UserInfoDTO(String p_login) {
		this.login = p_login;
	}

	@Override
	public String toString() {
		return login;
	}

}
