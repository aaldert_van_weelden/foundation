package nl.telecats.web.gwt.client.widgets.error;

import net.orcades.spring.gwt.security.client.GWTSecurityException;
import net.orcades.spring.gwt.security.client.GWTSecurityModule;
import nl.telecats.web.gwt.client.TCF;

import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.widget.Dialog;
import com.extjs.gxt.ui.client.widget.HtmlContainer;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.google.gwt.user.client.rpc.IncompatibleRemoteServiceException;
import com.google.gwt.user.client.rpc.InvocationException;
import com.google.gwt.user.client.rpc.StatusCodeException;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ScrollPanel;

/**
 * This is an out of the box implementation of the {@link AbstractErrorDialog}.
 * This implementation will be used by default. Exceptions of the type
 * {@link InvocationException} are handled specially, others as is.
 * 
 * Currently, only the LAST error is shown. If you have multiple callbacks in a
 * row, the last one given an error is shown is this dialog
 * 
 * TODO, show a list of all the errors that happened, not only the last one.
 * 
 * @see AbstractErrorDialog
 * @author Eelco Brolman
 * 
 */
public class DefaultErrorDialog extends AbstractErrorDialog {

    private ErrorDetails detailsDialog = new ErrorDetails();

    private Label logMessage = new Label();

    /**
     * Constructor.
     * 
     */
    public DefaultErrorDialog() {
        super(false, true);
        setButtons("");
        initGui();
    }

    public void reset() {
        logMessage.setText("");
    }

    private void initGui() {
        setHeading(TCF.MESSAGES.ErrorOccured());
        setLayout(new FitLayout());
        addButton(new Button(TCF.MESSAGES.Details(), new SelectionListener<ButtonEvent>() {

            @Override
            public void componentSelected(ButtonEvent ce) {
                detailsDialog.show();
                detailsDialog.center();
            }
        }));
        addButton(new Button(TCF.MESSAGES.Ok(), new SelectionListener<ButtonEvent>() {

            @Override
            public void componentSelected(ButtonEvent ce) {
                hide();
            }
        }));

        logMessage.setHeight("75");
        add(logMessage);
    }

    /**
     * This implementation knows about Exception of type
     * {@link InvocationException}. Other checked exceptions are presented as
     * is.
     * 
     * @see nl.telecats.web.gwt.client.security.SecuredAsyncCallbackListener#doOnFailure(java.lang.Throwable)
     */
    public void doOnFailure(Throwable throwable) {
        String message = "";
        String details = "";

        if (GWTSecurityModule.isSecurityRedirect(throwable)) {
            // we are cool with this, the SecuredCallBack mechanism has caught
            // this, and shown a login dialog
            return;
        }

        // extract messages
        try {
            throw throwable;
        } catch (GWTSecurityException sece) {
            // we are cool with this, the SecuredCallBack mechanism has caught
            // this, and shown a login dialog
            return;
        } catch (StatusCodeException sce) {
            if (sce.getStatusCode() == 0) {
                message = TCF.MESSAGES.CantConnectToGwtService();

            } else {
                message = TCF.MESSAGES.StatusCodeException("" + sce.getStatusCode());
                details = sce.getMessage();
            }
        } catch (IncompatibleRemoteServiceException irse) {
            message = TCF.MESSAGES.IncompatibleRemoteService();
            details = irse.getMessage();
        } catch (InvocationException ie) {
            message = TCF.MESSAGES.InvocationException();
            details = ie.getMessage();
        } catch (Throwable e) {
            message = e.getMessage();
            details = e.getMessage();
        }

        // TODO, idea: maybe a configurable skip unkwown errors, so the dialog
        // is not always shown, and the application can handle db errors and
        // such by itself
        showError(message, details);
    }

    protected void showError(String message, String details) {
        detailsDialog.setDetails(details);
        logMessage.setText(message);
        if (!isVisible()) {
            show();
            center();
        }
    }

    public class ErrorDetails extends Dialog {

        private ScrollPanel panel = new ScrollPanel();
        private HtmlContainer detailText = new HtmlContainer();

        public ErrorDetails() {
            setModal(true);
            initGui();
            setButtons(""); // we add our own close button
        }

        private void initGui() {
            setHeading(TCF.MESSAGES.ErrorDetails());
            setLayout(new FitLayout());
            setWidth(800);
            setHeight(600);
            addButton(new Button(TCF.MESSAGES.Close(), new SelectionListener<ButtonEvent>() {

                @Override
                public void componentSelected(ButtonEvent ce) {
                    hide();
                }
            }));
            // panel.setHeight("400");
            // panel.setWidth("600");
            panel.add(detailText);
            add(panel);
        }

        public void setDetails(String details) {
            if (!details.startsWith("<")) {
                details = "<pre>" + details + "</pre>";
            }
            detailText.setHtml(details);
        }
    }
}
