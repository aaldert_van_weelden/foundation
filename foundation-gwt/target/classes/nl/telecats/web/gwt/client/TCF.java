package nl.telecats.web.gwt.client;

import com.google.gwt.core.client.GWT;

/**
 * Main class for the Telecats GWT Foundation. This helps you to instantiate and
 * use the FoundationMessages
 * 
 * @author "Eelco Brolman"
 * 
 */
public class TCF {

    /**
     * The placeholder for the {@link FoundationMessages}.
     */
    public static FoundationMessages MESSAGES = (FoundationMessages) GWT.create(FoundationMessages.class);

}
